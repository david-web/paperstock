<?php

require_once(dirname(__FILE__)."/../system/config/config.cron.php");
$module = @$_GET["module"];
$rss = new rss($module);
$rss->xml_print(0);
class rss {
	
	public function __construct($module) {
		
		$this->defaultLang = getDefaultLang();
		$this->db = &loadLibClass('db');
		$this->set_rss($module);
		
	}
	
	public function get_data_news($parentId = ''){

	$dbQuery = "SELECT * FROM `ad_content` WHERE lang = '" . $this->defaultLang . "' AND enable = '1' AND `parent_id` = '" . $parentId . "' AND url IS NOT NULL";
	$query = new query($this->db, $dbQuery);

	while($query->getrow()){
		$hostArr = explode('.', $_SERVER['HTTP_HOST']);
		$urlLang = substr($query->field('url'), 0, 2);
		if ($hostArr[count($hostArr)-1] != $this->defaultLang) {
			$hostArr[count($hostArr)-1] = $this->defaultLang;
			$host = implode(".", $hostArr);
		} else { 
			$host = $_SERVER['HTTP_HOST'];
		}
		
		$url = "/" . makeUrlWithLangInTheEnd($query->field('url'));   
		
		$this->get_documents_news($query->field('id'), 'http://' . $host . $url);
		$this->get_data_news($query->field('id'));

	}
}

	public function get_documents_news($id, $link) {
	$result = '';
	$db = &loadLibClass('db');

	$dbQuery = "SELECT * FROM `mod_news` WHERE `content_id` = '" . $id . "' AND `enable` = '1' ORDER BY `created` ASC";
	
	$query = new query($db, $dbQuery);

	while ($query->getrow()) {
	

		$query->row["language"] = $this->defaultLang;
		$query->row["lead_image"] = $query->row["lead_image"] ? $_SERVER['HTTP_HOST'].'/files/news/small/'.$query->row["lead_image"] : "";
		$query->row["pubDate"] = $query->row["updated"] ? strftime ("%a, %d %b %Y %H:%M:%S", $query->row["updated"]) : "";
		 
		$query->row["rating"] = "1";
		$query->row["page_url"] = $query->row["page_url"] ? $link . $query->row["page_url"] . ".html" : "";
		
		$this->result[] = $query->row;
	}
	
}
	
	
	public function set_rss($module) {
	
	$this->rss = &loadLibClass('rss_lib');
	
	$structure = array(
			"title" => "title",
			"description" => "lead",
			"link" => "page_url",
			"language" => "language",
			"image" => "lead_image",
			"pubDate" => "pubDate",
			"rating" => "rating",
		);
	
	$dom = $this->rss->create_rss();	
		
		switch ($module) {
			case 'galleries':
				die('galleries module rss feed gen. here');
				break;
			case 'faq':
				die('faq module rss feed gen. here');
				break;
			default:
				$this->filename = 'news';
				$this->result["title"] = "News feed";
				$this->result["Description"] = "The news feed";
				$this->get_data_news();
				$this->rss->set_rss($this->result, $structure);
				break;
		}
	}
	
	function xml_print($file = 0) {
		$this->rss->dom->formatOutput = TRUE;
		$this->xml = $this->rss->dom->saveXML();
		if ($file) {
			$myFile = dirname(__FILE__)."/../rss/". (@$this->filename ? $this->filename."." : "") . "rss.xml";
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $this->xml);
			fclose($fh);
		} else {
			header('content-type: text/xml');
			echo $this->xml;
		
		}
	}
	
}

?>
