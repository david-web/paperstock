<?php

require_once(dirname(__FILE__)."/../system/config/config.cron.php");
loadFunc("site");

$cfg = &loadLibClass('config');
$cfg->set('defaultLang', getDefaultLang());

function getSitetree($parentId = '', $priority = 1.0){
    $zoneArray = array(
        'lv' => 'lv',
        'en' => 'lv',
    	'ru' => 'lv'
        );
	$result = '';
	$db = &loadLibClass('db');
	if ($parentId != '' && $priority > 0.1)
		$priority -= 0.1;
    
	$dbQuery = "SELECT c.* FROM `ad_content` c  LEFT JOIN ad_languages l ON l.lang=c.lang WHERE c.`parent_id` = '" . $parentId . "' AND c.`country` = '" . getCountry() . "' AND c.`enable` = '1' AND c.`active` = '1' AND c.`sitemap` = '1' AND l.`enable`=1 ORDER BY c.`lang`, c.`sort` ASC";
	$query = new query($db, $dbQuery);
	while($query->getrow()){

		if ($query->field('url')) {
            
            $hostArr = explode('.', $_SERVER['HTTP_HOST']);
            $urlLang = substr($query->field('url'), 0, 2);
            if ($hostArr[count($hostArr)-1] != $zoneArray[$urlLang]) {
                $hostArr[count($hostArr)-1] = $zoneArray[$urlLang];
                $host = implode(".", $hostArr);
            } else { 
                $host = $_SERVER['HTTP_HOST'];
            }
			
            $url = "/" . makeUrlWithLangInTheEnd($query->field('url'));   
            
			$result .= "http://" . $host . $url;
			$result .= '[' . $priority;
			$result .= '[' . $query->field('changefreq') . '|';
			$result .= getDocuments($query->field('id'), 'http://' . $host . $url, $priority, $query->field('changefreq'));
		}
		$result .= getSitetree($query->field('id'), $priority);

	}
	return $result;
}

function getDocuments($id, $link, $priority = 1.0, $changefreq = 'always') {
	$result = '';
	$db = &loadLibClass('db');

	$dbQuery = "SELECT * FROM `mod_news` WHERE `content_id` = '" . $id . "' AND `enable` = '1' ORDER BY `created` DESC";
	$query = new query($db, $dbQuery);
	if ($priority > 0.1)
		$priority -= 0.1;
	
	if ($query->num_rows() > 1) {
		while ($query->getrow()) {
			$result .= $link . $query->field('page_url') . ".html";
			$result .= '[' . $priority;
			$result .= '[' . $changefreq . '|';
		}

	}
	
	return $result;
	
}

$links = (explode('|', getSitetree()));
foreach ($links as $key => $value){
	$links[$key] = explode('[', $value);
}

// Create new dom object
$dom = new DOMDocument("1.0","UTF-8");

// Create urlset
$urlset = create_root($dom, "urlset");
add_attribute($dom, $urlset, "xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

foreach ($links as $key => $page_url) {
    if (!empty($page_url[0])) {
        $url = create_element($dom, $urlset, "url");
        $data = array(
                        "loc" => $page_url[0],
                        "lastmod" => date("Y-m-d"),
                        "changefreq" => $page_url[2],
						"priority" => number_format($page_url[1], 1, '.', '')
                    );
        foreach ($data as $k => $v) {
            create_child_element($dom, $url, $k, trim($v));
        }
    }
}
// Output XML
$strXML = xml_print($dom);


//------------------------------------------------------------------------------


/**
 * Creates XML root element
 *
 * @author                  Dailis TukÄ�ns <dailis@efumo.lv>
 * @version                 1.0
 * 
 * @param object $dom       dom document object
 * @param string $name      root element name
 * @param string $value     root element value (not obligate)
 * 
 * @return object $root     xml root element
 */
function create_root($dom, $name, $value = NULL)
{
    $root = $dom->createElement($name);
    $dom->appendChild($root);

    if ($value != NULL) {
        $root->appendChild($dom->createTextNode($value));
    }

    return $root;
}
	
/**
 * Creates XML element
 *
 * @author                  Dailis TukÄ�ns <dailis@efumo.lv>
 * @version                 1.0
 * 
 * @param object $dom       dom document object
 * @param object $parent    parent element
 * @param string $name      new element name
 * @param string $value     new element value (not obligate)
 * 
 * @return object $element  xml element
 */
function create_element($dom, $parent, $name, $value = NULL)
{
    $element = $dom->createElement($name);
    $parent->appendChild($element);

    if ($value != NULL) {
        $element->appendChild($dom->createTextNode($value));
    }

    return $element;
}

/**
 * Creates XML child element (automaticly adds to parent element)
 *
 * @author                  Dailis TukÄ�ns <dailis@efumo.lv>
 * @version                 1.0
 * 
 * @param object $dom       dom document object
 * @param object $parent    parent element
 * @param string $name      child element name
 * @param string $value     child element value (not obligate)
 * 
 * @return object $parent   xml parent element with child element
 */
function create_child_element($dom, $parent, $name, $value = NULL)
{
    $element = $dom->createElement($name);
    $parent->appendChild($element);

    if ($value != NULL) {
        $element->appendChild($dom->createTextNode($value));
    }

    return $parent;
}

/**
 * Creates XML attribute with value for element (automaticly adds to element)
 *
 * @author                  Dailis TukÄ�ns <dailis@efumo.lv>
 * @version                 1.0
 * 
 * @param object $dom       dom document object
 * @param object $parent    parent element
 * @param string $name      element attribute name
 * @param string $value     element attribute value
 * 
 * @return object $parent   xml parent element with attribute
 */
function add_attribute($dom, $parent, $name, $value = NULL)
{
    $attribute = $dom->createAttribute($name);
    $parent->appendChild($attribute);

    if ($value != NULL) {
        $attribute->appendChild($dom->createTextNode($value));
    }

    return $parent;
}

/**
 * Outputs XML tree
 *
 * @author                  Dailis TukÄ�ns <dailis@efumo.lv>
 * @version                 1.0
 * 
 * @param object $dom       dom document object
 * @param boalen $return    return variant
 * 
 * @return $xml             XML content print to screen
 */
function xml_print($dom) {
    $dom->formatOutput = TRUE;
    $xml = $dom->saveXML();
    $myFile = dirname(__FILE__)."/../sitemaps/sitemap.xml";
    $fh = fopen($myFile, 'w') or die("can't open file");
    fwrite($fh, $xml);
    fclose($fh);
    header('content-type: text/xml');
    echo $xml;
}
?>