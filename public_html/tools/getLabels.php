<?php

require_once(dirname(__FILE__) . "/../system/config/config.cron.php");
require_once(AD_LIB_FOLDER . 'PHPExcel/PHPExcel.php');
require_once(AD_LIB_FOLDER . 'PHPExcel/PHPExcel/Writer/Excel5.php');

$export = new exportLabel();
$export->export();

class exportLabel {
	
	public $fileName = 'Labels';
	public $countries = array();
	
	public function __construct() {
		global $mdb;
		
		$this->db = $mdb;
		
		$this->getCountries();
		
		$this->excel = new PHPExcel();
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Export');
	}
	
	private function getCountries() {
		$dbQuery = "SELECT `title`, `id` FROM `ad_countries`";
		$query = new query($this->db, $dbQuery);
		while ($query->getrow()) {
			$this->countries[$query->field('id')] = $query->field('title');
		}
	}
	
	public function export() {
		
		$dbQuery = "SELECT * FROM `ad_messages` m";
		$query = new query($this->db, $dbQuery);
		
		$N = 1;	
		while ($row = $query->getrow()) {
			
			$data = $this->getLabelData($query->field('id'));
			
			$F = 'A';
			
			$this->excel->getActiveSheet(0)->setCellValue($F . $N, $query->field('id'));
			$this->excel->getActiveSheet(0)->getColumnDimension($F)->setAutoSize(true);
			$F++;
			$this->excel->getActiveSheet(0)->setCellValue($F . $N, $query->field('name'));
			$this->excel->getActiveSheet(0)->getColumnDimension($F)->setAutoSize(true);
			$F++;
			$this->excel->getActiveSheet(0)->setCellValue($F . $N, $query->field('type'));
			$this->excel->getActiveSheet(0)->getColumnDimension($F)->setAutoSize(true);
			$F++;
			
			$FF = $F;
			
			if ($query->field('type') == 'l') {
				
				for ($i = 0; $i < count($data); $i++) {
					
					$FF = $F;
		
					$this->excel->getActiveSheet(0)->setCellValue($FF++ . $N, $data[$i]['lang']);
					$this->excel->getActiveSheet(0)->getColumnDimension($FF)->setAutoSize(true);
					$this->excel->getActiveSheet(0)->setCellValue($FF++ . $N, $data[$i]['value']);
					$this->excel->getActiveSheet(0)->getColumnDimension($FF)->setAutoSize(true);
					$N++;
				}
			
			} elseif ($query->field('type') == 'c') {
				
				for ($i = 0; $i < count($data); $i++) {
					
					$FF = $F;
					
					$this->excel->getActiveSheet(0)->setCellValue($FF++ . $N, $data[$i]['country'] . '(' . @$this->countries[$data[$i]['country']] . ')');
					$this->excel->getActiveSheet(0)->getColumnDimension($FF)->setAutoSize(true);
					$this->excel->getActiveSheet(0)->setCellValue($FF++ . $N, $data[$i]['lang']);
					$this->excel->getActiveSheet(0)->getColumnDimension($FF)->setAutoSize(true);
					$this->excel->getActiveSheet(0)->setCellValue($FF++ . $N, $data[$i]['value']);
					$this->excel->getActiveSheet(0)->getColumnDimension($FF)->setAutoSize(true);
					$N++;
				}
			}
			
			
			$N++;
			
		}
		
		$eWriter = new PHPExcel_Writer_Excel5($this->excel);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $this->fileName . '"');
		header('Cache-Control: max-age=0');
		
		$eWriter->save('php://output');
			
	}
	
	public function getLabelData($id) {
		
		$dbQuery = "SELECT * FROM `ad_messages_info` WHERE `id` = '" . $id . "'";
		$query = new query($this->db, $dbQuery);
		
		return $query->getArray();
	}
	
}