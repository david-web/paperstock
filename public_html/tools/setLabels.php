<?php

ini_set('memory_limit', '128M');
set_time_limit(-1);

require_once(dirname(__FILE__) . "/../system/config/config.cron.php");
require_once(AD_LIB_FOLDER . 'PHPExcel/PHPExcel.php'); 
require_once(AD_LIB_FOLDER . 'PHPExcel/PHPExcel/IOFactory.php');

$import = new importLabel();
$import->import();

class importLabel {
	
	public $fileName = 'message.xls';
	
	public function __construct() {
		global $mdb;
		
		$this->db = $mdb;
		
		$this->excel = PHPExcel_IOFactory::load($this->fileName);
		$this->excel->setActiveSheetIndex(0);

	}
	
	public function import() {
		
		$sheet = $this->excel->getActiveSheet();
		$j = 0;
		foreach ($sheet->getRowIterator() AS $row) {

			if (!is_null($row)) {
				
				$i = 0;
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false);
				foreach($cellIterator AS $cell) {

					if ($i == 0 && $cell->getCalculatedValue() != '') {
						$id = $cell->getCalculatedValue();
					}
					
					if ($i == 1 && $cell->getCalculatedValue() != '') {
						
						$name = $cell->getCalculatedValue();
						echo 'START PROCESSING: ' . $name . '<br />';
					}
					
					if ($i == 2 && $cell->getCalculatedValue() != '') {
						$type = $cell->getCalculatedValue();
					}
					
					if ($i == 3 && $type == 'l' && $cell->getCalculatedValue() != '') {
						$lang = $cell->getCalculatedValue();
					} elseif ($i == 3 && $type == 'c' && $cell->getCalculatedValue() != '') {
						$country = explode("(", $cell->getCalculatedValue());
						$country = $country[0];
					}
					
					if ($i == 4 && $type == 'l') {
						$value = $cell->getCalculatedValue();
						$this->updateMessage($id, '0', $lang, $value, $type);
					} elseif ($i == 4 && $type == 'c' && $cell->getCalculatedValue() != '') {
						$lang = $cell->getCalculatedValue();
					}
					
					if ($i == 5  && $type == 'c') {
						$value = $cell->getCalculatedValue();
						$this->updateMessage($id, $country, $lang, $value, $type);
					}

						
					$i++; 

			
				}
				
				$j++;
	
			}
		}
			
	}
	
	public function updateMessage($id, $country, $lang, $value, $type) {
		

		if ($value && $lang && $id) {
			$dbQuery = "DELETE FROM `ad_messages_info` 
						WHERE 
							(`id` = '" . mysql_real_escape_string($id) . "' 
							AND `country` = '" . mysql_real_escape_string($country) . "' 
							AND `lang` = '" . mysql_real_escape_string($lang) . "') 
						LIMIT 1";
			$query = new query($this->db, $dbQuery);

			$dbQuery = "INSERT INTO `ad_messages_info` 
						(`id`, `country`, `lang`, `value`) 
						VALUES 
						(" . mysql_real_escape_string($id) . ", '" . mysql_real_escape_string($country) . "' , '" . mysql_real_escape_string($lang) . "', '" . mysql_real_escape_string($value) . "')";
			$query = new query($this->db, $dbQuery);
		}
	
		
	}
	
}