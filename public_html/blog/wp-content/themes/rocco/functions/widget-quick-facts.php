<?php
/**
 * Widget Muffin Quick Facts
 *
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

class Mfn_Quick_Facts_Widget extends WP_Widget {

	
	/* ---------------------------------------------------------------------------
	 * Constructor
	 * --------------------------------------------------------------------------- */
	function Mfn_Quick_Facts_Widget() {
		$widget_ops = array( 'classname' => 'widget_mfn_quick_facts', 'description' => __( 'Displays a Quick Facts.', 'mfn-opts' ) );
		$this->WP_Widget( 'widget_mfn_quick_facts', __( 'Muffin Quick Facts', 'mfn-opts' ), $widget_ops );
		$this->alt_option_name = 'widget_mfn_quick_facts';
	}
	
	
	/* ---------------------------------------------------------------------------
	 * Outputs the HTML for this widget.
	 * --------------------------------------------------------------------------- */
	function widget( $args, $instance ) {

		if ( ! isset( $args['widget_id'] ) ) $args['widget_id'] = null;
		extract( $args, EXTR_SKIP );

		echo $before_widget;
		
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base);
		if( $title ) echo $before_title . $title . $after_title;

		$output = '<div class="quick_facts">';
			$output .= '<ul>';
				$output .= do_shortcode( $instance['content'] );
			$output .= '</ul>';
		$output .= '</div>'."\n";
		
		echo $output;

		echo $after_widget;
	}


	/* ---------------------------------------------------------------------------
	 * Deals with the settings when they are saved by the admin.
	 * --------------------------------------------------------------------------- */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title']		= strip_tags( $new_instance['title'] );
		$instance['content']	= strip_tags( $new_instance['content'] );
		
		return $instance;
	}

	
	/* ---------------------------------------------------------------------------
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 * --------------------------------------------------------------------------- */
	function form( $instance ) {
		
		$title		= isset( $instance['title']) ? esc_attr( $instance['title'] ) : '';
		$content	= isset( $instance['content']) ? esc_attr( $instance['content'] ) : '';

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'mfn-opts' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php _e( 'Content:', 'mfn-opts' ); ?></label>
				<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>"><?php echo esc_attr( $content ); ?></textarea>
				<?php _e( 'Please use<br /><strong>[fact title="drank gallons of coffee" icon="icon-coffee" value="469"]</strong><br />shortcodes here.', 'mfn-opts' ); ?>
			</p>
			
		<?php
	}
}
?>