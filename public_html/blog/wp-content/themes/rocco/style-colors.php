<?php
/**
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

header( 'Content-type: text/css;' );
	
$url = dirname( __FILE__ );
$strpos = strpos( $url, 'wp-content' );
$base = substr( $url, 0, $strpos );

require_once( $base .'wp-load.php' );
?>

/********************** Backgrounds **********************/
	
	#Subheader {
		background-color: <?php mfn_opts_show( 'background-subheader', '#2E3B48' ) ?>;
	}

	#Footer .widgets_wrapper {
		background-color: <?php mfn_opts_show( 'background-footer', '#2c3e50' ) ?>;			
	}	
	

/************************ Colors ************************/

/* Content font */
	body, .latest_posts ul li .desc h6 a, blockquote a, blockquote a:hover {
		color: <?php mfn_opts_show( 'color-text', '#34495e' ) ?>;
	}
	
/* Links color */
	a, .pager a.active, .pager a:hover.page {
		color: <?php mfn_opts_show( 'color-a', '#4ecc9f' ) ?>;
	}
	
	a:hover {
		color: <?php mfn_opts_show( 'color-a-hover', '#3aae85' ) ?>;
	}
	
/* Selections */
	*::-moz-selection {
		background-color: <?php mfn_opts_show( 'color-a', '#4ecc9f' ) ?>;
	}
	*::selection {
		background-color: <?php mfn_opts_show( 'color-a', '#4ecc9f' ) ?>;		
	}
	
/* Grey notes */
	.pager a, #Breadcrumbs ul.breadcrumbs li, #Breadcrumbs ul.breadcrumbs li.home a, .post .desc .footer p.tags, .post .desc .meta span.sep, 
	.latest_posts ul li .desc span.comments, blockquote .author .desc a, #Footer .copyrights {
		color: <?php mfn_opts_show( 'color-note', '#b4bbc1' ) ?>;
	}
	
/* Borders */

	.Recent_comments ul li, #Breadcrumbs .one, .post .desc .meta, .post .desc .footer, .ui-tabs, .get_in_touch ul li, .info_box .inside ul li, code, pre, #comments .commentlist > li, #comments .commentlist > li .photo, .post .l,
	#comments .commentlist li .comment-body, .widget_calendar td, .widget_calendar th, .single-portfolio .sp-inside .sp-inside-left,
	.widget_tp_widget_recent_tweets ul li, .quick_facts ul li, .widget_nav_menu li a, .widget_meta li a, .Recent_posts ul li {
		border-color: <?php mfn_opts_show( 'border-borders', '#ebeff2' ) ?>;
	}
	
/* Text hightlight */
	.highlight {
		background: <?php mfn_opts_show( 'background-highlight', '#4ECC9F' ) ?>;
		color: <?php mfn_opts_show( 'color-highlight', '#fff' ) ?>;
	}

/* Buttons */
	a.button, input[type="submit"], input[type="reset"], input[type="button"] {
		background-color: <?php mfn_opts_show( 'background-button', '#60d1c7' ) ?>;
		color: <?php mfn_opts_show( 'color-button', '#fff' ) ?>;
	}
	
	a:hover.button, input[type="submit"]:hover, input[type="reset"]:hover, input[type="button"]:hover {
		background-color: <?php mfn_opts_show( 'background-button-hover', '#3fb4aa' ) ?>;
		color: <?php mfn_opts_show( 'color-button-hover', '#fff' ) ?>;
	}
	
/* Headings font */
	h1, h1 a, h1 a:hover { color: <?php mfn_opts_show( 'color-h1', '#5ad4a0' ) ?>; }
	h2, h2 a, h2 a:hover { color: <?php mfn_opts_show( 'color-h2', '#5ad4a0' ) ?>; }
	h3, h3 a, h3 a:hover { color: <?php mfn_opts_show( 'color-h3', '#5ad4a0' ) ?>; }
	h4, h4 a, h4 a:hover { color: <?php mfn_opts_show( 'color-h4', '#5ad4a0' ) ?>; }
	h5, h5 a, h5 a:hover { color: <?php mfn_opts_show( 'color-h5', '#5ad4a0' ) ?>; }
	h6, h6 a, h6 a:hover { color: <?php mfn_opts_show( 'color-h6', '#5ad4a0' ) ?>; }

/* Social & Search */
	.social li a, #Header #searchform a.icon, #popup_contact > a {
		background: <?php mfn_opts_show( 'background-social', '#dae0e6' ) ?>;
		color: <?php mfn_opts_show( 'color-social', '#fff' ) ?> !important;
	}
	.social li a:hover, #Header #searchform a:hover.icon, #Header #searchform.focus a.icon,
	#popup_contact > a:hover, #popup_contact.focus > a {
		background: <?php mfn_opts_show( 'background-social-hover', '#4ecc9f' ) ?>;
		color: <?php mfn_opts_show( 'color-social-hover', '#fff' ) ?> !important;
	}

/* Menu */
	#Header .menu > li > a {
		color: <?php mfn_opts_show( 'color-menu-a', '#34495e' ) ?>;
	}

	#Header .menu > li.current-menu-item > a,
	#Header .menu > li.current_page_item > a,
	#Header .menu > li.current-menu-ancestor > a,
	#Header .menu > li.current_page_ancestor > a,
	#Header .menu > li > a:hover,
	#Header .menu > li.hover > a {
		color: <?php mfn_opts_show( 'color-menu-a-active', '#5ad4a0' ) ?>;		
	}
	
	#Header .menu > li ul li a {
		color: <?php mfn_opts_show( 'color-submenu-a', '#34495e' ) ?>;
	}
	
	#Header .menu > li.submenu > a:hover,
	#Header .menu > li.submenu.hover > a {
		color: <?php mfn_opts_show( 'color-submenu-a-hover', '#5ad4a0' ) ?>;
	}
	
	#Header .menu > li ul li a:hover, #Header .menu > li ul li.hover > a {
		color: <?php mfn_opts_show( 'color-submenu-a-hover', '#5ad4a0' ) ?>;
	}

	#Header .menu > li ul {
		background: <?php mfn_opts_show( 'background-submenu-2nd', '#FCFCFC' ) ?>;
	}
	
	#Header .menu li ul li.hover .menu-arr-top {
		border-left-color: <?php mfn_opts_show( 'background-submenu-2nd', '#FCFCFC' ) ?>;
	}

	#Header .menu li ul li ul {
		background: <?php mfn_opts_show( 'background-submenu-3rd', '#F5F5F5' ) ?>;
	}

/* Subheader */
	#Subheader h1 { 
		color: <?php mfn_opts_show( 'color-subheader-title', '#fff' ) ?>;
	}


/* Faq & Accordion & Tabs */
	.accordion .question h5,.faq .question h5 {
		color: <?php mfn_opts_show( 'color-accordion-title', '#384E65' ) ?>;
		background: <?php mfn_opts_show( 'background-accordion-title', '#F0F2F2' ) ?>;
		
	}
	.accordion .answer, .faq .answer {
		color: <?php mfn_opts_show( 'color-accordion-content', '#565151' ) ?>;
		background: <?php mfn_opts_show( 'background-accordion-content', '#F9F9F9' ) ?>;
	}
	
	.ui-tabs .ui-tabs-nav li a {
		color: <?php mfn_opts_show( 'color-tabs-title', '#34495e' ) ?>;
	}
	.ui-tabs .ui-tabs-nav {
		background: <?php mfn_opts_show( 'background-tabs-title', '#ebeff2' ) ?>;
	}
	.ui-tabs .ui-tabs-nav li.ui-state-active {
		border-top-color: <?php mfn_opts_show( 'color-tabs-title-active', '#3dc282' ) ?>;
	}
	.ui-tabs .ui-tabs-nav li.ui-state-active a {
		color: <?php mfn_opts_show( 'color-tabs-title-active', '#3dc282' ) ?>;
		background: <?php mfn_opts_show( 'background-tabs-content', '#fff' ) ?>;
	}
	.ui-tabs .ui-tabs-panel {
		color: <?php mfn_opts_show( 'color-tabs-content', '#717E8C' ) ?>;
		background: <?php mfn_opts_show( 'background-tabs-content', '#fff' ) ?>;
	}

/* Portfolio */
	.Projects_header .categories ul li.current-cat a {
		background: <?php mfn_opts_show( 'background-portfolio-tab-active', '#34D3A6' ) ?>;
	}
	
/* Call to action */
	.call_to_action .inner-padding {
		background-color: <?php hex2rgba( mfn_opts_get( 'background-call-to-action', '#07bb86' ), 0.85, true ) ?>;
	}
	.call_to_action h4, .call_to_action p, .call_to_action a {
		color: <?php mfn_opts_show( 'color-call-to-action', '#fff' ) ?>;
	} 
	
/* Pricing box */
	.pricing-box-featured {
		background: <?php mfn_opts_show( 'background-pricing-featured', '#F8FFFD' ) ?>;
		border-color: <?php mfn_opts_show( 'border-pricing-featured', '#E5F9F3' ) ?>;		
	}
	
/* Image overlays */
	.wp-caption .photo .photo_wrapper a .mask, .gallery .gallery-item .gallery-icon .mask,
	.da-thumbs li a div, .recent_works ul li a .mask { 
		background: <?php hex2rgba( mfn_opts_get( 'background-image-overlay', '#07bb86' ), 0.85, true ) ?>;
	}
	
/* Widgets */

/* widget_mfn_menu, widget_categories */
	.widget-area .widget_mfn_menu, .widget-area .widget_categories {
		background: <?php mfn_opts_show( 'background-widget-menu', '#5ad4a0' ) ?>;
	}
	.widget-area .widget_mfn_menu h3, .widget-area .widget_categories h3,
	.widget-area .widget_mfn_menu li a, .widget-area .widget_categories li a {
		color: <?php mfn_opts_show( 'color-widget-menu-link', '#fff' ) ?>;
	}
	.widget-area .widget_mfn_menu li a i.icon-angle-right, .widget-area .widget_mfn_menu li.current_page_item a, 
	.widget-area .widget_mfn_menu li a:hover, .widget-area .widget_categories li a:hover, .widget-area .widget_categories li.current-cat a {
		color: <?php mfn_opts_show( 'color-widget-menu-link-active', '#34495e' ) ?>;
	}
			
/* Widgets wrapper */

	.widgets_wrapper, .widgets_wrapper .widget ul.menu li a, .widgets_wrapper .widget_categories li a, .widgets_wrapper .widget_meta li a {
		color: <?php mfn_opts_show( 'color-footer', '#fff' ) ?>;
	}
	.widgets_wrapper a, .widgets_wrapper .quick_facts ul li span, .widgets_wrapper .fun_facts .num {
		color: <?php mfn_opts_show( 'color-footer-link', '#4ecc9f' ) ?>;
	}
	.widgets_wrapper a:hover, .widgets_wrapper .widget ul.menu li a:hover, .widgets_wrapper .widget_categories li a:hover, 
	.widgets_wrapper .widget_meta li a:hover, .widgets_wrapper .widget ul.menu li.current_page_item a {
		color: <?php mfn_opts_show( 'color-footer-link-hover', '#3aae85' ) ?>;
	}	
	.widgets_wrapper h1, .widgets_wrapper h1 a, .widgets_wrapper h1 a:hover,
	.widgets_wrapper h2, .widgets_wrapper h2 a, .widgets_wrapper h2 a:hover,
	.widgets_wrapper h3, .widgets_wrapper h3 a, .widgets_wrapper h3 a:hover,
	.widgets_wrapper h4, .widgets_wrapper h4 a, .widgets_wrapper h4 a:hover,
	.widgets_wrapper h5, .widgets_wrapper h5 a, .widgets_wrapper h5 a:hover,
	.widgets_wrapper h6, .widgets_wrapper h6 a, .widgets_wrapper h6 a:hover,
	.widgets_wrapper .widget_calendar caption {
		color: <?php mfn_opts_show( 'color-footer-heading', '#fff' ) ?>;
	}	
	.widgets_wrapper aside > h4 {
		color: <?php mfn_opts_show( 'color-footer-widget-title', '#4ecc9f' ) ?>;
	}
	
	/* Grey notes */
	.widgets_wrapper .Recent_comments ul li span.date, .widgets_wrapper .Recent_posts ul li .desc p, .Recent_comments ul li p.author {
		color: <?php mfn_opts_show( 'color-footer-note', '#8fa5b8' ) ?>;
	}
