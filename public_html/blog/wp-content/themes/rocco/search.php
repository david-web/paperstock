<?php
/**
 * The search template file.
 *
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

get_header();
?>

<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group" style="width:100% !important;">
		
			<div class="section">
				<div class="section_wrapper clearfix">
				
					<div class="column one">		
						<?php
							while ( have_posts() )
							{
								the_post();
								?>
								<div class="post clearfix no-photo">
									<div class="desc">
										<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
										<?php echo mfn_excerpt( get_the_ID(), 100, '<p><b><strong>' ); ?>
									</div>
								</div>
								<?php
							}
							
							// pagination
							if(function_exists( 'mfn_pagination' )):
								mfn_pagination();
							else:
								?>
								<div class="nav-next"><?php next_posts_link(__('&larr; Older Entries', 'rocco')) ?></div>
								<div class="nav-previous"><?php previous_posts_link(__('Newer Entries &rarr;', 'rocco')) ?></div>
								<?php
							endif;
						?>
					</div>
					
				</div>
			</div>
			
		</div>

	</div>
</div>

<?php get_footer(); ?>