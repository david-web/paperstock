<?php
/**
 * The Template for displaying all single testimonial.
 *
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

get_header();
?>

<!-- #Content -->
<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">
			<?php 
				while ( have_posts() ){
					the_post();
					
					echo '<div class="section">';
						echo '<div class="section_wrapper">';
							echo '<div class="column one">';
								
								echo '<blockquote>';
								
									echo '<p>'. get_the_content() .'</p>';
									echo '<span class="arrow"></span>';
									
									echo '<div class="author">';
										echo '<div class="photo">';
											if( has_post_thumbnail() ){
												echo get_the_post_thumbnail( null, 'testimonials', array('class'=>'scale-with-grid' ) );
											} else {
												echo '<img class="scale-with-grid" src="'. THEME_URI .'/images/testimonials-placeholder.png" alt="'. get_post_meta(get_the_ID(), 'mfn-post-author', true) .'" />';
											}
										echo '</div>';
										echo '<div class="desc">';
											echo '<h6>'. get_post_meta( get_the_ID(), 'mfn-post-author', true ) .'</h6>';
											if( $link = get_post_meta(get_the_ID(), 'mfn-post-link', true) ) echo '<a target="_blank" href="'. $link .'">';
												echo '<span>'. get_post_meta(get_the_ID(), 'mfn-post-company', true) .'</span>';
											if( $link ) echo '</a>';
										echo '</div>';
									echo '</div>';
									
								echo '</blockquote>'."\n";	
								
							echo '</div>';
						echo '</div>';
					echo '</div>';
				}
			?>
		</div>
		
		<!-- .four-columns - sidebar -->
		<?php get_sidebar(); ?>
			
	</div>
</div>

<?php get_footer(); ?>