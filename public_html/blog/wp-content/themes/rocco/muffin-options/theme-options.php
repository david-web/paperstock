<?php
/**
 * Theme Options - fields and args
 *
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

require_once( dirname( __FILE__ ) . '/fonts.php' );
require_once( dirname( __FILE__ ) . '/options.php' );


/**
 * Options Page fields and args
 */
function mfn_opts_setup(){
	
	// Navigation elements
	$menu = array(	
	
		// General --------------------------------------------
		'general' => array(
			'title' => __('Getting started', 'mfn-opts'),
			'sections' => array( 'general', 'sidebars', 'blog', 'portfolio', 'slider'),
		),
		
		// Layout --------------------------------------------
		'elements' => array(
			'title' => __('Layout', 'mfn-opts'),
			'sections' => array( 'layout-general', 'layout-header', 'social', 'custom-css' ),
		),
		
		// Colors --------------------------------------------
		'colors' => array(
			'title' => __('Colors', 'mfn-opts'),
			'sections' => array( 'colors-general', 'menu', 'colors-header', 'content', 'colors-footer', 'headings', 'colors-accordion', 'colors-shortcodes'),
		),
		
		// Fonts --------------------------------------------
		'font' => array(
			'title' => __('Fonts', 'mfn-opts'),
			'sections' => array( 'font-family', 'font-size' ),
		),
		
		// Translate --------------------------------------------
		'translate' => array(
			'title' => __('Translate', 'mfn-opts'),
			'sections' => array( 'translate-general', 'translate-blog', 'translate-404' ),
		),
		
	);

	$sections = array();

	// General ----------------------------------------------------------------------------------------
	
	// General -------------------------------------------
	$sections['general'] = array(
		'title' => __('General', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
				
			array(
				'id' => 'responsive',
				'type' => 'switch',
				'title' => __('Responsive', 'mfn-opts'), 
				'desc' => __('<b>Notice:</b> Responsive menu is working only with WordPress custom menu, please add one in Appearance > Menus and select it for Theme Locations section. <a href="http://en.support.wordpress.com/menus/" target="_blank">http://en.support.wordpress.com/menus/</a>', 'mfn-opts'), 
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'mfn-seo',
				'type' => 'switch',
				'title' => __('Use built-in SEO fields', 'mfn-opts'), 
				'desc' => __('Turn it off if you want to use external SEO plugin.', 'mfn-opts'), 
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'meta-description',
				'type' => 'text',
				'title' => __('Meta Description', 'mfn-opts'),
				'desc' => __('These setting may be overridden for single posts & pages.', 'mfn-opts'),
				'std' => get_bloginfo( 'description' ),
			),
			
			array(
				'id' => 'meta-keywords',
				'type' => 'text',
				'title' => __('Meta Keywords', 'mfn-opts'),
				'desc' => __('These setting may be overridden for single posts & pages.', 'mfn-opts'),
			),
			
			array(
				'id' => 'google-analytics',
				'type' => 'textarea',
				'title' => __('Google Analytics', 'mfn-opts'), 
				'sub_desc' => __('Paste your Google Analytics code here.', 'mfn-opts'),
			),
			
		),
	);
	
	// Sidebars --------------------------------------------
	$sections['sidebars'] = array(
		'title' => __('Sidebars', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
	
			array(
				'id' => 'sidebar-layout',
				'type' => 'radio_img',
				'title' => __('Default Layout', 'mfn-opts'), 
				'sub_desc' => __('Default post or page sidebar', 'mfn-opts'),
				'options' => array(
					'no-sidebar' => array('title' => 'Full width without sidebar', 'img' => MFN_OPTIONS_URI.'img/1col.png'),
					'left-sidebar' => array('title' => 'Left Sidebar', 'img' => MFN_OPTIONS_URI.'img/2cl.png'),
					'right-sidebar' => array('title' => 'Right Sidebar', 'img' => MFN_OPTIONS_URI.'img/2cr.png')
				),
				'std' => 'no-sidebar'																		
			),
	
			array(
				'id' => 'sidebars',
				'type' => 'multi_text',
				'title' => __('Sidebars', 'mfn-opts'),
				'sub_desc' => __('Manage custom sidebars', 'mfn-opts'),
				'desc' => __('Sidebars can be used on pages, blog and portfolio', 'mfn-opts')
			),
				
		),
	);
	
	// Blog --------------------------------------------
	$sections['blog'] = array(
		'title' => __('Blog', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
	
			array(
				'id' => 'blog-posts',
				'type' => 'text',
				'title' => __('Posts per page', 'mfn-opts'),
				'sub_desc' => __('Number of posts per page.', 'mfn-opts'),
				'class' => 'small-text',
				'std' => '4',
			),
				
			array(
				'id' => 'blog-layout',
				'type' => 'radio_img',
				'title' => __('Layout', 'mfn-opts'),
				'sub_desc' => __('Layout for Blog Page', 'mfn-opts'),
				'options' => array(
					'one'			=> array('title' => 'List', 'img' => MFN_OPTIONS_URI.'img/list.png'),
					'one-second'	=> array('title' => 'Grid Two columns', 'img' => MFN_OPTIONS_URI.'img/one-second.png'),
					'one-third'		=> array('title' => 'Grid Three columns', 'img' => MFN_OPTIONS_URI.'img/one-third.png'),
					'one-fourth'	=> array('title' => 'Grid Four columns', 'img' => MFN_OPTIONS_URI.'img/one-fourth.png'),
				),
				'std' => 'list'
			),
			
			array(
				'id' => 'blog-categories',
				'type' => 'switch',
				'title' => __('Show Categories', 'mfn-opts'), 
				'sub_desc' => __('Show categories on posts list and single post.', 'mfn-opts'), 
				'desc' => __('These setting may be overridden for single posts.', 'mfn-opts'), 
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'blog-comments',
				'type' => 'switch',
				'title' => __('Show Comments', 'mfn-opts'), 
				'sub_desc' => __('Show comments number on posts list and single post.', 'mfn-opts'),
				'desc' => __('These setting may be overridden for single posts.', 'mfn-opts'), 
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'blog-time',
				'type' => 'switch',
				'title' => __('Show Date', 'mfn-opts'), 
				'sub_desc' => __('Show date on posts list and single post.', 'mfn-opts'), 
				'desc' => __('These setting may be overridden for single posts.', 'mfn-opts'), 
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'blog-tags',
				'type' => 'switch',
				'title' => __('Show Tags', 'mfn-opts'), 
				'sub_desc' => __('Show tags list on posts list and single post.', 'mfn-opts'),
				'desc' => __('These setting may be overridden for single posts.', 'mfn-opts'),  
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'blog-readmore',
				'type' => 'text',
				'title' => __('Read more', 'mfn-opts'),
				'sub_desc' => __('Read more button text.', 'mfn-opts'),
				'desc' => __('Leave blank if you don`t want the buttons on blog page.', 'mfn-opts'),
				'std' => 'Read more',
			),
			
			array(
				'id' => 'pagination-show-all',
				'type' => 'switch',
				'title' => __('All pages in pagination', 'mfn-opts'),
				'desc' => __('Show all of the pages instead of a short list of the pages near the current page.', 'mfn-opts'),  
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
				
		),
	);
	
	// Portfolio --------------------------------------------
	$sections['portfolio'] = array(
		'title' => __('Portfolio', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
	
			array(
				'id' => 'portfolio-posts',
				'type' => 'text',
				'title' => __('Posts per page', 'mfn-opts'),
				'sub_desc' => __('Number of portfolio posts per page.', 'mfn-opts'),
				'class' => 'small-text',
				'std' => '8',
			),
			
			array(
				'id' => 'portfolio-layout',
				'type' => 'radio_img',
				'title' => __('Layout', 'mfn-opts'), 
				'sub_desc' => __('Layout for portfolio items list.', 'mfn-opts'),
				'options' => array(
					'one-second' => array('title' => 'Two columns', 'img' => MFN_OPTIONS_URI.'img/one-second.png'),
					'one-third' => array('title' => 'Three columns', 'img' => MFN_OPTIONS_URI.'img/one-third.png'),
					'one-fourth' => array('title' => 'Four columns', 'img' => MFN_OPTIONS_URI.'img/one-fourth.png'),
				),
				'std' => 'one-fourth'																		
			),
			
			array(
				'id' => 'portfolio-page',
				'type' => 'pages_select',
				'title' => __('Portfolio Page', 'mfn-opts'), 
				'sub_desc' => __('Assign page for portfolio.', 'mfn-opts'),
				'args' => array()
			),
			
			array(
				'id' => 'portfolio-slug',
				'type' => 'text',
				'title' => __('Single item slug', 'mfn-opts'),
				'sub_desc' => __('Link to single item.', 'mfn-opts'),
				'desc' => __('<b>Important:</b> Do not use characters not allowed in links. <br /><br />Must be different from the Portfolio site title chosen above, eg. "portfolio-item". After change please go to "Settings > Permalinks" and click "Save changes" button.', 'mfn-opts'),
				'class' => 'small-text',
				'std' => 'portfolio-item',
			),
			
			array(
				'id' => 'portfolio-orderby',
				'type' => 'select',
				'title' => __('Order by', 'mfn-opts'), 
				'sub_desc' => __('Portfolio items order by column.', 'mfn-opts'),
				'options' => array('date'=>'Date', 'menu_order' => 'Menu order', 'title'=>'Title'),
				'std' => 'menu_order'
			),
			
			array(
				'id' => 'portfolio-order',
				'type' => 'select',
				'title' => __('Order', 'mfn-opts'), 
				'sub_desc' => __('Portfolio items order.', 'mfn-opts'),
				'options' => array('ASC' => 'Ascending', 'DESC' => 'Descending'),
				'std' => 'ASC'
			),
			
			array(
				'id' => 'portfolio-isotope',
				'type' => 'switch',
				'title' => __('jQuery filtering', 'mfn-opts'),
				'desc' => __('When this option is enabled, portfolio looks great with all projects on single site, so please set "Posts per page" option to bigger number', 'mfn-opts'),  
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
				
		),
	);
	
	// Slider --------------------------------------------
	$sections['slider'] = array(
		'title' => __('Sliders', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
				
			array(
				'id' => 'slider-portfolio-auto',
				'type' => 'text',
				'title' => __('Portfolio - Timeout', 'mfn-opts'),
				'sub_desc' => __('Milliseconds between slide transitions.', 'mfn-opts'),
				'desc' => __('<strong>0 to disable auto</strong> advance.', 'mfn-opts'),
				'class' => 'small-text',
				'std' => '0',
			),
			
			array(
				'id' => 'slider-portfolio-visible',
				'type' => 'text',
				'title' => __('Portfolio - Visible Items', 'mfn-opts'),
				'sub_desc' => __('Number of visible items.', 'mfn-opts'),
				'desc' => __('Recommended number: 3-6', 'mfn-opts'),
				'class' => 'small-text',
				'std' => '4',
			),
				
			array(
				'id' => 'slider-clients-auto',
				'type' => 'text',
				'title' => __('Clients - Timeout', 'mfn-opts'),
				'sub_desc' => __('Milliseconds between slide transitions.', 'mfn-opts'),
				'desc' => __('<strong>0 to disable auto</strong> advance.', 'mfn-opts'),
				'class' => 'small-text',
				'std' => '0',
			),
			
			array(
				'id' => 'slider-clients-visible',
				'type' => 'text',
				'title' => __('Clients - Visible Items', 'mfn-opts'),
				'sub_desc' => __('Number of visible items.', 'mfn-opts'),
				'desc' => __('Recommended number: 3-6', 'mfn-opts'),
				'class' => 'small-text',
				'std' => '6',
			),
								
		),
	);
	
	// Layout ----------------------------------------------------------------------------------------
	
	// General --------------------------------------------
	$sections['layout-general'] = array(
		'title' => __('General', 'mfn-opts'),
		'fields' => array(
				
			array(
				'id' => 'favicon-img',
				'type' => 'upload',
				'title' => __('Custom Favicon', 'mfn-opts'),
				'sub_desc' => __('Site favicon', 'mfn-opts'),
				'desc' => __('Please use ICO format only.', 'mfn-opts')
			),
		
			array(
				'id' => 'layout',
				'type' => 'radio_img',
				'title' => __('Layout', 'mfn-opts'),
				'sub_desc' => __('Layout type', 'mfn-opts'),
				'options' => array(
					'boxed' => array('title' => 'Boxed', 'img' => MFN_OPTIONS_URI.'img/boxed.png'),
					'full-width' => array('title' => 'Full width', 'img' => MFN_OPTIONS_URI.'img/1col.png'),
				),
				'std' => 'full-width'
			),
				
			array(
				'id' => 'img-page-bg',
				'type' => 'upload',
				'title' => __('Background Image', 'mfn-opts'),
				'desc' => __('This option can be used <strong>only</strong> with Layout: Boxed.', 'mfn-opts'),	
			),
					
			array(
				'id' => 'position-page-bg',
				'type' => 'select',
				'title' => __('Background Image position', 'mfn-opts'),
				'desc' => __('This option can be used only with your custom image selected above.', 'mfn-opts'),
				'options' => array(
					'no-repeat;center top;;' => 'Center Top No-Repeat',
					'repeat;center top;;' => 'Center Top Repeat',
					'no-repeat;center;;' => 'Center No-Repeat',
					'repeat;center;;' => 'Center Repeat',
					'no-repeat;left top;;' => 'Left Top No-Repeat',
					'repeat;left top;;' => 'Left Top Repeat',
					'no-repeat;center top;fixed;' => 'Center No-Repeat Fixed',
					'no-repeat;center;fixed;cover' => 'Center No-Repeat Fixed Cover',
				)
			),
			
			array(
				'id' => 'img-subheader-bg',
				'type' => 'upload',
				'title' => __('Subheader Image', 'mfn-opts'),
			),
			
			array(
				'id' => 'clients-show',
				'type' => 'switch',
				'title' => __('Show Clients Slider', 'mfn-opts'),
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '0'
			),
			
			array(
				'id' => 'popup-contact-form',
				'type' => 'text',
				'title' => __('Popup Contact Form Shortcode', 'mfn-opts'),
				'desc' => __('eg. [contact-form-7 id="000" title="Popup Contact Form"]', 'mfn-opts'),
			),

		),
	);
	
	// Header --------------------------------------------
	$sections['layout-header'] = array(
		'title' => __('Header', 'mfn-opts'),
		'fields' => array(
				
			array(
				'id' => 'logo-img',
				'type' => 'upload',
				'title' => __('Custom Logo', 'mfn-opts'),
				'sub_desc' => __('Custom logo image', 'mfn-opts'),
			),
	
			array(
				'id' => 'retina-logo-img',
				'type' => 'upload',
				'title' => __('Retina Logo', 'mfn-opts'),
				'sub_desc' => __('2x larger logo image', 'mfn-opts'),
				'desc' => __('Retina Logo should be 2x larger than Custom Logo (field is optional).', 'mfn-opts'),
			),

			array(
				'id' => 'retina-logo-width',
				'type' => 'text',
				'title' => __('Custom Logo Width', 'mfn-opts'),
				'sub_desc' => __('for Retina Logo', 'mfn-opts'),
				'desc' => __('px. Please type width of Custom Logo image (<strong>not</strong> Retina Logo).', 'mfn-opts'),
				'class' => 'small-text',
			),

			array(
				'id' => 'retina-logo-height',
				'type' => 'text',
				'title' => __('Custom Logo Height', 'mfn-opts'),
				'sub_desc' => __('for Retina Logo', 'mfn-opts'),
				'desc' => __('px. Please type height of Custom Logo image (<strong>not</strong> Retina Logo).', 'mfn-opts'),
				'class' => 'small-text',
			),
			
			array(
				'id' => 'sticky-header',
				'type' => 'switch',
				'title' => __('Sticky Header', 'mfn-opts'),
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'top-bar-slogan',
				'type' => 'text',
				'title' => __('Top Bar Slogan', 'mfn-opts'),
				'desc' => __('Leave this and below fields empty if you don\'t want the Top Bar.', 'mfn-opts'),
			),
			
			array(
				'id' => 'top-bar-phone',
				'type' => 'text',
				'title' => __('Top Bar Phone number', 'mfn-opts'),
				'class' => 'small-text',
			),
			
			array(
				'id' => 'top-bar-email',
				'type' => 'text',
				'title' => __('Top Bar E-mail', 'mfn-opts'),
				'class' => 'small-text',
			),
	
		),
	);
	
	// Social Icons --------------------------------------------
	$sections['social'] = array(
		'title' => __('Social Icons', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
				
			array(
				'id' => 'social-facebook',
				'type' => 'text',
				'title' => __('Facebook', 'mfn-opts'),
				'sub_desc' => __('Type your Facebook link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-googleplus',
				'type' => 'text',
				'title' => __('Google +', 'mfn-opts'),
				'sub_desc' => __('Type your Google + link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-twitter',
				'type' => 'text',
				'title' => __('Twitter', 'mfn-opts'),
				'sub_desc' => __('Type your Twitter link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-vimeo',
				'type' => 'text',
				'title' => __('Vimeo', 'mfn-opts'),
				'sub_desc' => __('Type your Vimeo link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-youtube',
				'type' => 'text',
				'title' => __('YouTube', 'mfn-opts'),
				'sub_desc' => __('Type your YouTube link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-flickr',
				'type' => 'text',
				'title' => __('Flickr', 'mfn-opts'),
				'sub_desc' => __('Type your Flickr link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-linkedin',
				'type' => 'text',
				'title' => __('LinkedIn', 'mfn-opts'),
				'sub_desc' => __('Type your LinkedIn link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-pinterest',
				'type' => 'text',
				'title' => __('Pinterest', 'mfn-opts'),
				'sub_desc' => __('Type your Pinterest link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
			
			array(
				'id' => 'social-dribbble',
				'type' => 'text',
				'title' => __('Dribbble', 'mfn-opts'),
				'sub_desc' => __('Type your Dribbble link here', 'mfn-opts'),
				'desc' => __('Icon won`t show if you leave this field blank' , 'mfn-opts'),
			),
				
		),
	);
	
	// Custom CSS --------------------------------------------
	$sections['custom-css'] = array(
		'title' => __('Custom CSS', 'mfn-opts'),
		'fields' => array(

			array(
				'id' => 'custom-css',
				'type' => 'textarea',
				'title' => __('Custom CSS', 'mfn-opts'), 
				'sub_desc' => __('Paste your custom CSS code here.', 'mfn-opts'),
			),
				
		),
	);

	// Colors ----------------------------------------------------------------------------------------
	
	// General --------------------------------------------
	$sections['colors-general'] = array(
		'title' => __('General', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
							
			array(
				'id' => 'skin',
				'type' => 'select',
				'title' => __('Theme Skin', 'mfn-opts'), 
				'sub_desc' => __('Choose one of the predefined styles or set your own colors.', 'mfn-opts'), 
				'desc' => __('<b>Important:</b> Color options can be used only with the Custom Skin.', 'mfn-opts'), 
				'options' => array(
			
					'custom' => 'Custom',
					'green' => 'Green',
					'blue' => 'Blue',
					'orange' => 'Orange',
					'red' => 'Red',
				),
				'std' => 'custom',
			),
			
			array(
				'id' => 'background-body',
				'type' => 'color',
				'title' => __('Body background', 'mfn-opts'), 
				'std' => '#ffffff',
			),
			
		),
	);
	
	// Main menu --------------------------------------------
	$sections['menu'] = array(
		'title' => __('Menus', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
			
			array(
				'id' => 'color-menu-a',
				'type' => 'color',
				'title' => __('Menu Link color', 'mfn-opts'), 
				'std' => '#34495e'
			),
			
			array(
				'id' => 'color-menu-a-active',
				'type' => 'color',
				'title' => __('Hover Menu Link color', 'mfn-opts'),
				'std' => '#5ad4a0'
			),
				
			array(
				'id' => 'color-submenu-a',
				'type' => 'color',
				'title' => __('Submenu Link color', 'mfn-opts'),
				'std' => '#34495e'
			),
			array(
				'id' => 'color-submenu-a-hover',
				'type' => 'color',
				'title' => __('Submenu Hover Link color', 'mfn-opts'),
				'std' => '#5ad4a0'
			),

			array(
				'id' => 'background-submenu-2nd',
				'type' => 'color',
				'title' => __('Submenu 2nd level background', 'mfn-opts'),
				'std' => '#FCFCFC'
			),
			
			array(
				'id' => 'background-submenu-3rd',
				'type' => 'color',
				'title' => __('Submenu 3rd level background', 'mfn-opts'),
				'std' => '#F5F5F5'
			),
			
			array(
				'id' => 'background-widget-menu',
				'type' => 'color',
				'title' => __('Sidebar Menu background', 'mfn-opts'),
				'std' => '#5ad4a0',
			),
			
			array(
				'id' => 'color-widget-menu-link',
				'type' => 'color',
				'title' => __('Sidebar Menu Link color', 'mfn-opts'),
				'std' => '#ffffff',
			),
			
			array(
				'id' => 'color-widget-menu-link-active',
				'type' => 'color',
				'title' => __('Sidebar Active Menu Link color', 'mfn-opts'),
				'std' => '#34495e',
			),
	
		),
	);
	
	// Header --------------------------------------------
	$sections['colors-header'] = array(
		'title' => __('Header', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
					
			array(
				'id' => 'background-social',
				'type' => 'color',
				'title' => __('Social Icon background', 'mfn-opts'),
				'std' => '#dae0e6',
			),
				
			array(
				'id' => 'color-social',
				'type' => 'color',
				'title' => __('Social Icon color', 'mfn-opts'),
				'desc' => __('This is also Popup Contact Form Icon color.', 'mfn-opts'),
				'std' => '#ffffff',
			),

			array(
				'id' => 'background-social-hover',
				'type' => 'color',
				'title' => __('Hover Social Icon background', 'mfn-opts'),
				'desc' => __('This is also Popup Contact Form Icon background.', 'mfn-opts'),
				'std' => '#4ecc9f',
			),
			
			array(
				'id' => 'color-social-hover',
				'type' => 'color',
				'title' => __('Hover Social Icon color', 'mfn-opts'),
				'std' => '#ffffff',
			),
				
			array(
				'id' => 'background-subheader',
				'type' => 'color',
				'title' => __('Subheader background', 'mfn-opts'),
				'std' => '#2E3B48',
			),

			array(
				'id' => 'color-subheader-title',
				'type' => 'color',
				'title' => __('Subheader Title color', 'mfn-opts'), 
				'std' => '#ffffff',
			),

		),
	);
	
	// Content --------------------------------------------
	$sections['content'] = array(
		'title' => __('Content', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
	
			array(
				'id' => 'color-text',
				'type' => 'color',
				'title' => __('Text color', 'mfn-opts'), 
				'sub_desc' => __('Content text color.', 'mfn-opts'),
				'std' => '#34495e'
			),
			
			array(
				'id' => 'color-a',
				'type' => 'color',
				'title' => __('Link color', 'mfn-opts'), 
				'std' => '#4ecc9f'
			),
			
			array(
				'id' => 'color-a-hover',
				'type' => 'color',
				'title' => __('Hover Link color', 'mfn-opts'), 
				'std' => '#3aae85'
			),

			array(
				'id' => 'color-note',
				'type' => 'color',
				'title' => __('Grey Note text color', 'mfn-opts'),
				'std' => '#b4bbc1'
			),
			
			array(
				'id' => 'border-borders',
				'type' => 'color',
				'title' => __('Border color', 'mfn-opts'), 
				'std' => '#ebeff2'
			),
			
			array(
				'id' => 'background-highlight',
				'type' => 'color',
				'title' => __('Highlight text background', 'mfn-opts'),
				'std' => '#4ECC9F'
			),
	
			array(
				'id' => 'color-highlight',
				'type' => 'color',
				'title' => __('Highlight text color', 'mfn-opts'),
				'std' => '#ffffff'
			),

			array(
				'id' => 'background-button',
				'type' => 'color',
				'title' => __('Button background', 'mfn-opts'), 
				'std' => '#60d1c7',
			),
			
			array(
				'id' => 'color-button',
				'type' => 'color',
				'title' => __('Button text color', 'mfn-opts'), 
				'std' => '#ffffff',
			),

			array(
				'id' => 'background-button-hover',
				'type' => 'color',
				'title' => __('Hover Button background', 'mfn-opts'), 
				'std' => '#3fb4aa',
			),
			
			array(
				'id' => 'color-button-hover',
				'type' => 'color',
				'title' => __('Hover Button text color', 'mfn-opts'), 
				'std' => '#ffffff',
			),
	
		),
	);
	
	// Footer --------------------------------------------
	$sections['colors-footer'] = array(
		'title' => __('Footer', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
				
			array(
				'id' => 'background-footer',
				'type' => 'color',
				'title' => __('Footer background', 'mfn-opts'),
				'std' => '#2c3e50',
			),
	
			array(
				'id' => 'color-footer',
				'type' => 'color',
				'title' => __('Footer text color', 'mfn-opts'),
				'std' => '#ffffff',
			),
	
			array(
				'id' => 'color-footer-link',
				'type' => 'color',
				'title' => __('Footer Link color', 'mfn-opts'),
				'std' => '#4ecc9f',
			),
				
			array(
				'id' => 'color-footer-link-hover',
				'type' => 'color',
				'title' => __('Footer Hover Link color', 'mfn-opts'),
				'std' => '#3aae85',
			),
				
			array(
				'id' => 'color-footer-heading',
				'type' => 'color',
				'title' => __('Footer Heading color', 'mfn-opts'),
				'std' => '#ffffff',
			),
				
			array(
				'id' => 'color-footer-widget-title',
				'type' => 'color',
				'title' => __('Footer Widget Title color', 'mfn-opts'),
				'std' => '#4ecc9f',
			),
				
			array(
				'id' => 'color-footer-note',
				'type' => 'color',
				'title' => __('Footer Grey Note color', 'mfn-opts'),
				'std' => '#8fa5b8',
			),
	
		),
	);
	
	// Headings --------------------------------------------
	$sections['headings'] = array(
		'title' => __('Headings', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
	
			array(
				'id' => 'color-h1',
				'type' => 'color',
				'title' => __('Heading H1 color', 'mfn-opts'), 
				'std' => '#5ad4a0'
			),
			
			array(
				'id' => 'color-h2',
				'type' => 'color',
				'title' => __('Heading H2 color', 'mfn-opts'), 
				'std' => '#5ad4a0'
			),
			
			array(
				'id' => 'color-h3',
				'type' => 'color',
				'title' => __('Heading H3 color', 'mfn-opts'), 
				'std' => '#5ad4a0'
			),
			
			array(
				'id' => 'color-h4',
				'type' => 'color',
				'title' => __('Heading H4 color', 'mfn-opts'), 
				'std' => '#5ad4a0'
			),
			
			array(
				'id' => 'color-h5',
				'type' => 'color',
				'title' => __('Heading H5 color', 'mfn-opts'), 
				'std' => '#5ad4a0'
			),
			
			array(
				'id' => 'color-h6',
				'type' => 'color',
				'title' => __('Heading H6 color', 'mfn-opts'), 
				'std' => '#5ad4a0'
			),
				
		),
	);
	
	// Accordion & Tabs --------------------------------------------
	$sections['colors-accordion'] = array(
		'title' => __('Accordion & Tabs', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
		
			array(
				'id' => 'color-accordion-title',
				'type' => 'color',
				'title' => __('Accordion Title color', 'mfn-opts'),
				'desc' => __('To change icon please edit <strong>images/accordion_controls.png</strong> file.', 'mfn-opts'),
				'std' => '#384E65',
			),
		
			array(
				'id' => 'background-accordion-title',
				'type' => 'color',
				'title' => __('Accordion Title background', 'mfn-opts'),
				'std' => '#F0F2F2',
			),
				
			array(
				'id' => 'color-accordion-content',
				'type' => 'color',
				'title' => __('Accordion Content color', 'mfn-opts'),
				'std' => '#565151',
			),
		
			array(
				'id' => 'background-accordion-content',
				'type' => 'color',
				'title' => __('Accordion Content background', 'mfn-opts'),
				'std' => '#F9F9F9',
			),
		
			array(
				'id' => 'color-tabs-title',
				'type' => 'color',
				'title' => __('Tabs Title color', 'mfn-opts'),
				'std' => '#34495e',
			),
			
			array(
				'id' => 'background-tabs-title',
				'type' => 'color',
				'title' => __('Tabs Title background', 'mfn-opts'),
				'std' => '#ebeff2',
			),
		
			array(
				'id' => 'color-tabs-title-active',
				'type' => 'color',
				'title' => __('Tabs Active Title color', 'mfn-opts'),
				'std' => '#3dc282',
			),
			
			array(
				'id' => 'color-tabs-content',
				'type' => 'color',
				'title' => __('Tabs Content color', 'mfn-opts'),
				'std' => '#717E8C',
			),
			
			array(
				'id' => 'background-tabs-content',
				'type' => 'color',
				'title' => __('Tabs Content background', 'mfn-opts'),
				'desc' => __('This is also Active Tab Title background.', 'mfn-opts'),
				'std' => '#ffffff',
			),
		
		),
	);
	
	// Shortcodes --------------------------------------------
	$sections['colors-shortcodes'] = array(
		'title' => __('Shortcodes', 'mfn-opts'),
		'icon' => MFN_OPTIONS_URI. 'img/icons/sub.png',
		'fields' => array(
	
			array(
				'id' => 'background-call-to-action',
				'type' => 'color',
				'title' => __('Call To Action background', 'mfn-opts'),
				'std' => '#07bb86',
			),
				
			array(
				'id' => 'color-call-to-action',
				'type' => 'color',
				'title' => __('Call To Action color', 'mfn-opts'),
				'std' => '#ffffff',
			),
				
			array(
				'id' => 'background-image-overlay',
				'type' => 'color',
				'title' => __('Image Overlay background', 'mfn-opts'),
				'std' => '#07bb86',
			),
				
			array(
				'id' => 'background-portfolio-tab-active',
				'type' => 'color',
				'title' => __('Portfolio Active Tab background', 'mfn-opts'),
				'std' => '#34D3A6',
			),
				
			array(
				'id' => 'background-pricing-featured',
				'type' => 'color',
				'title' => __('Pricing Item Featured background', 'mfn-opts'),
				'std' => '#F8FFFD',
			),
				
			array(
				'id' => 'border-pricing-featured',
				'type' => 'color',
				'title' => __('Pricing Item Featured border', 'mfn-opts'),
				'std' => '#E5F9F3',
			),

		),
	);

	// Font Family --------------------------------------------
	$sections['font-family'] = array(
		'title' => __('Font Family', 'mfn-opts'),
		'fields' => array(
			
			array(
				'id' => 'font-content',
				'type' => 'font_select',
				'title' => __('Content Font', 'mfn-opts'), 
				'sub_desc' => __('This font will be used for all theme texts except headings and menu.', 'mfn-opts'), 
				'std' => 'Open Sans'
			),
			
			array(
				'id' => 'font-menu',
				'type' => 'font_select',
				'title' => __('Main Menu Font', 'mfn-opts'), 
				'sub_desc' => __('This font will be used for header menu.', 'mfn-opts'), 
				'std' => 'Open Sans'
			),
			
			array(
				'id' => 'font-headings',
				'type' => 'font_select',
				'title' => __('Headings Font', 'mfn-opts'), 
				'sub_desc' => __('This font will be used for all headings.', 'mfn-opts'), 
				'std' => 'Open Sans'
			),
			
			array(
				'id' => 'font-subset',
				'type' => 'text',
				'title' => __('Google Font Subset', 'mfn-opts'),				
				'sub_desc' => __('Specify which subsets should be downloaded. Multiple subsets should be separated with coma (,)', 'mfn-opts'),
				'desc' => __('Some of the fonts in the Google Font Directory support multiple scripts (like Latin and Cyrillic for example). In order to specify which subsets should be downloaded the subset parameter should be appended to the URL. For a complete list of available fonts and font subsets please see <a href="http://www.google.com/webfonts" target="_blank">Google Web Fonts</a>.', 'mfn-opts'),
				'class' => 'small-text'
			),
				
		),
	);
	
	// Content Font Size --------------------------------------------
	$sections['font-size'] = array(
		'title' => __('Font Size', 'mfn-opts'),
		'fields' => array(

			array(
				'id' => 'font-size-content',
				'type' => 'sliderbar',
				'title' => __('Content', 'mfn-opts'),
				'sub_desc' => __('This font size will be used for all theme texts.', 'mfn-opts'),
				'std' => '13',
			),
				
			array(
				'id' => 'font-size-menu',
				'type' => 'sliderbar',
				'title' => __('Main menu', 'mfn-opts'),
				'sub_desc' => __('This font size will be used for top level only.', 'mfn-opts'),
				'std' => '13',
			),
			
			array(
				'id' => 'font-size-h1',
				'type' => 'sliderbar',
				'title' => __('Heading H1', 'mfn-opts'),
				'sub_desc' => __('Subpages header title.', 'mfn-opts'),
				'std' => '43',
			),
			
			array(
				'id' => 'font-size-h2',
				'type' => 'sliderbar',
				'title' => __('Heading H2', 'mfn-opts'),
				'std' => '40',
			),
			
			array(
				'id' => 'font-size-h3',
				'type' => 'sliderbar',
				'title' => __('Heading H3', 'mfn-opts'),
				'std' => '28',
			),
			
			array(
				'id' => 'font-size-h4',
				'type' => 'sliderbar',
				'title' => __('Heading H4', 'mfn-opts'),
				'std' => '25',
			),
			
			array(
				'id' => 'font-size-h5',
				'type' => 'sliderbar',
				'title' => __('Heading H5', 'mfn-opts'),
				'std' => '19',
			),
			
			array(
				'id' => 'font-size-h6',
				'type' => 'sliderbar',
				'title' => __('Heading H6', 'mfn-opts'),
				'std' => '15',
			),
	
		),
	);
	
	// Translate / General --------------------------------------------
	$sections['translate-general'] = array(
		'title' => __('General', 'mfn-opts'),
		'fields' => array(
	
			array(
				'id' => 'translate',
				'type' => 'switch',
				'title' => __('Enable Translate', 'mfn-opts'), 
				'desc' => __('Turn it off if you want to use .mo .po files for more complex translation.', 'mfn-opts'),
				'options' => array('1' => 'On','0' => 'Off'),
				'std' => '1'
			),
			
			array(
				'id' => 'translate-search-placeholder',
				'type' => 'text',
				'title' => __('Search Placeholder', 'mfn-opts'),
				'desc' => __('Widget Search', 'mfn-opts'),
				'std' => 'Enter your search',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-search',
				'type' => 'text',
				'title' => __('Search', 'mfn-opts'),
				'desc' => __('Widget Search', 'mfn-opts'),
				'std' => 'Search',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-home',
				'type' => 'text',
				'title' => __('Home', 'mfn-opts'),
				'desc' => __('Breadcrumbs', 'mfn-opts'),
				'std' => 'Home',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-contact-form-title',
				'type' => 'text',
				'title' => __('Send us a message', 'mfn-opts'),
				'desc' => __('Popup Contact Form', 'mfn-opts'),
				'std' => 'Send us a message',
				'class' => 'small-text',
			),

		),
	);
	
	// Translate / Blog & Portfolio --------------------------------------------
	$sections['translate-blog'] = array(
		'title' => __('Blog & Portfolio', 'mfn-opts'),
		'fields' => array(
				
			array(
				'id' => 'translate-comment',
				'type' => 'text',
				'title' => __('Comment', 'mfn-opts'),
				'sub_desc' => __('Text to display when there is one comment', 'mfn-opts'),
				'desc' => __('Blog', 'mfn-opts'),
				'std' => 'Comment',
				'class' => 'small-text',
			),
				
			array(
				'id' => 'translate-comments',
				'type' => 'text',
				'title' => __('Comments', 'mfn-opts'),
				'sub_desc' => __('Text to display when there are more than one comments', 'mfn-opts'),
				'desc' => __('Blog', 'mfn-opts'),
				'std' => 'Comments',
				'class' => 'small-text',
			),
				
			array(
				'id' => 'translate-comments-off',
				'type' => 'text',
				'title' => __('Comments off', 'mfn-opts'),
				'sub_desc' => __('Text to display when comments are disabled', 'mfn-opts'),
				'desc' => __('Blog', 'mfn-opts'),
				'std' => 'Comments off',
				'class' => 'small-text',
			),
	
			array(
				'id' => 'translate-select-category',
				'type' => 'text',
				'title' => __('Select category', 'mfn-opts'),
				'desc' => __('Portfolio', 'mfn-opts'),
				'std' => 'Select category',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-all',
				'type' => 'text',
				'title' => __('All', 'mfn-opts'),
				'desc' => __('Portfolio', 'mfn-opts'),
				'std' => 'All',
				'class' => 'small-text',
			),

			array(
				'id' => 'translate-project-description',
				'type' => 'text',
				'title' => __('Project Description', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Project Description',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-client',
				'type' => 'text',
				'title' => __('Client', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Client',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-date',
				'type' => 'text',
				'title' => __('Date', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Date',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-category',
				'type' => 'text',
				'title' => __('Category', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Category',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-project-url',
				'type' => 'text',
				'title' => __('Project URL', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Project URL',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-visit-online',
				'type' => 'text',
				'title' => __('Visit online', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Visit online',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-details',
				'type' => 'text',
				'title' => __('Details', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Details',
				'class' => 'small-text',
			),
			
			array(
				'id' => 'translate-back',
				'type' => 'text',
				'title' => __('Back to list', 'mfn-opts'),
				'desc' => __('Single Portfolio', 'mfn-opts'),
				'std' => 'Back to list',
				'class' => 'small-text',
			),
			
		),
	);
	
	// Translate Error 404 --------------------------------------------
	$sections['translate-404'] = array(
		'title' => __('Error 404', 'mfn-opts'),
		'fields' => array(
	
			array(
				'id' => 'translate-404-title',
				'type' => 'text',
				'title' => __('Title', 'mfn-opts'),
				'desc' => __('Ooops... Error 404', 'mfn-opts'),
				'std' => 'Ooops... Error 404',
			),
			
			array(
				'id' => 'translate-404-subtitle',
				'type' => 'text',
				'title' => __('Subtitle', 'mfn-opts'),
				'desc' => __('We are sorry, but the page you are looking for does not exist.', 'mfn-opts'),
				'std' => 'We are sorry, but the page you are looking for does not exist.',
			),
			
			array(
				'id' => 'translate-404-text',
				'type' => 'text',
				'title' => __('Text', 'mfn-opts'),
				'desc' => __('Please check entered address and try again or', 'mfn-opts'),
				'std' => 'Please check entered address and try again or ',
			),
			
			array(
				'id' => 'translate-404-btn',
				'type' => 'text',
				'title' => __('Button', 'mfn-opts'),
				'sub_desc' => __('Go To Homepage button', 'mfn-opts'),
				'std' => 'go to homepage',
				'class' => 'small-text',
			),
	
		),
	);
								
	global $MFN_Options;
	$MFN_Options = new MFN_Options( $menu, $sections );
}
//add_action('init', 'mfn_opts_setup', 0);
mfn_opts_setup();


/**
 * This is used to return and option value from the options array
 */
function mfn_opts_get($opt_name, $default = null){
	global $MFN_Options;
	return $MFN_Options->get( $opt_name, $default );
}


/**
 * This is used to echo and option value from the options array
 */
function mfn_opts_show($opt_name, $default = null){
	global $MFN_Options;
	$option = $MFN_Options->get( $opt_name, $default );
	if( ! is_array( $option ) ){
		echo $option;
	}	
}

?>