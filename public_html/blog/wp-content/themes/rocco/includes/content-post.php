<?php
/**
 * The template for displaying content in the index.php template
 *
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

// post meta
$posts_meta = array();
$posts_meta['time'] 		= mfn_opts_get( 'blog-time' );
$posts_meta['comments'] 	= mfn_opts_get( 'blog-comments' );
$posts_meta['categories']	= mfn_opts_get( 'blog-categories' );
$posts_meta['tags'] 		= mfn_opts_get( 'blog-tags' );

// post classes
$post_classes = array( 'clearfix', 'post', 'column' );
if( ! has_post_thumbnail() ) 	$post_classes[] = 'no-photo';
if( post_password_required() ) 	$post_classes[] = 'no-photo';

// layout
if( $_GET && key_exists('mfn-b', $_GET) ){
	$post_classes[] = $_GET['mfn-b']; // demo
} else {
	$post_classes[] = mfn_opts_get( 'blog-layout', 'one' );
}

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( $post_classes ); ?>>
	
	<?php 
		if( ! post_password_required() ){
			echo '<div class="photo">';
				// post thumbnail --------------
				if( $blog_slider = get_post_meta( get_the_ID(), 'mfn-post-slider', true ) ){
					putRevSlider( $blog_slider );
				} elseif( $video = get_post_meta( $post->ID, 'mfn-post-vimeo', true ) ){
					echo '<iframe class="scale-with-grid" src="http://player.vimeo.com/video/'. $video .'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'."\n";
				} elseif( $video = get_post_meta( $post->ID, 'mfn-post-youtube', true ) ){
					echo '<iframe class="scale-with-grid" src="http://www.youtube.com/embed/'. $video .'?wmode=opaque" frameborder="0" allowfullscreen></iframe>'."\n";
				} elseif( has_post_thumbnail() ){
					echo '<a href="'. get_permalink() .'">';
						the_post_thumbnail( 'blog', array('class'=>'scale-with-grid' ) );
					echo '</a>';
				}
			echo '</div>';
		}
	?>
	
	<div class="desc">
		

		
		<div class="meta">
			<?php 
				if( $posts_meta['time'] ){
					echo '<div class="date"><i class="icon-calendar"></i> '. get_the_date() .'</div><span class="sep">|</span>';
				}
				if( $posts_meta['comments'] ){
					echo '<div class="comments"><i class="icon-comment-alt"></i> '. mfn_comments_number() .'</div><span class="sep">|</span>';
				}
				if( $posts_meta['categories'] ){
					echo '<div class="category"><i class="icon-reorder"></i> '. get_the_category_list(', ') .'</div>';
				}
			?>
		</div>
			
		<div class="post_content">			
			<?php echo mfn_excerpt( get_the_ID(), 48, '<b><strong><h1><h2><h3><h4><h5><h6>' ); ?>
		</div>
		
		<?php if( $blog_readmore = mfn_opts_get( 'blog-readmore' ) ) echo '<a href="'. get_permalink() .'" class="button">'. __( $blog_readmore ) .'</a>'; ?>
		
		<div class="footer">
			<?php 
				if( $posts_meta['tags'] && ( $terms = get_the_terms( false, 'post_tag' ) ) ){
					echo '<p class="tags">';
					echo '<i class="icon-tags"></i> ';
					$terms_count = count( $terms );
					foreach( $terms as $term ){
						$terms_count--;
						$sep = ( $terms_count ) ? ',' : false;
						$link = get_term_link( $term, 'post_tag' );
						echo '<a href="' . esc_url( $link ) . '" rel="tag"><span>' . $term->name . $sep .'</span></a> ';
					}
					echo '</p>';
				}
			?>
		</div>

	</div>
	
</div>