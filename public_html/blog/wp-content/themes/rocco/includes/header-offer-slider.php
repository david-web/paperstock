<?php 
$slide_args = array( 
	'post_type' => 'slide',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'ignore_sticky_posts' => 1,
);
$slide_query = new WP_Query();
$slide_query->query( $slide_args );

$slide_post_count = $slide_query->post_count;
// print_r($slide_query);
?>

<!-- #mfn-offer-slider -->
<div id="mfn-offer-slider">
	<ul class="slider-wrapper jcarousel-skin-tango">
				
		<?php 
			if ($slide_query->have_posts()) :
			while ($slide_query->have_posts()) :
			$slide_query->the_post();
			
			$slide_classes 	= get_post_meta($post->ID, 'mfn-slide-position', true);
			$slide_classes .= ' '. get_post_meta($post->ID, 'mfn-slide-background', true);
		?>
	
		<li>
			<div class="slide-wrap clearfix">
			
				<?php if( $slide_link = get_post_meta($post->ID, 'mfn-slide-link', true) ) echo '<a href="'. $slide_link .'">'; ?>
		
					<div class="slide-img">
						<?php the_post_thumbnail( 'offer-slider', array('class'=>'img scale-with-grid' )); ?>
					</div>
					
					<div class="slide-desc">
						<div class="arrow"></div>
						<h2 class="slide-title"><?php the_title(); ?></h2>
					</div>
					
				<?php if( $slide_link ) echo '</a>'; ?>
				
			</div>
		</li>
			
		<?php 
			endwhile;
			endif;
			wp_reset_query();
		?>	
			
	</ul>
</div>