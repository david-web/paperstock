<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

$client_args = array(
	'post_type' => 'client',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
);

$client_query = new WP_Query();
$client_query->query( $client_args );

?>

<div class="Our_clients_slider">
	<div class="container">
		<div class="column one">
			<a href="#" class="slider_control slider_control_prev"></a>
			<a href="#" class="slider_control slider_control_next"></a>
			<div class="inside">
			
				<?php
				if ( $client_query->have_posts() )
				{
					echo '<ul>';
					while ( $client_query->have_posts() )
					{
						$client_query->the_post();
						
						echo '<li>';
							echo '<div class="slide-wrapper">';
								$link = get_post_meta(get_the_ID(), 'mfn-post-link', true);
								if( $link ) echo '<a target="_blank" href="'. $link .'" title="'. the_title(false, false, false) .'">';
									the_post_thumbnail( 'clients-slider', array('class'=>'scale-with-grid') );
								if( $link ) echo '</a>';
							echo '</div>';
						echo '</li>';
						
					}
					echo '</ul>'."\n";
				}
				wp_reset_query();
				?>
				
			</div>
		</div>
	</div>
</div>
