<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package Rocco
 * @author Muffin group
 * @link http://muffingroup.com
 */

// post meta
$posts_meta = array();
$posts_meta['time'] = get_post_meta($post->ID, 'mfn-post-time', true);
$posts_meta['comments'] = get_post_meta($post->ID, 'mfn-post-comments', true);
$posts_meta['categories'] = get_post_meta($post->ID, 'mfn-post-categories', true);
$posts_meta['tags'] = get_post_meta($post->ID, 'mfn-post-tags', true);

// post classes
$post_classes = array( 'clearfix', 'post' );
if( ! has_post_thumbnail() ) 	$post_classes[] = 'no-photo';
if( post_password_required() ) 	$post_classes[] = 'no-photo';
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( $post_classes ); ?>>
	
	<?php 
		if( ! post_password_required() ){
			echo '<div class="photo">';
				// post thumbnail --------------
				if( $blog_slider = get_post_meta( get_the_ID(), 'mfn-post-slider', true ) ){
					putRevSlider( $blog_slider );
				} elseif( $video = get_post_meta($post->ID, 'mfn-post-vimeo', true) ){
					echo '<iframe class="scale-with-grid" src="http://player.vimeo.com/video/'. $video .'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'."\n";
				} elseif( $video = get_post_meta($post->ID, 'mfn-post-youtube', true) ){
					echo '<iframe class="scale-with-grid" src="http://www.youtube.com/embed/'. $video .'" frameborder="0" allowfullscreen></iframe>'."\n";
				} elseif( has_post_thumbnail() ){
					$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
					echo '<a rel="prettyPhoto" href="'. $large_image_url[0] .'">';
						the_post_thumbnail( 'blog', array('class'=>'scale-with-grid' ));
					echo '</a>';
				}	
			echo '</div>';
		}
	?>
	
	<div class="desc">
		
		<div class="meta">
			<?php 
				if( $posts_meta['time'] ){
					echo '<div class="date"><i class="icon-calendar"></i> '. get_the_date() .'</div><span class="sep">|</span>';
				}
				if( $posts_meta['comments'] ){
					echo '<div class="comments"><i class="icon-comment-alt"></i> '. mfn_comments_number() .'</div><span class="sep">|</span>';
				}
				if( $posts_meta['categories'] ){
					echo '<div class="category"><i class="icon-reorder"></i> '. get_the_category_list(', ') .'</div>';
				}
			?>
		</div>
			
		<div class="post_content">
		
			<?php 
				// Content Builder & WordPress Editor Content
				mfn_builder_print( $post->ID );

				// List of pages
				wp_link_pages();
			?>
			
		</div>
		
		<div class="footer">
			<?php 
				if( $posts_meta['tags'] && ( $terms = get_the_terms( false, 'post_tag' ) ) ){
					echo '<p class="tags">';
					echo '<i class="icon-tags"></i> ';
					$terms_count = count( $terms );
					foreach( $terms as $term ){
						$terms_count--;
						$sep = ( $terms_count ) ? ',' : false;
						$link = get_term_link( $term, 'post_tag' );
						echo '<a href="' . esc_url( $link ) . '" rel="tag"><span>' . $term->name . $sep .'</span></a> ';
					}
					echo '</p>';
				}
			?>
		</div>

	</div>

</div>

<div class="section section-post-footer">
	<div class="section_wrapper clearfix">
		<div class="column one comments">
			<?php comments_template( '', true ); ?>
		</div>
	</div>
</div>