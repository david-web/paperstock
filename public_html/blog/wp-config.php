<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'papersst_blog');

/** MySQL database username */
define('DB_USER', 'papersst_blog');

/** MySQL database password */
define('DB_PASSWORD', ',TZ[78##FoOE');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MSd_+vF^-O@?t`y^.?emv[+=K8,>&i+|7yCiUcW<^uz&~xP5Y7^ mg?jc.4< %j5');
define('SECURE_AUTH_KEY',  'z*p1Sz+|q|6Wj%fpVwUc:<7AS!hs],URgf~d}NsN->GoJ;gXg.n+)[y-uK#SB|S0');
define('LOGGED_IN_KEY',    '`s|*nn@0Cq)+)8ey<x{SVTD4041Tm,4H9MvvigP+mNhKZUML-(D6E1aPX|*X,kVd');
define('NONCE_KEY',        '),q%?z>B0Y44i}-N-XE!;)Ho~g5Ov-|oztM?+d`PRLRdFtjk<Z`QxN!+r8yw)79g');
define('AUTH_SALT',        'Md_[Cwgw=zPSc-d s-a> B>/qTq*TYP,8JhF>,HFM3@w~U_na cmqhd^D?:-3D]C');
define('SECURE_AUTH_SALT', 'z/Q705|eH}y,LKh)`Emh]vKVK>.D_4N m+P?Z FEn! Qb:^k.XNC<&D*O+/I4[DR');
define('LOGGED_IN_SALT',   'w<vMwwC ls.!96#.9UD;r)+rC@8$s;Y50 #+4oc(]YC|v1j%ezh!*euvvrxgNjR,');
define('NONCE_SALT',       '0<[ZIhAX87o||P;_/y` 5gQ AZ|&kt0A`by&iCI0Q}2_oEC-z&~>:mmn&AC^3Ajf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
error_reporting(0);
@ini_set('display_errors', 0);

//        define('WP_DEBUG', true);
//        define('WP_DEBUG_LOG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
