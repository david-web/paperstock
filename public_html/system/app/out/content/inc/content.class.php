<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/**
 * CMS content module
 * Default module in cms. Generate content, load templates and show it
 * Be careful, if wanna edit it.
 * 21.04.2008
 */

class contentData extends Module {

	private $dbTable = "ad_content";
	private $pageParentIds = array();
	private $contentData = array();
	private $siteMenus = array();

	/**
	 * Class constructor
	 */
	public function __construct() {		
		
		parent :: __construct();
	}
	
	/**
	 * Get inherit page content
	 * 
	 * @param int	content id
	 */
	private function getInheritContent($id) {
		
		$dbQuery = "SELECT `page_title`, `title`, `full_title`, `content`, `image`, `keywords`, `description`
						FROM " . $this->dbTable . " WHERE `id` = '" . $id . "' LIMIT 0,1";
		$query = new query($this->db, $dbQuery);
		if ($query->num_rows() > 0) {
            return $query->getrow();
		} else {
			return false;
		}
	}
	
	/**
	 * Get template name by id
	 * 
	 * @param int	template id
	 */
	 public function getTemplateById($id) {
	 	
	 	$dbQuery = "SELECT `filename` FROM `ad_templates` WHERE `id` = '" . $id . "' LIMIT 0,1";
		$query = new query($this->db, $dbQuery);
		
		return $query->getOne();
	 }

	/**
	 * Checking page url.
	 * If is not correct opening default site page
	 * 
	 * @param string		page url
	 */
	public function checkPageUrl($pageUrl) {

		if (!$pageUrl) {
			openDefaultPage();
		}

		$dbQuery = "SELECT content.*
									FROM 
										`ad_countries_domains` domains,
										`ad_content` content,
										`ad_languages` lang,
										`ad_languages_to_ct` lc,
										`ad_templates` tpl
									WHERE 1
										AND domains.`domain` = '" . $_SERVER["HTTP_HOST"] . "' 
										AND content.`active` = '1'
										AND content.`country` = domains.`country_id`
										AND content.`url` = '" . mres($pageUrl) . "'
										AND lang.`enable` = '1'
										AND lang.`lang` = content.`lang`
										AND lc.`lang_id` = lang.`id`
										AND lc.`country_id` = domains.`country_id`
								  LIMIT 1";
        
		$query = new query($this->db, $dbQuery);
		if ($query->num_rows() > 0) {
			
			$this->contentData = $query->getrow();
			
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
				$this->noLayout(true);
			} else {
				if ($this->contentData['cache'] && empty($_POST)) {
					if (file_exists(AD_CACHE_FOLDER . md5($this->contentData['url']) . '.' . $this->contentData['edit_date'] . '.cache')) {
						if ($cache = fopen(AD_CACHE_FOLDER . md5($this->contentData['url']) . '.' . $this->contentData['edit_date'] . '.cache', 'r')) {
							$output = '';
							while (!feof($cache)) {
	  							$output .= fread($cache, 8192);
							}
							 
							fclose($cache); 
							die($output);	
						} 
					}
				}
			}
			
			if ($this->contentData['ssl'] && !isset($_SERVER['HTTPS'])) {
				redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			} elseif (!$this->contentData['ssl'] && isset($_SERVER['HTTPS'])) {
				redirect('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			}
			
			if (isset($_SERVER["HTTP_HOST"])) {
				$langToDomain = $this->cfg->get('langDomains');
				if (isset($langToDomain[$this->contentData['lang']]) && $langToDomain[$this->contentData['lang']] != $_SERVER["HTTP_HOST"]) {
					redirect('http://' . $langToDomain[$this->contentData['lang']] . '/' . $this->contentData['url']);
				}
			}
			
			// Setting site lang
			$this->setLang($this->contentData['lang']);
			
			// Setting country
			$this->setCountry($this->contentData['country']);
			
			if ($this->contentData["type"] == "r") {
				if ($this->contentData["target"]) {			
					if (is_numeric($this->contentData["target"])) {
						redirect(getLink($this->contentData["target"]));
					}
					else {
						redirect($this->contentData["target"]);
					}		
				}
			} elseif ($this->contentData["type"] == "i") {
				if ($this->contentData["target"]) {			
					if (is_numeric($this->contentData["target"])) {
						if ($inherit = $this->getInheritContent($this->contentData["target"])) {
							$this->contentData = array_merge($this->contentData, $inherit);
						}
					}	
				}
			}
			
			/**
			 * Getting template filename
			 */
			$this->contentData["template"] = $this->contentData["template"] ? $this->getTemplateById($this->contentData["template"]) : ''; 
			
			/**
			 * Getting all parent id's
			 */
			$this->contentData["parentIds"] = $this->getAllParentIds($this->contentData["id"]);
			
			return $this->contentData;

		} else {
			if ($this->cfg->get('404')) {
				$this->show404Page();
			} else {
				if (isset($_SESSION['ad_language']) && $id = getLangMainPage($_SESSION['ad_language'])) {
					redirect(getLink($id));
					//redirectToClosestParent();
				} else {
					if ($id = getLangMainPage($this->cfg->get("langInTheEnd") ? $this->uri->segment($this->uri->totalSegments() - 1) : $this->uri->segment(0))) {
						redirect(getLink($id));
					} else {
						openDefaultPage();
						//redirectToClosestParent();
					}
				}	
			}
			
		}
	}
	
	/**
	 * Get all page parent ids from db
	 * 
	 * @param int	content id
	 */
	function getAllParentIds($id) {

		$dbQuery = "SELECT `id`, `parent_id` FROM " . $this->dbTable . " WHERE `id` = '" . $id . "' LIMIT 0,1";
		$query = new query($this->db, $dbQuery);
		if ($query->getrow()) {
			$this->pageParentIds[] = $query->field("id");
			$parentId = $query->field('parent_id');
			if ($parentId) {
				$this->getAllParentIds($parentId);
			}
		}
		$query->free();
		
		return $this->pageParentIds;
	}

	/**
	 * Display all content menus on the page
	 */
	public function displayPageMenus() {
		
		$this->getAllMenus();
		//pR($this->siteMenus);
		$this->setPData($this->siteMenus, "menu");
		$this->setPData($this->getCountryMenu(), "ctrAr");
		$this->setPData($this->getLangMenu(), "langMenu");
	}

	/**
	 * Get all menu nodes
	 * 
	 * @param int	menu id
	 * @param int	parent page id
	 */	
	private function getAllMenus($parentId = 0, $pParentId = '') {

		$r = array();
		$dbQuery = "SELECT c.`id`, c.`parent_id`, c.`title`, c.`url`, m.`id` AS menu_id, m.`name`, m.`expanded`  
						FROM `ad_content` c, `ad_menus` m, `ad_menus_on_page` mp
						WHERE mp.`menu_id` = m.id
								AND mp.page_id = c.id
								AND c.`lang` = '" . $this->getLang() . "' 
								AND c.`enable` = '1' 
								AND c.`active` = '1' 
								AND c.`country` = '" . $this->getCountry() . "' 
								" . ($pParentId ? " AND c.`parent_id` = '" . $pParentId . "'" : "") . "
								AND m.enable = '1'
								AND m.parent_id = '" . $parentId . "'
						ORDER BY m.name, c.`sort` ASC";
		$query = new query($this->db, $dbQuery);
		$cnt = $query->num_rows();

		$first = true;
		$menuName = '';
		$i = 1;
		
		$mirrorId = getMirror(getDefaultPageId());
		
		while($row = $query->getrow()) {
            $dont_include = false;
            if($this->isNewsContent($query->field('id')) && !$this->containsDocuments($query->field('id'), 'mod_news', " AND n.enable = '1' ")){
                $dont_include = true;
            }
			if($this->isNewsContent($query->field('id'), 'faq') && !$this->containsDocuments($query->field('id'), 'mod_faq', " AND n.enable = '1' ")){
                $dont_include = true;
            }
            if(!$dont_include){

                $isAct = (in_array($query->field('id'), $this->pageParentIds) ? true : false);

                if ($query->field('id') == $mirrorId && $this->contentData["parent_id"] != 0) {
                    $isAct = false;
                }

                if ($menuName && $menuName != $query->field('name')) {
                    $first = true;

                    // Change last item of previous menu to last
                    if (isset($this->siteMenus[$menuName][count($this->siteMenus[$menuName]) - 1])) {
                        $this->siteMenus[$menuName][count($this->siteMenus[$menuName]) - 1]['last'] = true;
                    }


                } else {
                    $first = false;	
                }

                $menuName = $query->field('name');

                if ($pParentId) {
                    $this->siteMenus[$query->field('name')][$pParentId][] = array(
                            "id" 	 => $query->field('id'),
                    		"pid"	 =>	$query->field('parent_id'),
                            "first"	 => ($i == 1 ? true : $first),
                            "last"	 => ($i == $cnt ? true : false),
                            "title"  => $query->field('title'),
                            "url"	 => "/" . makeUrlWithLangInTheEnd($query->field('url')),
                            "active" => $isAct
                            );
                } else {
                    $this->siteMenus[$query->field('name')][] = array(
                            "id" 	 => $query->field('id'),
                    		"pid"	 =>	$query->field('parent_id'),
                            "first"	 => ($i == 1 ? true : $first),
                            "last"	 => ($i == $cnt ? true : false),
                            "title"  => $query->field('title'),
                            "url"	 => "/" . makeUrlWithLangInTheEnd($query->field('url')),
                            "active" => $isAct
                            );
                }


                if ($isAct || $query->field('expanded')) {
                    $this->getAllMenus($query->field('menu_id'), $query->field('id'));
                }


                $i++;
            }
		}
			
		return $r;
	}
    
    public function containsDocuments($id, $table = 'mod_news', $extra = '') {
        $result = 0;

        $dbQuery = "SELECT COUNT(`id`) FROM `" . $table . "` n WHERE n.`content_id` = '" . $id . "' " . $extra . " ";
        $query = new query($this->db, $dbQuery);
        $documents_cnt = $query->getOne();
        if ($documents_cnt) {
            $result = 1;
        }

        return $result;

    }
    
    public function isNewsContent($id, $module_name = 'news') {
        $result = false;

        $dbQuery = "
        	SELECT mop.`page_id` 
        	FROM `ad_modules` AS m
        	INNER JOIN `ad_modules_on_page` AS mop
        		ON m.`id` = mop.`module_id`
        	WHERE 
        		mop.`page_id` = '" . $id . "' 
        		AND m.`name` = '".$module_name."'
        ";
        $query = new query($this->db, $dbQuery);

        $isNews = $query->getOne();
        if ($isNews) {
            $result = true;
        }

        return $result;

    }

	/**
	 * Display page language menu
	 */
	public function getLangMenu() {
		
		$langArray = array();
		
	    $dbQuery = "SELECT * FROM `ad_languages` l, `ad_languages_to_ct` lc WHERE l.id = lc.lang_id AND lc.country_id = '" . $this->getCountry() . "' AND l.enable = '1' ORDER BY l.sort ASC";
		$query = new query($this->db, $dbQuery);
		$cnt = $query->num_rows();
		
		if ($cnt > 0) {
	    	$langDomains = $this->cfg->get('langDomains');		
			$first = true;
			$i = 1; 
			while ($query->getrow()) {
                
                $domain = isset($langDomains[$query->field('lang')]) ? 'http://' . $langDomains[$query->field('lang')] : '';
                $thismirror = getMirror($this->contentData["id"], $this->getCountry(), $query->field('lang'));
				$checkid = $this->contentData["id"];
                if($thismirror == $this->contentData["id"]){
                    $checkid =  getDefaultPageId();
                }
				
				$langArray["langs"][] = array(
						"first"	 => $first,
						"last"	 => ($i == $cnt ? true : false),
						"title"	 => $query->field('title'),
						"lang"	 => $query->field('lang'),
						"active" =>	($this->getLang() == $query->field('lang') ? true : false),
						"link"   => $domain.getLink(getMirror($checkid, $this->getCountry(), $query->field('lang')))
						);

				if ($first) {
					$first = false;
				}
				
				$i++;

			}

		}
		
		return $langArray;
	}

	/**
	 * Display country menu
	 */
	public function getCountryMenu() {
		
		$ctrArr = array();
		
	    $dbQuery = "SELECT c.*, cd.domain 
							FROM 
								`ad_countries` c, `ad_countries_domains` cd  
							WHERE 
								cd.`country_id` = c.id	
								AND cd.`default` = '1'
							ORDER BY c.`id` ASC";
		$query = new query($this->db, $dbQuery);
		$cnt = $query->num_rows();
		
		if ($cnt > 0) {
	    			
			$first = true;
			$i = 1; 
			while ($query->getrow()) {
				
				$link = 'http://' . $query->field('domain');
				
				if ($this->getCountry() == $query->field('id')) {
					$ctrArr["active"]["first"] = $first;
					$ctrArr["active"]["last"] = ($i == $cnt ? true : false);
					$ctrArr["active"]["title"] = $query->field('title');
					$ctrArr["active"]["ctr"] = $query->field('id');
					$ctrArr["active"]["link"] = $link;
					$ctrArr["active"]["id"] = $query->field('id');
					$ctrArr["active"]["image"] = $query->field('image');
					$ctrArr["active"]["google_analytics"] = $query->field('google_analytics');
					$ctrArr["active"]["google_webmasters"] = $query->field('google_webmasters');

				}
				
				$ctrArr["ctr"][] = array(
											"first"	 => $first,
											"last"	 => ($i == $cnt ? true : false),
											"title"	 => $query->field('title'),
											"ctr"	 => $query->field('id'),
											"active" =>	($this->getCountry() == $query->field('id') ? true : false),
											"link"   => $link,
											"id"   => $query->field('id'),
											"google_analytics"   => $query->field('google_analytics'),
											"google_webmasters"   => $query->field('google_webmasters')
											);

				if ($first) {
					$first = false;
				}
				
				$i++;
			}

		}

		return $ctrArr;
	}
	
	/**
	 * Load all site mirrors
	 */
	public function loadMirrors() {
		$this->mirrors = &loadLibClass('mirrors');
		
		$this->mirrors->getMirrors();
	}
	
	/**
	 * Get full page path, tree
	 * 
	 * @param bool	create links or not, default false
	 * @param int	content id
	 */
	public function getPagePath ($noLinks = false, $id = "") {

		$outHtml = "";
		$parentIds = $id ? $this->getAllParentIds($id) : $this->pageParentIds;
		
		$pCount = count($parentIds);
		
		for ($i = $pCount; $i > 0; $i--) {
			
			if (($i - 1) == 0) {
				
				$outHtml[] = '<a href="' . AD_WEB_FOLDER . $this->contentData["url"] . '" title="' . $this->contentData["title"] . '">' . $this->contentData["title"] . '</a>';
			
			} else {
				
				$dbQuery = "SELECT `title`, `url` FROM " . $this->dbTable . " WHERE id = '" . $parentIds[$i - 1] . "' LIMIT 0,1";
				$query = new query($this->db, $dbQuery);
				$query->getrow();
				
				$idString = $query->field("url");
				
				if ($noLinks) {
					
					$outHtml[] = '' . $query->field("title") . '';
					
				} else {
					
					$outHtml[] = '<a href="' . AD_WEB_FOLDER . $query->field("url") . '" title="' . $query->field("title") . '">' . $query->field("title") . '</a>';
					
				}
			
			}
		}
		
		return implode(' / ', $outHtml);
	}
	
}

?>