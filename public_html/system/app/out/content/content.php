<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */
// ------------------------------------------------------------------------

/**
 * CMS content module
 * Default module in cms. Generate content, load templates and show it
 * Be careful, if wanna edit it.
 * 21.04.2008
 */
class content extends Module {

    /**
     * Class constructor
     */
    public function __construct() {

        parent :: __construct();
        $this->name = get_class($this);
        $this->getModuleId();


        require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
        $this->module = new contentData();

        $this->setCData($this->module->checkPageUrl($this->getUrlDir()));

        if ($this->cfg->get('mirrors')) {
            $this->module->loadMirrors();
        }

        $this->loadLabels('', true);

        $tpl = array();
        $tpl["tpl"]["layout"] = $this->getCData("layout") ? $this->getCData("layout") . '/' : '';
        $tpl["dir"] = AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/' . $tpl["tpl"]["layout"];
        $tpl["tpl"]["head"] = "head";
        $tpl["tpl"]["body"] = $this->getCData("template") ? $this->getCData("template") : '';
        $tpl["tpl"]["footer"] = "footer";
        $tpl["tpl"]["header"] = "header";
        $tpl["tpl"]["contact_right_info"] = "contact-right-info";
        $tpl["tpl"]["main"] = "main";

        $this->setPData($tpl, "tpl");
    }

    /**
     * Modules process function
     * This function runs auto from Module class
     */
    public function run() {

        // Creating all menus on page
        $this->module->displayPageMenus();

        $data = array();
        $data["lang"] = $this->getLang();
        $data["country"] = $this->getCountry();
        $data["pageTitle"] = $this->getCData("page_title") ? $this->getCData("page_title") : $this->getCData("title") ." - ". gL("defaultPageTitle");
        $data["pageDescription"] = $this->getCData("description") ? $this->getCData("description") : gL("defaultPageDescription");
        $data["pageKeywords"] = $this->getCData("keywords") ? $this->getCData("keywords") : gL("defaultPageKeywords");
        $data["title"] = $this->getCData("title") ? $this->getCData("title") : '';
        $data["full_title"] = $this->getCData("full_title") ? $this->getCData("full_title") : '';
        $data["html"] = $this->getCData("content") ? $this->getCData("content") : '';
        $data["image"] = $this->getCData("image") ? $this->getCData("image") : '';
        $data["image_alt"] = $this->getCData("image_alt") ? $this->getCData("image_alt") : '';
        $data["mirror_id"] = $this->getCData("mirror_id") ? $this->getCData("mirror_id") : '';
        $data["url"] = '/' . makeUrlWithLangInTheEnd($this->getUrlDir());
        $data["url_orig"] = $this->uri->orig_uri;
        $data["pagePath"] = $this->module->getPagePath();
        $data["jsArray"] = array('jquery');
        $data["cssArray"] = array($this->getCData("country") . '/css');
        
        $this->setPData(curPostUrl(), "curPostUrl");

        $this->setPData($data, "web");
        $this->setPData($this->getCData(), "content");
        
        $Module = &loadAppClass('orders', $this->app);
        $Module->module->getOrdersDataForPages();
        		
        $this->setPData(getBackLink(), "backLink");
        $this->setPData(curPageURL(), "curPageUrl");
        $this->setPData(getMirror(getDefaultPageId()), "mainpageId");
        $this->setPData($this->getCData("id"), "curpageId");
        if(getG("page")){
    		$p=(int)getG("page");
    		if($p<1){
    			$p=1;
    		}
    		$_GET['page']=$p;
    	}
        if(getG("page") == 1) {
            redirect(getLM($this->getCData("id")));
        }
		       
    }

}

?>