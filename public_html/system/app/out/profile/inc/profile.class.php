<?php

class profileData extends Module {
	
	public $config = array(
		'status' => array(
			'profile' => array(
				'0' => 'profile_AllOrders',
				'1' => 'profile_OrderStatus1',
				'2' => 'profile_OrderStatus2',
				'3' => 'profile_OrderStatus3',
				'4' => 'profile_OrderStatus4',
			), 
			'apply' => array(
				'0' => 'profile_AllOrders',
				'1' => 'profile_OrderStatus1',
				'2' => 'profile_OrderStatus2',
				'3' => 'profile_OrderStatus3',
				'4' => 'profile_OrderStatus4',
			),
		)						
	);
	
	private $data = array();
	private $return = false;
	private $userData = false;
	private $userId = false;
	public $formConfig = array();

	
	/**
	 * Class constructor
	 */
	public function __construct() {		
		
		parent :: __construct();
		$this->name = 'profile';
		
		if ($this->getCountry() == 8) {
			$this->dbTable = $this->cfg->getDbTable('writers');
			$this->formConfig = array(
				'form' => 'apply',
				'useSteps' => true,
				'template' => 'apply',
				'type' => 2,			
			);
		} else {
			$this->dbTable = $this->cfg->getDbTable('profiles');
			$this->formConfig = array(
				'form' => 'profile',
				'useSteps' => false,
				'template' => 'register',
				'type' => 1,
			);
		}
		
		
		$this->forms = loadLibClass('forms', true, $this);
		
		
		$this->loadConfig();
	}
	
	private function loadConfig() {
		$this->setPData(array('config' => $this->config), 'profile');
	}
	
	public function getForm() {
		$this->data = $this->forms->getForm($this->formConfig['form'], $this->formConfig['useSteps']);
		
		if (!$this->formConfig['useSteps']) {
			
			if (isset($this->data[$this->profile['step']])) {
				$this->data = $this->data[$this->profile['step']];
			} else {
				$this->data = array();
			}
			
		} 
		
		//pR($this->data);
		return $this;
	}
	
	public function loadFormData() {
		$this->profile['step'] = '0';
		
		return $this;

	}
	
	public function getData() {
		return $this->data;
	}
	
	public function getReturn() {
		return $this->return;
	}
	
	public function checkFieldsData() {
		return true;
	}
	
	private function getCountryCode($code) {
		$dbQuery = "SELECT `hint` FROM `mod_forms_values_data` fvd 
							LEFT JOIN `mod_forms_values` fv ON (fvd.value_id = fv.id)
						WHERE fv.form_id = '29' AND fvd.name = '" . mres($code) . "'
						LIMIT 1";
		$query = new query($this->db, $dbQuery);
		if ($query->num_rows() > 0) {
			return $query->getOne();
		}
		
		return false;
	}
	
	public function saveUser() {
		
		$this->data = $this->forms->getForm($this->formConfig['form'], true);
		
		$dbData = $this->forms->getDataArrayToDB($this->data, getP('fields'));
 		$dbData['created'] = time();
 		if ($this->formConfig['type'] == 1) {
 			$dbData['night_calls'] = 'No';
 		} else {
 			$dbData['available_24_7'] = 'No';
 		}
 		
 		$dbData['phone'] = '(' . $this->getCountryCode($dbData['country']) . ') ' . $dbData['phone'];
		
		
		$id  = saveValuesInDb($this->dbTable, $dbData);
		//pR($dbData);
		
		if ($id) {
			
			
			
			if ($this->formConfig['form'] == 'apply') {
				
				$this->sendRegistrationEmail($dbData['email'], getP('fields/mob_43'));
				
				$code = '';
				
				for ($i = 0; $i < 9; $i++) {
					$code .= mt_rand(0, 9);
				}
				
				saveValuesInDb($this->dbTable, array('verified' => $code), $id);
				$this->sendVerificationEmail($dbData['email'], $code);
			} else {
				$this->sendRegistrationEmail($dbData['email'], getP('fields/mob_27'));
			}
			
			if ($this->loginUser($dbData['email'], $dbData['password'], true)) {
				$_SESSION['useraction']['registration'] = true;
				return $id;
			} else {
				return false;
			}
		}
		
		
	}
	
	public function resendVerify() {
		$code = '';
		
		for ($i = 0; $i < 9; $i++) {
			$code .= mt_rand(0, 9);
		}
		
		saveValuesInDb($this->dbTable, array('verified' => $code), getS('user/id'));
		$this->sendVerificationEmail(getS('user/email'), $code);
	}
	
	public function loadTests() {
		
		$dbQuery = "SELECT t.*, td.*, tw.comment
							FROM `" . $this->cfg->getDbTable('tests', 'self') . "` t
								LEFT JOIN `" . $this->cfg->getDbTable('tests', 'data') . "` td ON (t.id = td.test_id)
								LEFT JOIN `" . $this->cfg->getDbTable('tests', 'writers') . "` tw ON (t.id = tw.test_id AND tw.writer_id = '" . getS('user/id') . "')		
							WHERE 1
								AND t.enable = '1'		
								AND td.lang = '" . $this->getLang() . "'
							ORDER BY t.type, t.id ASC";
		$query = new query($this->db, $dbQuery);
		
		
		$data = array();
		$data['tests'] = $query->getArray();
		$data['userTests'] = $this->getUserTests();
		
		$this->setPData($data, 'tests');
		
		$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output('tests', $this->getPData()));
		
	}
	
	public function inquiryForm() {
		
		$order = loadAppClass('orders', $this->app, true, true);
		$order->module->loadInquiryData()->getForm(" AND f.inquiry = '1' ");
		
		$this->setPData(array('data' => $order->module->data, 'order' => $order->module->order), 'order');
		
		$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output($this->getCountry() . '/inquiry', $this->getPData()));
	}
	
	private function getUserTests() {
		if (getS('user/id')) {
			$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('tests', 'writers') . "` t
							WHERE writer_id = '" . mres(getS('user/id')) . "'";
			$query = new query($this->db, $dbQuery);
			return $query->getArray('test_id');
		}
	}
	
	public function startTest() {
		if (getP('test') && getS('user/id')) {
			
			$dbData[] = " `writer_id` = '" . mres(getS('user/id')) . "' ";
			$dbData[] = " `test_id` = '" . mres(getP('test')) . "' ";
			$dbData[] = " `status` = '1' ";
			$dbData[] = " `created` = '" . time() . "' ";

			$dbQuery = "INSERT INTO `" . $this->cfg->getDbTable('tests', 'writers') . "` SET " . implode(',', $dbData) . 
									" ON DUPLICATE KEY UPDATE " . implode(',', $dbData);
			$query = new query($this->db, $dbQuery);
			
			return 'ok';
		} else {
			return 'error';
		}
	}
	
	public function verify() {
		if (getP('verified')) {
			
			$dbQuery = "SELECT `verified`
							FROM `" . $this->dbTable . "`
							WHERE 1
								AND `id` = '" . mres(getS('user/id')) . "'
							LIMIT 1";
			$query = new query($this->db, $dbQuery);
			if ($query->getOne() == getP('verified')) {
				
				saveValuesInDb($this->dbTable, array('verified' => ''), getS('user/id'));
				
				$this->loginUser(getS('user/email'), getS('user/password'), true);
				
				return array('ok' => true);
			} else {
				return array('error' => gL('writers_ErrorOnVerification', 'Code is not valid!'));
			}
			
		} else {
			return array('error' => gL('writers_ErrorOnVerification', 'Code is not valid!'));
		}
	}
	
	public function uploadTest() {
		if (getS('user/id')) {
				
			if (file_exists(AD_SERVER_UPLOAD_FOLDER . 'tests/results/' . getP('file'))) {
				
				$dbData[] = " `writer_id` = '" . mres(getS('user/id')) . "' ";
				$dbData[] = " `test_id` = '" . mres(getP('id')) . "' ";
				$dbData[] = " `status` = '2' ";
				$dbData[] = " `created` = '" . time() . "' ";
				$dbData[] = " `filename` = '" . mres(getP('file')) . "' ";
				
				$dbQuery = "INSERT INTO `" . $this->cfg->getDbTable('tests', 'writers') . "` SET " . implode(',', $dbData) .
												" ON DUPLICATE KEY UPDATE " . implode(',', $dbData);
				new query($this->db, $dbQuery);
				
				sendWriterTestsEmail(getS('user/id'));
		
			}
		
				
		}
	}
	
	public function removeTest() {
		if (getS('user/id')) {
		
			$dbData[] = " `writer_id` = '" . mres(getS('user/id')) . "' ";
			$dbData[] = " `test_id` = '" . mres(getP('id')) . "' ";
			$dbData[] = " `status` = '1' ";
			$dbData[] = " `created` = '" . time() . "' ";
			$dbData[] = " `filename` = '' ";
			
			$dbQuery = "INSERT INTO `" . $this->cfg->getDbTable('tests', 'writers') . "` SET " . implode(',', $dbData) .
											" ON DUPLICATE KEY UPDATE " . implode(',', $dbData);
			new query($this->db, $dbQuery);
		
		
		}
	}
	
	public function sendVerificationEmail($email, $code) {
		
		
	
		$body = str_replace(array('{{code}}'), array($code), $this->cfg->getData('mailBodyVerificationWS/' . $this->getLang()));
	
		sendMail($email, $this->cfg->getData('mailSubjVerification/' . $this->getLang()), $body, array(), $this->cfg->getData('mailFromVerification/' . $this->getLang()), true);
	
	}
	
	public function sendRegistrationEmail($email, $password) {
		
		$body = str_replace(array('{{email}}', '{{password}}'), array($email, $password), $this->cfg->getData('mailBodyWS/' . $this->getCountry() . '/' . $this->getLang()));
		
		sendMail($email, $this->cfg->getData('mailSubjWS/' . $this->getCountry() . '/' . $this->getLang()), $body, array(), $this->cfg->getData('mailFromWS/' . $this->getCountry() . '/' . $this->getLang()), true);

	}
	
	public function loginUser($email, $password, $dontCrypt = false) {
		if (trim($email) && trim($password)) {
			
			if ($this->formConfig['form'] == 'apply') {
				$dbQuery = "SELECT 
								w.*, 
								(SELECT count(ow.id) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE 1 AND ow.writer_id = w.id) AS total_applications,
								(SELECT SUM(ow.amount) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow LEFT JOIN `" . $this->cfg->getDbTable('orders', 'self') . "` owo ON (ow.order_id = owo.id AND owo.writer_id = ow.writer_id) WHERE 1 AND owo.status = 3 AND ow.status = 1 AND ow.writer_id = w.id) AS total_amount,
								(SELECT SUM(ow.amount) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow LEFT JOIN `" . $this->cfg->getDbTable('orders', 'self') . "` owo ON (ow.order_id = owo.id AND owo.writer_id = ow.writer_id) WHERE 1 AND owo.status = 3 AND ow.status = 1 AND ow.paid = 0 AND ow.writer_id = w.id) AS balance,		
								(SELECT SUM(amount) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE 1 AND ow.status = 1 AND ow.writer_id = w.id AND ow.paid = 1) AS total_paid_amount,
								(SELECT AVG(rate) FROM `" . $this->cfg->getDbTable('orders', 'writers_rates') . "` ow WHERE ow.writer_id = w.id) AS rating,
								(SELECT count(id) FROM `" . $this->cfg->getDbTable('orders', 'self') . "` ow WHERE ow.writer_id = w.id AND status = 3) AS completed_orders,
								(SELECT count(ow.id) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow LEFT JOIN `" . $this->cfg->getDbTable('orders', 'self') . "` owo ON (ow.order_id = owo.id) WHERE 1 AND owo.writer_id = ow.writer_id AND ow.writer_id = w.id) AS applications		
							FROM `" . $this->dbTable . "` w
							WHERE 1
								AND w.`enable` = '1'
								AND w.`email` = '" . mres($email) . "'
								AND w.`password` = '" . ($dontCrypt ? $password : md5($password)) . "'
							LIMIT 1";
			} else {
				$dbQuery = "SELECT *
							FROM `" . $this->dbTable . "`
							WHERE 1
								AND `enable` = '1'
								AND `email` = '" . mres($email) . "'
								AND `password` = '" . ($dontCrypt ? $password : md5($password)) . "'
							LIMIT 1";
			}
			
			
			$query = new query($this->db, $dbQuery);
			if ($query->num_rows() > 0) {
				
				$row = $query->getrow();
				
				if ($this->formConfig['form'] == 'apply') {
					$row['familiar_with'] = json_decode($row['familiar_with'], true);
					$row['disciplines'] = json_decode($row['disciplines'], true);
				}
				
				$this->userData = $_SESSION['user'] = $row;
				$this->userId = $this->userData['id']; 
				
				$this->setPData($this->userData, 'userData');
				//pR($this->getPData('userData'));
				return true;
			} else {
				return false;
			}
			
		}
	}
	
	public function getUserId() {
		return $this->userId;
	}
	
	public function getUserData($field = '') {
		if ($field) {
			if (isset($this->userData[$field])) {
				return $this->userData[$field];
			} else {
				return false;
			}
		} else {
			return $this->userData;
		}
	}
	
	public function isLogged() {
		if (getS('user')) {
			if ($this->loginUser(getS('user/email'), getS('user/password'), true)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public function loadOrdersList($id = false) {
		$orders = array();
		
		$where = '';
		if (getG('status') && array_key_exists(getG('status'), $this->config['status'][$this->formConfig['form']])) {
			$where .= " AND o.`status` = '" . mres(getG('status')) . "'";
		}
		if ($id) {
			$where .= " AND o.`id` = '" . mres($id) . "'";
		}
		
		if (getG('sort') == 'created') {
			$order = " ORDER BY o.created ASC";
		} else {
			$order = " ORDER BY o.first_draft_deadline ASC";
		}
		
		$dbQuery = "SELECT o.*, 
						(SELECT count(writer_id) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = o.id) AS writer_appointed,
						(SELECT count(id) FROM `" . $this->cfg->getDbTable('orders', 'support_rates') . "` osr WHERE osr.order_id = o.id AND osr.user_id = '" . mres(getS('user/id')) . "') AS rated		
							FROM `" . $this->cfg->getDbTable('orders', 'self') . "` o
							WHERE 1
								AND o.`user_id` = '" . mres($this->userId) . "'
								" . $where . $order . "";
		$query = new query($this->db, $dbQuery);
		while ($row = $query->getrow()) {
			
			//$row['first_draft_deadline'] += 86400 * $this->cfg->getData('orderDeadlineDaysPlus');
			$diff = $row['first_draft_deadline'] - time();

			if ($diff > 0) {
				
				$dtF = new DateTime("@0");
				$dtT = new DateTime("@$diff");
				
				$row['diffDays'] = $dtF->diff($dtT)->format('%a');
				$row['diffHours'] = $dtF->diff($dtT)->format('%h');
			} else {
				$row['diffDays'] = '0';
				$row['diffHours'] = '0';
			}
			
			
			$row['additional_extras'] = json_decode($row['additional_extras'], true);
			
			$row['messages'] = $this->getOrderMessages($row['id']);
			$row['files'] = $this->getOrderFiles($row['id']);
			
			$orders[] = $row;
		}
		
		if ($id) {
			
			if (isset($orders[0])) {
				
				$_SESSION['orderopen'] = $id;
				
				$this->order['3step'] = $this->forms->getForm('order', true, " AND f.show_in_3step = '1' ");
				$this->setPData(array('data' => $orders[0], 'order' => $this->order), 'profile');
					
				$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output($this->getCountry() . '/orders-open-' . $orders[0]['status'], $this->getPData()));
				
			} else {
				openDefaultPage();
			}
			
		} else {
			$this->setPData(array('data' => $orders), 'profile');
			
			$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output($this->getCountry() . '/orders-list', $this->getPData()));
		}
		
		
	}
	
	public function loadWritersOrdersList($id = false) {
		$orders = array();
	
		$where = '';
		if (getG('status') && array_key_exists(getG('status'), $this->config['status'][$this->formConfig['form']])) {
			
			if (getG('status') == 1) {
				$where .= " AND o.`status` = '2' AND o.`writer_id` = '' AND exists(SELECT null FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = o.id AND ow.writer_id = '" . getS('user/id') . "') ";
			} elseif (getG('status') == 2) {
				$where .= " AND o.`status` = '2' AND o.`writer_id` = '" . mres($this->userId) . "'";
			} elseif (getG('status') == 3) {
				$where .= " AND o.`status` = '3' AND o.`writer_id` = '" . mres($this->userId) . "'";
			} elseif (getG('status') == 4) {
				$where .= " AND o.`status` = '4' AND o.`writer_id` = '" . mres($this->userId) . "'";
			}
			
		} elseif ($id) {
			$where .= " AND `id` = '" . mres($id) . "'";
		} else {
			$where .= " AND o.`status` = '2' AND o.`writer_id` = '' 
							AND NOT exists(SELECT null FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = o.id AND ow.writer_id = '" . getS('user/id') . "') ";
		}
		
		if (getG('sort') == 'created') {
			$order = " ORDER BY o.created ASC";
		} else {
			$order = " ORDER BY o.first_draft_deadline ASC";
		}
	
		$dbQuery = "SELECT o.*, 
						(SELECT writer_id FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = o.id AND ow.writer_id = '" . getS('user/id') . "') AS apply,
						(SELECT count(id) FROM `" . $this->cfg->getDbTable('orders', 'support_rates') . "` orr WHERE orr.order_id = o.id AND orr.user_id = '" . mres(getS('user/id')) . "') AS rated		
							FROM `" . $this->cfg->getDbTable('orders', 'self') . "` o
							WHERE 1
								" . $where . $order . "";
		$query = new query($this->db, $dbQuery);
		while ($row = $query->getrow()) {
			
			$dbQuery = "SELECT * FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = '" . $row['id'] . "'";
			$queryWriters = new query($this->db, $dbQuery);
			if ($queryWriters->num_rows() > 0) {
				$row['writers'] = $queryWriters->getArray('writer_id');
			} else {
				$row['writers'] = array();
			}
			
			if (isset($row['writers'][getS('user/id')])) {
				$row['writers_apply'] = $row['writers'][getS('user/id')];
			} else {
				$row['writers_apply'] = false;
			}
			
			$row['price'] = number_format($row['price'] - $row['price'] * $this->cfg->getData('orderPriceReduced') / 100, 2, '.', '');

			
			$row['first_draft_deadline'] = ($row['first_draft_deadline'] - $row['created']) * 0.8 + $row['created'];
			$diff = $row['first_draft_deadline'] - time();

			if ($diff > 0) {
				
				$dtF = new DateTime("@0");
				$dtT = new DateTime("@$diff");
				
				$row['diffDays'] = $dtF->diff($dtT)->format('%a');
				$row['diffHours'] = $dtF->diff($dtT)->format('%h');
			} else {
				$row['diffDays'] = '0';
				$row['diffHours'] = '0';
			}
				
			$row['additional_extras'] = json_decode($row['additional_extras'], true);
				
			$row['messages'] = $this->getOrderMessages($row['id']);
			$row['files'] = $this->getOrderFiles($row['id']);
				
			$orders[] = $row;
		}
	
		if ($id) {
				
			if (isset($orders[0])) {
	
				$_SESSION['orderopen'] = $id;
	
				$this->order['3step'] = $this->forms->getForm('order', true, " AND f.show_in_3step = '1' ");
				
				$this->setPData(array('data' => $orders[0], 'order' => $this->order), 'profile');
				
				$template = 1;
				if ($orders[0]['status'] == 2 && $orders[0]['writer_id'] == '0' && !$orders[0]['apply']) {
					$template = 1;
				} elseif ($orders[0]['status'] == 2 && $orders[0]['writer_id'] == '0' && $orders[0]['apply']) {
					$template = 2;
				} elseif ($orders[0]['status'] == 2 && $orders[0]['writer_id'] == getS('user/id')) {
					$template = 3;
				} elseif ($orders[0]['status'] == 3 && $orders[0]['writer_id'] == getS('user/id') && $orders[0]['writers_apply'] && $orders[0]['writers_apply']['paid'] == 0) {
					$template = 4;
				} elseif ($orders[0]['status'] == 3 && $orders[0]['writer_id'] == getS('user/id') && $orders[0]['writers_apply'] && $orders[0]['writers_apply']['paid'] == 1) {
					$template = 5;
				} elseif ($orders[0]['status'] == 4 && $orders[0]['writer_id'] == getS('user/id')) {
					$template = 6;
				}
				
				$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output($this->getCountry() . '/orders-open-' . $template, $this->getPData()));
	
			} else {
				openDefaultPage();
			}
				
		} else {
			$this->setPData(array('data' => $orders), 'profile');
				
			$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output($this->getCountry() . '/orders-list', $this->getPData()));
		}
	
	
	}
	
	public function orderApply() {
		if (getS('user/id')) {
				
			$dbData['order_id'] = getS('orderopen');
			$dbData['writer_id'] = getS('user/id');
			$dbData['reason'] = getP('reason');
			$dbData['amount'] = preg_replace( '/[^.,0-9]/', '', getP('amount'));
			$dbData['created'] = time();
			$dbData['status'] = 1;
			
			sendOrderApplyEmail(getS('orderopen'), getP('reason'), getP('amount'));
	
			return saveValuesInDb($this->cfg->getDbTable('orders', 'writers'), $dbData);
		}
	}
	
	public function orderNewMessage() {
		if (getS('user/id')) {
			
			$dbData['order_id'] = getS('orderopen');
			$dbData['user_id'] = getS('user/id');
			$dbData['message'] = getP('message');
			$dbData['created'] = time();
			$dbData['type'] = $this->formConfig['type'];
			
			sendOrderMessageEmail(getS('orderopen'), getP('message'), $this->getCountry());
				
			return saveValuesInDb($this->cfg->getDbTable('orders', 'messages'), $dbData);
		}
	}
	
	public function previewComment() {
		$dbData['order_id'] = getS('orderopen');
		$dbData['user_id'] = getS('user/id');
		$dbData['message'] = getP('preview_comment');
		$dbData['created'] = time();
		$dbData['type'] = $this->formConfig['type'];
		
		return saveValuesInDb($this->cfg->getDbTable('orders', 'messages'), $dbData);
	}
	
	private function getOrderWriterId($id) {
		$dbQuery = "SELECT `writer_id`
							FROM `" . $this->cfg->getDbTable('orders', 'self') . "`
							WHERE 1
								AND `id` = '" . mres($id) . "'";
		$query = new query($this->db, $dbQuery);
		return $query->getOne();
	}
	
	public function rateOrder() {
		if (getS('user/id')) {
			

			
			if (getP('writers')) {
				$dbData['order_id'] = getS('orderopen');
				$dbData['user_id'] = getS('user/id');
				$dbData['rate'] = getP('writers');
				$dbData['writer_id'] = $this->getOrderWriterId(getS('orderopen'));
				$dbData['created'] = time();
				
				saveValuesInDb($this->cfg->getDbTable('orders', 'writers_rates'), $dbData);
			}
			
			if (getP('support')) {
				$dbData['order_id'] = getS('orderopen');
				$dbData['user_id'] = getS('user/id');
				$dbData['rate'] = getP('support');
				$dbData['created'] = time();
				$dbData['type'] = $this->formConfig['type'];
			
				saveValuesInDb($this->cfg->getDbTable('orders', 'support_rates'), $dbData);
			}

		}
	}
	
	public function getOrderRatesForm() {
		if (getP('orderID')) {
			$_SESSION['orderopen'] = getP('orderID');
		}
		return $this->tpl->output($this->getCountry() . '/orders-rate', $this->getPData());
	}
	
	public function previewOrder() {
		
		$this->tpl->assign("filename", getP('filename'));
		return $this->tpl->output($this->getCountry() . '/orders-preview', $this->getPData());
	}
	
	public function acceptOrder() {
		if (getS('orderopen')) {
			$dbData['status'] = 3;
			sendUserAprooveEmail(getS('orderopen'));
			return saveValuesInDb($this->cfg->getDbTable('orders', 'self'), $dbData, getS('orderopen'));
		}
	}
	
	public function uploadFile() {
		if (getS('user/id')) {
			
			if (file_exists(AD_SERVER_UPLOAD_FOLDER . 'orders/temp/' . getP('file'))) {
				
				if(!is_dir(AD_SERVER_UPLOAD_FOLDER . 'orders/' . md5(getS('orderopen')) . '/')){
					mkdir(AD_SERVER_UPLOAD_FOLDER . 'orders/' . md5(getS('orderopen')) . '/');
					chmod(AD_SERVER_UPLOAD_FOLDER . 'orders/' . md5(getS('orderopen'))  . '/', 0777);
				}
				
				copy(AD_SERVER_UPLOAD_FOLDER . 'orders/temp/' . getP('file'), 
						AD_SERVER_UPLOAD_FOLDER . 'orders/' . md5(getS('orderopen'))  . '/' . getP('file'));
				unlink(AD_SERVER_UPLOAD_FOLDER . 'orders/temp/' . getP('file'));
				
				
				$dbData['order_id'] = getS('orderopen');
				$dbData['user_id'] = getS('user/id');
				$dbData['filename'] = getP('file');
				$dbData['created'] = time();
				$dbData['type'] = $this->formConfig['type'];
				$dbData['enabled'] = '1';
				
				sendUploadedFileEmail($dbData['order_id'], $this->getCountry());
				
				return saveValuesInDb($this->cfg->getDbTable('orders', 'files'), $dbData);
			}
				
			
		}
	}
	
	public function getOrderMessages($id) {
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('orders', 'messages') . "`
							WHERE 1
								AND `order_id` = '" . mres($id) . "'
								AND `type` = '" . $this->formConfig['type'] . "'";
		$query = new query($this->db, $dbQuery);
		return $query->getArray('id');
	}
	
	public function getOrderFiles($id) {
		
		$where = "";
		if ($this->formConfig['type'] == 1) {
			$where = " AND (`user_id` = '" . getS('user/id') . "' || `user_id` = '0')"; 
		}
		
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('orders', 'files') . "`
							WHERE 1
								AND `order_id` = '" . mres($id) . "'
								AND `enabled` = '1'
								AND `type` = '" . $this->formConfig['type'] . "'";		
		$query = new query($this->db, $dbQuery);
		return $query->getArray('id');
	}
	
	public function logout() {
		if (isset($_SESSION['user'])) {
			unset($_SESSION['user']);
		}	
		
		openDefaultPage();
	}
	
	public function loginForm() {
		
		if (getP('action') == 'login') {
				
			if ($this->loginUser(getP('fields/email'), getP('fields/password'))) {
		
				$this->return['location'] = getLM($this->cfg->getData('mirros_default_profile_page'));
				
			} else {
				
				$this->return['errors']['fields']['email'] = gL('order_Errors_Login_Failed');
				$this->return['errors']['fields']['password'] = gL('order_Errors_Login_Failed');
				return false;
			}
				
		} else {
			$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output('login', $this->getPData()));
		}
		
		
	}
	
	public function registrationForm() {
		if (getP('action') == 'register') {
			
			$this->data = $this->forms->getForm($this->formConfig['form'], true);
			
			$result = $this->forms->checkFields($this->data, getP('fields'));
			if (!$result) {
				
				if (getP('fields/agreement') == '1') {
					if ($userId = $this->saveUser()) {
						
						if ($this->formConfig['form'] == 'apply') {
							$this->return['location'] = getLM($this->cfg->getData('mirros_verification_page')) . '?registration-submited';
						} else {
							$this->return['location'] = getLM($this->cfg->getData('mirros_default_profile_page')) . '?registration-submited';
						}
						
						
					} else {
						$this->return['errors']['global'] = gL('order_Errors_Global_RegisterUser');
						return false;
					}
				} else {
					$this->return['errors']['fields']['agreement'] = gL('order_Errors_Fields_Agreement');
					return false;
				}
				
				
			} else {
				$this->return['errors']['fields'] = $result;
				return false;
			}
		
		} else {
			
			$this->loadFormData()->getForm();
			$this->setPData(array('data' => $this->getData()), 'profile');
			
			$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output($this->formConfig['template'], $this->getPData()));
		}
	}
	
	public function setNewPassword() {
		if (md5(getP('current_password')) != getS('user/password')) {
			$this->return['errors']['fields']['current_password'] = gL('profile_Errors_Fields_CurrentPassword');
			return false;
		}
		
		$errors = loadLibClass('form.errors');
		
		if (!$errors->error_isPassword(getP('password'), 'password')) {
			$this->return['errors']['fields']['password'] = gL('profile_Errors_Fields_Password');
			return false;
		}
		
		if (getP('password') != getP('confirm_password')) {
			$this->return['errors']['fields']['confirm_password'] = gL('profile_Errors_Fields_ConfirmPassword');
			return false;
		}
		
		$dbData['password'] = $_SESSION['user']['password'] = md5(getP('password'));
			
		return saveValuesInDb($this->dbTable, $dbData, getS('user/id'));
	}
	
	public function edit() {
		
		
		$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output($this->getCountry() . '/edit', $this->getPData()));
	}
	
	public function verification() {
		$this->tpl->assign("PROFILE_CONTENT", $this->tpl->output('verification', $this->getPData()));
	}
	
	public function editAdditionalInfo() {
		
		$this->data = $this->forms->getForm('profile_edit', true, '', getS('user'));
		
		$this->setPData(array('data' => $this->data), 'profile');
		
		return $this->tpl->output('edit-extra', $this->getPData());
	}
	
	public function getChangePasswordForm() {
		return $this->tpl->output('change-password', $this->getPData());
	}
	
	public function saveExtraInfo() {
		$dbData['night_calls'] = getP('night_calls');
		$dbData['alternative_email'] = getP('alternative_email');
		$dbData['alternative_phone'] = getP('alternative_phone');
		$dbData['preferred_language'] = getP('preferred_language');
			
		return saveValuesInDb($this->dbTable , $dbData, getS('user/id'));
	}
	
	public function uploadFiles() {
		
		if (getG('folder')) {
			$data = array();
			
			$config = array('upload_folder' => getG('folder'));
			
			$data = jsonDecode(getG('config'));
			if (is_array($data) && count($data) > 0) {
				foreach ($data AS $key => $value) {
					$config[$key] = $value;
				}
			}
			
			$this->upload = &loadLibClass('upload', true, $config);
			
			if ($this->upload->do_upload()) {
				$data['info'] = $this->upload->data();
				return $data;
			} else {
				$data['error'] = true;
				$data['errorMsg'] = $this->upload->display_errors('','');
				return $data;
			}
		}
		
	
	}
	
	public function passwordReminder($email) {
		if ($email) {
			
			$dbQuery = "SELECT 1
							FROM `" . $this->dbTable . "`
							WHERE 1
								AND `enable` = '1'
								AND `email` = '" . mres($email) . "'
							LIMIT 1";
			$query = new query($this->db, $dbQuery);
			if ($query->num_rows() > 0) {
				$password = $this->generatePassword();
					
				$dbData[] = " `password` = '" . md5($password) . "' ";
				$dbData[] = " `email` = '" . mres($email) . "' ";
					
				$dbQuery = "INSERT INTO `" . $this->dbTable . "` SET " . implode(',', $dbData) .
								" ON DUPLICATE KEY UPDATE " . implode(',', $dbData);
				new query($this->db, $dbQuery);
					
					
				$body = str_replace(array('{{password}}'), array($password), $this->cfg->getData('mailBodyPassword/' . $this->getCountry() . '/' . $this->getLang()));
					
				return sendMail($email, $this->cfg->getData('mailSubjPassword/' . $this->getCountry() . '/' . $this->getLang()), $body, array(), $this->cfg->getData('mailFromPassword/' . $this->getCountry() . '/' . $this->getLang()), true);
			} else {
				return false;
			}
			
			
		}
	}
	
	public function generatePassword($length = 12) {
	    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $count = mb_strlen($chars);
	
	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }
	
	    return $result;
	}
	
}

?>