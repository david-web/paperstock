<?php


class profile extends Module {
	
	/**
	 * Class constructor
	 */
	public function __construct($setTmplFolder = true)	{		

		parent :: __construct();
		$this->name = get_class($this);
		$this->getModuleId(true);

		require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
		$this->module = new profileData();
		
		if ($setTmplFolder) {
			$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		}
		
	}
	
	/**
	 * Modules process function
	 * This function runs auto from Module class
	 */
	public function run() {
		
		$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		
		if ($this->getCData("id") == getMirror($this->cfg->getData('mirros_tests_page'))) {
			if ($this->module->isLogged()) {
				
				if (getS('user/verified') == '') {
					$this->module->loadTests();
				} else {
					redirect(getLM($this->cfg->getData('mirros_verification_page')));
				}
				
				
			} else {
				redirect(getLM($this->cfg->getData('mirros_signin_page')));
			}
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_profile_inquiry_page'))) {
			if ($this->module->isLogged()) {
				
				$this->module->inquiryForm();
				
			} else {
				redirect(getLM($this->cfg->getData('mirros_signin_page')));
			}
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_verification_page'))) {
			if ($this->module->isLogged()) {
				
				$this->module->verification();
				
			} else {
				redirect(getLM($this->cfg->getData('mirros_signin_page')));
			}
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_profile_edit_page'))) {
			if ($this->module->isLogged()) {
				
				if ($this->module->formConfig['form'] == 'apply') {
					if (getS('user/verified') != '') {
						redirect(getLM($this->cfg->getData('mirros_verification_page')));
					} elseif (getS('user/status') != 2) {
						redirect(getLM($this->cfg->getData('mirros_tests_page')));
					}
				}
				
				$this->module->edit();
				
			} else {
				redirect(getLM($this->cfg->getData('mirros_signin_page')));
			}
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_profile_orders_page'))) {
			
			if ($this->module->isLogged()) {
				
				if ($this->module->formConfig['form'] == 'apply') {
					if (getS('user/verified') != '') {
						redirect(getLM($this->cfg->getData('mirros_verification_page')));
					} elseif (getS('user/status') != 2) {
						redirect(getLM($this->cfg->getData('mirros_tests_page')));
					}
					
					$this->module->loadWritersOrdersList(getG('id'));
					
				} else {
					
					if (getG('m') == 1) {
						unset($_SESSION['order']);
						unset($_SESSION['payment']);
						
						redirect(getLM($this->cfg->getData('mirros_profile_orders_page'))  . '?order-submited');
					}
					
					if (getG('m') == 2) {
						unset($_SESSION['order']);
						unset($_SESSION['payment']);
					
						$this->tpl->assign("SHOW_PAYMENT_ERROR", true); 
					}
					
					if (getS('useraction/inquiry')) {
						unset($_SESSION['useraction']['inquiry']);
						$this->tpl->assign("SHOW_INQUIRY_MESSAGE", true);
					}
					
					if (getS('useraction/registration')) {
						unset($_SESSION['useraction']['registration']);
						$this->tpl->assign("SHOW_REGISTRATION_MESSAGE", true);
					}
					
					if (getS('useraction/order')) {
						unset($_SESSION['useraction']['order']);
						$this->tpl->assign("SHOW_ORDER_MESSAGE", true);
					}
					
					if (getG('action') == 'pay' && getG('id')) {

						$_SESSION['order']['ID'] = getG('id');
						redirect(getLM($this->cfg->getData('mirros_order_payment_page')));
						
					} else {
						$this->module->loadOrdersList(getG('id'));
					}
					
				}
				
				
				
			} else {
				redirect(getLM($this->cfg->getData('mirros_signin_page')));
			}
			
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_signin_page'))) {
			
			if ($this->module->isLogged()) {
				redirect(getLM($this->cfg->getData('mirros_default_profile_page')));
			} else {
				$this->module->loginForm();
			}
			
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_signup_page'))) {
			if ($this->module->isLogged()) {
				redirect(getLM($this->cfg->getData('mirros_default_profile_page')));
			} else {
				$this->module->registrationForm();
			}
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_profile_logout_page'))) {
			$this->module->logout();
		}
			
	}
	
	public function action_upload() {
		$this->noLayout(true);
		$data = $this->module->uploadFiles();
		jsonSend($data);
	}
	
	public function action_orderNewMessage() {
		if (getP('message') && getS('orderopen')) {
			
			jsonSend($this->module->orderNewMessage());
		}
	}
	
	public function action_uploadFile() {
		if (getP('file') && getS('orderopen')) {
				
			jsonSend($this->module->uploadFile());
		}
	}
	
	public function action_rateOrder() {
		if (getP('support')) {
		
			jsonSend($this->module->rateOrder());
		}
	}
	
	public function action_login() {
	
		$this->module->loginForm();	
	
		jsonSend($this->module->getReturn());
	}
	
	public function action_register() {
	
		$this->module->registrationForm();
	
		jsonSend($this->module->getReturn());
	}
	
	public function action_changePassword() {
		$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		jsonSend($this->module->getChangePasswordForm());
	}
	
	public function action_getOrderRatesForm() {
		$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		jsonSend($this->module->getOrderRatesForm());
	}
	
	public function action_editAdditionalInfo() {
		$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		jsonSend($this->module->editAdditionalInfo());
	}
	
	public function action_setNewPassword() {
		$this->module->setNewPassword();
		
		jsonSend($this->module->getReturn());
	}
	
	public function action_saveExtraInfo() {
		$this->module->saveExtraInfo();
		
		jsonSend($this->module->getReturn());
	}
	
	public function action_resendVerify() {
		jsonSend($this->module->resendVerify());
	}
	
	public function action_verify() {
		jsonSend($this->module->verify());
	}
	
	public function action_startTest() {
		jsonSend($this->module->startTest());
	}
	
	public function action_uploadTest() {
		if (getP('file') && getP('id')) {
	
			jsonSend($this->module->uploadTest());
		}
	}
	
	public function action_removeTest() {
		if (getP('id')) {
	
			jsonSend($this->module->removeTest());
		}
	}
	
	public function action_orderApply() {
		if (getP('amount') && getP('reason')) {
		
			jsonSend($this->module->orderApply());
		}
	}
	
	public function action_passwordReminder() {
		if (getP('email')) {
		
			jsonSend($this->module->passwordReminder(getP('email')));
		}
	}
	
	public function action_saveInquiry() {
		
		if (getS('user/id')) {
			$order = loadAppClass('orders', $this->app, true, false);
			
			if ($order->module->checkInquiryFieldsData()) {
				$order->module->sendInquiryEmail(getS('user/email'));
			}
			
			jsonSend($order->module->getReturn());
		}
			
	}
	
	public function action_previewOrder() {
		if (getS('user/id')) {
			$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
			jsonSend($this->module->previewOrder());
		}
	}
	
	public function action_acceptOrder() {
		if (getS('user/id')) {
			jsonSend($this->module->acceptOrder());
		}
	}
	
	public function action_previewComment() {
		if (getS('user/id')) {
			jsonSend($this->module->previewComment());
		}
	}
	
}
?>