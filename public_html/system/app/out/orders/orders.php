<?php


class orders extends Module {
	
	/**
	 * Class constructor
	 */
	public function __construct($notSetTmplFolder = true)	{		
		
		parent :: __construct();
		$this->name = get_class($this);
		$this->getModuleId(true);
		

		require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
		$this->module = new ordersData();
		
		if (!$notSetTmplFolder) {
			$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		}
	}
	
	/**
	 * Modules process function
	 * This function runs auto from Module class
	 */
	public function run() {
		//pR($_SESSION['order']);
		//unset($_SESSION['order']);
		if ($this->getCData("id") == getMirror($this->cfg->getData('mirros_order_page'))) {
			$_SESSION['order']['step'] = 1;
			
			if (isset($_SESSION['order'], $_SESSION['order']['SAVEID'])) {
				unset($_SESSION['order']['SAVEID']);
			}
			
			if (!isset($_SESSION['order']['step']) || $_SESSION['order']['step'] == 1 || $_SESSION['order']['step'] == 2) {
				$this->module->loadFormData()
											->getForm()
											->assignData()
											->loadHeader();
			} elseif ($_SESSION['order']['step'] == 3) {
				$this->module->loadFormData()
											->getLoginForm()
											->assignData(false, '3step')
											->loadHeader();
			} elseif ($_SESSION['order']['step'] == 4) {
				$this->module->loadFormData()
											->getForm()
											->assignData(false, '4step')
											->loadHeader();
			}
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_order_payment_page'))) {
			if (getS('order/ID')) {
				
				if (isset($_SESSION['order'], $_SESSION['order']['SAVEID'])) {
					unset($_SESSION['order']['SAVEID']);
				}
				
				$this->module->loadPayment(getS('order/ID'));
			
			} else {
				openDefaultPage();
			}
		} elseif ($this->getCData("id") == getMirror($this->cfg->getData('mirros_inquiry_page'))) {
			
			if (getS('user/id')) {
				redirect(getLM($this->cfg->getData('mirros_profile_inquiry_page')));
			}
			
			$this->module->loadInquiryData()
										->getForm(" AND f.inquiry = '1' ")
										->getLoginForm(true)
										->assignData(false, 'inquiry', false)
										->loadHeader(false, 'inquiry-header');
		}
			
	}
	
	public function action_saveInquiry() {
		
		if ($this->module->checkInquiryFieldsData()) {
			$this->module->inquiryUserActions();
		}
		
		jsonSend($this->module->getReturn());
	}
	
	public function action_changeStep() {
		
		if ($_SESSION['order']['step'] == 1 || $_SESSION['order']['step'] == 2) {
			$this->module->loadFormData()
								->getForm()
								->assignData(true)
								->loadHeader(true);
		} elseif ($_SESSION['order']['step'] == 3) {
			$this->module->loadFormData()
								->getLoginForm()
								->assignData(true, '3step')
								->loadHeader(true);
		} elseif ($_SESSION['order']['step'] == 4) {
			
			$this->module->loadFormData()
								->getForm()
								->assignData(true, '4step')
								->loadHeader(true);
		}
		
		
		jsonSend($this->module->getReturn());
	}
	
	public function action_checkData() {
		
		if ($this->module->checkStep()) {
			
			if ((int)getP('step') < (int)$_SESSION['order']['step']) {
				$this->module->saveFieldsData();
				jsonSend($this->module->getReturn());
			} else {
				if ($this->module->checkFieldsData()) {
		
					if (getS('order/step') == 3 || (getS('order/step') == 2 && getS('user/id'))) {
							
						if ($this->module->saveOrder()) {
							$this->module->saveFieldsData();
						}
							
					} else {
						$this->module->saveFieldsData();
					}
				
					
				}
			}		
		}

	
		jsonSend($this->module->getReturn());
	}
	
	public function action_calculatePrice() {
		
		$this->module->saveFieldsData();
		jsonSend($this->module->calculatePrice());;
	}
	
	public function action_changeDeadlineNotice() {
		if (getP('deadline')) {
			jsonSend(array('deadline1' => date("j M h A", time() + getP('deadline')), 'deadline2' => date("j M h A", time() + getP('deadline') + (86400 * $this->cfg->getData('orderDeadlineDaysPlus')))));
		}
	}
	
}
?>