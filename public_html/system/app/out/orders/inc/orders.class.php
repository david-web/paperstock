<?php

class ordersData extends Module {
	
	public $config = array(
		'steps' => array(
			'1' => 'order_Step1Title',
			'2' => 'order_Step2Title',
			'3' => 'order_Step3Title',
			'4' => 'order_Step4Title',
		),
		'tabs' => array(
			'3' => array('login' => 'order_Step3TabLoginTitle', 'registration' => 'order_Step3TabRegistrationTitle'),
		),
	);
	public $data = array();
	public $order = array();
	private $return = false;
	private $userId = false;

	
	/**
	 * Class constructor
	 */
	public function __construct() {		
		
		parent :: __construct();
		$this->name = 'orders';
		$this->dbTable = $this->cfg->getDbTable('orders', 'self');
		
		$this->forms = loadLibClass('forms', true, $this);
		
		$dbQuery = "SELECT sd.value FROM `ad_sitedata` s
										LEFT JOIN  `ad_sitedata_values` sd ON (sd.fid = s.id)
									WHERE `name` = 'ordersPrices'";
		$query = new query($this->db, $dbQuery);
		
		$this->config['prices'] = json_decode($query->getOne(), true);

		//pR($this->config['prices']);
		
		$this->loadConfig();
	}
	
	private function loadConfig() {
		$this->setPData(array('config' => $this->config), 'order');
	}
	
	public function loadHeader($collect = false, $template = 'header') {
		
		if ($collect) {
			$this->return['header'] = $this->tpl->output("header", $this->getPData());
		} else {
			$this->tpl->assign("TEMPLATE_ORDERS_HEAD_MODULE", $this->tpl->output($template, $this->getPData()));
		}
		
		
		return $this;
	}
	
	public function getForm($where = '') {
		//unset($_SESSION['order']['fields']);
		
		if ($where) {
			$this->data = $this->forms->getForm('order', true, $where);
		} else {
			$this->data = $this->forms->getForm('order');
		}
		
		if (isset($this->data[$this->order['step']])) {
			$this->data = $this->data[$this->order['step']];
		} else {
			//$this->data = array();
		}
		//pR($this->data);
		return $this;
	}
	
	public function loadFormData() {
		if (isset($_SESSION['order'], $_SESSION['order']['step'])) {
			if (array_key_exists($_SESSION['order']['step'], $this->config['steps'])) {
				$this->order['step'] = $_SESSION['order']['step'];
			} else {
				$this->order['step'] = $_SESSION['order']['step'] = 1;
			}		
			
		} else {
			
			$this->order['step'] = $_SESSION['order']['step'] = 1;
		}
		
		$this->order['nextStep'] = $this->order['step'] + 1;
		$this->order['prevStep'] = $this->order['step'] - 1;
		
		if (isset($_SESSION['order'], $_SESSION['order']['fields'])) {
			$this->order['fields'] = $_SESSION['order']['fields'];
		}
		
		return $this;

	}
	
	public function loadInquiryData() {
		$this->order['step'] = 1;
	
		return $this;
	
	}
	
	public function assignData($collect = false, $template = '1-2step', $ajax = true) {
		
		$this->order['3step'] = $this->forms->getForm('order', true, " AND f.show_in_3step = '1' ");
		$this->order['deadlinedays'] = 86400 * $this->cfg->getData('orderDeadlineDaysPlus');
		//pR($this->order['3step']);
		$this->setPData(array('data' => $this->data, 'order' => $this->order), 'order');
		
		
		if ($collect) {
			
			$this->return['body'] = $this->tpl->output($template . '-ajax', $this->getPData());
			
			if ($result = $this->loadAllLinks()) {
				$this->return['body'] = str_replace($result['ids'], $result['links'], $this->return['body']);
			}
			
			
		} else {
			if ($ajax) {
				$this->tpl->assign("ORDERS_MODULE_STEP", $this->tpl->output($template . '-ajax', $this->getPData()));
			}
			
			$this->tpl->assign("TEMPLATE_ORDERS_MODULE", $this->tpl->output($template, $this->getPData()));
		}
		
		return $this;
	}
	
	public function getReturn() {
		return $this->return;
	}
	
	public function checkStep() {
		if (!isset($_SESSION['order']['step'])) {
			$this->return['errors']['global'] = gL('order_Errors_Global_StepJumping');
			return false;
		}
		
		if ((int)getP('step') - (int)$_SESSION['order']['step'] > 1) {
			$this->return['errors']['global'] = gL('order_Errors_Global_StepJumping');
			return false;
		}
		
		return true;
	}
	
	public function checkInquiryFieldsData() {
		$this->data = $this->forms->getForm('order', true, " AND f.inquiry = '1' ");
			
		$result = $this->forms->checkFields($this->data, getP('fieldsInquiry'));
		
		if ($result) {
			$this->return['errors']['fields'] = $result;
			return false;
		}  else {
			return true;
		}
	}
	
	public function inquiryUserActions() {
		if (getP('action') == 'register') {
			$this->data = $this->forms->getForm('profile', true);
			$result = $this->forms->checkFields($this->data, getP('fields'));
			if (!$result) {
				//return true;
			} else {
				$this->return['errors']['fields'] = $result;
				return false;
			}
		}
		
		$profile = loadAppClass('profile', $this->app, true, false);
			
		if (getP('action') == 'login') {
		
			if ($profile->module->loginUser(getP('fields/email'), getP('fields/password'))) {
				$this->setUserId($profile->module->getUserId());
		
				$this->sendInquiryEmail($profile->module->getUserData('email'));
				return true;
		
			} else {
				$this->return['errors']['global'] = gL('order_Errors_Login_Failed');
				return false;
			}
		
		} elseif (getP('action') == 'register') {
		
			if (getP('fields/agreement') == '1') {
				if ($userId = $profile->module->saveUser()) {
		
					$this->setUserId($userId);
		
					$this->sendInquiryEmail($profile->module->getUserData('email'));
					
					return true;
						
				} else {
					$this->return['errors']['global'] = gL('order_Errors_Global_RegisterUser');
					return false;
				}
			} else {
				$this->return['errors']['fields']['agreement'] = gL('order_Errors_Fields_Agreement');
				return false;
			}
		
		} else {
			return false;
		}
	}
	
	public function sendInquiryEmail($email) {
		
		$this->return['location'] = getLM($this->cfg->getData('mirros_default_profile_page')) . '?inquiry-submited';
		
		$this->data = $this->forms->getForm('order', true, " AND f.inquiry = '1' ");
		
		$dbData = $this->forms->getDataArrayToEmail($this->data, getP('fieldsInquiry'));
		$dbData[] = array(
			'name' => 'Number of pages',
			'value' => getP('fieldsInquiry/number_of_pages'),
		);
		$dbData[] = array(
			'name' => 'UserID',
			'value' => getS('user/id'),
		);
		
		$body = '';
		foreach ($dbData AS $info) {
			$body .= $info['name'] . ': ' . $info['value'] . '<br />';
		}
		
		$_SESSION['useraction']['inquiry'] = true;
		
		sendMail($this->cfg->getData('mailToInquiry/' . $this->getLang()), $this->cfg->getData('mailSubjInquiry/' . $this->getLang()), $body, array(), $email, true);
	}
	
	public function checkFieldsData() {
		
		if ($_SESSION['order']['step'] == 3) {
			
			if (getP('action') == 'register') {
				$this->data = $this->forms->getForm('profile', true);
				$result = $this->forms->checkFields($this->data, getP('fields'));
				if (!$result) {
					return true;
				} else {
					$this->return['errors']['fields'] = $result;
					return false;
				}
			} else {
				return true;
			}
			
			
		} elseif ($_SESSION['order']['step'] == 1 || $_SESSION['order']['step'] == 2) {
			$this->data = $this->forms->getForm('order');
			if (isset($this->data[$_SESSION['order']['step']])) {
				$this->data = $this->data[$_SESSION['order']['step']];
			} else {
				return false;
			}
			
			$result = $this->forms->checkFields($this->data, getP('fields'));
			if (!$result) {
				return true;
			} else {
				$this->return['errors']['fields'] = $result;
				return false;
			}
		}
		
		return true;
	}
	
	public function saveFieldsData() {
		
		if (getP('step')) {
			if (getP('step') == 3 && getS('user/id')) {
				if (getS('order/step') == 4) {
					$_POST['step'] = 2;
				} else {
					$_POST['step'] = 4;
				}
				
			}
			
			$_SESSION['order']['step'] = getP('step');
		}	
		
		if (isset($_SESSION['order']['fields']) && is_array($_SESSION['order']['fields']) && is_array(getP('fields'))) {
			foreach (getP('fields') AS $key => $value) {
				$_SESSION['order']['fields'][$key] = $value;
			}
		} else {
			if (getP('fields')) {
				$_SESSION['order']['fields'] = getP('fields');
			}
			
		}
	}
	
	public function setData($data) {
		$this->data = $data;
		
		return $this;
	}
	
	public function loadPayment($id) {
		
		if (!$id) {
			openDefaultPage();
		}
		
		$pdata = array();
		$ret = '';
		$dbQuery = "SELECT * FROM `" . $this->dbTable . "` 
								WHERE 1
							 		AND `id` = '" . mres($id) . "'
							 		AND `user_id` = '" . mres(getS('user/id')) . "'";
		$query = new query($this->db, $dbQuery);
		if ($query->num_rows() == 1) {
			$pdata = $query->getrow();
		} else {
			openDefaultPage();
		}
		 
		$ssl = ((int)PAYMENTS_SSL == 1) ? true : false;
		
		require_once(PAYMENTS_FOLDER . "paypal/paypal.class.php");
		$paypal = new Paypal($pdata['site_id']);
		
		// Check for avialable paypal currency
		$result = $paypal->checkCurreny('USD', $pdata['price']);
		$pdata['payCurrency'] = $result['currency'];
		$pdata['amount'] = $pdata['price'];
		
		// Save paypal sums for order
		$tmp = array(
				'order_id'	    => $pdata['id'],
				'pp_sum'	    => $pdata['amount'],
				'pp_currency'   => $pdata['payCurrency']
		);
		$paypal->saveOrderPaypalData($tmp);
		
		// Set site main link
		$siteUrl = getDCountryLink($paypal->country, $ssl);
		
		// Add variables to Form
		$paypal->addVar('business', $paypal->business_account); // Merchant Paypal or Sandbox email
		$paypal->addVar('notify_url', $siteUrl . '/pproc/paypal/?action=ipn'); // Paypal IPN URL
		$paypal->addVar('return', $siteUrl . '/pproc/paypal/?action=retok&p=' . $pdata['id']); // Thank you Page
		$paypal->addVar('cancel_return', $siteUrl . '/pproc/paypal/?action=retfail&p=' . $pdata['id']); // Payment canceled or error
		$paypal->addVar('cmd', '_xclick');
		$paypal->addVar('rm', '2');
		$paypal->addVar('invoice', 'Papersstock.com Order ID: ' . $pdata['id']); // Passthrough variable you can use to identify your invoice number for this purchase.
		$paypal->addVar('item_name', 'Papersstock.com Order ID: ' . $pdata['id']);
		$paypal->addVar('quantity', '1');
		$paypal->addVar('amount', $pdata['amount']);
		$paypal->addVar('tax', '0');
		$paypal->addVar('shipping', '0');
		$paypal->addVar('no_shipping', '2');
		$paypal->addVar('currency_code', $pdata['payCurrency']);//'EUR'
		//$paypal->addVar('first_name', $pdata['clientName']); // customer name
		//$paypal->addVar('last_name', $pdata['clientSurname']); // customer surname
		$paypal->addVar('charset', 'utf-8');
		$paypal->addVar('custom', $pdata['id']);
		
		// Get form code
		$ret = $paypal->getForm(0);
		$this->setPData($ret, 'payform');

		$this->tpl->assign("TEMPLATE_ORDERS_HEAD_MODULE", $this->tpl->output("start-payment", $this->getPData()));
		
	}
	
	public function getLoginForm($inquiry = false) {
		$profile = loadAppClass('profile', $this->app, true, false);
		$profile->module->loadFormData()->getForm();
		if ($inquiry) {
			$this->order['profile'] = $profile->module->getData();
		} else {
			$this->setData($profile->module->getData());
		}
		
		$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		return $this;
	}
	
	public function setUserId($userId) {
		$this->userId = $userId;
	}
	
	public function saveOrder() {
		
		$profile = loadAppClass('profile', $this->app, true, false);			
			
		if (getP('action') == 'login') {
			
			if ($profile->module->loginUser(getP('fields/email'), getP('fields/password'))) {
				$this->setUserId($profile->module->getUserId());
				
				if ($orderId = $this->save()) {
					
					
					
					$_SESSION['order']['ID'] = $_SESSION['order']['SAVEID'] = $orderId;
					return true;
					
				} else {
					$this->return['errors']['fields']['email'] = gL('order_Errors_Login_Failed');
					$this->return['errors']['fields']['password'] = gL('order_Errors_Login_Failed');
					return false;
				}
			} else {
				$this->return['errors']['fields']['email'] = gL('order_Errors_Login_Failed');
				$this->return['errors']['fields']['password'] = gL('order_Errors_Login_Failed');
				return false;
			}
			
		} elseif (getP('action') == 'register') {
			
			if (getP('fields/agreement') == '1') {
				if ($userId = $profile->module->saveUser()) {
					
					$this->setUserId($userId);
				
					if ($orderId = $this->save()) {
						
						$_SESSION['order']['ID'] = $_SESSION['order']['SAVEID'] = $orderId;
						return true;
						
					} else {
						$this->return['errors']['global'] = gL('order_Errors_Global_RegisterOrder');
						return false;
					}
				
				} else {
					$this->return['errors']['global'] = gL('order_Errors_Global_RegisterUser');
					return false;
				}
			} else {
				$this->return['errors']['fields']['agreement'] = gL('order_Errors_Fields_Agreement');
				return false;
			}
						
		} elseif (getS('user/id')) {
			
			$this->setUserId(getS('user/id'));
				
			if ($orderId = $this->save()) {
					
				$_SESSION['order']['ID'] = $_SESSION['order']['SAVEID'] = $orderId;
				return true;
					
			} else {
				$this->return['errors']['global'] = gL('order_Errors_Global_RegisterOrder');
				return false;
			}
			
		} else {
			return false;
		}
		
	}
	
	private function getExtraValues($extras) {
		$data = array();
	
		if (is_array($extras) && count($extras) > 0) {
			foreach ($extras AS $extra) {
				$dbQuery = "SELECT `name`
							FROM `mod_forms_values_data`
							WHERE 1
								AND `value_id` = '" . mres($extra) . "'
								AND `lang` = '" . $this->getLang() . "'";
				$query = new query($this->db, $dbQuery);
				$data[] = $query->getOne();
			}
		}
		
	
		return $data;
	}
	
	public function save() {
		if ($this->userId) {
			$this->data = $this->forms->getForm('order', true);
			
			$dbData = $this->forms->getDataArrayToDB($this->data, getS('order/fields'));
			$dbData['user_id'] = $this->userId;
			$dbData['created'] = time();
			$dbData['price'] = $this->calculatePrice();
			$dbData['site_id'] = $this->getCountry();
			$dbData['lang'] = $this->getLang();
			$dbData['number_of_pages'] = getS('order/fields/number_of_pages');
			$dbData['additional_extras'] = json_encode($this->getExtraValues(getS('order/fields/additional_extras')));
			
			$id = "";
			if (getS('order/SAVEID')) {
				$id = getS('order/SAVEID');
			}
			
			$id = saveValuesInDb($this->dbTable, $dbData, $id);
			
			if (getS('order/SAVEID')) {
				
			} else {
				
				$dbData = array();
				
				$dbData['order_id'] = $id;
				$dbData['user_id'] = 0;
				$dbData['message'] = str_replace(array('{{user}}', '{{order}}'), array(getS('user/first_name'), $id), $this->cfg->getData('orderDefaultMessage'));
				$dbData['created'] = time();
				$dbData['type'] = 1;
					
				saveValuesInDb($this->cfg->getDbTable('orders', 'messages'), $dbData);
			}

			$_SESSION['useraction']['order'] = true;
			
			return $id;
		}
		
		return false;
	}
	
	public function calculatePrice() {
		$price = 0;
		//pR(getS('order/fields'));
		
		if (getS('order/fields/mob_2', '', 1221) && isset($this->config['prices'][getS('order/fields/mob_2', '', 1221)])) {
			$data = $this->config['prices'][getS('order/fields/mob_2', '', 1221)];
			
			if (getS('order/fields/mob_2', '', 1221) == '1221' || getS('order/fields/mob_2', '', 1221) == '1222') {
				$typeOfWork = getS('order/fields/mob_4-groupid');
				
				$defaultPages = 1;
				$defaultDeadline = 172800;
				
				if (getS('order/fields/mob_17', '', 1356) == '1355') {
					$defaultDivPrice = $this->cfg->getData('orderDoubleSpace');
				} else {
					$defaultDivPrice = 1;
				}
				
			} else {
				$typeOfWork = 0;
				
				$defaultPages = 1;
				$defaultDeadline = 172800;
				$defaultDivPrice = 1;
			}
			
			if (getS('order/fields/mob_18')) {
				$defaultDeadline = getS('order/fields/mob_18');
			}
			
			if (isset($data[$typeOfWork], $data[$typeOfWork][getS('order/fields/mob_3')])) {
				$data = $data[$typeOfWork][getS('order/fields/mob_3')];
		
				if (isset($data[$defaultDeadline])) {
					$price = $data[$defaultDeadline];
				}
		
			}
			
			if (getS('order/fields/number_of_pages', '', 1)) {
				$defaultPages = getS('order/fields/number_of_pages', '', 1);
			}
			
			
			if (getS('order/fields/mob_19') == 215 || $this->isTopWriter(getS('order/fields/mob_20'))) {
				$price = $price * $this->cfg->getData('orderTopWriterCoef');
			}

			$price = $price * $defaultPages;
			$price = $price / $defaultDivPrice;
			
			if (getS('order/fields/additional_extras')) {
				$price += $this->getExtraPrices(getS('order/fields/additional_extras'));
			}
			
			if ($discount = $this->checkBonusCode(getS('order/fields/mob_22'))) {
				$price = $price - $price * $discount / 100;
			}

		}
		
		
		return number_format($price, 2, '.', '');
	}
	
	private function getExtraPrices($extras) {
		$price = 0;
		
		foreach ($extras AS $extra) {
			$dbQuery = "SELECT `price`
							FROM `mod_forms_values`
							WHERE 1
								AND `id` = '" . mres($extra) . "'";
			$query = new query($this->db, $dbQuery);
			$price += $query->getOne();
		}
		
		return $price;
	}
	
	private function checkBonusCode($id) {
		if ($id) {
			$dbQuery = "SELECT `discount_value`
							FROM `mod_bonus_codes`
							WHERE 1
								AND `code` = '" . mres($id) . "'
								AND `valid_from` <= '" . time() . "'
								AND `valid_to` >= '" . time() . "'
								AND `enable` = '1'
								AND `status` = 'active'";
			$query = new query($this->db, $dbQuery);
			return $query->getOne();
		}
		return false;
	}
	
	private function isTopWriter($id) {
		if ($id) {
			$dbQuery = "SELECT AVG(rate) 
									FROM `" . $this->cfg->getDbTable('orders', 'writers_rates') . "` ow 
									WHERE ow.writer_id = '" . mres($id) . "'";
			$query = new query($this->db, $dbQuery);
			if ($query->getOne() > 4) {
				return true;
			} else { 
				return false;	
			}
			
		}
		return false;
	}
	
	public function getOrdersDataForPages() {
		if ($this->getCountry() == 8) {
			
			$orders = array();
			
			$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('orders', 'self') . "` o
							WHERE 1
								AND o.`status` = '2' 
								AND o.`writer_id` = ''";
			$query = new query($this->db, $dbQuery);
			while ($row = $query->getrow()) {
				$row['price'] = number_format($row['price'] - $row['price'] * $this->cfg->getData('orderPriceReduced') / 100, 2, '.', '');
				
				$orders[] = $row;
			}
			
			$this->setPData(array('orders' => $orders), 'apply');
			
		} else {
			
			$orders = $this->forms->getForm('order', true, " AND f.startpage = '1' ");
			
			$this->setPData(array('orders' => $orders), 'paper');
		}
	}
}

?>