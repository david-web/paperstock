<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */
// ------------------------------------------------------------------------

/**
 * faq module
 * 30.06.2010
 */
class faqData extends Module {

    /**
     * Class constructor
     */
    public function __construct() {

        parent :: __construct();
        $this->name = 'faq';
        
    }

    /**
     * Get all faq by category/content id
     * 
     */
    public function showList() {
        
        $dbQuery = "SELECT SQL_CALC_FOUND_ROWS * FROM `" . $this->cfg->getDbTable('faq') . "` n
								WHERE 1
									AND n.enable = '1'
									AND n.content_id = '" . $this->getCData('id') . "'
								ORDER BY `sort` DESC";
		$query = new query($this->db, $dbQuery);

        $faq = $query->getArray();

		$this->setPData($faq, "list");

		$this->tpl->assign("TEMPLATE_FAQ_MODULE", $this->tpl->output("list", $this->getPData()));
        
        return $this;
    }
    
	/**
	 * 
	 * Set products pager
	 */
	public function setPager() {
		
		$curPage = 1;
		if (getG('page') && getG('page') > 0) {
		
			$curPage = (int)getG('page');
		}

		$query = new query($this->db, "SELECT FOUND_ROWS()");
		
		// Load paginator lib
		$pager = &loadLibClass('paginator');
		$this->pager = $pager
							->set_items_on_page($this->itemsPerPage)
							->advancedPages($query->getOne(), $curPage);

		return $this->pager;
	}
}

?>