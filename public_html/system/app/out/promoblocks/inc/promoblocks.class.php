<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/**
 * promoblock module
 */

class promoblocksData extends Module {

	
	/**
	 * Class constructor
	 */
	public function __construct() {		
		
		parent :: __construct();
		$this->name = 'promoblock';
		$this->dbTable = $this->cfg->getDbTable('promoblock', 'self');
		$this->imagesConfig = $this->cfg->getImageConfig('promoblock');
	}
	
	/**
	 * Get all promoblock
	 * 
	 */
	public function showList() {
		$result = array();
		
		$dbQuery = "SELECT *
							FROM `" . $this->dbTable . "`
							WHERE 1 
								AND `enabled` = '1' 
								AND `lang` = '" . $this->getLang() . "'
								AND `country` = '" . $this->getCountry() . "'
							ORDER BY `sort` ASC";
		$query = new query($this->db, $dbQuery);
		while ($row = $query->getrow()) {
			$row["title"] = stripslashes($row["title"]);
			$row['description'] = stripslashes($row['lead']);
			
			if ($row['doc_id']) {
				$row['url'] = getLink($row['url_id']) . getDocUrl($row['doc_id']) . '.html';
			} elseif ($row['url_id']) {
				$row['url'] = getLink($row['url_id']);
			}
			
			$row['slide_time'] = $row['seconds'] * 1000;
			
			$result[] = $row;
		}
		$this->setPData($result, "promoblock");
		$this->setPData($this->imagesConfig['original']['upload_path'], "promoblockUploadFolder");

		$this->tpl->assign("PROMOBLOCK_CONTENT", $this->tpl->output("list", $this->getPData()));	
	
	}	
}

?>