<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		Jānis Šakars <janis.sakars@efumo.lv>
 * @copyright   Copyright (c) 2010, Efumo.
 * @link		http://efumosoftware.lv
 * @version		1
 */

// ------------------------------------------------------------------------

class bonuscodes extends Module_cms {

    public $module;

    public function __construct() {

        parent :: __construct();
        
        $this->name = get_class($this);

        require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
        $this->module = new bonuscodes_data();
        
        $this->module->db_table = 'mod_bonus_codes';
        $this->module->parameters = array('statuses' => array('active', 'ended'));

        $this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');

        $this->clearOtherFiltersData($this->name);
        
        $this->tpl->assign('MODULE_HEAD', $this->tpl->output('head'));
        $this->tpl->assign('MAIN_URL', '/' . $this->uri->segment(0) . '/' . $this->uri->segment(1) . '/');
        $this->tpl->assign('moduleName', $this->getModuleTitle());
        
        if(! $this->uri->segment(3) ) {
            $data = $this->createModuleTableNav(true, false, true);

            $data['statuses'] = $this->module->createParameters('statuses', ! empty($_SESSION['filters'][$this->name]['status']) ? $_SESSION['filters'][$this->name]['status'] : null);

            $filters = $this->module->getFilterData();
            //pR($filters);
            $this->tpl->assign("filters", $filters);
            $this->tpl->assign("moduleFrom", isset($_SESSION['filters'][$this->name]["itemsFrom"]) ? $_SESSION['filters'][$this->name]["itemsFrom"] : 0);
            $this->includeTemplate($data);
        }
        
    }

    public function run() {
        
        // Variables used in requests
        $action = $this->uri->segment(3);
        $id     = getP('id') ? getP('id') : ($this->uri->segment(4) ? $this->uri->segment(4) : 0);
        $values = getP('values');
        $value = getP('value');
        $status = getP('status');
        
        switch($action) {
            // Module data output via cmsTable
            case 'moduleTable':
                if($this->cmsUser->haveUserRole('VIEW', $this->getModuleId())) {
                    $this->noLayout(true);
					$result = $this->module->showTable();
					jsonSend($result);
                }
                break;
            // Edit data
            case 'edit':
                if($this->cmsUser->haveUserRole('EDIT', $this->getModuleId())) {
					$this->includeTemplate($this->module->edit($id), 'edit');
                }
                break;
            // Edit data
            case 'view':
                if($this->cmsUser->haveUserRole('VIEW', $this->getModuleId())) {
					$this->includeTemplate($this->module->edit($id), 'view');
                }
                break;
            // Save data
            case 'save':
                if($this->cmsUser->haveUserRole(($id ? 'EDIT' : 'ADD'), $this->getModuleId())) {
                    $this->noLayout(true);                    
                    jsonSend($this->module->save($id, $value));
                }
                break;
            // Delete
            case 'delete':
                if($this->cmsUser->haveUserRole('DELETE', $this->getModuleId())) {
                    $this->noLayout(true);
                    jsonSend($this->module->delete($id));
                }
                break;
            // Change code status
            case 'enable':
                if($this->cmsUser->haveUserRole('EDIT', $this->getModuleId())) {
					$this->noLayout(true);
					
					$this->cmsLog->writeLog($this->getModuleName(), "enable, id=" . $id . " value=" . $value);
					$this->module->enable($id, $value);
				}
                break;
            // Check code uniqueness
            case 'uniquecode':
                if($this->cmsUser->haveUserRole('ADD', $this->getModuleId()) || $this->cmsUser->haveUserRole('EDIT', $this->getModuleId())) {
                    $this->noLayout(true);
					jsonSend($this->module->isUniqCode(getP('code'), $id));
                }
                break;
            case 'unsetFilter':
                if(isset($_SESSION['filters'][$this->name])){
                    unset($_SESSION['filters'][$this->name]);
                }
                $_SESSION['filters'][$this->name]['itemsFrom'] = 0;
                jsonSend(array(
                    'result' => '1'
                ));
                break;
        }
    }
}
