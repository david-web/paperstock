<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		Jānis Šakars <janis.sakars@efumo.lv>
 * @copyright   Copyright (c) 2010, Efumo.
 * @link		http://efumosoftware.lv
 * @version		1
 */

// ------------------------------------------------------------------------

class bonuscodes_data extends Module_cms {
    
    public $db_table;
    public $parameters;
    
    public function __construct() {
        parent :: __construct();
        $this->name = "bonuscodes";
    }
    
    public function createParameters($type, $selected_id = null) {
        
        if(array_key_exists($type, $this->parameters) && is_array($this->parameters[$type])) {
            $values = array();
            foreach($this->parameters[$type] as $parameter) {
                $values[$parameter] = ucfirst($parameter);
            }
            
            return dropDownFieldOptions($values, $selected_id, true);
        }
        
        return array();
    }
    
    /**
     * Returns HTML as table content
     */
    public function showTable() {
        
        $return = array(
            'html'      =>  null,
            'rCounts'   =>  0
        );
        
        $table = array(
            'id'              => array(
                'sort'          => true,
                'title'         => gLA('id','ID'),
                'function'      => array(),
                'fields'        => array()
            ),
            'title'              => array(
                'sort'          => true,
                'title'         => gLA('m_title','Title'),
                'function'      => array(&$this, 'clear'),
                'fields'        => array('title')
            ),
            'code'              => array(
                'sort'          => true,
                'title'         => gLA('code','Code'),
                'function'      => array(&$this, 'clear'),
                'fields'        => array('code')
            ),
            'max_uses'              => array(
                'sort'          => true,
                'title'         => gLA('max_uses','Max uses'),
                'function'      => array(&$this, 'clearMaxUserVal'),
                'fields'        => array('max_uses')
            ),
            'used'              => array(
                'sort'          => true,
                'title'         => gLA('used','Used'),
                'function'      => array(&$this, 'clearMaxUserVal'),
                'fields'        => array('max_uses', 'used')
            ),
            'discount_value'              => array(
                'sort'          => true,
                'title'         => gLA('value','Value'),
            ),
            'valid_from'     => array(
                'sort'          => true,
                'title'         => gLA('valid_from','Valid from'),
                'function'      => array(&$this, 'convertDate'),
                'fields'        => array('valid_from')
            ),
            'valid_to'     => array(
                'sort'          => true,
                'title'         => gLA('valid_to','Valid to'),
                'function'      => array(&$this, 'convertDate'),
                'fields'        => array('valid_to')
            ),
            'status'            => array(
                'sort'          => true,
                'title'         => gLA('status','Status'),
                'function'      => array(&$this, 'getTitle'),
                'fields'        => array('status')
            ),
            "enable" => array(
                'sort' => false,
                'title' => gLA('m_enable','Enable'),
                'function' => array(&$this, 'moduleEnableLinkIfValidDate'),
                'fields'	=> array('id', 'enable', 'valid_to')
            ),
            'actions'           => array(
                'sort'          => false,
                'title'         => gLA('actions','Actions'),
                'function'      => array(&$this, 'moduleActionsLink'),
                'fields'        => array('id', 'type')
            )
        );
        
        if(getP('itemsFrom') != ''){
            $_SESSION['filters'][$this->name]["itemsFrom"] = getP('itemsFrom');
        }
        
        
        // Get search WHERE clouse and set search values in SESSION
        $search = $this->getSearchQuery();
        
        $sql = "SELECT * FROM `" . $this->db_table . "` WHERE 1 " . $search;
        $query = new query($this->db, $sql);

        $return['rCounts'] = $query->num_rows();
        
        $sql .= $this->moduleTableSqlParms('id', 'DESC');
        $query = new query($this->db, $sql);
        $result = $query->getArray();
        
        $result = $this->checkStatus($result);
        
        $this->cmsTable->createTable($table, $result);
        $return['html'] = $this->cmsTable->returnTable;
        
        return $return;
    }
    
    public function clearMaxUserVal($max_uses, $used = ''){
        if($max_uses == 0){
            $max_uses = '';
        }
        
        if($used != '' && $max_uses == 0){
            $used = '';
            return $used;
        } else if($used != ''){
            return $used;
        }
        return $max_uses;
    }
    
    /**
     * Check if bonuscode is active. If is then show checkbox, else nothing.
     */
    public function moduleEnableLinkIfValidDate($id, $enable, $valid_to){
        if($valid_to != '' && $valid_to > time()){
            return $this->moduleEnableLink($id, $enable);
        }
        return $this->moduleEnableLink($id, $enable, true);
    }
    
    
    /**
     * Returns interface translation message for parameter
     * @param type $parameter
     * @return type 
     */
    public function getTitle($type){

        return ucfirst($type);
    }
    
    // Get search WHERE clouse and set filter values in SESSION
    public function getSearchQuery(){
        $search = '';
        $valid_from = getP('valid_from');
        $valid_to = getP('valid_to');
        $status = getP('status');
        $title = getP('title');
        
        if($valid_from) {
            $valid_from_c = strtotime(getP('valid_from') . ' 23:59:59');
            $search .= " AND `valid_from` >= '". mres($valid_from_c) ."'";
            $_SESSION['filters'][$this->name]["valid_from"] = $valid_from;
        }else if (isset($_SESSION['filters'][$this->name]["valid_from"])){
            $valid_from = strtotime($_SESSION['filters'][$this->name]["valid_from"] . ' 23:59:59');
            $search .= " AND `valid_from` >= '" . mres($valid_from) . "'";
        }
        
        if($valid_to) {
            $valid_to_c = strtotime(getP('valid_to') . ' 23:59:59');
            $search .= " AND `valid_to` <= '". mres($valid_to_c) ."'";
            $_SESSION['filters'][$this->name]["valid_to"] = $valid_to;
        }else if (isset($_SESSION['filters'][$this->name]["valid_to"])){
            $valid_to = strtotime($_SESSION['filters'][$this->name]["valid_to"] . ' 23:59:59');
            $search .= " AND `valid_to` <= '" . mres($valid_to) . "'";
        }
        
        if($status) {
            $search .= " AND `status` = '" . mres($status) . "'";
            $_SESSION['filters'][$this->name]["status"] = $status;
        }else if (isset($_SESSION['filters'][$this->name]["status"])){
            $search .= " AND `status` = '" . mres($_SESSION['filters'][$this->name]["status"]) . "'";
        }
        
        if($title) {
            $search .= ' AND `title` LIKE "%' . mres($title) . '%"';
            $_SESSION['filters'][$this->name]["title"] = $title;
        }else if (isset($_SESSION['filters'][$this->name]["title"])){
            $search .= " AND `title` LIKE '%" . mres($_SESSION['filters'][$this->name]["title"]) . "%'";
        }
        
        return $search;
    }
    
    public function checkStatus($result){
        
        foreach($result as &$bonus){
            if($bonus['valid_to'] != '' && $bonus['valid_to'] < time() && $bonus['status'] == 'active'){
                $bonus['status'] = 'ended';
                saveValuesInDb($this->db_table, $bonus, $bonus['id']);
            }
            
            if($bonus['valid_to'] != '' && $bonus['valid_to'] > time() && $bonus['status'] == 'ended'){
                $bonus['status'] = 'active';
                saveValuesInDb($this->db_table, $bonus, $bonus['id']);
            }
        }
        return $result;
    }
    
    /**
     * Returns data for editing
     * @param   $id AS int
     */
    public function edit($id) {
        
        $data = array();
        $data['edit']['id'] = 0;
        
        if($id) {
            $query = new query($this->db, "SELECT * FROM `" . $this->db_table . "` WHERE `id` = " . intval($id));
            if($query->num_rows()) {
                $data['edit'] = $query->getrow();
                if($data['edit']['valid_to'] != '' && $data['edit']['valid_to'] < time()){
                    $data['edit']['disable_checkbox'] = 1;
                }
                $data['edit']['valid_from'] = convertDate($data['edit']['valid_from'], 'd.m.Y');
                $data['edit']['valid_to'] = convertDate($data['edit']['valid_to'], 'd.m.Y');
            }
        }
       
        
        return $data;
    }
    
    /**
     * Saves/update module data
     * @param   $id AS int - default 0 - new row
     * @param   $values AS json serialized array
     */
    public function save($id, $values) {
        
        $values['valid_from'] = strtotime($values['valid_from'] . ' 00:00:00');
        $values['valid_to'] = strtotime($values['valid_to'] . ' 23:59:59');
        
        if($values['valid_to'] != '' && $values['valid_to'] < time()){
            $values['status'] = 'ended';
        }
        
        if($values['code'] != ''){
            $values['code'] = strtoupper($values['code']);
        }

    	if($values['valid_to'] != '' && $values['valid_to'] > time()){
            $values['status'] = 'active';
        }
            
        $id = saveValuesInDb($this->db_table, $values, $id);
        
        
        return array('status' => $values['status'], 'msg' => gLA('bonus_'.$values['status']));
    }
    
    /**
     * Deletes row by ID
     * @param type $id 
     */
    public function delete($id) {
        
        deleteFromDbById($this->db_table, $id);
    }

    /**
	 * Enable or disable
	 * 
	 * @param int/array 	news id
	 * @param bool 			enable/disable value
	 */
	public function enable($id, $value) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->db_table . "` SET `enable` = '" . $value . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}			
	}
    /**
     * Checks if given code is unique
     * @param type $code
     * @param type $code_id
     * @return type 
     */
    public function isUniqCode($code, $code_id = 0) {
        
        $query = new query($this->db, "SELECT `id` FROM `" . $this->db_table . "` WHERE `code` = '" . mres($code) . "' AND `id` <> " . intval($code_id));
        if($query->num_rows()) {
            return false;
        }
        
        return true;
    }
    
    public function getFilterData(){
        $status = getP('status');
        $from = getP('valid_from');
        $to = getP('valid_to');
        //pR($_POST); die;
        
        if($status == ''){
            $status = isset($_SESSION['filters'][$this->name]["status"]) ? $_SESSION['filters'][$this->name]["status"] : '';
        }
        
        if($from == ''){
            $from = isset($_SESSION['filters'][$this->name]["valid_from"]) ? $_SESSION['filters'][$this->name]["valid_from"] : '';
        }
        if($to == ''){
            $to = isset($_SESSION['filters'][$this->name]["valid_to"]) ? $_SESSION['filters'][$this->name]["valid_to"] : '';
        }
        
        $result = array(
            'status' => $status,
            'valid_from' => $from,
            'valid_to' => $to,
        );
        
        return $result;
    }
    
     
}