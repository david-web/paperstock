
function updateModule() {
    moduleTable.updateModule();
}
function moduleEdit(id) {
    window.location.href = moduleTable.getRequestUrl() + 'edit/' + (id ? id + '/' : '');
}
function moduleView(id) {
    window.location.href = moduleTable.getRequestUrl() + 'view/' + (id ? id + '/' : '');
}
function moduleDelete(id) {
    if(id) {
        if(ruSure(langStrings.getMsg('rusure_delete'))) {
            ajaxRequest(
                moduleTable.getRequestUrl() + 'delete/',
                'id=' + id,
                updateModule
            );
        }
    }
}

function moduleEnable(id, value) {
	ajaxRequest(moduleTable.getRequestUrl() + "enable/", "value=" + value + "&id=" + id, updateModule);
}

function changeFilter() {
    moduleTable.from = 0;
    moduleTable.additionalParms = '&valid_from=' + jQuery.trim($('#valid_from').val()) +'&valid_to=' + jQuery.trim($('#valid_to').val()) + '&status=' + jQuery.trim($('#status').val());
    
    updateModule();
}
function clearFilter() {
    $('#status').val(0);
    $('#valid_from, #valid_to').val('');
    
    moduleTable.from = 0;
    moduleTable.additionalParms = '';
    ajaxRequest(moduleTable.getRequestUrl() + "unsetFilter/", "id='0'", updateModule);
}

function checkFields(type) {
	result = true;
	
	$('.required').each(function(n, element) {
		if ($(this).val() == '') {
			$(this).css('background', '#f7b5b5');
			$(this).focus();
			result = false;
		} else {
			$(this).css('background', '#ffffff');
		}
	});

	if (result) {
		saveType = type;
		
		saveData();
	}	
}

function saveData() {
	
	returnArray = {};

	$('btn *').removeAttr('onclick'); 
	
	$('.simple').each(function(n, element) {
		
		if($(element).attr('type') == 'checkbox') {
			if ($(element).is(':checked')) {
				returnArray[$(element).attr('id')] = $(element).val();
			}
			else {
				returnArray[$(element).attr('id')] = 0;
			}	
		} else {
			returnArray[$(element).attr('id')] = $(element).val();
		}
		

		
	});
	
	
	sendData = {};
	sendData['value'] = returnArray;

	if (saveType == "apply") {
		idUrl = $('#id').val() ? $('#id').val() + "/" : "";
		ajaxRequest(moduleTable.getRequestUrl() + "save/" + idUrl, sendData, function () {});		
	}
	if (saveType == "save") {
		
		idUrl = $('#id').val() ? $('#id').val() + "/" : "";
		ajaxRequest(moduleTable.getRequestUrl() + "save/" + idUrl, sendData, function () {
			
			window.location.href = moduleTable.getRequestUrl();
		});
	}
}

$(document).ready(function(){
    $('#valid_to').bind('click', function() {
        if($('#valid_from').val()){
            $('#valid_to').datepicker('option', {
                dateFormat  : 'dd.mm.yy',
                minDate     : $('#valid_from').datepicker('getDate') || new Date(),
            });
        }
    });
});