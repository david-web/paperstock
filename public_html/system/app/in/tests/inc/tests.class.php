<?php

class testsData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	public $config;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "tests";
		
		$this->defLang = getDefaultLang();
		
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable() {
		
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"chekcbox" => array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleCheckboxLink'),
				'fields' => array('id'),
				'params' => array('profile')
			),
			"title" => array(
				'sort' => false,
				'title' => gLA('title','Title'),
				'function' => '',
			),
			"created" => array(
				'sort' => false,
				'title' => gLA('created','Created'),
				'function' => 'convertDate',
				'fields'	=> array('created'),
				'params' => array('d-m-Y')
			),
			"enable" => array(
				'sort' => false,
				'title' => gLA('m_enable','Enable'),
				'function' => array(&$this, 'moduleEnableLink'),
				'fields'	=> array('id', 'enable')
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLink'),
				'fields'	=> array('id')
			)
		);

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}	
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS * 
							FROM `" . $this->cfg->getDbTable('tests', 'self') . "` t
								LEFT JOIN  `" . $this->cfg->getDbTable('tests', 'data') . "` td ON (t.id = td.test_id)
							WHERE 1
								AND td.lang = '" . $this->defLang . "'
							" . $this->moduleTableSqlParms("id", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	/**
	 * Enable or disable
	 * 
	 * @param int/array 	news id
	 * @param bool 			enable/disable value
	 */
	public function enable($id, $value) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->cfg->getDbTable('tests', 'self') . "` SET `enable` = '" . $value . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}			
	}
	
	/**
	 * Delete news from DB
	 * 
	 * @param int/Array 	news id
	 */
	public function delete($id) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			deleteFromDbById($this->cfg->getDbTable('tests', 'self'), $id);
		}		
	}
	
	/**
	 * Edit news in DB
	 * 
	 * @param int 	news id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();

		if(isset($id) && intval($id)) {
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "SELECT * FROM `" . $this->cfg->getDbTable('tests', 'self') . "` 
								WHERE 1 
									AND `id` = '" . $id . "'
								LIMIT 0,1";
			$query = new query($this->db, $dbQuery);		
			
			$data["edit"] = $query->getrow();
			
			$dbQuery = "SELECT * FROM `" . $this->cfg->getDbTable('tests', 'data') . "`
												WHERE `test_id`= '" . mres($id) . "'";
			$query = new query($this->db, $dbQuery);
			$data["edit"]['data'] = $query->getArray('lang');
			
		} 
		
		
		return $data;
	}
	
	/**
	 * Saving information in DB
	 * 
	 * @param int	 id, it's need if we are editing language
	 * @param array  information values
	 */
	public function save($id, $value) {
		
		$langValues = getP('valueLangs');
		$siteLangs = getSiteLangs();
		
		if (!$id) {
			$value["created"] = time();
		}
			
			
		$id = saveValuesInDb($this->cfg->getDbTable('tests', 'self'), $value, $id);	
		
		deleteFromDbById($this->cfg->getDbTable('tests', 'data'), $id, 'test_id');
		foreach ($siteLangs AS $key => $values) {
			$data = array(
					'test_id' => $id,
					'lang' => $values['lang'],
					'title' => $langValues['title'][$values['lang']],
					'description' => $langValues['description'][$values['lang']],
					'header' => $langValues['header'][$values['lang']],
			);
				
			saveValuesInDb($this->cfg->getDbTable('tests', 'data'), $data);
		
		}
		

		return $id;
	}
	
}
?>