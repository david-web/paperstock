<?php

class profileData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	public $config;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "profile";
		
		$this->defLang = getDefaultLang();
		
		$this->dbTable = $this->cfg->getDbTable('profiles');
		
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable() {
		
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"chekcbox" => array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleCheckboxLink'),
				'fields' => array('id'),
				'params' => array('profile')
			),
			"id" => array(
					'sort' => false,
					'title' => gLA('id','ID'),
					'function' => '',
			),
			"email" => array(
				'sort' => false,
				'title' => gLA('email','Email'),
				'function' => '',
			),
			"first_name" => array(
				'sort' => false,
				'title' => gLA('first_name','First name'),
				'function' => '',
			),
			"last_name" => array(
				'sort' => false,
				'title' => gLA('last_name','Last Name'),
				'function' => '',
			),
			"country" => array(
					'sort' => false,
					'title' => gLA('country','Country'),
					'function' => '',
			),
			"phone" => array(
					'sort' => false,
					'title' => gLA('phone','Phone'),
					'function' => '',
			),
			"night_calls" => array(
					'sort' => false,
					'title' => gLA('night_calls','Night calls'),
					'function' => '',
			),
			"created" => array(
				'sort' => false,
				'title' => gLA('created','Created'),
				'function' => 'convertDate',
				'fields'	=> array('created'),
				'params' => array('d-m-Y')
			),
			"enable" => array(
				'sort' => false,
				'title' => gLA('m_enable','Enable'),
				'function' => array(&$this, 'moduleEnableLink'),
				'fields'	=> array('id', 'enable')
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLink'),
				'fields'	=> array('id')
			)
		);

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}	
		
		$search = $this->getSearchQuery();
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS * 
							FROM `" . $this->dbTable . "` 
							WHERE 1
							" . $search . $this->moduleTableSqlParms("id", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	public function getSearchQuery() {
		$search = '';
	
		if (getP('filterNightCalls')) {
			$search .= " AND `night_calls` = '" . mres(getP('filterNightCalls')) . "'";
			$_SESSION['filters']['profile']["filterNightCalls"] = getP('filterNightCalls');
		} elseif (isset($_SESSION['filters']['profile']["filterNightCalls"])){
			$search .= " AND `night_calls` = '" . mres($_SESSION['filters']['profile']["filterNightCalls"]) . "'";
		}
	
		if (getP('filterID')) {
			$search .= " AND id LIKE '%" . mres(getP('filterID')) . "%'";
			$_SESSION['filters']['profile']["filterID"] = getP('filterID');
		} elseif (isset($_SESSION['filters']['profile']["filterID"])){
			$search .= " AND id LIKE '%" . mres($_SESSION['filters']['profile']["filterID"]) . "%'";
		}
	
		if (getP('filterName')) {
			$search .= " AND first_name LIKE '%" . mres(getP('filterName')) . "%'";
			$_SESSION['filters']['profile']["filterName"] = getP('filterName');
		} elseif (isset($_SESSION['filters']['profile']["filterName"])){
			$search .= " AND first_name LIKE '%" . mres($_SESSION['filters']['profile']["filterName"]) . "%'";
		}
		
		if (getP('filterLastName')) {
			$search .= " AND last_name LIKE '%" . mres(getP('filterLastName')) . "%'";
			$_SESSION['filters']['profile']["filterName"] = getP('filterLastName');
		} elseif (isset($_SESSION['filters']['profile']["filterLastName"])){
			$search .= " AND last_name LIKE '%" . mres($_SESSION['filters']['profile']["filterLastName"]) . "%'";
		}
		
		if (getP('filterEmail')) {
			$search .= " AND email LIKE '%" . mres(getP('filterEmail')) . "%'";
			$_SESSION['filters']['profile']["filterEmail"] = getP('filterEmail');
		} elseif (isset($_SESSION['filters']['profile']["filterEmail"])){
			$search .= " AND email LIKE '%" . mres($_SESSION['filters']['profile']["filterEmail"]) . "%'";
		}
		
		if (getP('filterPhone')) {
			$search .= " AND phone LIKE '%" . mres(getP('filterPhone')) . "%'";
			$_SESSION['filters']['profile']["filterPhone"] = getP('filterPhone');
		} elseif (isset($_SESSION['filters']['profile']["filterPhone"])){
			$search .= " AND phone LIKE '%" . mres($_SESSION['filters']['profile']["filterPhone"]) . "%'";
		}
		
	
		return $search;
	}
	
	/**
	 * Enable or disable
	 * 
	 * @param int/array 	news id
	 * @param bool 			enable/disable value
	 */
	public function enable($id, $value) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `enable` = '" . $value . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}			
	}
	
	/**
	 * Delete news from DB
	 * 
	 * @param int/Array 	news id
	 */
	public function delete($id) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			deleteFromDbById($this->dbTable, $id);
		}		
	}
	
	/**
	 * Edit news in DB
	 * 
	 * @param int 	news id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();

		if(isset($id) && intval($id)) {
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "SELECT * FROM `" . $this->dbTable . "` 
								WHERE 1 
									AND `id` = '" . $id . "'
								LIMIT 0,1";
			$query = new query($this->db, $dbQuery);		
			
			$data["edit"] = $query->getrow();
			
		} 
		
		
		return $data;
	}
	
	/**
	 * Saving information in DB
	 * 
	 * @param int	 id, it's need if we are editing language
	 * @param array  information values
	 */
	public function save($id, $value) {
		
		$langValues = getP('valueLangs');
		$siteLangs = getSiteLangs();
		
		if (!$id) {
			$value["created"] = time();
		}
			
		$id = saveValuesInDb($this->dbTable, $value, $id);	
		

		return $id;
	}
	
}
?>