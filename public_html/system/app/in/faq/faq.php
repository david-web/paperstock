<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/**
 * CMS faq/textlist module
 * Admin path. Edit/Add/Delete and other actions.
 * 27.07.2010
 */

class faq extends Module_cms {
		
	/**
	 * $module - Object of module admin class
	 */
	public $module;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		
   		parent :: __construct();
		$this->name = get_class($this);
		
		require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
		$this->module = new faqData();
		
		$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		
		$this->tpl->assign("MODULE_HEAD", $this->tpl->output("head"));
		$this->tpl->assign("MAIN_URL", "/" . $this->uri->segment(0) . "/" . $this->uri->segment(1) . "/");
		$this->tpl->assign("moduleName", $this->getModuleTitle());	
		
		if (getP('clear')) {
			unset($_SESSION[$this->name]['filter']);
		}
		
		// Including and preparing needs templates
		switch ($this->uri->segment(3)) {								
			case "" : 

				$data = $this->createModuleTableNav();

				$country_id = isset($_SESSION[$this->name]['filter']['country']) ? $_SESSION[$this->name]['filter']['country'] : '';
                $data['countries'] = $this->module->getSiteCountriesDropDown($country_id);       
                
                $lang = '';
        		if(isset($_SESSION[$this->name]["filter"]["lang"])){
                    $lang = $_SESSION[$this->name]["filter"]["lang"];
                } 
                $data['langs'] = getAllSiteLangs($lang);
				
				$this->includeTemplate($data);
				break;			
		}
		
	}
	
	public function run() {

		$action = $this->uri->segment(3);
		
		$id = getP("id") ? getP("id") : $this->uri->segment(4);
		$value = getP("value");
		
		switch ($action) {
			case 'updateLanguages':
                if($this->cmsUser->haveUserRole('VIEW', $this->getModuleId())) {
                    $this->noLayout(true);
                    
                	if (getP('value')) {
        				jsonSend(getSiteLangsByCountryDD(getP('value'), getP('filterLang')));
       				}
					
                }
                break;
			case "moduleTable" : 
				if ($this->cmsUser->haveUserRole("VIEW", $this->getModuleId())) {
					$this->noLayout(true);
					
					
					jsonSend($this->module->showTable());

				}
				break;
			case "enable" : 
				if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
					$this->noLayout(true);
					
					$this->cmsLog->writeLog($this->getModuleName(), "enable, id=" . $id . " value=" . $value);
					$this->module->enable($id, $value);
				}
				break;
			case "edit" : 
				if ($this->cmsUser->haveUserRole($id ? "EDIT" : "ADD", $this->getModuleId())) {
							
					$data = $this->module->edit($id);
					$this->includeTemplate($data, 'edit');
				}
				break;
			case "save" : 
				if ($this->cmsUser->haveUserRole($id ? "EDIT" : "ADD", $this->getModuleId())) {
					$this->noLayout(true);
					$this->cmsLog->writeLog($this->getModuleName(), "save, id=" . $id);
					
					$result["id"] = $this->module->save($id, $value);
					jsonSend($result);
				}
				break;	
			case "delete" : 
				if ($this->cmsUser->haveUserRole("DELETE", $this->getModuleId())) {
					$this->noLayout(true);
					
					$this->cmsLog->writeLog($this->getModuleName(), "delete, id=" . $id);
					$this->module->delete($id);
				}
				break;	
			case "sort" : 
				if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
					$this->noLayout(true);
					
					$this->cmsLog->writeLog($this->getModuleName(), "sort, id=" . $id . " value=" . $value);
					$this->module->changeSort($id, $value);
				}
				break;
		}
	
	}
}

?>
