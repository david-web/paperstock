<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/**
 * CMS faq/textlist module
 * Admin path. Edit/Add/Delete and other actions.
 * 27.07.2010
 */

class faqData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "faq";
		
		$this->dbTable = $this->cfg->getDbTable('faq');
		
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable() {
			
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"chekcbox" => array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleCheckboxLink'),
				'fields' => array('id'),
				'params' => array('faq')
			),
			"country" => array(
				'sort' => false,
				'title' => gLA('country', 'Country'),
				'function' => '',
				'fields'	=> array('country')
			),
			"lang" => array(
				'sort' => false,
				'title' => gLA('lang', 'Lang'),
				'function' => '',
				'fields'	=> array('lang')
			),
			"title" => array(
				'sort' => false,
				'title' => gLA('m_title','Title'),
				'function' => 'clear',
				'fields'	=> array('title')
			),
			"category" => array(
				'sort' => false,
				'title' => gLA('category', 'Category'),
				'function' => '',
				'fields'	=> array('category')
			),
			"enable" => array(
				'sort' => false,
				'title' => gLA('m_enable','Enable'),
				'function' => array(&$this, 'moduleEnableLink'),
				'fields'	=> array('id', 'enable')
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLink'),
				'fields'	=> array('id')
			)
		);
		
		$where = array();
		if (getP('content_id')) {
			$table['sort'] = array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleSortLinks'),
				'fields'	=> array('id')
			);
			
			$where[] = " `content_id` = '" . mres(getP('content_id')) . "' ";
		}

		// filter by language
        if(getP('filterLang') !== false){
            switch(getP('filterLang')) {
                case '0':
                    if(isset($_SESSION[$this->name]["filter"]["lang"])) {
                        unset($_SESSION[$this->name]["filter"]["lang"]);
                    }
                    break;
               default:
                   $_SESSION[$this->name]["filter"]["lang"] =  getP('filterLang');
            }
        }
        $lang = isset($_SESSION[$this->name]["filter"]["lang"]) ? $_SESSION[$this->name]["filter"]["lang"] : "0";
        
		if ($lang != "0") {
			$where[] = 'l.`lang` = \'' . mres($lang) . '\''; 
		}

    	// filter by language
        if(getP('filterCountry') !== false){
            switch(getP('filterCountry')) {
                case '0':
                    if(isset($_SESSION[$this->name]["filter"]["country"])) {
                        unset($_SESSION[$this->name]["filter"]["country"]);
                    }
                    break;
               default:
                   $_SESSION[$this->name]["filter"]["country"] =  getP('filterCountry');
            }
        }
        $country = isset($_SESSION[$this->name]["filter"]["country"]) ? $_SESSION[$this->name]["filter"]["country"] : "0";
        
		if ($country != "0") {
			$where[] = 'c.`country` = \'' . mres($country) . '\''; 
		}
        
        $whereSql = implode(' AND ', $where);

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}			
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS f.*, l.title AS lang, ct.title AS country, c.title AS category
								FROM `" . $this->dbTable . "` f
								LEFT JOIN `ad_content` c ON (c.id = f.content_id)
								LEFT JOIN `ad_countries` ct ON (ct.id = c.country)
								LEFT JOIN ad_languages  l ON (c.lang = l.lang)
								
								WHERE 1 " . ($whereSql ? " AND " . $whereSql : '') . $this->moduleTableSqlParms("sort", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	/**
	 * Enable or disable
	 * 
	 * @param int/array 	faq id
	 * @param bool 			enable/disable value
	 */
	public function enable($id, $value) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `enable` = '" . $value . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}		

	}
	
	/**
	 * Delete faq from DB
	 * 
	 * @param int/Array 	faq id
	 */
	public function delete($id) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			deleteFromDbById($this->dbTable, $id);
		}		
	}
	
	/**
	 * Edit faq in DB
	 * 
	 * @param int 	faq id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();

		if(isset($id) && intval($id)) {
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "
				SELECT * 
				FROM `" . $this->dbTable . "` 
				WHERE `id` = '" . $id . "'
				LIMIT 0,1
			";
			$query = new query($this->db, $dbQuery);		
			
			$data["edit"] = $query->getrow();
			$data["edit"]["content_idTitle"] = $this->getContentTitle($data["edit"]["content_id"]);
			
		} 
		else {
			$data["edit"]["content_id"] = getG('content_id'); 
			$data["edit"]["content_idTitle"] = $this->getContentTitle($data["edit"]["content_id"]);
		}
		
		return $data;
	}
	
	/**
	 * Saving information in DB
	 * 
	 * @param int	 id, it's need if we are editing language
	 * @param array  information values
	 */
	public function save($id, $value) {
		
		$value = jsonDecode($value);
		
		$value["updated"] = time();
		
		if (!$id) {
			$value["created"] = time();
			$value["sort"] = $this->getNextSort($value["content_id"]);
		}
        
        if(!$id){
            $id = 0;
        }		
			
		$id = saveValuesInDb($this->dbTable, $value, $id);
		
		
		return $id;
	}
	
	/**
	 * Getting next sort id for add new new
	 * 
	 * @param int		content faq id, default value = 0
	 */
	private function getNextSort($cId = "0") {
		
		$dbQuery = "SELECT MAX(sort) AS sort FROM `" . $this->dbTable . "` 
													WHERE `content_id` = '" . $cId . "'";
		$query = new query($this->db, $dbQuery);
		
		return $query->getOne() + 1;
	}
	
	/**
	 * Changing faq sort order
	 * 
	 * @param int		faq id
	 * @param string	sort changing value
	 */
	public function changeSort($id, $value) {
		
		$dbQuery = "SELECT * FROM `" . $this->dbTable . "` WHERE `id` = '" . $id . "'";
                $query = new query($this->db, $dbQuery);
		$content = $query->getrow();
		
		if ($value == "down") {
			$sqlParm = "<";
			$sqlParm2 = "DESC";	
		}
		else {
			$sqlParm = ">";
			$sqlParm2 = "ASC";
		}
		
		$dbQuery = "SELECT `id`, `sort` FROM `" . $this->dbTable . "` WHERE `sort` " . $sqlParm . " '" . $content['sort'] . "' AND `content_id` = '" . $content['content_id'] . "' ORDER BY `sort` " . $sqlParm2 . " LIMIT 0,1";
		$query->query($this->db, $dbQuery);
		if ($query->num_rows() > 0) {
			$info = $query->getrow();
			
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `sort` = '" . $content['sort'] . "' WHERE `id` = '" . $info['id'] . "'";
			$query->query($this->db, $dbQuery);
			
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `sort` = '" . $info['sort'] . "' WHERE `id` = '" . $id . "'";
			$query->query($this->db, $dbQuery);
			
		}
	}
	
	public function getSiteCountriesDropDown($selected = ''){
        $values = array();
        
		$categories = getSiteCountries();
		
		foreach($categories as $category){
			$values[$category['id']] = clear($category['title']);
		}
		return dropDownFieldOptions($values, $selected, true);
    }
	
}
?>