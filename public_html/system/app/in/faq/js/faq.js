
function updateModule() {
	
	moduleTable.additionalParms = "&content_id=" + $('#content_id').val();
	moduleTable.updateModule();
	
	if ($('#content_id').val() != "") {
		$('.addNew').show();
	} else {
		$('.addNew').hide();
	}	
}

function siteMapReturn(NODE) {
	
	id = $(NODE).attr("id").replace("node", "");
	
	$('#' + idField).val(id);
	$('#' + titleField).val($(NODE).attr("title"));
	moduleTable.from = 0;
	updateModule();
}

function moduleSort(id, value){
	ajaxRequest(moduleTable.getRequestUrl() + "sort/", "value=" + value + "&id=" + id, updateModule);
}

function moduleEnable(id, value) {
	ajaxRequest(moduleTable.getRequestUrl() + "enable/", "value=" + value + "&id=" + id, updateModule);
}

function moduleEnableSelected(module, value) {

	returnArray = new Array();
	
	checkboxes = document.getElementsByTagName("input");
	for (i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].type == "checkbox" && checkboxes[i].name == module + "Box" && checkboxes[i].checked == true) {
			returnArray.push(checkboxes[i].id.substr(module.length + 1));
		}
	}

	if (returnArray.length > 0) {
		ajaxRequest(moduleTable.getRequestUrl() + "enable/", "value=" + value + "&id=" + encodeURIComponent(JSON.stringify(returnArray)), updateModule);
	}	
	
}

function moduleDelete(id) {
	if (ruSure(langStrings.getMsg('rusure_delete','Are yor sure want to delete this ?'))) {
		ajaxRequest(moduleTable.getRequestUrl() + "delete/", "id=" + id, updateModule);
	}
}

function changeFilter() {
	moduleTable.additionalParms = "&content_id=" + $('#content_id').val() + "&filterLang=" + $('#lang').val() + "&filterCountry=" + $('#country').val();
	moduleTable.updateModule();
	
	updateFilterValues();
}

function updateFilterValues() {
	if ($('#country').val()) {
		ajaxRequest(moduleTable.getRequestUrl() + 'updateLanguages/', 'value=' + $('#country').val() + "&filterLang=" + $('#lang').val(), function (data){
			$('#lang').find("option:not(:first-child)").remove();
			$('#lang').append(data);
		});
	}
}

function clearFilter() {
	$("#lang").val("0");
	$("#country").val("0");
	$("#content_id").val();
	$("#contentTitle").val();
	moduleTable.additionalParms = "&clear=true";
	moduleTable.updateModule();
}

function moduleDeleteSelected(module) {
	
	returnArray = new Array();
	
	checkboxes = document.getElementsByTagName("input");
	for (i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].type == "checkbox" && checkboxes[i].name == module + "Box" && checkboxes[i].checked == true) {
			returnArray.push(checkboxes[i].id.substr(module.length + 1));
		}
	}
	if (returnArray.length > 0) {
		if (ruSure(langStrings.getMsg('rusure_delete','Are yor sure want to delete this ?'))) {
			ajaxRequest(moduleTable.getRequestUrl() + "delete/", "id=" + encodeURIComponent(JSON.stringify(returnArray)), updateModule);
		}
	}	
	
}

function moduleEdit(id) {
	if (id) {
			idUrl = id + "/";
			window.location.href = moduleTable.getRequestUrl() + "edit/" + idUrl;	
	} else {
		if ($('#content_id').val() != "") {
			idUrl = id ? id + "/" : "?content_id=" + $('#content_id').val();
			window.location.href = moduleTable.getRequestUrl() + "edit/" + idUrl;	
		}
	}
		
	return false;
}

var saveType;

function checkFields(type) {
	result = true;
	
	if ($('#title').val() == '') {
		$('#title').css('background', '#f7b5b5');
		$('#title').focus();
		result = false;
	} else {
		$('#title').css('background', '#ffffff');
	}
	
	if ($('#question').val() == '') {
		$('#question').css('background', '#f7b5b5');
		$('#question').focus();
		result = false;
	} else {
		$('#question').css('background', '#ffffff');
	}
	if ($('#answer').val() == '') {
		$('#answer').css('background', '#f7b5b5');
		$('#answer').focus();
		result = false;
	} else {
		$('#answer').css('background', '#ffffff');
	}
	
	if (result) {
		saveType = type;
		
		saveData();
	}	
}

function saveData() {
	
	returnArray = new Object();

	$('btn *').removeAttr('onclick'); 
	
	$('.simple').each(function(n, element) {

		if($(element).attr('type') == 'checkbox') {
			if ($(element).is(':checked')) {
				returnArray[$(element).attr('id')] = $(element).val();
			}
			else {
				returnArray[$(element).attr('id')] = 0;
			}	
		} else {
			returnArray[$(element).attr('id')] = $(element).val();
		}

		
	});

	if (saveType == "apply") {
		idUrl = $('#id').val() ? $('#id').val() + "/" : "";
		ajaxRequest(moduleTable.getRequestUrl() + "save/" + idUrl, "value=" + encodeURIComponent(JSON.stringify(returnArray)), function () {});		
	}
	if (saveType == "save") {
		
		idUrl = $('#id').val() ? $('#id').val() + "/" : "";
		ajaxRequest(moduleTable.getRequestUrl() + "save/" + idUrl, "value=" + encodeURIComponent(JSON.stringify(returnArray)), function () {
			
			window.location.href = moduleTable.getRequestUrl() + '#content_id:' + $("#content_id").val() + '/';
		});
	}
}
