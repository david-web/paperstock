<?php

class paymentsData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	public $config;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "payments";
		
		$this->defLang = getDefaultLang();
		
		$this->dbTable = $this->cfg->getDbTable('orders');
		
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable() {
		
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"chekcbox" => array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleCheckboxLink'),
				'fields' => array('id'),
				'params' => array($this->name)
			),
			"id" => array(
					'sort' => false,
					'title' => gLA('id','ID'),
					'function' => '',
			),
			"price" => array(
				'sort' => false,
				'title' => gLA('price','Price'),
				'function' => '',
			),
			"payment_type" => array(
				'sort' => false,
				'title' => gLA('payment_type','Payment type'),
				'function' => '',
			),
			"created" => array(
					'sort' => false,
					'title' => gLA('created','Created'),
					'function' => 'convertDate',
					'fields'	=> array('created'),
					'params' => array('d-m-Y')
			),
			"paid" => array(
					'sort' => false,
					'title' => gLA('paid','Paid'),
					'function' => array(&$this, 'checkPaidStatus'),
					'fields'	=> array('paid')
			),
			"first_name" => array(
					'sort' => false,
					'title' => gLA('first_name','First name'),
					'function' => '',
			),
			"last_name" => array(
				'sort' => false,
				'title' => gLA('last_name','Last Name'),
				'function' => '',
			),
			"email" => array(
					'sort' => false,
					'title' => gLA('email','Email'),
					'function' => '',
			),
			"phone" => array(
					'sort' => false,
					'title' => gLA('phone','Phone'),
					'function' => '',
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLink'),
				'fields'	=> array('id')
			)
		);

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}	
		
		$search = $this->getSearchQuery();
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS o.*, c.*, o.id AS id 
							FROM `" . $this->dbTable['self'] . "` o
								LEFT JOIN " . $this->cfg->getDbTable('profiles') . " c ON (o.user_id = c.id) 			
							WHERE 1
							" . $search . $this->moduleTableSqlParms("o.id", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	/**
	 * Return html tag with actions buttons element link
	 *
	 * @param int	 element id
	 */
	public function moduleActionsLink($id) {
		$r = '';
	
		if ($this->cmsUser->haveUserRole("VIEW", $this->getModuleId())) {
			$r .= '<a class="edit" href="javascript:;" onclick="moduleView(\'' . $id . '\'); return false;">' . gLA('m_view','View') . '</a>';
		}
		else {
			$r .= '';
		}
		return $r;
	}
	
	public function checkPaidStatus($paid) {
		if ($paid) {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	
	public function getSearchQuery() {
		$search = '';
	
		if (getP('paid')) {
			$search .= " AND o.`paid` = '" . mres(getP('paid')) . "'";
			$_SESSION['filters'][$this->name]["paid"] = getP('paid');
		} elseif (isset($_SESSION['filters'][$this->name]["paid"])){
			$search .= " AND o.`paid` = '" . mres($_SESSION['filters'][$this->name]["paid"]) . "'";
		}
	
		if (getP('id')) {
			$search .= " AND o.id LIKE '%" . mres(getP('id')) . "%'";
			$_SESSION['filters'][$this->name]["id"] = getP('id');
		} elseif (isset($_SESSION['filters'][$this->name]["id"])){
			$search .= " AND o.id LIKE '%" . mres($_SESSION['filters'][$this->name]["id"]) . "%'";
		}
	
		if (getP('filterName')) {
			$search .= " AND c.first_name LIKE '%" . mres(getP('filterName')) . "%'";
			$_SESSION['filters'][$this->name]["filterName"] = getP('filterName');
		} elseif (isset($_SESSION['filters'][$this->name]["filterName"])){
			$search .= " AND c.first_name LIKE '%" . mres($_SESSION['filters'][$this->name]["filterName"]) . "%'";
		}
		
		if (getP('filterLastName')) {
			$search .= " AND c.last_name LIKE '%" . mres(getP('filterLastName')) . "%'";
			$_SESSION['filters'][$this->name]["filterName"] = getP('filterLastName');
		} elseif (isset($_SESSION['filters'][$this->name]["filterLastName"])){
			$search .= " AND c.last_name LIKE '%" . mres($_SESSION['filters'][$this->name]["filterLastName"]) . "%'";
		}
		
		if (getP('filterEmail')) {
			$search .= " AND c.email LIKE '%" . mres(getP('filterEmail')) . "%'";
			$_SESSION['filters'][$this->name]["filterEmail"] = getP('filterEmail');
		} elseif (isset($_SESSION['filters'][$this->name]["filterEmail"])){
			$search .= " AND c.email LIKE '%" . mres($_SESSION['filters'][$this->name]["filterEmail"]) . "%'";
		}
		
		if (getP('filterPhone')) {
			$search .= " AND c.phone LIKE '%" . mres(getP('filterPhone')) . "%'";
			$_SESSION['filters'][$this->name]["filterPhone"] = getP('filterPhone');
		} elseif (isset($_SESSION['filters'][$this->name]["filterPhone"])){
			$search .= " AND c.phone LIKE '%" . mres($_SESSION['filters'][$this->name]["filterPhone"]) . "%'";
		}
		
		if (getP('price_from')) {
			$search .= " AND o.price >= '" . mres(getP('price_from')) . "'";
			$_SESSION['filters'][$this->name]["price_from"] = getP('price_from');
		} elseif (isset($_SESSION['filters'][$this->name]["price_from"])){
			$search .= " AND o.price >= '" . $_SESSION['filters'][$this->name]["price_from"] . "'";
		}
		
		if (getP('price_to')) {
			$search .= " AND o.price <= '" . mres(getP('price_to')) . "'";
			$_SESSION['filters'][$this->name]["price_to"] = getP('price_to');
		} elseif (isset($_SESSION['filters'][$this->name]["price_to"])){
			$search .= " AND o.price <= '" . $_SESSION['filters'][$this->name]["price_to"] . "'";
		}
		
		if (getP('created_from')) {
			$search .= " AND o.created >= '" . mres($this->getDateFilter(getP('created_from'))) . "'";
			$_SESSION['filters'][$this->name]["created_from"] = getP('created_from');
		} elseif (isset($_SESSION['filters'][$this->name]["created_from"])){
			$search .= " AND o.created >= '" . $this->getDateFilter($_SESSION['filters'][$this->name]["created_from"]) . "'";
		}
		
		if (getP('created_to')) {
			$search .= " AND o.price <= '" . mres($this->getDateFilter(getP('created_to'))) . "'";
			$_SESSION['filters'][$this->name]["created_to"] = getP('created_to');
		} elseif (isset($_SESSION['filters'][$this->name]["created_to"])){
			$search .= " AND o.created <= '" . $this->getDateFilter($_SESSION['filters'][$this->name]["created_to"]) . "'";
		}
		
	
		return $search;
	}
	
	public function getDateFilter($date) {
		list($dd, $mm, $yy) = explode("-", $date);
		return mktime(0, 0, 0, $mm, $dd, $yy);
	}
	
	/**
	 * Delete news from DB
	 * 
	 * @param int/Array 	news id
	 */
	public function delete($id) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			deleteFromDbById($this->dbTable, $id);
		}		
	}
	
	/**
	 * Edit news in DB
	 * 
	 * @param int 	news id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();

		if(isset($id) && intval($id)) {
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "SELECT SQL_CALC_FOUND_ROWS o.*, c.*, o.id AS id 
							FROM `" . $this->dbTable['self'] . "` o
								LEFT JOIN " . $this->cfg->getDbTable('profiles') . " c ON (o.user_id = c.id) 			
							WHERE 1
								AND o.id = '" . mres($id) . "'";
		$query = new query($this->db, $dbQuery);		
			
			$data["edit"] = $query->getrow();
			
			$order = loadAppClass('orders', $this->app);
			
			$data["edit"]['order'] = $order->module->showTable($id);
			
		} 
		
		
		return $data;
	}
	
	/**
	 * Saving information in DB
	 * 
	 * @param int	 id, it's need if we are editing language
	 * @param array  information values
	 */
	public function save($id, $value) {
		
		$langValues = getP('valueLangs');
		$siteLangs = getSiteLangs();
		
		if (!$id) {
			$value["created"] = time();
		}
			
		$id = saveValuesInDb($this->dbTable, $value, $id);	
		

		return $id;
	}
	
}
?>