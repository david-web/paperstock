function updateModule() {
	
	moduleTable.updateModule();	
}

function moduleEnable(id, value) {
	ajaxRequest(moduleTable.getRequestUrl() + "enable/", "value=" + value + "&id=" + id, updateModule);
}



function moduleView(id) {
	idUrl = id ? id + "/" : "";
	window.location.href = moduleTable.getRequestUrl() + "edit/" + idUrl;	
	return false;
}

function changeFilter() {
	$('.filter').each(function(n, element) {
		moduleTable.additionalParms += "&" + $(element).attr('id') + "=" + $(element).val();
		
	});
	
	moduleTable.from = 0;
	
	updateModule();
}

function clearFilter() {
	$('.filter').each(function(n, element) {
		$(element).val('');
	});
	
	moduleTable.additionalParms = "&filter=clear";
	moduleTable.from = 0;
	
	updateModule();
}