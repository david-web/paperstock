<?php

class writersData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	public $config;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "writers";
		
		$this->defLang = getDefaultLang();
		
		$this->dbTable = $this->cfg->getDbTable('writers');
		
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable() {
		
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"chekcbox" => array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleCheckboxLink'),
				'fields' => array('id'),
				'params' => array('profile')
			),
			"id" => array(
					'sort' => false,
					'title' => gLA('id','ID'),
					'function' => '',
			),
			"email" => array(
				'sort' => false,
				'title' => gLA('email','Email'),
				'function' => '',
			),
			"first_name" => array(
				'sort' => false,
				'title' => gLA('first_name','First name'),
				'function' => '',
			),
			"last_name" => array(
				'sort' => false,
				'title' => gLA('last_name','Last Name'),
				'function' => '',
			),
			"country" => array(
					'sort' => false,
					'title' => gLA('country','Country'),
					'function' => '',
			),
			"phone" => array(
					'sort' => false,
					'title' => gLA('phone','Phone'),
					'function' => '',
			),
			"created" => array(
				'sort' => false,
				'title' => gLA('created','Created'),
				'function' => 'convertDate',
				'fields'	=> array('created'),
				'params' => array('d-m-Y')
			),
			"status" => array(
					'sort' => false,
					'title' => gLA('status','Status'),
					'function' => array(&$this, 'checkStatus'),
					'fields'	=> array('status')
			),
			"enable" => array(
				'sort' => false,
				'title' => gLA('m_enable','Enable'),
				'function' => array(&$this, 'moduleEnableLink'),
				'fields'	=> array('id', 'enable')
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLink'),
				'fields'	=> array('id')
			)
		);

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}	
		
		$search = $this->getSearchQuery();
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS * 
							FROM `" . $this->dbTable . "` 
							WHERE 1
							" . $search . $this->moduleTableSqlParms("id", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	public function checkStatus($status) {
		if ($status == '1') {
			return 'Being reviewed';
		} elseif ($status == '2') {
			return 'Aprooved';
		} elseif ($status == '3') {
			return 'Denied';
		} elseif ($status == '4') {
			return 'Revised';
		} else {
			return 'Canceled';
		}
	}
	
	public function getSearchQuery() {
		$search = '';
	
	
		if (getP('filterID')) {
			$search .= " AND id LIKE '%" . mres(getP('filterID')) . "%'";
			$_SESSION['filters'][$this->name]["filterID"] = getP('filterID');
		} elseif (isset($_SESSION['filters'][$this->name]["filterID"])){
			$search .= " AND id LIKE '%" . mres($_SESSION['filters'][$this->name]["filterID"]) . "%'";
		}
	
		if (getP('filterName')) {
			$search .= " AND first_name LIKE '%" . mres(getP('filterName')) . "%'";
			$_SESSION['filters'][$this->name]["filterName"] = getP('filterName');
		} elseif (isset($_SESSION['filters'][$this->name]["filterName"])){
			$search .= " AND first_name LIKE '%" . mres($_SESSION['filters'][$this->name]["filterName"]) . "%'";
		}
		
		if (getP('filterLastName')) {
			$search .= " AND last_name LIKE '%" . mres(getP('filterLastName')) . "%'";
			$_SESSION['filters'][$this->name]["filterName"] = getP('filterLastName');
		} elseif (isset($_SESSION['filters'][$this->name]["filterLastName"])){
			$search .= " AND last_name LIKE '%" . mres($_SESSION['filters'][$this->name]["filterLastName"]) . "%'";
		}
		
		if (getP('filterEmail')) {
			$search .= " AND email LIKE '%" . mres(getP('filterEmail')) . "%'";
			$_SESSION['filters'][$this->name]["filterEmail"] = getP('filterEmail');
		} elseif (isset($_SESSION['filters'][$this->name]["filterEmail"])){
			$search .= " AND email LIKE '%" . mres($_SESSION['filters'][$this->name]["filterEmail"]) . "%'";
		}
		
		if (getP('filterPhone')) {
			$search .= " AND phone LIKE '%" . mres(getP('filterPhone')) . "%'";
			$_SESSION['filters'][$this->name]["filterPhone"] = getP('filterPhone');
		} elseif (isset($_SESSION['filters'][$this->name]["filterPhone"])){
			$search .= " AND phone LIKE '%" . mres($_SESSION['filters'][$this->name]["filterPhone"]) . "%'";
		}
		
	
		return $search;
	}
	
	/**
	 * Enable or disable
	 * 
	 * @param int/array 	news id
	 * @param bool 			enable/disable value
	 */
	public function enable($id, $value) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `enable` = '" . $value . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}			
	}
	
	/**
	 * Delete news from DB
	 * 
	 * @param int/Array 	news id
	 */
	public function delete($id) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			deleteFromDbById($this->dbTable, $id);
		}		
	}
	
	/**
	 * Edit news in DB
	 * 
	 * @param int 	news id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();

		if(isset($id) && intval($id)) {
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "SELECT 
								w.*, 
								(SELECT count(ow.id) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE 1 AND ow.writer_id = w.id) AS total_applications,
								(SELECT SUM(ow.amount) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow LEFT JOIN `" . $this->cfg->getDbTable('orders', 'self') . "` owo ON (ow.order_id = owo.id AND owo.writer_id = ow.writer_id) WHERE 1 AND owo.status = 3 AND ow.status = 1 AND ow.writer_id = w.id) AS total_amount,
								(SELECT SUM(amount) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE 1 AND ow.status = 1 AND ow.writer_id = w.id AND ow.paid = 1) AS total_paid_amount,
								(SELECT AVG(rate) FROM `" . $this->cfg->getDbTable('orders', 'writers_rates') . "` ow WHERE ow.writer_id = w.id) AS rating,
								(SELECT count(id) FROM `" . $this->cfg->getDbTable('orders', 'self') . "` ow WHERE ow.writer_id = w.id AND status = 3) AS completed_orders,
								(SELECT count(ow.id) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow LEFT JOIN `" . $this->cfg->getDbTable('orders', 'self') . "` owo ON (ow.order_id = owo.id) WHERE 1 AND owo.writer_id = ow.writer_id AND ow.writer_id = w.id) AS applications		
							FROM `" . $this->dbTable . "` w
							WHERE 1
								AND w.`id` = '" . mres($id) . "'
							LIMIT 1";
			$query = new query($this->db, $dbQuery);		
			
			$data["edit"] = $query->getrow();
			
			$data["edit"]['familiar_with'] = json_decode($data["edit"]['familiar_with'], true);
			$data["edit"]['disciplines'] = json_decode($data["edit"]['disciplines'], true);
			$data["edit"]['status'] = $this->checkStatus($data["edit"]['status']);
			
			$order = loadAppClass('orders', $this->app);
				
			$data["edit"]['order'] = $order->module->showTable('', $id);
			$data["edit"]['tests'] = $this->loadTests($id);
			
		} 
		
		
		return $data;
	}
	
	public function loadTests($id) {
	
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('tests', 'self') . "` t
								LEFT JOIN `" . $this->cfg->getDbTable('tests', 'data') . "` td ON (t.id = td.test_id)
							WHERE 1
								AND t.enable = '1'
								AND td.lang = '" . $this->defLang . "'
							ORDER BY t.type ASC";
		$query = new query($this->db, $dbQuery);
	
	
		$data = array();
		$data['tests'] = $query->getArray();
		$data['userTests'] = $this->getUserTests($id);
	
		return $data;
	
	}
	
	private function getUserTests($id) {
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('tests', 'writers') . "` t
							WHERE writer_id = '" . mres($id) . "'";
		$query = new query($this->db, $dbQuery);
		return $query->getArray('test_id');
	}
	
	public function publishManualy($id) {
		$dbQuery = "UPDATE `" . $this->cfg->getDbTable('tests', 'writers') . "`
									SET `comment` = `admin_comment`
									WHERE id = '" . mres(getP('test')) . "'";
		new query($this->db, $dbQuery);
		
		$dbQuery = "UPDATE `" . $this->dbTable . "`
									SET `status` = '4'
									WHERE id = '" . mres($id) . "'";
		new query($this->db, $dbQuery);
	}
	
	public function setStatus($id) {
	
		$dbQuery = "UPDATE `" . $this->dbTable . "`
									SET `status` = '" . getP('status') . "'
									WHERE id = '" . mres($id) . "'";
		new query($this->db, $dbQuery);
		
		if (getP('status') == 2) {
			sendWriterAproovedEmail($id);
		} elseif (getP('status') == 3) {
			sendWriterBlockEmail($id);
		}
	}
	
	/**
	 * Saving information in DB
	 * 
	 * @param int	 id, it's need if we are editing language
	 * @param array  information values
	 */
	public function save($id, $value) {
			
		$id = saveValuesInDb($this->dbTable, $value, $id);	
		
		if (getP('tests') && is_array(getP('tests'))) {
			foreach (getP('tests') AS $key => $data) {
				if (isset($data['score'])) {
					$dbQuery = "UPDATE `" . $this->cfg->getDbTable('tests', 'writers') . "`
									SET `score` = '" . mres($data['score']) . "'
									WHERE id = '" . mres($key) . "'";
					new query($this->db, $dbQuery);
				} else {
					$dbQuery = "UPDATE `" . $this->cfg->getDbTable('tests', 'writers') . "`
									SET `admin_comment` = '" . mres($data['admin_comment']) . "'
									WHERE id = '" . mres($key) . "'";
					new query($this->db, $dbQuery);
				}
			}
		}
		

		return $id;
	}
	
	public function orderPaid($id) {
		if ($id && getP('writer')) {
			$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` 
							WHERE 1
								AND `order_id` = '" . mres($id) . "'
								AND `writer_id` = '" . mres(getP('writer')) . "'		
							LIMIT 1";
			$query = new query($this->db, $dbQuery);
			$data = $query->getrow();
			
			$dbQuery = "UPDATE `" . $this->cfg->getDbTable('orders', 'writers') . "`
							SET `paid` = 1
							WHERE 1
								AND `order_id` = '" . mres($id) . "'
								AND `writer_id` = '" . mres(getP('writer')) . "'";
			$query = new query($this->db, $dbQuery);

		}
	}
	
}
?>