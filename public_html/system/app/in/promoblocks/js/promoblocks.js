function updateModule() {
    moduleTable.updateModule();
}
function moduleEdit(id) {
    window.location.href = moduleTable.getRequestUrl() + 'edit/' + (id && parseInt(id) ? parseInt(id) : 0) + '/';
}

function setCountry() {
	if ($('#country').val()) {
		params = {};
		params['country'] = $('#country').val();
		ajaxRequest(moduleTable.getRequestUrl() + 'edit/', params,function (data) {
			if (data) {
				$('#editBlock').html(data);
			}
		});
	}
}

function moduleDelete(id) {
    if(ruSure(langStrings.getMsg('rusure_delete','Are yor sure want to delete this ?'))) {
        ajaxRequest(moduleTable.getRequestUrl() + 'delete/' + id + '/', updateModule);
    }
}
function moduleSort(id, sort) {
    ajaxRequest(moduleTable.getRequestUrl() + 'sort/' + id + '/', 'sort=' + sort, updateModule);
}
function moduleEnable(id, value) {
	ajaxRequest(moduleTable.getRequestUrl() + 'enable/', 'enabled=' + value + '&id=' + id, updateModule);
}

function changeFilter() {
	moduleTable.additionalParms = "&filterLang=" + $('#lang').val() + "&filterCountry=" + $('#country').val();
	moduleTable.updateModule();
	
	updateFilterValues();
}

function updateFilterValues() {
	if ($('#country').val()) {
		ajaxRequest(moduleTable.getRequestUrl() + 'updateLanguages/', 'value=' + $('#country').val() + "&filterLang=" + $('#lang').val(), function (data){
			$('#lang').find("option:not(:first-child)").remove();
			$('#lang').append(data);
		});
	}
}

function clearFilter() {
	$("#lang").val("0");
	$("#country").val("0");
	moduleTable.additionalParms = "&clear=true";
	moduleTable.updateModule();
}

function scrollToTop() {
    $(this).scrollTop($('div.content').position().top);
}
function checkFields(type) {
    $('#errorBlock').hide();
    $('#okBlock').hide();
    
    $(".error").removeClass("error");
    
    var errorStr = "";
    
    $errors = 0;
    $int_regex = /^\d+$/;
    
    if(jQuery.trim($('#lang').val()) == 0) {
        errorStr = errorStr + "<div class='err_lang'>" + langStrings.getMsg('m_choose_lang','Please choose language') + "</div>";
        $('#lang').addClass("error");
        $errors++;
    }
   
    if(!$('#image').size()) {
        errorStr = errorStr + "<div class='err_image'>" + langStrings.getMsg('m_choose_image','Please upload image') + "</div>";
        $("#imageDiv").parent().addClass("error");
        $errors++;
    }
    if(jQuery.trim($('#seconds').val())) {
        if(!$int_regex.test(jQuery.trim($('#seconds').val())) || jQuery.trim($('#seconds').val()) <= 0) {
            errorStr = errorStr + "<div class='err_seconds'>" + langStrings.getMsg('m_enter_valid_seconds','Please enter numeric seconds value') + "</div>";
            $('#seconds').addClass("error");
            errors++;
        }
    }
    
    if ($('#with_text').is(':checked')) {
    	 if(jQuery.trim($('#title').val()) == '') {
		    errorStr = errorStr + "<div class='err_title'>" + langStrings.getMsg('m_enter_title','Please enter title') + "</div>";
		    $('#title').addClass("error");
		    $errors++;
		}
    	 if(jQuery.trim($('#lead').val()) == '') {
	        errorStr = errorStr + "<div class='err_lead'>" + langStrings.getMsg('m_enter_lead','Please enter decription') + "</div>";
	        $('#lead').addClass("error");
	        $errors++;
	    }
    }
    
    if($errors) {
        $('#errorBlock').html(errorStr);
        $('#errorBlock').show();
        scrollToTop();
        return false;
    } else {
        save(type);
        return true;
    }
}
function save(type) {
    returnData = new Object();
    
    returnData['country'] = jQuery.trim($('#country').val());
    returnData['lang'] = jQuery.trim($('#lang').val());
    returnData['title'] = jQuery.trim($('#title').val());
    returnData['lead'] = jQuery.trim($('#lead').val());
    returnData['url'] = jQuery.trim($('#url').val());
    returnData['url_id'] = jQuery.trim($('#url_id').val());
    returnData['doc_id'] = jQuery.trim($('#doc_id').val());
    returnData['target'] = jQuery.trim($('#target').val());
    returnData['seconds'] = jQuery.trim($('#seconds').val());
    returnData['image'] = jQuery.trim($('#image').val());
    
    if ($('#with_text').is(':checked')) {
    	returnData['with_text'] = jQuery.trim($('#with_text').val());
    } else {
    	returnData['with_text'] = '';
    }
    
    // Bug #4
    returnData['alt'] = jQuery.trim($('#alt').val());
    
    $.post(
        moduleTable.getRequestUrl() + 'save/' + $id + '/',
        'values=' + encodeURIComponent(JSON.stringify(returnData)),
        function(data) {
            var returndata = JSON.parse(data);
            if(returndata != null && returndata != undefined && returndata.id != null && returndata.id != undefined) {
                if(type == 'apply') {
                    $('#okBlock').show();
                    scrollToTop();
                } else {
                    window.location.href = moduleTable.getRequestUrl();
                }
                
                return true;
            }

            return false;
        }
    );
}

$(document).ready(function(){
    $("#lang").change(function(){
        if($(this).val()){
            $(this).removeClass("error");
            if($(".err_lang").length){
                $(".err_lang").remove();
                if($("#errorBlock div").length){
                    $("#").hide();
                }
            }
        }
    });
});