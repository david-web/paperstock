<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @link		http://adweb.lv
 * @version		1
 */
// ------------------------------------------------------------------------

class data extends Module_cms {

    private $table;
    private $defaultSeconds = 5;
    public $imagesConfig = array(	
    								'width' => '1400', 
    								'height' => '500',
    								/*'1024' => array('width' => '680', 'height' => '348'),
								    '768' => array('width' => '728', 'height' => '372'),
								    '480' => array('width' => '440', 'height' => '225')*/);

    /**
     * Constructor
     */
    public function __construct() {
		
        parent :: __construct();
        $this->name = "promoblocks";
        $this->table = $this->cfg->getDbTable('promoblock', 'self');
        
        $this->imagesConfig = $this->cmsConfig->getImageConfig('promoblock');
        
        check_upload_path($this->imagesConfig['original']['upload_path']);
    }

    /**
     * Returns HTML as promo blocks table content     
     */
    public function showTable() {
        
        $where = array();

		// filter by language
        if(getP('filterLang') !== false){
            switch(getP('filterLang')) {
                case '0':
                    if(isset($_SESSION[$this->name]["filter"]["lang"])) {
                        unset($_SESSION[$this->name]["filter"]["lang"]);
                    }
                    break;
               default:
                   $_SESSION[$this->name]["filter"]["lang"] =  getP('filterLang');
            }
        }
        $lang = isset($_SESSION[$this->name]["filter"]["lang"]) ? $_SESSION[$this->name]["filter"]["lang"] : "0";
        
		if ($lang != "0") {
			$where[] = 'p.`lang` = \'' . mres($lang) . '\''; 
		}

    	// filter by language
        if(getP('filterCountry') !== false){
            switch(getP('filterCountry')) {
                case '0':
                    if(isset($_SESSION[$this->name]["filter"]["country"])) {
                        unset($_SESSION[$this->name]["filter"]["country"]);
                    }
                    break;
               default:
                   $_SESSION[$this->name]["filter"]["country"] =  getP('filterCountry');
            }
        }
        $country = isset($_SESSION[$this->name]["filter"]["country"]) ? $_SESSION[$this->name]["filter"]["country"] : "0";
        
		if ($country != "0") {
			$where[] = 'p.`country` = \'' . mres($country) . '\''; 
		}
        
        $whereSql = implode(' AND ', $where);

        $table = array(
            'id' => array(
                'sort' => true,
                'title' => gLA('m_id','Id'),
            ),
            'countryName' => array(
                'sort' => true,
                'title' => gLA('m_country', 'Country')
            ),
            'lang' => array(
                'sort' => true,
                'title' => gLA('m_lang','language')
            ),
            'title' => array(
                'sort' => true,
                'title' => gLA('m_title','Title'),
                'function' => array(&$this, 'clear'),
                'fields' => array('title')
            ),
            'url' => array(
                'sort' => true,
                'title' => gLA('m_target_url','Target url'),
                'function' => array(),
                'fields' => array()
            ),
            'enabled' => array(
                'sort' => true,
                'title' => gLA('m_enable','Enable'),
                'function' => array(&$this, 'moduleEnableLink'),
                'fields' => array('id', 'enabled')
            ),
            'actions' => array(
                'sort' => false,
                'title' => gLA('m_actions','Actions'),
                'function' => array(&$this, 'moduleActionsLink'),
                'fields' => array('id')
            )
        );
        
        if($lang != '0' && $country != '0'){
            $table["sort"] = array(
                'sort' => false,
                'title' => '',
                'function' => array(&$this, 'moduleSortLinks'),
                'fields' => array('id')
            );
        }
	

        // SQL request for promo blocks
        $sql = "SELECT p.*, c.title AS countryName 
        				FROM `" . $this->table . "` p
        				LEFT JOIN `ad_countries` c ON (c.id = p.country)"
                		.  ($whereSql ? " WHERE $whereSql " : "");
        $query = new query($this->db, $sql);

        // Set total count
        $return['rCounts'] = $query->num_rows();

        // Add ORDER BY clouse
        if($lang != '0' && $country != '0'){
        	$sql .= $this->moduleTableSqlParms('sort', 'ASC');	
        } else {
        	$sql .= $this->moduleTableSqlParms('id', 'DESC');
        }
        
        $query = new query($this->db, $sql);

        // Create table
        $this->cmsTable->createTable($table, $query->getArray());
        $return['html'] = $this->cmsTable->returnTable;
        
        

        return $return;
    }

    /**
     * Returns array of data for promo block
     * @param   $id AS int, default 0
     */
    public function edit($id = 0, $country = 0) {

        $data = array();
        $data['edit']['id'] = 0;
        $data['edit']['seconds'] = $this->defaultSeconds;

        if ($id) {
            $query = new query($this->db, "SELECT * FROM `" . $this->table . "` WHERE `id` = " . intval($id));
            if ($query->num_rows()) {
                $data['edit'] = $query->getrow();
                $data['langs'] = getSiteLangsByCountry($data['edit']['country']);
            }
        } else {
        	$data['edit']['country'] = $country;
        	$data['langs'] = getSiteLangsByCountry($country);
        }
               
        
        $data['edit']['uploadFolder'] = $this->imagesConfig['original']['upload_path'];

        return $data;
    }
    
    public function setPBCountry() {
    	$data = array();
    	$data["countries"]["data"] = getSiteCountries();
    	return $data;
    }

    /**
     * Save/update promo block
     * @param   $id AS int - product id if exists, default 0 - new promo block
     * @param   $values AS json serialized array
     */
    public function save($id, $values) {

        // Decode data to get array; Add slashes as well
        $values = addSlashesDeep(jsonDecode($values));

        if (!isset($values["image"])) {

            $values["image"] = '';
        } 

        // Bug #5
        $values['seconds'] = intval($values['seconds']) > 0 ? $values['seconds'] : $this->defaultSeconds;
        
        $reSort = 0;
        if (!$id) {
            $values['sort'] = 1;
            $reSort = 1;
        } else {
			//delete old images from folder
			$dbQuery = "SELECT `image` FROM `" . $this->table . "` WHERE `id` = " . $id;
			$query = new query($this->db, $dbQuery);
			$image = $query->getOne();
			
			if($image != '' && $image != $values["image"]){
				deleteFileFromFolder(AD_SERVER_UPLOAD_FOLDER . $this->imagesConfig['original']['upload_path'] . $image);
			}
		}

        $id = saveValuesInDb($this->table, $values, $id);
        
        if ($reSort) {
            reSort($this->table, $id, array(
                'lang' => $values['lang'],
            	'country' => $values['country'],
                    )
            );
        }

        return $id;
    }

    /**
     * Delete existing row from table
     * @param   $id AS int 
     */
    public function delete($id) {

        if ($id) {
			//Find images and delete them from file
			$dbQuery = "SELECT `image` FROM `" . $this->table . "` WHERE `id` = " . $id;
			$query = new query($this->db, $dbQuery);
			$image = $query->getOne();
			
			if($image != ''){
				deleteFileFromFolder(AD_SERVER_UPLOAD_FOLDER . $this->imagesConfig['original']['upload_path'] . $image);
			}
			
            deleteFromDbById($this->table, $id);

            return true;
        }

        return false;
    }

    /**
     * Enable or disable promo block
     * @param   $id AS int or array
     * @param   $enabled AS boolen
     */
    public function enable($id, $enabled) {

        if (!is_numeric($id)) {
            $id = addSlashesDeep(jsonDecode($id));
        }

        if (!empty($id)) {
            new query($this->db, "UPDATE `" . $this->table . "` SET `enabled` = '" . $enabled . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(", ", $id) . ")" : "`id` = " . intval($id)));

            return true;
        }

        return false;
    }

    /**
     * Changes sort values for table row
     * @param   $id AS int
     * @param   $sort AS int - up or down     
     */
    public function sort($id, $sort) {

        $query = new query($this->db, "SELECT * FROM `" . $this->table . "` WHERE `id` = " . intval($id));
        if ($query->num_rows()) {
            $content = $query->getrow();

            $lang = $content['lang'];
            $country = $content['country'];

            // Set sorting parameters
            if ($sort == 'down') {
                $sql_parm = '>';
                $sql_parm2 = 'ASC';
            } else {
                $sql_parm = '<';
                $sql_parm2 = 'DESC';
            }

            // Select next or previous row
            $sql = "SELECT `id`, `sort` FROM `" . $this->table . "` WHERE `sort` " . $sql_parm . " '" . $content['sort'] . "' AND `lang` = '" . mres($lang) . "' AND `country` = '" . mres($country) . "' ORDER BY `sort` " . $sql_parm2 . " LIMIT 0, 1";
            $query->query($this->db, $sql);

            if ($query->num_rows()) {
                $info = $query->getrow();

                new query($this->db, "UPDATE `" . $this->table . "` SET `sort` = " . intval($content['sort']) . " WHERE `id` = " . intval($info['id']));
                new query($this->db, "UPDATE `" . $this->table . "` SET `sort` = " . intval($info['sort']) . " WHERE `lang` = '" . mres($lang) . "' AND `country` = '" . mres($country) . "' AND `id` = " . intval($id));

                return true;
            }
        }

        return false;
    }

    /**
     * Returns next sort values for table
     * @param   $table AS string
     * @param   $params AS array, default empty
     * @author  Jānis Šakars <janis.sakars@efumo.lv>
     */
    private function getNextSort($table, $params = array()) {

        $mdb = &loadLibClass('db');

        $where = '';
        if (sizeof($params)) {
            foreach ($params as $key => $val) {
                $where .= " AND `" . $key . "` = '" . mres($val) . "'";
            }
        }
        if ($where != '') {
            $where = ' WHERE ' . substr($where, 5) . ' ';
        }

        $query = new query($mdb, "SELECT `sort` FROM `" . mres($table) . "` " . $where . " ORDER BY `sort` DESC LIMIT 0, 1");
        if ($query->num_rows()) {
            return $query->getOne() + 1;
        }

        return 1;
    }
    
    public function clear($text){
        return stripslashes($text);
    }
    
	public function getSiteCountriesDropDown($selected = ''){
        $values = array();
        
		$categories = getSiteCountries();
		
		foreach($categories as $category){
			$values[$category['id']] = clear($category['title']);
		}
		return dropDownFieldOptions($values, $selected, true);
    }

}

?>