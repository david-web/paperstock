<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		Rolands Eņģelis <rolands@efumo.lv>
 * @copyright   Copyright (c) 2012, Efumo.
 * @link		http://adweb.lv
 * @version		1
 */

// ------------------------------------------------------------------------

class promoblocks extends Module_cms {

    /**
     * $module - Object of module admin class
     */
    public $module;

    /**
     * Constructor
     */
    public function __construct() {

        parent :: __construct();
        $this->name = get_class($this);

        require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
        $this->module = new data();

        $this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');

        $this->tpl->assign('MODULE_HEAD', $this->tpl->output('head'));
        $this->tpl->assign('MAIN_URL', '/' . $this->uri->segment(0) . '/' . $this->uri->segment(1) . '/');
        $this->tpl->assign('moduleName', $this->getModuleTitle());
        
    	if (getP('clear')) {
			unset($_SESSION[$this->name]['filter']);
		}
        
        // Include head/bottom actions
        switch($this->uri->segment(3)) {
            case '':
            	
                $data = $this->createModuleTableNav(true, false, true);
                $country_id = isset($_SESSION[$this->name]['filter']['country']) ? $_SESSION[$this->name]['filter']['country'] : '';
                $data['countries'] = $this->module->getSiteCountriesDropDown($country_id);       
                
                $lang = '';
        		if(isset($_SESSION[$this->name]["filter"]["lang"])){
                    $lang = $_SESSION[$this->name]["filter"]["lang"];
                } 
                $data['langs'] = getAllSiteLangs($lang);
                
                $this->includeTemplate($data);
                break;
        }
    }

    /**
     * Index function
     */
    public function run() {
        
        // Variables used in requests
        $action = $this->uri->segment(3);
        $id = getP('id') ? getP('id') : ($this->uri->segment(4) ? $this->uri->segment(4) : 0);
        $values = getP('values');
        $enabled = getP('enabled');
        $sort = getP('sort');
        
        switch($action) {
            // Promo block table
            case 'updateLanguages':
                if($this->cmsUser->haveUserRole('VIEW', $this->getModuleId())) {
                    $this->noLayout(true);
                    
                	if (getP('value')) {
        				jsonSend(getSiteLangsByCountryDD(getP('value'), getP('filterLang')));
       				}
					
                }
                break;
            case 'moduleTable':
                if($this->cmsUser->haveUserRole('VIEW', $this->getModuleId())) {
                    $this->noLayout(true);
					jsonSend($this->module->showTable());
                }
                break;    
            // Edit promo block
            case 'edit':
                if($this->cmsUser->haveUserRole('EDIT', $this->getModuleId())) {
                    if ($id || getP('country')) {
                    	$data = $this->module->edit($id, getP('country'));
                    	if (getP('country')) {
                    		jsonSend($this->tpl->output("edit", $data));	
                    	} else {
                    		$this->includeTemplate($data, 'edit');	
                    	}
                    	
                    } else {
                    	$data = $this->module->setPBCountry();
                    	$this->includeTemplate($data, 'country');
                    }	
                }
                break;
            // Save/update promo block
            case 'save':
                if($this->cmsUser->haveUserRole(($id ? 'EDIT' : 'ADD'), $this->getModuleId())) {
                    $this->noLayout(true);
                    jsonSend(array(
                        'id'    => $this->module->save($id, $values)
                    ));
                }
                break;
            // Enable/disable promo block
            case 'enable':
                if($this->cmsUser->haveUserRole('EDIT', $this->getModuleId())) {
                    $this->noLayout(true);
                    $this->module->enable($id, $enabled);
                }
                break;
            // Delete promo block
            case 'delete':
                if($this->cmsUser->haveUserRole('DELETE', $this->getModuleId())) {
                    $this->noLayout(true);
                    $this->module->delete($id);
                }
                break;
            // Sort promo blocks
            case 'sort':
                if($this->cmsUser->haveUserRole('EDIT', $this->getModuleId())) {
                    $this->noLayout(true);
                    $this->module->sort($id, $sort);
                }
                break;
        }
    }
}
?>
