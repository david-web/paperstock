<?php 

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */
	
	/**
	 * Generates siteData output
	 * @return string HTML
	 */
	class orderpricesData extends Module_cms {
		
		public $priceConfig = array(

				'1221' => 'Writing from Scrach',
				'1310' => 'Regular',
				'1339' => 'College',
				'1340' => 'Undergraduate',
				'1341' => 'Postgraduate',
				'1342' => 'Phd',
				'1332' => 'Admissions',
				'1337' => 'Other',
				'1222' => 'Editing / Proofreading',
				'1224' => 'Problem solving',
				'1223' => 'Multiple Choices',
		);
		
		public $deadlineConfig = array(
				'32400' => '9h',
				'86400' => '24h',
				'172800' => '2d',
				'259200' => '3d',
				'518400' => '6d',
				'864000' => '10d',
				'1209600' => '14d',
		);
		
		public $result;
		
		public function __construct() {
			
			parent :: __construct();
			$this->name 	= 'orderprices';
			$this->sdTable 	= 'ad_sitedata';
			$this->sdvTable = 'ad_sitedata_values';
			$this->fieldName = 'ordersPrices';
			
			
			/*require_once(AD_APP_FOLDER . 'out/orders/config/config.php');
			
			$dbQuery = "SELECT * FROM `" . $this->sdTable . "` WHERE `name` = 'ordersPrices'";
			$query = new query($this->db, $dbQuery);
			$query->getrow();
			new query($this->db, "DELETE FROM `" . $this->sdvTable . "` WHERE `fid` = '". $query->field('id') ."'");
			new query($this->db, "INSERT INTO `" . $this->sdvTable . "` SET `value` = '". json_encode($priceConfig) ."', `fid` = '". $query->field('id') ."'");
			*/
		}
		
		/**
		 * Get all siteData from Db 
		 * @return string HTML
		 */
		public function getData () {	
			
			$dbQuery = "SELECT sd.value FROM `" . $this->sdTable . "` s
										LEFT JOIN  `" . $this->sdvTable . "` sd ON (sd.fid = s.id)
									WHERE `name` = 'ordersPrices'";
			$query = new query($this->db, $dbQuery);
			$prices = $query->getOne();
			$prices = json_decode($prices, true);
			

			$data['prices'] = $prices;
			$data['priceConfig'] = $this->priceConfig;
			$data['deadlines'] = $this->deadlineConfig;

			return $data;
		}
		
		
		/**
		 * Save all site data
		 * @param array posted values
		 */
		public function save($values) {
		
			$dbQuery = "SELECT * FROM `" . $this->sdTable . "` WHERE `name` = 'ordersPrices'";
			$query = new query($this->db, $dbQuery);
			$query->getrow();
			new query($this->db, "DELETE FROM `" . $this->sdvTable . "` WHERE `fid` = '". $query->field('id') ."'");
			new query($this->db, "INSERT INTO `" . $this->sdvTable . "` SET `value` = '". json_encode($values) ."', `fid` = '". $query->field('id') ."'");
		}
	}

?>