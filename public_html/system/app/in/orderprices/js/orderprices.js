function saveData() {
	
	returnArray = {};

	$('btn *').removeAttr('onclick'); 
	
	$('.simple').each(function(n, element) {
		
		if (typeof(returnArray[$(this).attr("id")]) == 'undefined') {
			returnArray[$(this).attr("id")] = {};
        }
		
		if (typeof(returnArray[$(this).attr("id")][$(this).attr("rel")]) == 'undefined') {
			returnArray[$(this).attr("id")][$(this).attr("rel")] = {};
        }
		
		if (typeof(returnArray[$(this).attr("id")][$(this).attr("rel")][$(this).attr("name")]) == 'undefined') {
			returnArray[$(this).attr("id")][$(this).attr("rel")][$(this).attr("name")] = {};
        }
		
		if (typeof(returnArray[$(this).attr("id")][$(this).attr("rel")][$(this).attr("name")][$(this).data("deadline")]) == 'undefined') {
			returnArray[$(this).attr("id")][$(this).attr("rel")][$(this).attr("name")][$(this).data("deadline")] = $(element).val();
        }

		
	});
	
	sendData = {};
	sendData['value'] = returnArray;

	ajaxRequest(moduleTable.getRequestUrl() + "save/", sendData, function () {
		
		//window.location.href = moduleTable.getRequestUrl();
	});
}
