<?php 

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

	class orderprices extends Module_cms {
		
		public $module;
		
		function __construct() {
			
			parent :: __construct();
			$this->name = get_class($this);
			require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
			$this->module = new orderpricesData();
			$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
			$this->tpl->assign("MODULE_HEAD", $this->tpl->output("head"));
			$this->tpl->assign("MAIN_URL", "/" . $this->uri->segment(0) . "/" . $this->uri->segment(1) . "/");
			$this->tpl->assign("moduleName", $this->getModuleTitle());
			$data = $this->module->getData();
			$this->includeTemplate($data);
		}
		
		function run() {
			
			$action = $this->uri->segment(3);
			
			switch ($action) {
				case "save" :
					if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
						$this->noLayout(true);
							
						$values = getP("value");
						//pR($values, '', true);	
						$this->module->save( $values );
					}
					break;
			}
		}
	}

?>