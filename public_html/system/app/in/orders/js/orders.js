function updateModule() {
	
	moduleTable.updateModule();	
}

function moduleEnableFile(id, value) {
	ajaxRequest(moduleTable.getRequestUrl() + "enableFile/", "value=" + value + "&id=" + id, function (data){
		//window.location.href += "#files_form";
		window.location.reload();
	});
}

function moduleDeleteFile(id, value) {
	ajaxRequest(moduleTable.getRequestUrl() + "deleteFile/", "id=" + id, function (data){
		//window.location.href += "#files_form";
		window.location.reload();
	});
}

function modulePreviewFile(id) {
	ajaxRequest(moduleTable.getRequestUrl() + "previewFile/", "id=" + id, function (data){
		//window.location.href += "#files_form";
		window.location.reload();
	});
}

function moduleOrderCancel(id, reload) {
	if (ruSure('Are yor sure want to cancel this order ?')) {
		ajaxRequest(moduleTable.getRequestUrl() + "cancelOrder/", "id=" + id, function (){
			if (reload) {
				window.location.reload();
			} else {
				updateModule();
			}
		});
	}
}

function moduleAssignWriter(writerId) {
	if (ruSure('Are yor sure want to assign writer this order ?')) {
		
		sendData = {};
		sendData['writerId'] = writerId;
		
		idUrl = $('#id').val() ? $('#id').val() + "/" : "";
		ajaxRequest(moduleTable.getRequestUrl() + "assign/" + idUrl, sendData, function (){
			window.location.reload();
		});
	}
}

function moduleAccept(id, reload) {
	if (ruSure('Are yor sure want to accept this order ?')) {
		ajaxRequest(moduleTable.getRequestUrl() + "accept/", "id=" + id, function (){
			if (reload) {
				window.location.reload();
			} else {
				updateModule();
			}
		});
	}
}

function moduleView(id) {
	idUrl = id ? id + "/" : "";
	window.location.href = moduleTable.getRequestUrl() + "view/" + idUrl;	
	return false;
}

var saveType;

function checkFields(type) {
	result = true;
	
	$('.required').each(function(n, element) {
		if ($(this).val() == '') {
			$(this).css('background', '#f7b5b5');
			$(this).focus();
			result = false;
		} else {
			$(this).css('background', '#ffffff');
		}
	});

	if (result) {
		saveType = type;
		
		saveData();
	}	
}

function saveData(type) {
	
	returnArray = {};
	returnMsg = {};
	returnFiles = {};

	$('btn *').removeAttr('onclick'); 
	
	$('.simple').each(function(n, element) {
		
		if ($(element).val()) {
			returnArray[$(element).attr('id')] = $(element).val();
		}
		
	});
	
	$('.files').each(function(n, element) {
		
		if ($(element).val()) {
			
			
			if($(element).attr('type') == 'checkbox') {
				if ($(element).is(':checked')) {
					returnFiles[$(element).attr('id')] = $(element).val();
				}
				else {
					returnFiles[$(element).attr('id')] = 0;
				}	
			} else {
				returnFiles[$(element).attr('id')] = $(element).val();
			}
		}
		
	});
	
	$('.message').each(function(n, element) {
		
		if ($(element).val()) {
			returnMsg[$(element).attr('rel')] = $(element).val();
		}
		
	});
	
	sendData = {};
	sendData['value'] = returnArray;
	sendData['message'] = returnMsg;
	sendData['files'] = returnFiles;

	idUrl = $('#id').val() ? $('#id').val() + "/" : "";
	ajaxRequest(moduleTable.getRequestUrl() + "save/" + idUrl, sendData, function () {
		if (type == 'save') {
			window.location.href = moduleTable.getRequestUrl();
		} else {
			window.location.reload();
		}		
	});
}

function changeFilter() {
	$('.filter').each(function(n, element) {
		moduleTable.additionalParms += "&" + $(element).attr('id') + "=" + $(element).val();
		
	});
	
	moduleTable.from = 0;
	
	updateModule();
}

function clearFilter() {
	$('.filter').each(function(n, element) {
		$(element).val('');
	});
	
	moduleTable.additionalParms = "&filter=clear";
	moduleTable.from = 0;
	
	updateModule();
}