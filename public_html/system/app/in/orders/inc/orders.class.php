<?php

class ordersData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	public $config;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "forms";
		
		$this->defLang = getDefaultLang();
		
		$this->dbTable = $this->cfg->getDbTable('orders', 'self');
		
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable($id = '', $writerId = '') {
		
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"chekcbox" => array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleCheckboxLink'),
				'fields' => array('id'),
				'params' => array('profile')
			),
			"id" => array(
				'sort' => true,
				'title' => gLA('order_id','Order ID'),
				'function' => '',
			),
			"price" => array(
				'sort' => true,
				'title' => gLA('price','Price'),
				'function' => '',
			),
			"number_of_pages" => array(
				'sort' => false,
				'title' => gLA('number_of_pages','Items'),
				'function' => '',
			),
			"type_of_work" => array(
				'sort' => false,
				'title' => gLA('type_of_work','Type of work'),
				'function' => '',
			),
			"subject" => array(
				'sort' => false,
				'title' => gLA('subject','Subject'),
				'function' => '',
			),
			"topic" => array(
				'sort' => false,
				'title' => gLA('topic','Topic'),
				'function' => '',
			),
			"paid" => array(
				'sort' => false,
				'title' => gLA('paid','Paid'),
				'function' => array(&$this, 'checkPaidStatus'),
				'fields'	=> array('paid')
			),
			"first_draft_deadline" => array(
				'sort' => false,
				'title' => gLA('first_draft_deadline','Deadline'),
				'function' => array(&$this, 'checkDeadline'),
				'fields'	=> array('first_draft_deadline')
			),
			"status" => array(
				'sort' => false,
				'title' => gLA('status','Status'),
				'function' => array(&$this, 'checkStatus'),
				'fields'	=> array('status')
			),
			"writer_appointed" => array(
				'sort' => false,
				'title' => gLA('writer_appointed','Writers app.'),
				'function' => array(&$this, 'checkWritersStatus'),
				'fields'	=> array('writer_id')
			),
			"user_id" => array(
				'sort' => false,
				'title' => gLA('user_id','Customer'),
			),
			"created" => array(
				'sort' => false,
				'title' => gLA('created','Created'),
				'function' => 'convertDate',
				'fields'	=> array('created'),
				'params' => array('d-m-Y H:i:s')
			),
			"writers" => array(
					'sort' => false,
					'title' => gLA('writers','Writers'),
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLinkOrder'),
				'fields'	=> array('id', 'status', 'paid')
			)
		);
		
		$where = '';
		$subselect = '';
		if ($id) {
			
			unset($table['chekcbox']);
			unset($table['actions']);
			$where = (is_array($id) ? " AND `id` IN (" . implode(",", $id) . ")" : " AND `id` = '" . $id . "'");
		}
		
		if ($writerId) {
			unset($table['chekcbox']);
			unset($table['actions']);
			unset($table['paid']);
			
			$table['writer_amount'] = array(
				'sort' => false,
				'title' => gLA('writer_amount','Writer amount'),
			);
			
			$table['actions'] = array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsWirtersLink'),
				'fields'	=> array('id', 'writer_paid', 'writer_id')
			);
			
			$where = (is_array($writerId) ? " AND `writer_id` IN (" . implode(",", $writerId) . ")" : " AND `writer_id` = '" . $writerId . "'");
			$subselect = " , (SELECT `amount` FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = o.id AND ow.writer_id = '" . $writerId . "') AS writer_amount";
			$subselect .= " , (SELECT `paid` FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = o.id AND ow.writer_id = '" . $writerId . "') AS writer_paid";
		}

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}	
		
		$search = $this->getSearchQuery();			
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS o.*, (SELECT count(id) FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow WHERE ow.order_id = o.id) AS writers " . $subselect . "
							FROM `" . $this->dbTable . "` o
							WHERE 1
							" . $where . $search . $this->moduleTableSqlParms("id", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	public function getSearchQuery() {
		$search = '';
		
		if (getP('filterStatus') == 'all') {
			if (isset($_SESSION['filters'][$this->name]["filterStatus"])) {
				unset($_SESSION['filters'][$this->name]["filterStatus"]);
			}
		}
	
		if (getP('filterStatus') && getP('filterStatus') != 'all') {
			$search .= " AND `status` = '" . mres(getP('filterStatus')) . "'";
			$_SESSION['filters'][$this->name]["filterStatus"] = getP('filterStatus');
		} elseif (isset($_SESSION['filters'][$this->name]["filterStatus"])){
			$search .= " AND `status` = '" . mres($_SESSION['filters'][$this->name]["filterStatus"]) . "'";
		} 
		
		
		
		if (getP('filterPaid')) {
			
			if (getP('filterPaid') == 'yes') {
				$search .= " AND `paid` = '1'";
			} elseif (getP('filterPaid') == 'no') {
				$search .= " AND `paid` = '0'";
			}

			$_SESSION['filters'][$this->name]["filterPaid"] = getP('filterPaid');
		} elseif (isset($_SESSION['filters'][$this->name]["filterPaid"])){
			
			if ($_SESSION['filters'][$this->name]["filterPaid"] == 'yes') {
				$search .= " AND `paid` = '1'";
			} elseif ($_SESSION['filters'][$this->name]["filterPaid"] == 'no') {
				$search .= " AND `paid` = '0'";
			}
		}
	
		if (getP('filterSearch')) {
			$search .= " AND (work_details LIKE '%" . mres(getP('filterSearch')) . "%' OR topic LIKE '%" . mres(getP('filterSearch')) . "%' OR subject LIKE '%" . mres(getP('filterSearch')) . "%')";
			$_SESSION['filters'][$this->name]["filterSearch"] = getP('filterSearch');
		} elseif (isset($_SESSION['filters'][$this->name]["filterSearch"])){
			$search .= " AND (work_details LIKE '%" . mres($_SESSION['filters'][$this->name]["filterSearch"]) . "%' OR topic LIKE '%" . mres($_SESSION['filters'][$this->name]["filterSearch"]) . "%' OR subject LIKE '%" . mres($_SESSION['filters'][$this->name]["filterSearch"]) . "%')";
		}
		
		if(getP('deadlineFrom')) {
			$search .= " AND `first_draft_deadline` >= '". mres(strtotime(getP('deadlineFrom') . ' 00:00:00')) ."'";
			$_SESSION['filters'][$this->name]["deadlineFrom"] = getP('deadlineFrom');
		} elseif (isset($_SESSION['filters'][$this->name]["deadlineFrom"])) {
			$search .= " AND `first_draft_deadline` >= '" . mres(strtotime($_SESSION['filters'][$this->name]["deadlineFrom"] . ' 00:00:00')) . "'";
		}
		
		if(getP('deadlineTo')) {
			$search .= " AND `first_draft_deadline` <= '". mres(strtotime(getP('deadlineTo') . ' 23:59:59')) ."'";
			$_SESSION['filters'][$this->name]["deadlineTo"] = getP('deadlineTo');
		} elseif (isset($_SESSION['filters'][$this->name]["deadlineTo"])){
			$search .= " AND `first_draft_deadline` <= '" . mres(strtotime($_SESSION['filters'][$this->name]["deadlineTo"] . ' 23:59:59')) . "'";
		}
	
		return $search;
	}
	
	public function moduleActionsWirtersLink($id, $paid, $writer) {
		$r = '';
	
		if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
			if (!$paid) {
				$r .= '<a class="edit" href="javascript:;" onclick="moduleWriterPaid(\'' . $id . '\', \'' . $writer . '\'); return false;">' . gLA('paid','Paid') . '</a>';
			}
			
		}
		else {
			$r .= '';
		}
	
	
		return $r;
	}
	
	/**
	 * Return html tag with actions buttons element link
	 *
	 * @param int	 element id
	 */
	public function moduleActionsLinkOrder($id, $status, $paid) {
		$r = '';
	
		if ($this->cmsUser->haveUserRole("VIEW", $this->getModuleId())) {
			$r .= '<a class="edit" href="javascript:;" onclick="moduleView(\'' . $id . '\'); return false;">' . gLA('m_view','View') . '</a>';
		}
		else {
			$r .= '';
		}
		
		if ($status == 1) {
			if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
				$r .= '<a class="edit" href="javascript:;" onclick="moduleAccept(\'' . $id . '\'); return false;">' . gLA('m_accept','Accept') . '</a>';
			} else {
				$r .= '';
			}
		}
		
		if ($status != 3 && $status != 4) {
			if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
				$r .= '<a class="edit" href="javascript:;" onclick="moduleOrderCancel(\'' . $id . '\'); return false;">' . gLA('m_cancel','Cancel') . '</a>';
			} else {
				$r .= '';
			}
		}

	
	
	
		return $r;
	}
	
	public function checkPaidStatus($paid) {
		if ($paid) {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	
	public function checkWritersStatus($status) {
		if ($status) {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	
	public function checkDeadline($deadline) {
		/*$diff = $deadline - time();
		
		return date("d", $diff) . 'd ' . date("H", $diff) . 'h';*/
		
		$diff = $deadline - time();
		
		if ($diff > 0) {
		
			$dtF = new DateTime("@0");
			$dtT = new DateTime("@$diff");
		
			$row['diffDays'] = $dtF->diff($dtT)->format('%a');
			$row['diffHours'] = $dtF->diff($dtT)->format('%h');
		} else {
			$row['diffDays'] = '0';
			$row['diffHours'] = '0';
		}
		
		return $row['diffDays'] . 'd ' . $row['diffHours'] . 'h';
	}
	
	public function checkStatus($status) {
		if ($status == '1') {
			return 'Pending...';
		} elseif ($status == '2') {
			return 'In progress';
		} elseif ($status == '3') {
			return 'Finished';
		} else {
			return 'Canceled';
		}
	}
	
	/**
	 * Enable or disable
	 * 
	 * @param int/array 	news id
	 * @param bool 			enable/disable value
	 */
	public function enableFile($id, $value) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->cfg->getDbTable('orders', 'files') . "` SET `enabled` = '" . $value . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}			
	}
	
	public function changeStatus($id, $status) {
	
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
	
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->cfg->getDbTable('orders', 'self') . "` SET `status` = '" . mres($status) . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}
		
		return true;
	}
	
	public function assign($id, $writerId) {
	
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->cfg->getDbTable('orders', 'self') . "` SET `writer_id` = '" . mres($writerId) . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}
	
		return true;
	}
	
	/**
	 * Delete news from DB
	 *
	 * @param int/Array 	news id
	 */
	public function deleteFile($id) {
	
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
	
		if (!empty($id)) {
			deleteFromDbById($this->cfg->getDbTable('orders', 'files'), $id);
		}
		
		return true;
	}
	
	/**
	 * Edit news in DB
	 * 
	 * @param int 	news id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();

		if(isset($id) && intval($id)) {
			
			check_upload_path('orders/' . md5($id) . '/');
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "SELECT o.*, o.created AS createdTime, p.*, o.id AS id FROM `" . $this->dbTable . "` o
									LEFT JOIN `" . $this->cfg->getDbTable('profiles') . "` p ON (o.user_id = p.id)
								WHERE 1 
									AND o.`id` = '" . $id . "'
								LIMIT 0,1";
			$query = new query($this->db, $dbQuery);		
			
			$data["edit"] = $query->getrow();
			$data["edit"]["timeleft"] = $this->checkDeadline($data["edit"]["first_draft_deadline"]);
			$data["edit"]["statusOrig"] = $data["edit"]["status"];
			//$data["edit"]["status"] = $this->checkStatus($data["edit"]["status"]);
			$data["edit"]["paid"] = $this->checkPaidStatus($data["edit"]["paid"]);
			$data["edit"]["additional_extras"] = @json_decode($data["edit"]["additional_extras"], true);
			
			$data["edit"]['messages'][1] = $this->getOrderMessages($data["edit"]['id'], 1);
			$data["edit"]['messages'][2] = $this->getOrderMessages($data["edit"]['id'], 2);
			$data["edit"]['files'][1] = $this->getOrderFiles($data["edit"]['id'], 1);
			$data["edit"]['files'][2] = $this->getOrderFiles($data["edit"]['id'], 2);
			
			$data["edit"]['writers'] = $this->getOrderWriters($data["edit"]['id']);
			
			$data["edit"]['writers_deadline'] = ($data["edit"]['first_draft_deadline'] - $data["edit"]['createdTime']) * 0.8 + $data["edit"]['createdTime'];

			
		} 
		
		
		return $data;
	}
	
	public function getOrderWriters($id) {
		$result = array();
		
		$dbQuery = "SELECT *, 
						(SELECT AVG(rate) FROM `" . $this->cfg->getDbTable('orders', 'writers_rates') . "` ow WHERE ow.writer_id = w.id) AS rating,
						(SELECT count(id) FROM `" . $this->cfg->getDbTable('orders', 'self') . "` ow WHERE ow.writer_id = w.id AND status = 3) AS completed_orders
							FROM `" . $this->cfg->getDbTable('orders', 'writers') . "` ow
								LEFT JOIN `" . $this->cfg->getDbTable('writers') . "` w ON (ow.writer_id = w.id)
							WHERE 1
								AND `order_id` = '" . mres($id) . "'";
		$query = new query($this->db, $dbQuery);
		while ($row = $query->getrow()) {
			$row['disciplines'] = json_decode($row['disciplines'], true);
			$result[$row['id']] = $row;
		}
		return $result;
	}
	
	public function getOrderMessages($id, $type = 1) {
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('orders', 'messages') . "`
							WHERE 1
								AND `order_id` = '" . mres($id) . "'
								AND `type` = '" . $type . "'";
		$query = new query($this->db, $dbQuery);
		return $query->getArray('id');
	}
	
	public function getOrderFiles($id, $type = 1) {
		
		$result = array();
		
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('orders', 'files') . "`
							WHERE 1
								AND `order_id` = '" . mres($id) . "'
								AND `type` = '" . $type . "'";
		$query = new query($this->db, $dbQuery);
		while ($row = $query->getrow()) {
			
			$row['ext'] = substr(strtolower(strrchr($row['filename'], ".")), 1);
			
			$result[$row['id']] = $row;
		}
		return $result;
	}
	
	public function previewFile($id) {
		$dbQuery = "SELECT `filename`, `order_id`
							FROM `" . $this->cfg->getDbTable('orders', 'files') . "`
							WHERE 1
								AND `id` = '" . mres($id) . "'";
		$query = new query($this->db, $dbQuery);
		if ($query->num_rows() > 0) {
			$query->getrow();
			
			$this->image = &loadLibClass('image');
			
			$in = AD_SERVER_UPLOAD_FOLDER . 'orders/' . md5($query->field('order_id')) . '/' . $query->field('filename');
			$out = AD_SERVER_UPLOAD_FOLDER . 'orders/' . md5($query->field('order_id')) . '/preview-' . $query->field('filename');
			
			list($w, $h) = getimagesize($in);
			
			$this->image->resizeImg($in, $out, $w, $h, null, array(
				array(
					'file' => AD_SERVER_IMAGE_FOLDER . 'design/rules_bg.png',
					'position' => IR_POS_CENTER,
					'x' => '-1',
					'y' => '-1',
				),
			));
			
			$dbData['order_id'] = $query->field('order_id');
			$dbData['user_id'] = '';
			$dbData['preview'] = 1;
			$dbData['type'] = 1;
			$dbData['filename'] = 'preview-' . $query->field('filename');
			$dbData['created'] = time();
				
			return saveValuesInDb($this->cfg->getDbTable('orders', 'files'), $dbData);
			
			
		}
	}
	
	/**
	 * Saving information in DB
	 * 
	 * @param int	 id, it's need if we are editing language
	 * @param array  information values
	 */
	public function save($id, $value) {
		
		if ($id) {
			
			//if (!empty($value['admin_comment'])) {
				$id = saveValuesInDb($this->dbTable, $value, $id);
			//}
			
			
			if (getP('message/1')) {
				$dbData['order_id'] = $id;
				$dbData['user_id'] = '';
				$dbData['message'] = getP('message/1');
				$dbData['type'] = 1;
				$dbData['created'] = time();
					
				saveValuesInDb($this->cfg->getDbTable('orders', 'messages'), $dbData);
			}
			
			if (getP('message/2')) {
				$dbData['order_id'] = $id;
				$dbData['user_id'] = '';
				$dbData['message'] = getP('message/2');
				$dbData['type'] = 2;
				$dbData['created'] = time();
					
				saveValuesInDb($this->cfg->getDbTable('orders', 'messages'), $dbData);
			}
			
			if (getP('files/filename_customers')) {
				$dbData['order_id'] = $id;
				$dbData['user_id'] = '';
				$dbData['enabled'] = '0';
				$dbData['final'] = getP('files/final');;
				$dbData['type'] = '1';
				$dbData['filename'] = getP('files/filename_customers');
				$dbData['created'] = time();
					
				saveValuesInDb($this->cfg->getDbTable('orders', 'files'), $dbData);
			}
			
			if (getP('files/filename_writers')) {
				$dbData['order_id'] = $id;
				$dbData['user_id'] = '';
				$dbData['enabled'] = '0';
				$dbData['type'] = '2';
				$dbData['filename'] = getP('files/filename_writers');
				$dbData['created'] = time();
					
				saveValuesInDb($this->cfg->getDbTable('orders', 'files'), $dbData);
			}
		}
		
		

		return $id;
	}
	
	public function createStatusList($sSel = "") {
		
		$values = array(
			'1' => 'Pending',
			'2' => 'In Progress',
			'3' => 'Finished',
			'4' => 'Canceled',
		);
	
		return dropDownFieldOptions($values, $sSel, true);
	
	}
	
}
?>