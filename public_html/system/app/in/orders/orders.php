<?php

class orders extends Module_cms {
		
	/**
	 * $module - Object of module admin class
	 */
	public $module;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		
   		parent :: __construct();
		$this->name = get_class($this);
		
		require_once(AD_APP_FOLDER . $this->app . '/' . $this->name . '/inc/' . $this->name . '.class.php');
		$this->module = new ordersData();
		
		$this->tpl->setTmplDir(AD_APP_FOLDER . $this->app . '/' . $this->name . '/tmpl/');
		
		if (getP('filter') == 'clear') {
			if (isset($_SESSION['filters'][$this->name])){
				unset($_SESSION['filters'][$this->name]);
			}
			$_SESSION['filters'][$this->name]['itemsFrom'] = 0;
		}
		$this->tpl->assign("moduleFrom", isset($_SESSION['filters'][$this->name]["itemsFrom"]) ? $_SESSION['filters'][$this->name]["itemsFrom"] : 0);
		
		$this->tpl->assign("langs", get_site_langs_with_code());

		// Including and preparing needs templates
		switch ($this->uri->segment(3)) {								
			case "view" : 
				$this->tpl->assign("leadTextLength", getSiteData('lead_text_lenght','News','lead text lenght','500','text'));
				$this->tpl->assign("MODULE_HEAD", $this->tpl->output("head"));
				$this->tpl->assign("MAIN_URL", "/" . $this->uri->segment(0) . "/" . $this->uri->segment(1) . "/");
				$this->tpl->assign("moduleName", $this->getModuleTitle());				

				break;
			case "" : 
				$this->tpl->assign("MODULE_HEAD", $this->tpl->output("head"));
				$this->tpl->assign("MAIN_URL", "/" . $this->uri->segment(0) . "/" . $this->uri->segment(1) . "/");
				$this->tpl->assign("moduleName", $this->getModuleTitle());
				$this->tpl->assign("sList", $this->module->createStatusList(getP("filterStatus")));

				$data = $this->createModuleTableNav(false, false, true);		
				$this->includeTemplate($data);
				break;			
		}
		
	}
	
	public function run() {

		$action = $this->uri->segment(3);
		
		$id = getP("id") ? getP("id") : $this->uri->segment(4);
		$value = getP("value");
		
		switch ($action) {
			case "moduleTable" : 
				if ($this->cmsUser->haveUserRole("VIEW", $this->getModuleId())) {
					$this->noLayout(true);
					
					
					jsonSend($this->module->showTable());

				}
				break;
			case "enableFile" : 
				if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
					$this->noLayout(true);
					
					$this->cmsLog->writeLog($this->getModuleName(), "enable file, id=" . $id . " value=" . $value);
					jsonSend($this->module->enableFile($id, $value));
				}
				break;
			case "cancelOrder" :
				if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
					$this->noLayout(true);
						
					$this->cmsLog->writeLog($this->getModuleName(), "cancel order, id=" . $id);
					$this->module->changeStatus($id, 4);
					sendOrderCanceledEmail($id);
				}
				break;
			case "accept" :
				if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
					$this->noLayout(true);
			
					$this->cmsLog->writeLog($this->getModuleName(), "accept order, id=" . $id);
					$this->module->changeStatus($id, 2);
					sendOrderAcceptedEmail($id);
				}
				break;
			case "assign" :
				if ($this->cmsUser->haveUserRole("EDIT", $this->getModuleId())) {
					$this->noLayout(true);
						
					$this->cmsLog->writeLog($this->getModuleName(), "assign order, id=" . $id);
					$this->module->assign($id, getP('writerId'));
					sendAppointedEmail($id);
				}
				break;
			case "deleteFile" :
				if ($this->cmsUser->haveUserRole("DELETE", $this->getModuleId())) {
					$this->noLayout(true);
						
					$this->cmsLog->writeLog($this->getModuleName(), "delete file, id=" . $id);
					jsonSend($this->module->deleteFile($id));
				}
				break;
			case "previewFile" :
				if ($this->cmsUser->haveUserRole("DELETE", $this->getModuleId())) {
					$this->noLayout(true);
			
					$this->cmsLog->writeLog($this->getModuleName(), "make preview file, id=" . $id);
					jsonSend($this->module->previewFile($id));
				}
				break;
			case "view" : 
				if ($this->cmsUser->haveUserRole($id ? "EDIT" : "ADD", $this->getModuleId())) {
							
					$data = $this->module->edit($id);
					$this->includeTemplate($data, 'view');
				}
				break;
			case "save" : 
				if ($this->cmsUser->haveUserRole($id ? "EDIT" : "ADD", $this->getModuleId())) {
					$this->noLayout(true);
					$this->cmsLog->writeLog($this->getModuleName(), "save, id=" . $id);
					
					$result["id"] = $this->module->save($id, $value);
					jsonSend($result);
				}
				break;	
		}
	
	}
}

?>
