<?php

class formsData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	public $config;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "forms";
		
		$this->defLang = getDefaultLang();
		
		$this->dbTable = $this->cfg->getDbTable('forms', 'self');
		$this->dbTableData = $this->cfg->getDbTable('forms', 'data');
		$this->dbValuesTable = $this->cfg->getDbTable('forms', 'values');
		$this->dbValuesDataTable = $this->cfg->getDbTable('forms', 'values_data');
		
		$this->initConfig();
		
	}
	
	private function initConfig() {
		
		$this->config['types'] = array(
			'text' => gLA('types_text', 'Text'),
			'short_text' => gLA('types_sort_text', 'Short text'),
			'textarea' => gLA('types_textarea', 'Textarea'),
			'select' => gLA('types_select', 'Select'),
			'extra' => gLA('types_extra', 'Extra'),
			'tabselect' => gLA('types_tabselect', 'Tab select'),
			'pages' => gLA('types_pages', 'Pages'),
			'hourdate' => gLA('types_hourdate', 'Hour / Date'),
			'phone' => gLA('types_phone', 'Phone'),
			'password' => gLA('types_password', 'Password'),
			'radio' => gLA('types_radio', 'Radio'),
			'checkbox' => gLA('types_checkbox', 'Checkbox'),
		);
		
		$this->config['modules'] = array(
				'order' => gLA('modules_order', 'Order'),
				'apply' => gLA('modules_apply', 'Apply'),
				'profile' => gLA('modules_profile', 'Profile'),
				'profile_edit' => gLA('modules_profile_edit', 'Profile edit'),
		);
		
		$this->config['steps'] = array(
				'profile' => array(),
				'order' => array(1, 2, 3),
				'apply' => array(1, 2, 3),
		);
		
		$this->config['form_filters'] = $this->getCorrectMethods('form.filters', 'filter_');
		$this->config['form_errors'] = $this->getCorrectMethods('form.errors', 'error_');
		$this->config['form_selactions'] = $this->getCorrectMethods('form.selactions', 'action_');
		
		$this->config['parents'] = $this->getAllParents();
	}
	
	private function getCorrectMethods($class, $prefix) {
		
		$this->{$class} = loadLibClass($class);
		
		$methods = array();
		foreach (get_class_methods($this->{$class}) AS $method) {
			if (strpos($method, $prefix) === 0) {
				
				$method = explode("_", $method);
				$methods[] = $method[1];
			}
		}
		
		return $methods;
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable() {
		
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"chekcbox" => array(
				'sort' => false,
				'title' => '',
				'function' => array(&$this, 'moduleCheckboxLink'),
				'fields' => array('id'),
				'params' => array('news')
			),
			"name" => array(
				'sort' => false,
				'title' => gLA('m_name','Name'),
				'function' => 'clear',
				'fields'	=> array('name')
			),
			"type" => array(
				'sort' => false,
				'title' => gLA('m_type','Type'),
				'function' => '',
			),
			"module" => array(
				'sort' => false,
				'title' => gLA('module','Module'),
				'function' => '',
				'fields'	=> array('module'),
			),
			"created" => array(
					'sort' => false,
					'title' => gLA('created','Created'),
					'function' => 'convertDate',
					'fields'	=> array('created'),
					'params' => array('d-m-Y H:i:s')
			),
			"enable" => array(
				'sort' => false,
				'title' => gLA('m_enable','Enable'),
				'function' => array(&$this, 'moduleEnableLink'),
				'fields'	=> array('id', 'enable')
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLink'),
				'fields'	=> array('id')
			)
		);

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}			
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS f.*, fd.name 
							FROM `" . $this->dbTable . "` f
								LEFT JOIN `" . $this->dbTableData . "` fd ON (f.id = fd.form_id)
							WHERE 1
								AND fd.lang = '" . $this->defLang . "'
							" . $this->moduleTableSqlParms("id", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	public function getAllParents() {
		$dbQuery = "SELECT * FROM `" . $this->dbTable . "` f
								LEFT JOIN `" . $this->dbTableData . "` fd ON (f.id = fd.form_id)
							WHERE 1
								AND fd.lang = '" . $this->defLang . "'";
		$query = new query($this->db, $dbQuery);
		
		return $query->getArray('id');
	}
	
	/**
	 * Enable or disable
	 * 
	 * @param int/array 	news id
	 * @param bool 			enable/disable value
	 */
	public function enable($id, $value) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `enable` = '" . $value . "' WHERE " . (is_array($id) ? "`id` IN (" . implode(",", $id) . ")" : "`id` = '" . $id . "'");
			$query = new query($this->db, $dbQuery);
		}			
	}
	
	/**
	 * Delete news from DB
	 * 
	 * @param int/Array 	news id
	 */
	public function delete($id) {
		
		if (!is_numeric($id)) {
			$id = addSlashesDeep(jsonDecode($id));
		}
		
		if (!empty($id)) {
			deleteFromDbById($this->dbTable, $id);
		}		
	}
	
	/**
	 * Edit news in DB
	 * 
	 * @param int 	news id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();
		$data['config'] = $this->config;
		$data['configJson'] = json_encode($this->config);

		if(isset($id) && intval($id)) {
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "SELECT * FROM `" . $this->dbTable . "` 
								WHERE 1 
									AND `id` = '" . $id . "'
								LIMIT 0,1";
			$query = new query($this->db, $dbQuery);		
			
			$data["edit"] = $query->getrow();
			$data["edit"]['form_errors'] = json_decode($data["edit"]['form_errors'], true);
			$data["edit"]['form_filters'] = json_decode($data["edit"]['form_filters'], true);
			
			$dbQuery = "SELECT * FROM `" . $this->dbTableData . "`
												WHERE `form_id`= '" . mres($id) . "'";
			$query = new query($this->db, $dbQuery);
			$data["edit"]['data'] = $query->getArray('lang');
			
			
			$dbQuery = "SELECT * FROM `" . $this->dbValuesTable . "`
												WHERE `form_id`= '" . mres($id) . "'";
			$query = new query($this->db, $dbQuery);
			while ($row = $query->getrow()) {
				
				$row['show_fields'] = json_decode($row['show_fields'], true);
				
				$dbQuery = "SELECT * FROM `" . $this->dbValuesDataTable . "`
												WHERE `value_id`= '" . mres($row['id']) . "'";
				$queryData = new query($this->db, $dbQuery);
				$row['data'] = $queryData->getArray('lang');
				
				$data["edit"]['values'][] = $row;
				
			}
			
			
		} 
		
		
		return $data;
	}
	
	/**
	 * Saving information in DB
	 * 
	 * @param int	 id, it's need if we are editing language
	 * @param array  information values
	 */
	public function save($id, $value) {
		
		$langValues = getP('valueLangs');
		$siteLangs = getSiteLangs();
		
		if (!$id) {
			$value["created"] = time();
			$value["sort"] = $this->getNextSort();
		}
		
		if (isset($value['form_filters'])) {
			$value['form_filters'] = json_encode($value['form_filters']);
		} else {
			$value['form_filters'] = json_encode(array());
		}
		
		if (isset($value['form_errors'])) {
			$value['form_errors'] = json_encode($value['form_errors']);
		} else {
			$value['form_errors'] = json_encode(array());
		}
			
			
		$id = saveValuesInDb($this->dbTable, $value, $id);	
		
		deleteFromDbById($this->dbTableData, $id, 'form_id');
		foreach ($siteLangs AS $key => $values) {
			$data = array(
					'form_id' => $id,
					'lang' => $values['lang'],
					'name' => $langValues['name'][$values['lang']],
			);
				
			saveValuesInDb($this->dbTableData, $data);
		
		}

		$this->saveValues($id, getP('values'));

		return $id;
	}
	
	private function saveValues($id, $values) {
	
		$dbQuery = "DELETE FROM `" . $this->dbValuesTable . "` WHERE `form_id` = '" . mres($id) . "'";
		new query($this->db, $dbQuery);
			
		if	($id && $values) {
				
			$siteLangs = getSiteLangs();
			for ($i = 0; $i < count($values); $i++) {
				
				$dbData = array();

				$dbData[] = " `id` = '" . mres(trim($values[$i]['id'])) . "' ";
				$dbData[] = " `form_id` = '" . $id . "' ";
				$dbData[] = " `price` = '" . mres(trim($values[$i]['price'])) . "' ";
				$dbData[] = " `parent` = '" . mres(trim($values[$i]['parent'])) . "' ";
				
				if (isset($values[$i]['show_fields'])) {
					$dbData[] = " `show_fields` = '" . json_encode($values[$i]['show_fields']) . "' ";
				} else {
					$dbData[] = " `show_fields` = '" . json_encode(array()) . "' ";
				}
				
				$dbQuery = "INSERT INTO `" . $this->dbValuesTable . "` SET " . implode(',', $dbData);
				$query = new query($this->db, $dbQuery);
				
				$ValueId = $this->db->get_insert_id();
	
				foreach ($siteLangs AS $key => $lang) {
	
					$dbData = array();
	
					$dbData[] = " `value_id` = '" . $ValueId . "' ";
					$dbData[] = " `lang` = '" . $lang['lang'] . "' ";
					$dbData[] = " `name` = '" . mres(trim($values[$i]['name'][$lang['lang']])) . "' ";	
					$dbData[] = " `hint` = '" . mres(trim($values[$i]['hint'][$lang['lang']])) . "' ";
						
	
					$dbQuery = "INSERT INTO `" . $this->dbValuesDataTable . "` SET " . implode(',', $dbData) .
									"ON DUPLICATE KEY UPDATE " . implode(',', $dbData);
					new query($this->db, $dbQuery);
	
				}
	
			}
		}
			
	}
	
	/**
	 * Getting next sort id for add new new
	 * 
	 * @param int		content news id, default value = 0
	 */
	private function getNextSort($cId = "0") {
		
		$dbQuery = "SELECT MAX(sort) AS sort FROM `" . $this->dbTable . "`";
		$query = new query($this->db, $dbQuery);
		
		return $query->getOne() + 1;
	}
	
	/**
	 * Changing news sort order
	 * 
	 * @param int		news id
	 * @param string	sort changing value
	 */
	public function changeSort($id, $value) {
		
		$dbQuery = "SELECT * FROM `" . $this->dbTable . "` WHERE `id` = '" . $id . "'";
                $query = new query($this->db, $dbQuery);
		$content = $query->getrow();
		
		if ($value == "down") {
			$sqlParm = "<";
			$sqlParm2 = "DESC";	
		}
		else {
			$sqlParm = ">";
			$sqlParm2 = "ASC";
		}
		
		$dbQuery = "SELECT `id`, `sort` FROM `" . $this->dbTable . "` WHERE `sort` " . $sqlParm . " '" . $content['sort'] . "' ORDER BY `sort` " . $sqlParm2 . " LIMIT 0,1";
		$query->query($this->db, $dbQuery);
		if ($query->num_rows() > 0) {
			$info = $query->getrow();
			
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `sort` = '" . $content['sort'] . "' WHERE `id` = '" . $info['id'] . "'";
			$query->query($this->db, $dbQuery);
			
			$dbQuery = "UPDATE `" . $this->dbTable . "` SET `sort` = '" . $info['sort'] . "' WHERE `id` = '" . $id . "'";
			$query->query($this->db, $dbQuery);
			
		}
	}
	
}
?>