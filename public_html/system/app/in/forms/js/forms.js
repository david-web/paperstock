
function createBla() {
	return;
	//test = $('#name_en').val();
	//test = test.toLowerCase().replace(/ /g, '_');
	//$('#db_field').val(test);
}

function updateModule() {
	
	moduleTable.updateModule();	
}

function moduleSort(id, value){
	ajaxRequest(moduleTable.getRequestUrl() + "sort/", "value=" + value + "&id=" + id, updateModule);
}

function moduleEnable(id, value) {
	ajaxRequest(moduleTable.getRequestUrl() + "enable/", "value=" + value + "&id=" + id, updateModule);
}

function moduleEnableSelected(module, value) {

	returnArray = new Array();
	
	checkboxes = document.getElementsByTagName("input");
	for (i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].type == "checkbox" && checkboxes[i].name == module + "Box" && checkboxes[i].checked == true) {
			returnArray.push(checkboxes[i].id.substr(module.length + 1));
		}
	}

	if (returnArray.length > 0) {
		ajaxRequest(moduleTable.getRequestUrl() + "enable/", "value=" + value + "&id=" + encodeURIComponent(JSON.stringify(returnArray)), updateModule);
	}	
	
}

function moduleDelete(id) {
	if (ruSure(langStrings.getMsg('rusure_delete','Are yor sure want to delete this ?'))) {
		ajaxRequest(moduleTable.getRequestUrl() + "delete/", "id=" + id, updateModule);
	}
}

function moduleDeleteSelected(module) {
	
	returnArray = new Array();
	
	checkboxes = document.getElementsByTagName("input");
	for (i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].type == "checkbox" && checkboxes[i].name == module + "Box" && checkboxes[i].checked == true) {
			returnArray.push(checkboxes[i].id.substr(module.length + 1));
		}
	}
	if (returnArray.length > 0) {
		if (ruSure(langStrings.getMsg('rusure_delete','Are yor sure want to delete this ?'))) {
			ajaxRequest(moduleTable.getRequestUrl() + "delete/", "id=" + encodeURIComponent(JSON.stringify(returnArray)), updateModule);
		}
	}	
	
}

function moduleEdit(id) {
	idUrl = id ? id + "/" : "";
	window.location.href = moduleTable.getRequestUrl() + "edit/" + idUrl;	
	return false;
}

var saveType;

function checkFields(type) {
	result = true;
	
	$('.required').each(function(n, element) {
		if ($(this).val() == '') {
			$(this).css('background', '#f7b5b5');
			$(this).focus();
			result = false;
		} else {
			$(this).css('background', '#ffffff');
		}
	});

	if (result) {
		saveType = type;
		
		saveData();
	}	
}

function saveData() {
	
	returnArray = {};
	returnArrayLangs = {};
	returnValues = {};

	$('btn *').removeAttr('onclick'); 
	
	$('.simple').each(function(n, element) {
		
		if($(this).attr("rel")){
            if (typeof(returnArrayLangs[$(this).attr("name")]) == 'undefined') {
            	returnArrayLangs[$(this).attr("name")] = {};
            }

            
            returnArrayLangs[$(this).attr("name")][$(this).attr("rel")] = $(this).val(); 
        } else {
        	if($(element).attr('type') == 'checkbox') {
    			if ($(element).is(':checked')) {
    				returnArray[$(element).attr('id')] = $(element).val();
    			}
    			else {
    				returnArray[$(element).attr('id')] = 0;
    			}	
    		} else {
    			returnArray[$(element).attr('id')] = $(element).val();
    		}
        }
		

		
	});
	
	i = 0;
    $('.add-value').each(function(n, element) {
    	
    	returnValues[i] = {};
    	
    	$(element).find('input, select').each(function() {
    		
    		if ($(this).attr("data")) {
    			returnAttrsTexts[i]['option'] = $(this).attr("data");
    		}
    		if ($(this).attr("data-attr")) {
    			returnAttrsTexts[i]['attribute'] = $(this).attr("data-attr");
    		}
    		
			if ($(this).attr("rel")) {
				
				if (typeof(returnValues[i][$(this).attr("name")]) == 'undefined') {
					returnValues[i][$(this).attr("name")] = {};
				}
				
				returnValues[i][$(this).attr("name")][$(this).attr("rel")] = $(this).val();
				
			} else {
				
				if($(this).attr('type') == 'checkbox') {
					console.log($(this).attr('type'));
					if ($(this).is(':checked')) {
						returnValues[i][$(this).attr("name")] = $(this).val();
					} else {
						returnValues[i][$(this).attr("name")] = 0;
					}
					
				} else {
					returnValues[i][$(this).attr("name")] = $(this).val();
				}
				
			}			
			
		});	
		
		i++;
        
    });
	console.log(returnValues);
	sendData = {};
	sendData['value'] = returnArray;
	sendData['valueLangs'] = returnArrayLangs;
	sendData['values'] = returnValues;

	if (saveType == "apply") {
		idUrl = $('#id').val() ? $('#id').val() + "/" : "";
		ajaxRequest(moduleTable.getRequestUrl() + "save/" + idUrl, sendData, function () {});		
	}
	if (saveType == "save") {
		
		idUrl = $('#id').val() ? $('#id').val() + "/" : "";
		ajaxRequest(moduleTable.getRequestUrl() + "save/" + idUrl, sendData, function () {
			
			window.location.href = moduleTable.getRequestUrl();
		});
	}
}

function checkModuleStepTab() {
	
	$('#form_step option:not(:first)').remove();
	if ($('#module').val() != '') {
		$.each(data.steps[$('#module').val()], function( index, value ) {
			$('#form_step').append('<option value="' + value + '">' + value + '</option>');
		});
		
		if ($("#form_step").attr('#data') != '') {
			$("#form_step").val($("#form_step").attr('data'));
		}
	}
}

function addValue() {
    
    data = {};
    data['cnt'] = $('.add-value').length;

    ajaxRequest(moduleTable.getRequestUrl() + "addValue/", data, function (data) {
        if (data.html) {
            $('#add-value-table').append(data.html);
        }
    });
    
}
