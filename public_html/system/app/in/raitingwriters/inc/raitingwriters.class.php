<?php

class raitingwritersData extends Module_cms {
	
	/**
	 * $result - Mixed, used with return in functions
	 */
	public $result;
	public $config;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent :: __construct();
		$this->name = "raitingwriters";
		
		$this->defLang = getDefaultLang();
		
		$this->dbTable = $this->cfg->getDbTable('orders', 'writers_rates');
		
	}
	
	/**
	 * Get all data from db and create module table
	 */
	public function showTable() {
		
				
		/**
		 * Creating module table, using cmsTable class
		 * This is table information
		 */
		$table = array(
			"writer_id" => array(
				'sort' => false,
				'title' => gLA('Writer_id','Writer ID'),
				'function' => array(&$this, 'getWriterLink'),
				'fields'	=> array('writer_id')
			),
			"rate" => array(
				'sort' => false,
				'title' => gLA('rate','Rate'),
				'function' => '',
			),
			"actions" => array(
				'sort' => false,
				'title' => gLA('m_actions','Actions'),
				'function' => array(&$this, 'moduleActionsLink'),
				'fields'	=> array('writer_id')
			)
		);

		if (getP("itemsFrom") !== false) {
			$_SESSION['ad_' . $this->getModuleName()]["itemsFrom"] = getP("itemsFrom");
		}	
		elseif (isset($_SESSION['ad_' . $this->getModuleName()]["itemsFrom"])) {
			$_POST["itemsFrom"] = $_SESSION['ad_' . $this->getModuleName()]["itemsFrom"];
		}	
		
		/**
		 * Getting all information from DB about this module
		 */
		$dbQuery = "SELECT SQL_CALC_FOUND_ROWS r.writer_id, r.id, AVG(r.rate)  AS rate
							FROM `" . $this->dbTable . "` r 			
							WHERE 1
							GROUP BY r.writer_id									
							" . $this->moduleTableSqlParms("r.id", "DESC");
		$query = new query($this->db, $dbQuery);
		
		
		$result["rCounts"] = $this->getTotalRecordsCount(false);

		// Create module table
		$this->cmsTable->createTable($table, $query->getArray());
		$result["html"] = $this->cmsTable->returnTable;
			
		return $result;
		
	}
	
	public function getWriterLink($id) {
		return '<a href="/admin/modules/writers/edit/' . $id . '/">' . $id . '</a>';
	}
	
	/**
	 * Return html tag with actions buttons element link
	 *
	 * @param int	 element id
	 */
	public function moduleActionsLink($id) {
		$r = '';
	
		if ($this->cmsUser->haveUserRole("VIEW", $this->getModuleId())) {
			$r .= '<a class="edit" href="javascript:;" onclick="moduleView(\'' . $id . '\'); return false;">' . gLA('m_view','View') . '</a>';
		}
		else {
			$r .= '';
		}
		return $r;
	}
	
	
	
	/**
	 * Edit news in DB
	 * 
	 * @param int 	news id, it's need if we are editing
	 */
	public function edit($id = "") {
		
		$data = array();

		if(isset($id) && intval($id)) {
			
			/**
			 * Getting all information from DB about this module
			 */
			$dbQuery = "SELECT r.*
							FROM `" . $this->dbTable . "` r
								LEFT JOIN " . $this->cfg->getDbTable('orders', 'self') . " o ON (r.order_id = o.id) 			
							WHERE 1
								AND o.writer_id = '" . mres($id) . "'";
			$query = new query($this->db, $dbQuery);		
				
			$data["edit"] = $query->getArray();
			
		} 
		
		
		return $data;
	}
	
	
}
?>