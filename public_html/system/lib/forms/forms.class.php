<?php

class Forms {
	
	private $module;
	private $errorsReturn = false;

	public function __construct(Module $module) {
		
		$this->module = $module;
		$this->cfg = $module->cfg;
		$this->db = $module->db;
		$this->filters = loadLibClass('form.filters');
		$this->errors = loadLibClass('form.errors');
		
		$this->errorsReturn = false;
		
	}
	
	public function getForm($module, $allSteps = false, $extra = '', $data = '') {
		
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('forms', 'self') . "` f
								LEFT JOIN `" . $this->cfg->getDbTable('forms', 'data') . "` fd ON (f.id = fd.form_id)
							WHERE 1
								AND f.`enable` = '1'
								AND f.`module` = '" . mres($module) . "'
								AND fd.lang = '" . $this->module->getLang() . "'
								" . $extra . "				
							ORDER BY `id` ASC";
		$query = new query($this->db, $dbQuery);
		
		$data = array();
		while ($row = $query->getrow()) {
			
			$fields = $this->getFieldValues($row['id']);
			$row['values'] = $fields['fields'];
			$row['hiddenData'] = $fields['hidden'];
			
			if ($row['db_field'] && $row['save_in_db'] && isset($data[$row['db_field']])) {
				$row['selected'] = $data[$row['db_field']];
			} else {
				$row['selected'] = $this->checkForSelectedValue($module, 'mob_' . $row['id']);
			}
			
			
			if ($allSteps) {
				$data[$row['id']] = $row;
			} else {
				$data[$row['form_step']][] = $row;
			}
			
		}
		//pR($data);
		return $data;
		
	}
	
	private function checkForSelectedValue($module, $id) {
		if (isset($_SESSION[$module], $_SESSION[$module]['fields'], $_SESSION[$module]['fields'][$id])) {
			return $_SESSION[$module]['fields'][$id];
		}
		
		return false;
	}
	
	private function getFieldValues($id) {
		$fields = array();
		$hidden = array();
		
		$dbQuery = "SELECT *
							FROM `" . $this->cfg->getDbTable('forms', 'values') . "` v
								LEFT JOIN `" . $this->cfg->getDbTable('forms', 'values_data') . "` vd ON (v.id = vd.value_id)
							WHERE 1
								AND v.`form_id` = '" . mres($id) . "'
								AND vd.lang = '" . $this->module->getLang() . "'		
							ORDER BY `id` ASC";
		$query = new query($this->db, $dbQuery);
		while ($row = $query->getrow()) {
			
			$row['show_fields'] = json_decode($row['show_fields'], true);
			if (is_array($row['show_fields']) && count($row['show_fields']) > 0) {
				for ($i = 0; $i < count($row['show_fields']); $i++) {
					if (!in_array($row['show_fields'][$i], $hidden)) {
						$hidden[] = $row['show_fields'][$i];
					}
				}
			}
			
			
			$fields[$row['id']] = $row;
		}
		
		return array('fields' => $fields, 'hidden' => '.hide_' . implode(' .hide_', $hidden));
		
	}
	
	public function getDataArrayToDB($data, $fields) {
		
		$dbData = array();
		
		foreach ($data AS $key => $value) {
			if ($value['db_field'] && $value['save_in_db'] && isset($fields['mob_' . $value['id']])) {
				$value['form_filters'] = json_decode($value['form_filters'], true);
				
				if (is_array($value['form_filters']) && count($value['form_filters']) > 0) {
					for ($i = 0; $i < count($value['form_filters']); $i++) {
						if (method_exists($this->filters, 'filter_' . $value['form_filters'][$i])) {
							$fields['mob_' . $value['id']] = $this->filters->{'filter_' . $value['form_filters'][$i]}($fields['mob_' . $value['id']]);
						}
					}
				}
				
				
				
				if ($value['type'] == 'select' || $value['type'] == 'tabselect' || $value['type'] == 'radio' || $value['type'] == 'pages') {
					
					if (isset($value['values'][$fields['mob_' . $value['id']]])) {
						$dbData[$value['db_field']] = $value['values'][$fields['mob_' . $value['id']]]['name'];
					}
					
				} elseif ($value['type'] == 'checkbox') {
					
					if (is_array($fields['mob_' . $value['id']])) {
						$checkbox = array();
						foreach ($fields['mob_' . $value['id']] AS $val) {
							if (isset($value['values'][$val])) {
								$checkbox[] = $value['values'][$val]['name'];
							}
						}
						
						$dbData[$value['db_field']] = json_encode($checkbox);
						
					} else {
						$dbData[$value['db_field']] = $fields['mob_' . $value['id']];
					}
					
				} else {
					$dbData[$value['db_field']] = $fields['mob_' . $value['id']];
				}
				
			}
		}
		//pR($dbData, '', true);
		return $dbData;
		
	}
	
	public function getDataArrayToEmail($data, $fields) {
	
		$dbData = array();
	
		foreach ($data AS $key => $value) {
			if (isset($fields['mob_' . $value['id']])) {
				$value['form_filters'] = json_decode($value['form_filters'], true);
	
				if (is_array($value['form_filters']) && count($value['form_filters']) > 0) {
					for ($i = 0; $i < count($value['form_filters']); $i++) {
						if (method_exists($this->filters, 'filter_' . $value['form_filters'][$i])) {
							$fields['mob_' . $value['id']] = $this->filters->{'filter_' . $value['form_filters'][$i]}($fields['mob_' . $value['id']]);
						}
					}
				}
	
	
	
				if ($value['type'] == 'select' || $value['type'] == 'tabselect' || $value['type'] == 'radio' || $value['type'] == 'pages') {
						
					if (isset($value['values'][$fields['mob_' . $value['id']]])) {
						
						if ($value['type'] == 'pages') {
							$dbData[] = array(
									'name' => $value['name'],
									'value' => getP('fieldsInquiry/number_of_pages') . ' ' . $value['values'][$fields['mob_' . $value['id']]]['name'],
							);
						} else {
							$dbData[] = array(
									'name' => $value['name'],
									'value' => $value['values'][$fields['mob_' . $value['id']]]['name'],
							);
						}
						
					}
				
						
				} elseif ($value['type'] == 'checkbox') {
						
					if (is_array($fields['mob_' . $value['id']])) {
						$checkbox = array();
						foreach ($fields['mob_' . $value['id']] AS $val) {
							if (isset($value['values'][$val])) {
								$checkbox[] = $value['values'][$val]['name'];
							}
						}
						
						$dbData[] = array(
								'name' => $value['name'],
								'value' => implode(',', $checkbox),
						);
						
	
					} else {
						$dbData[] = array(
								'name' => $value['name'],
								'value' => $fields['mob_' . $value['id']],
						);
					}
						
				} elseif ($value['type'] == 'hourdate') {
					$dbData[] = array(
							'name' => $value['name'],
							'value' => date("d-m-Y H:i:s", $fields['mob_' . $value['id']]),
					);
				} else {
					$dbData[] = array(
							'name' => $value['name'],
							'value' => $fields['mob_' . $value['id']],
					);
				}
	
			}
		}
		//pR($dbData, '', true);
		return $dbData;
	
	}
	
	public function checkFields($data, $fields) {
	
		foreach ($data AS $key => $value) {
			$value['form_errors'] = json_decode($value['form_errors'], true);
			
			if ($value['required'] && empty($fields['mob_' . $value['id']])) {
				$this->setError('mob_' . $value['id'], 'isRequired');
				continue;
			}
	
			if (is_array($value['form_errors']) && count($value['form_errors']) > 0) {
				for ($i = 0; $i < count($value['form_errors']); $i++) {
					if (method_exists($this->errors, 'error_' . $value['form_errors'][$i])) {
						if (!$this->errors->{'error_' . $value['form_errors'][$i]}($fields['mob_' . $value['id']], 'mob_' . $value['id'])) {
							$this->setError('mob_' . $value['id'], $value['form_errors'][$i]);
						}
					}
				}
			}
		}
		
		if (isset($fields['number_of_pages'])) {
			if (empty($fields['number_of_pages']) || !preg_match('/^[0-9]*$/', $fields['number_of_pages'])) {
				$this->setError('number_of_pages', 'isRequired');
			}
		}
	
		return $this->errorsReturn;
	
	}
	
	private function setError($id, $error) {
		$this->errorsReturn[$id] = gL('Errors_' . $error);
	}
		
}
?>
