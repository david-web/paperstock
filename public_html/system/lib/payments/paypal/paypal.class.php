<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		Dailis Tukāns <dailis@efumo.lv>
 * @copyright   Copyright (c) 2011, Efumo.
 * @link		http://adweb.lv
 * @version		1.2
 * @modiefied	31.10.2012
 */

// ------------------------------------------------------------------------

/** 
 * Adweb paypal class file
 * 03.02.2011
 */

class Paypal {
	
    private $url = 'https://www.paypal.com/cgi-bin/webscr';
    private $testMode = false;
    private $vars; // arrat contains value for paypal form
    var $ipnResponse; // holds the IPN response from paypal
    var $ipnData = array(); // array contains the POST values for IPN
    var $country = 0; // country id
    var $business_account = ''; // paypal business account
    
    /**
     * Constructor
     */
    public function __construct($country_id = 0) {
        $this->db = &loadLibClass('db');
	
		// Set country values - id, account, test/production mode
		$this->country = (int)$country_id < 1 ? getCountry() : (int)$country_id;
		$this->setCountryData();
		$this->setTestMode($this->testMode);
    }
    
    /**
     * Set country data:
     * - paypal business account;
     * - test/production mode
     */
    private function setCountryData() {
		$sql = "SELECT `test_mode`, `account` FROM `mod_payment_methods_paypal` WHERE `id` = " . $this->country;
        $query = new query($this->db, $sql);
        if ($query->num_rows() > 0) {
            $query->getrow();
			$this->business_account = (string)$query->field('account');
			$this->testMode = (int)$query->field('test_mode') == 1 ? true : false;
        }
    }

    /**
     * Add paypal variable and data to $vars array
     *
     * @param string $name
     * @param string $value
     */
    public function addVar($name, $value)
    {
        $this->vars[$name][0] = $name;
        $this->vars[$name][1] = $value;
    }
    
    /**
     * Save paypal sums for order, so later we can validate paid and original sum
     *
     * @param array $d 
     */
    public function saveOrderPaypalData($d = array())
    {
		if (is_array($d) && count($d) > 2) {
			$sql = "INSERT INTO `pproc_pp_transaction` 
						(`order_id`, `pp_sum`, `pp_currency`, `status`, `time`)
					VALUES 
						(" . $d['order_id'] . ", " . $d['pp_sum'] . ", '" . $d['pp_currency'] . "', 1, " . time() . ")";
			$query = new query($this->db, $sql);
		}
    }

    /**
     * Set test mode and payment subit form
     * Also will set correct url for paypal service
     *
     * @param bool $value           test mode status. True - if must be test mode
     */
    public function setTestMode($value = false)
    {
        $this->testMode = $value;

        if ($this->testMode === true) {
            $this->url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        } else {
            $this->url = 'https://www.paypal.com/cgi-bin/webscr';
        }
    }

    /**
     * Returns paypal link.
     * Also will add all added variables as params
     *
     * @return string               url link with added variables like link params
     */
    public function getLink()
    {
        $link = $this->url . '?';
	
        foreach ($this->vars as $item => $sub) {
            $link.= $sub[0] . '=' . $sub[1] . '&';
        }

        return $link;
    }

    /**
     * Returns paypal form with hidden fields setted up with added variables.
     *
     * @param int $type             button type
     * @param string $image         image name with full url path
     * @return string               form html code
     */
    public function getForm($btnType = 1, $btnImage = NULL)
    {

        $output = '<form action="' . $this->url . '" method="post" name="paymentForm" id="paymentForm" style="display:inline;">'."\n";
        foreach ($this->vars as $item => $sub) {
            $output.= '<input type="hidden" name="' . $sub[0] . '" value="' . $sub[1] . '" />'."\n";
        }
        if ($btnType > 0) {
            $output.= $this->getButton($btnType, $btnImage);
        }
        $output.= '</form>';

        return $output;

    }

    /**
     * Get button html code
     *
     * @param int $type             button type
     * @param string $image         image name with full url path
     * @return string               button html code
     */
    private function getButton($type, $image = NULL)
    {

        $btnPaypalPath = 'https://www.paypal.com/en_US/i/btn/';
        $btnAltText = 'PayPal - The safer, easier way to pay online!';

        switch ($type) {

            // Buy now
            case 1:
                $btn = '<input type="image" height="21" style="width:86;border:0px;"';
                $btn .= 'src="' . $btnPaypalPath . 'btn_paynow_SM.gif" border="0" name="submit" ';
                break;

            // Add to cart
            case 2:
                $btn = '<input type="image" height="26" style="width:120;border:0px;"';
                $btn .= 'src="' . $btnPaypalPath . 'btn_cart_LG.gif" border="0" name="submit"';
                break;

            // Donate
            case 3:
                $btn = '<input type="image" height="47" style="width:122;border:0px;"';
                $btn .= 'src="' . $btnPaypalPath . 'btn_donateCC_LG.gif" border="0" name="submit"';
                break;

            // Gift certificate
            case 4:
                $btn = '<input type="image" height="47" style="width:179;border:0px;"';
                $btn .= 'src="' . $btnPaypalPath . 'btn_giftCC_LG.gif" border="0" name="submit"';
                break;

            // Subscribe
            case 5:
                $btn = '<input type="image" height="47" style="width:122;border:0px;"';
                $btn .= 'src="' . $btnPaypalPath . 'btn_subscribeCC_LG.gif" border="0" name="submit"';
                break;

            // Custom image button
            default:
                $btn = '<input type="image" src="' . $image . '" border="0" name="submit"';
                break;

        }
        $btn .= 'alt="' . $btnAltText . '">';
        $btn .= "\n";

        return $btn;

    }

    /**
     * Write data to log file
     *
     * @param array $data           posted data
     * @param string $logFile       log file name
     */
    private function doLog($data, $logFile = null)
    {

        /*// Add date
        $text = date('d.m.Y H:i:s') . "<br>";

        // Success or failure being logged?
        if ($data[0] == 'OK') $text .= $data[1] . "<br>";
        else $text .= $data[1] . "<br>";
        
        // Log the POST variables
        $text .= "IPN POST Vars from Paypal:<br>";
        foreach ($this->ipnData as $key=>$value) {
            $text .= $key . "=" . $value . " <br>";
        }

        // Log the response from the paypal server
        $text .= "IPN Response from Paypal Server: " . $this->ipnResponse;
        $text .= "------------------------------------------------------<br>";

        // Write to log file
        //$fp = fopen($logFile, 'a');
        //fwrite($fp, $text . "\n\n");
        //fclose($fp);
		sendMail('dailis@efs.lv', 'paypal IPN checked', $text, array(), 'info@bio2you.lv', true);
		sendMail('tukans_dailis@inbox.lv', 'paypal IPN checked', $text, array(), 'info@bio2you.lv', true);*/
        
    }

    /**
     * Check Paypal payment
     *
     * @param array $_POST              posted data
     * @param string $logErrorFile      log file name for errors
     * @param string $logSucceedFile    log file name for ok payments
     * @return bool                     true - if ok, false - if error
     */
    public function checkPayment($_POST, $logErrorFile = null, $logSucceedFile = null)
    {

        // parse the paypal URL
        $urlParsed = parse_url($this->url);
		//pR($urlParsed);

        // Add 'cmd' to req (ipn command)
        $req = 'cmd=_notify-validate';

		// Read the post from PayPal system and add them to req
		foreach ($_POST as $key => $value) {
			$this->ipnData["$key"] = $value;
			$value = urlencode(stripslashes($value));
			$req .= "&" . $key . "=" . $value;
		}
		
        // Open the connection to paypal
        $fp = fsockopen($urlParsed['host'], "80", $err_num, $err_str, 30);

        // If could not open the connection, log error
        if (!$fp) {
            if ($logErrorFile != NULL) {
                $this->doLog(array("ERROR", "IPN Connection: fsockopen error"), $logErrorFile);
            }
            return false;
        }
        // Else post the data back to paypal
        else {
            
            fputs($fp, "POST " . $urlParsed['path'] . " HTTP/1.1\r\n");
            fputs($fp, "Host: " . $urlParsed['host'] . "\r\n");
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-length: " . strlen($req) . "\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $req . "\r\n\r\n");
			
            // Loop through the response from the server and append to variable
            while (!feof($fp)) {
                $this->ipnResponse .= fgets($fp, 1024);
            }
            fclose($fp);
        }

        // Valid IPN transaction.
        if (preg_match('/^VERIFIED/', $this->ipnResponse)) {
            
            if ($logSucceedFile != NULL) {
                $this->doLog(array("OK", "IPN Validation: Success"), $logSucceedFile);
            }
            return true;       
        }
        // Invalid IPN transaction
        else {
            if ($logErrorFile != NULL) {
                $this->doLog(array("ERROR", "IPN Validation: Failed"), $logErrorFile);
            }
            return false;
        }

        return false;

    }
    
	/**
	 * Method to validate IPN response
	 *
	 * @since		1.2
	 * @return		array 
	 */
	public function checkIPN()
	{
        // parse the paypal URL
        $urlParsed = parse_url($this->url);
		
		// reading posted data from directly from $_POST causes serialization 
		// issues with array data in POST
		// reading raw POST data from input stream instead. 
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2)
				$this->ipnData[$keyval[0]] = urldecode($keyval[1]);
		}
		
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if (function_exists('get_magic_quotes_gpc')) {
		   $get_magic_quotes_exists = true;
		}
		foreach ($this->ipnData as $key => $value) {
		   if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
		   } else {
				$value = urlencode($value);
		   }
		   $req .= "&$key=$value";
		}
		
		// Post IPN data back to paypal to validate
		$ch = curl_init($this->url);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		
		// In wamp like environments that do not come bundled with root authority certificates,
		// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
		// of the certificate as shown below.
		// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		if ( !($this->ipnResponse = curl_exec($ch)) ) {
			// error_log("Got " . curl_error($ch) . " when processing IPN data");
			curl_close($ch);
			exit;
		}
		curl_close($ch);
		
		// Inspect IPN validation result and act accordingly
		if ( strcmp($this->ipnResponse, "VERIFIED") == 0 ) {
			
			if ( 
				strcmp($_POST['payment_status'], "Completed") == 0 
				/*&& (
					$this->business_account == $_POST['receiver_email'] || $this->business_account == $_POST['business']
				)*/
			) {
				
				$this->doLog(array("OK", "IPN Validation: Success"));

				$result = array(
					'txn_id' => $_POST['txn_id'],
					'order_db_id' => $_POST['custom'],
					'invoice' => isset($_POST['invoice']) ? $_POST['invoice'] : '',
					'item_name' => $_POST['item_name'],
					'item_number' => $_POST['item_number'],
					'payment_status' => $_POST['payment_status'],
					'mc_gross' => $_POST['mc_gross'],
					'mc_currency' => $_POST['mc_currency'],
					'receiver_email' => $_POST['receiver_email'],
					'business' => $_POST['business']
				);
				
				return $result;
				
			} else {
				return false;
			}
				
		} else if (strcmp($this->ipnResponse, "INVALID") == 0) {
			
			$this->doLog(array("ERROR", "IPN Validation: Failed"));
			return false;
			
		}
		
	}
	
    /**
     * Round up sum
     *
     * @param type $value
     * @param type $precision
     * @return type 
     */
    private function round_up($value, $precision = 2)
    {
        if ( $value <= 0 ) {
            return $value;
        }
		
        $amt = explode(".", $value);
        if ( strlen($amt[1]) > $precision ) {
			$next = (int)substr($amt[1], $precision);
			$amt[1] = (float)( "." . substr($amt[1], 0, $precision) );
			if ( $next != 0 ) {
				$rUp = "";
				for ( $x=1; $x<$precision; $x++ ) {
					$rUp .= "0";
				}
				$amt[1] = $amt[1] + (float)(".".$rUp."1");
			}
        } else {
			$amt[1] = (float)(".".$amt[1]);
        }
		
        return $amt[0]+$amt[1];
    }
    
    /**
     * Convert sum if need for current currency
     *
     * @param string $currency
     * @param decimal $amount
     * @return array		    contains new currency and amount 
     */
    public function checkCurreny($currency, $amount) {
        $result = array();

        $result = array(
                'rate' => '1',
                'currency' => $currency,
                'amount' => $amount
            );
        
        return $result;
    }

}

?>