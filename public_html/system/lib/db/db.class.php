<?php
/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/** 
 * Database class
 * open connection with DB
 * close connections, etc...
 * 16.02.2010
 */
class Db {

	public $connect_id;
	public $type;
	public $query_id;

	/**
	 * Constructor
	 */
	public function __construct($database_type = "mysql") {
		$this->type = $database_type;
	}

	/**
	 * Open connection with selected database
	 * 
	 * @param	string	db name
	 * @param	string	db host
	 * @param	string	db user name
	 * @param	string	db password
	 * 
	 * @return int 
	 */
	public function open($database = "{database}", $host = "{host}", $user = "{user}", $password = "{password}") {

		$this->connect_id = @mysql_connect($host, $user, $password);
		if ($this->connect_id) {
			$result = mysql_select_db($database);
			if (!$result) {
				mysql_close($this->connect_id);
				$this->connect_id = $result;
				
				showError("Can't select the database!");
			}
			else {
				$query = new query($this, "SET NAMES 'utf8'");
			}
		}
		else {
			showError("Can't connect to database!");
		}
		
		return $this->connect_id;
	}
	
	/**
	 * Lock table
	 *
	 * @param	string	table name
	 * @param	enum	maybe 'read' or 'write'	
	 */
	public function lock($table, $mode = "write") {

		$query = new query($this, "LOCK TABLES " . $table . " " . $mode);
		$result = $query->result;
		
		return $result;
	}

	/**
	 * Unlock tables
	 * unlocks any and all tables which this process locked
	 *
	 */
	public function unlock() {

		$query = new query($this, "UNLOCK TABLES");
		$result = $query->result;
		
		return $result;
	}
	
	/**
	 * Function returns the next available id for $sequence, if it's not
	 * already defined, the first id will start at 1.
	 * This function will create a table for each sequence called
	 * '{sequence_name}_seq' in the current database.
	 *
	 * @param	string	sequence
	 */
	public function nextid($sequence) {

		$esequence = ereg_replace("'", "''", $sequence) . "_seq";
		$query = new query($this, "REPLACE INTO " . $esequence . " VALUES ('', nextval + 1)");
		if ($query->result) {
			$nextid = mysql_insert_id($this->connect_id);
		} 
		else {
			$query->query($this, "CREATE TABLE " . $esequence . " ( seq char(1)
									DEFAULT '' NOT NULL, nextval bigint(20) unsigned DEFAULT '0' NOT NULL auto_increment,
									PRIMARY KEY (seq), KEY nextval (nextval) )");

			$query->query($this, "REPLACE INTO " . $esequence . " VALUES ( '', nextval+1 )");
			if ($query->result) {
				$nextid = mysql_insert_id($this->connect_id);
			} 
			else {
				$nextid = 0;
			}
		}
		
		return $nextid;
	}

	/**
	 * Get last insert id
	 *
	 */
	public function get_insert_id() {
		$ins_id = mysql_insert_id($this->connect_id);
		return $ins_id;
	}
	
	/**
	 * Return mysql errors
	 *
	 */
	public function error() {
		return mysql_errno($this->connect_id) . ": " . mysql_error($this->connect_id);
	}
	
	/**
	 * Close db connection
	 * Closes the database connection and frees any query results left.
	 * 
	 */
	public function close() {

		if ($this->query_id && is_array($this->query_id)) {
			while (list($key,$val) = each($this->query_id)) {
				@mysql_free_result($val);
			}
		}
		
		$result = @mysql_close($this->connect_id);
		return $result;
	}
	
	/**
	 * Function used by the constructor of query. Notifies the
	 * this object of the existance of a query_result for later cleanup
	 * internal function, don't use it yourself.
	 * 
	 * @param	mix		query id
	 * @param 	string	query
	 * @param 	int		seconds
	 */
	public function addquery($query_id, $query, $time = '') {

		$this->query_id[] = $query_id;
		$this->queries[] = $query;
		$this->qTimes[] = $time;
	}

};

// ------------------------------------------------------------------------

/** 
 * Query class
 * work with queries and results
 * 16.02.2010
 */

class query {

	public $result;
	public $row;
	public $rArray = Array();

  	/**
  	 * Constructor of the query object.
  	 * executes the query, notifies the db object of the query result to clean
  	 * up later.
  	 * 
  	 * @param object	db connector
  	 * @param string	query string
  	 * @param bool 		die on sql error or not
  	 * @return void
  	 */
	public function query(&$db, $query = "", $die = true) {

		if ($query) {
			
			// query not called as constructor therefore there may be something to clean up.
			if ($this->result) {
				$this->free();
			}
					
			$wk = &loadLibClass('workTime');
			$wk->mark("query-start");
			$this->result = mysql_query($query, $db->connect_id);
			$wk->mark("query-end");
			
			if ($wk->elapsedTime("query-start", "query-end") > 1) {
				logWarn("Slow QUERY: " . $query);
			}
			
			$db->addquery($this->result, $query, $wk->elapsedTime("query-start", "query-end"));
      
			if (!$this->result) {
				logWarn("SQL error! Query: " . $query . " Error:" . $db->error());
				if ($die) {
					showError("SQL error! \n " . $query . " \n Error: " . $db->error());
				}
			}
		}
	}
	
	/**
	 * Gets the next row for processing with $this->field function later.
	 *
	 */
	public function getrow() {
		$this->row = $this->result ? @mysql_fetch_assoc($this->result) : 0;
		return $this->row;
	}
	
	/**
	 * Get one field value
	 *
	 */
	public function getOne() {

		$return = ($this->result ? @mysql_result($this->result, 0, 0) : false);
		$this->free();
		return $return;
	}
  
	/**
	 * Get array from DB
	 */
	public function getArray($field = '', $simple = true) {
	
		while ($r = $this->getrow()) {
			if ($field && isset($r[$field])) {
				if ($simple) {
					$this->rArray[$r[$field]] = $r;
				} else {
					$this->rArray[] = $r[$field];
				}			
			} else {
				$this->rArray[] = $r;	
			}
			
		}
		
		$this->free();
		return $this->rArray;
	}
	
	/**
	 * Get the value of the field with name $field
	 * in the current row
	 * 
	 * @param	string 	field name
	 */
	public function field($field) {

		$result = $this->row[$field];
		if (!get_magic_quotes_gpc()) {
			$result = stripslashes($result);
		}
		
		return $result;
	}

	/**
	 * Return the name of field number $fieldnum
	 * only call this after query->getrow() has been called at least once
	 * 
	 * @param	string	field number
	 */
	public function fieldname($fieldnum) {

		$result = @mysql_field_name($this->result, $fieldnum);
		return $result;
	}

	/**
	 * Return the current row pointer to the first row
	 * (CAUTION: other versions may execute the query again!! (e.g. for oracle))
	 * 
	 */
	public function firstrow() {

		$result = @mysql_data_seek($this->result, 0);
		if ($result) {
			$result = $this->getrow();
		}
	
		if (!get_magic_quotes_gpc()) {
			$this->row = array_map('stripslashes', $this->row);
		}
	
		return $this->row;
	}

	/**
	 * Free the mysql result tables
	 *
	 */
	public function free() {
		return @mysql_free_result($this->result);
	}

	/**
	 * Get query num rows
	 *
	 */
	public function num_rows() {
		$result = @mysql_num_rows($this->result);
		return  $result;
	}
  	
	/**
	 * Get affected rows
	 *
	 */
	public function affected_rows() {
		$result = @mysql_affected_rows();
		return  $result;
	}

};

?>
