<?php

class Form_errors {

	public function __construct() {
		global $mdb;
		
		$this->db = $mdb;
		$this->cfg = loadLibClass('config');
	}
	
	public function error_isValidEmail($value, $field) {
		
		$emailRegEx = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,6})$/i";
		if (trim($value) && preg_match($emailRegEx, $value)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public function error_isUniqEmail($value, $field) {	
		
		$dbQuery = "SELECT id
							FROM `" . (getCountry() == 8 ? $this->cfg->getDbTable('writers') : $this->cfg->getDbTable('profiles')) . "` 
							WHERE 1
								AND `email` = '" . mres($value) . "'";
		$query = new query($this->db, $dbQuery);
		if ($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public function error_isConfirmEmail($value, $field) {
		
		if ($field == 'mob_24') {
			if (trim($value) && trim($value) == getP('fields/mob_23')) {
				return true;
			} else {
				return false;
			}
		}
		
		return true;
	
		
	
	}
	
	public function error_isValidPhone($value, $field) {
		if (preg_match("/^\+?\s?[0-9\s]+$/", $value)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function error_isNotEmpty($value, $field) {
		if (trim($value)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function error_isAlpha($value, $field) {
		if (ctype_alpha(str_replace(' ', '', $value))) {
			return true;
		} else {
			return false;
		}
	}
	
	public function error_isAlphaNumeric($value, $field) {
		if (ctype_alnum($value)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function error_isCorrectLen($value, $field) {
		if ($field == 'mob_25') {
			if (strlen($value) < 0 || strlen($value) > 25) {
				return false;
			} else {
				return true;
			}
		} elseif ($field == 'mob_26') {
			if (strlen($value) < 3 || strlen($value) > 15) {
				return false;
			} else {
				return true;
			}
		} elseif ($field == 'mob_15') {
			if (strlen($value) > 1300) {
				return false;
			} else {
				return true;
			}
		}
		
		return true;
	}
	
	public function error_isPassword($value, $field) {
		if (preg_match("/^.*(?=.{6,24})(?=.*\d)(?=.*[a-zA-Z]).*$/", $value)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function error_isConfirmPassword($value, $field) {
		
		if ($field == 'mob_27') {
			if (trim($value) && trim($value) == getP('fields/mob_28')) {
				return true;
			} else {
				return false;
			}
		}
		
		return true;
	
	}
	
	public function error_isMinimumWords($value, $field, $default = 3) {
		$words = explode(' ', $value);
		if (count($words) >= $default) {
			return true;
		} else {
			return false;
		}
	}
		
}