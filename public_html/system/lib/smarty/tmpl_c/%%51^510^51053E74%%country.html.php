<?php /* Smarty version 2.6.26, created on 2014-07-31 10:37:23
         compiled from /home/papersst/public_html/system/config/../../system/app/in/promoblocks/tmpl/country.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/promoblocks/tmpl/country.html', 7, false),)), $this); ?>
<div id="editBlock">
	<div class="content">
	    <form id="edit_form">
	        <table class="search-table" style="margin-bottom: 4px;">
	            <tr>
	                <td>
	                    <strong><?php echo ((is_array($_tmp='m_promo_add_edit')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add/edit promo bloc') : gLA($_tmp, 'Add/edit promo bloc')); ?>
</strong>
	                </td>
	            </tr>
	        </table>
	        <div class="error-msg hidden" id="errorBlock"></div>
	        <div class="ok-msg hidden" id="okBlock"><?php echo ((is_array($_tmp='m_data_saved')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Data has been saved') : gLA($_tmp, 'Data has been saved')); ?>
</div>
	        <table class="holder-table">
	            <tr>
	                <td>* <?php echo ((is_array($_tmp='m_country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</td>
	                <td>
	                    <select name="country" id="country" class="country">
	                        <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
	                        <?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['countries']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
		                		<option <?php if ($this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['id'] == $this->_tpl_vars['countries']['sel']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['title']; ?>
</option>
		                	<?php endfor; endif; ?> 
	                    </select>
	                </td>
	            </tr>
	        </table>
	        <table class="bttns-table">
	            <tr>
	                <td>
	                    <div class="btn cancel">
	                        <a href="javascript: void(0);" onclick="setCountry();">
	                            <span><?php echo ((is_array($_tmp='m_next')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Next') : gLA($_tmp, 'Next')); ?>
</span>
	                        </a>
	                    </div>
	                </td>
	            </tr>
	        </table>
	    </form>
	    <script type="text/javascript">
		    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
		    moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
		</script>
	</div>
</div>	