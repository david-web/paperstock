<?php /* Smarty version 2.6.26, created on 2014-07-29 10:11:23
         compiled from /home/papersst/public_html/system/config/../../system/app/in/faq/tmpl/faq.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/faq/tmpl/faq.html', 7, false),)), $this); ?>
<div class="content">
	<form class="forma" method="post" action="" onsubmit="return false;">
		<fieldset>
			<table class="search-table" cellpadding="0" cellspacing="0">
			    <tr>
			        <td>
			            <label for="contentTitle"><?php echo ((is_array($_tmp='content')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Content') : gLA($_tmp, 'Content')); ?>
:</label>
			            <input type="hidden" name="content_id" id="content_id" readonly="readonly" />
			            <input class="long" type="text" id="contentTitle" name="contentTitle" readonly="readonly" />
			            <a href="#" onclick="openSiteMapDialog('content_id', 'contentTitle', 'siteMapReturn'); return false;" class="select-btn"><?php echo ((is_array($_tmp='select')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select') : gLA($_tmp, 'Select')); ?>
</a>
			        </td>
			        <td><?php echo ((is_array($_tmp='m_country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</td>
		            <td>
		                <select class="long" id="country" onchange="changeFilter(); return false;">
		                    <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
		                    <?php echo $this->_tpl_vars['countries']; ?>

		                </select>
		            </td>
		            <td><?php echo ((is_array($_tmp='m_lang')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Language') : gLA($_tmp, 'Language')); ?>
:</td>
		            <td>
		                <select class="long" id="lang" onchange="changeFilter(); return false;">
		                    <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
		                    <?php echo $this->_tpl_vars['langs']; ?>

		                </select>
		            </td>
		            <td class="bttns">
		                <div class="btn" style="margin-left: 4px;">
		                    <a href="javascript: void(0);" onclick="clearFilter(); return false;">
		                        <span><?php echo ((is_array($_tmp='clear')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Clear') : gLA($_tmp, 'Clear')); ?>
</span>
		                    </a>
		                </div>
		            </td>
			    </tr>
			</table>
	
			<?php echo $this->_tpl_vars['tTable']; ?>

			
			<div id="modulePath">
			</div>
			
			<?php echo $this->_tpl_vars['bTable']; ?>

	
		</fieldset>
	</form>
	
	<script type="text/javascript">
		moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
		moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
		moduleTable.usePaging = true;
		<?php if ($this->_tpl_vars['MODULE_FROM']): ?>
	    moduleTable.from = <?php echo $this->_tpl_vars['MODULE_FROM']; ?>
;
	    <?php endif; ?>

		$(document).ready(function() {
			
			if (typeof(anchors['content_id']) != 'undefined') {
				$('#content_id').val(anchors['content_id']);
			}
			
			updateModule();

			if ($('#content_id').val() == '') {
				$('.addNew').hide();
			}	
			
		});
	</script>
</div>	