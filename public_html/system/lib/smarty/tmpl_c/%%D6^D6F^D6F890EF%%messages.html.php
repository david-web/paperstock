<?php /* Smarty version 2.6.26, created on 2015-07-14 18:19:03
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/in/messages/tmpl/messages.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/in/messages/tmpl/messages.html', 8, false),)), $this); ?>
<div class="content">
	<form class="forma" method="post" action="" onsubmit="return false;">
		<fieldset>
		    <legend>FORM</legend>
			<table class="search-table" cellpadding="0" cellspacing="0">
			    <tr>
			        <td>
			            <label for="filterModule"><?php echo ((is_array($_tmp='select_module')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select module') : gLA($_tmp, 'Select module')); ?>
:</label>
			            <select id="filterModule" name="filterModule" onchange="changeFilter(); return false;">
							<option value=""><?php echo ((is_array($_tmp='select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
							<?php echo $this->_tpl_vars['mList']; ?>

						</select>
			        </td>
			        <td>
			            <label for="notTranslated"><?php echo ((is_array($_tmp='not_translated')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Not translated') : gLA($_tmp, 'Not translated')); ?>
:</label>
			            <select id="notTranslated" name="notTranslated" onchange="changeFilter(); return false;">
			                <option value=""><?php echo ((is_array($_tmp='unselected')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Unselected') : gLA($_tmp, 'Unselected')); ?>
</option>
			                <?php echo $this->_tpl_vars['lList']; ?>

			            </select>
			        </td>
			        <td>
			            <label for="filterSearch"><?php echo ((is_array($_tmp='content')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Content') : gLA($_tmp, 'Content')); ?>
:</label>
			            <input class="long" type="text" id="filterSearch" name="filterSearch" />
			        </td>
			        <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="changeFilter(); return false;"><span><?php echo ((is_array($_tmp='m_search')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Search') : gLA($_tmp, 'Search')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
	               <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="clearFilter(); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
			    </tr>
			</table>
	
	<?php echo $this->_tpl_vars['tTable']; ?>

	
	<div id="modulePath">
	</div>
	
	<?php echo $this->_tpl_vars['bTable']; ?>

	
		</fieldset>
	</form>
	
	<script type="text/javascript">
		moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
		moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
		moduleTable.usePaging = true;

		$(function () { 

			$('#filterSearch').keyup(function(e){
				if (!e) var e = window.event;
				var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : null;
				if (keyCode == 13) {
					changeFilter();
					
				}
				return false;	
			});

			updateModule();
		});	
	</script>
</div>	