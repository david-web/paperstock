<?php /* Smarty version 2.6.26, created on 2014-07-15 10:33:09
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/verification.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/verification.html', 12, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/verification.html', 44, false),)), $this); ?>
<section class="green_title order">
	<section class="wrap">
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
	</section>
</section>
<div class="sp60-40-0"></div>

<section class="wrap mob_nowrap clearfix mob_verification_bg">
	<section class="col1a">
		<div class="message_green p2" <?php if ($this->_tpl_vars['userData']['verified'] != ''): ?> style="display:none;"<?php endif; ?>>
			<div class="wrap_onlymob">
				<h2><?php echo ((is_array($_tmp='writers_VerificationOkTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Congratulations!') : gL($_tmp, 'Congratulations!')); ?>
</h2>
				<?php echo ((is_array($_tmp='writers_VerificationOkLead')) ? $this->_run_mod_handler('gL', true, $_tmp, '<p>Your email has beed successfully verified! Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>') : gL($_tmp, '<p>Your email has beed successfully verified! Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>')); ?>

			</div>
		</div>
		<div class="verification" <?php if ($this->_tpl_vars['userData']['verified'] == ''): ?> style="display:none;"<?php endif; ?>>
			<div class="wrap_onlymob">
				<h2><?php echo ((is_array($_tmp='writers_VerificationTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Please verify your contact information') : gL($_tmp, 'Please verify your contact information')); ?>
</h2>
				<?php echo ((is_array($_tmp='writers_VerificationLead')) ? $this->_run_mod_handler('gL', true, $_tmp, '
				<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
				<p>Pellentesque sit amet varius ante. Integer aliquam felis ut commodo sagittis. Suspendisse tincidunt dictum tellus, id facilisis mauris tempus sed.</p>') : gL($_tmp, '
				<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
				<p>Pellentesque sit amet varius ante. Integer aliquam felis ut commodo sagittis. Suspendisse tincidunt dictum tellus, id facilisis mauris tempus sed.</p>')); ?>

				<hr>
				<h2><?php echo ((is_array($_tmp='writers_EnterVerificationCode')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Enter your verification code') : gL($_tmp, 'Enter your verification code')); ?>
</h2>
				<div class="field clearfix">
					<div class="col1">
						<div class="cinput">
							<input type="text" id="verified">
						</div>
					</div>
					<div class="col2">
						<a href="javascript:;" onclick="profile.verify();" class="btn1" title="<?php echo ((is_array($_tmp='submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
"><?php echo ((is_array($_tmp='submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
					</div>
					<div class="col3">
						<div class="resend">
							<a href="javascript:;" onclick="profile.resendVerify();"><?php echo ((is_array($_tmp='writers_resendVerificationCode')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Resend verification code') : gL($_tmp, 'Resend verification code')); ?>
</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="col2a">
		<section class="side_safety css3">
			<?php if (count($this->_tpl_vars['menu']['ORDERS_RIGHT']) > 0): ?>	
					<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['menu']['ORDERS_RIGHT']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
					<div><a href="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
"><?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
</a></div>	
					<?php endfor; endif; ?>
				<?php endif; ?>
		</section>
		<section class="small_stats clearfix">
			<img alt="<?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats.png">
			<div class="nr"><?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
</div>
			<div class="text"><?php echo ((is_array($_tmp='news_OurLiveStats_OrdersDelivered_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Orders<br>delivered') : gL($_tmp, 'Orders<br>delivered')); ?>
</div>
		</section>
	</section>
</section>