<?php /* Smarty version 2.6.26, created on 2014-07-22 17:38:19
         compiled from /home/papersst/public_html/system/config/../../system/app/in/bonuscodes/tmpl/bonuscodes.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/bonuscodes/tmpl/bonuscodes.html', 7, false),)), $this); ?>
<div class="content">
	<fieldset>
    <table class="search-table" cellpadding="0" cellspacing="0">
        <tr>
            <td>
            	
            	<label for="valid_from"><?php echo ((is_array($_tmp='valid_from')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Valid from') : gLA($_tmp, 'Valid from')); ?>
:</label>
			    <input class="filter" type="text" id="valid_from" name="valid_from"  value="<?php echo $this->_tpl_vars['filters']['valid_from']; ?>
" />
            </td>
            <td>
            	
            	<label for="valid_to"><?php echo ((is_array($_tmp='valid_to')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Valid to') : gLA($_tmp, 'Valid to')); ?>
:</label>
			    <input class="filter" type="text" id="valid_to" name="valid_to"  value="<?php echo $this->_tpl_vars['filters']['valid_to']; ?>
" />
            </td>
            <td>
	            <label for="status"><?php echo ((is_array($_tmp='status')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Status') : gLA($_tmp, 'Status')); ?>
:</label>
	            <select class="filter" id="status" name="status">
					<option value="0"><?php echo ((is_array($_tmp='select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select option') : gLA($_tmp, 'Select option')); ?>
</option>
                    <?php echo $this->_tpl_vars['statuses']; ?>

				</select>
	        </td>
	        <td class="bttns">
                 <div class="btn"><a href="javascript:;" onclick="changeFilter(); return false;"><span><?php echo ((is_array($_tmp='m_search')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Search') : gLA($_tmp, 'Search')); ?>
</span></a></div>
                 <div class="clr"><!-- clear --></div>
             </td>
             <td class="bttns">
                 <div class="btn"><a href="javascript:;" onclick="clearFilter(); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
                 <div class="clr"><!-- clear --></div>
             </td>
        </tr>
    </table>
    <?php echo $this->_tpl_vars['tTable']; ?>

    <div id="modulePath"></div>
    <?php echo $this->_tpl_vars['bTable']; ?>

    </fieldset>
</div>
<script type="text/javascript">
    $(function() {
        moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
        moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
        moduleTable.usePaging = true;
        moduleTable.itemsOnPage = 20;
        moduleTable.from = <?php echo $this->_tpl_vars['moduleFrom']; ?>
;
        
        updateModule();
    });
    
    $(document).ready(function(){
        //Set calendar settings
        $('#valid_from').datepicker({
            dateFormat  : 'dd.mm.yy',
        });

        $('#valid_to').datepicker({
            dateFormat  : 'dd.mm.yy',
        });

    });
</script>