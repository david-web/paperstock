<?php /* Smarty version 2.6.26, created on 2014-10-08 08:56:38
         compiled from /home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/main.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/main.html', 4, false),array('modifier', 'replace', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/main.html', 4, false),array('modifier', 'strip_tags', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/main.html', 4, false),array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/main.html', 8, false),)), $this); ?>
<!DOCTYPE html>
<html lang="<?php echo $this->_tpl_vars['web']['lang']; ?>
" class="no-js">
	<head>
		<title><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['web']['pageTitle'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('replace', true, $_tmp, '"', '') : smarty_modifier_replace($_tmp, '"', '')))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</title>
		<meta name="description" content="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['web']['pageDescription'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('replace', true, $_tmp, '"', '') : smarty_modifier_replace($_tmp, '"', '')))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
"/>
		<meta name="keywords" content="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['web']['pageKeywords'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('replace', true, $_tmp, '"', '') : smarty_modifier_replace($_tmp, '"', '')))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
"/>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta content="<?php echo ((is_array($_tmp='siteAuthor')) ? $this->_run_mod_handler('gL', true, $_tmp, 'www.beattum.com') : gL($_tmp, 'www.beattum.com')); ?>
" name="author" />
		
		<meta id="Viewport" name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0">
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		
		<meta property="og:title" content="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['web']['pageTitle'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('replace', true, $_tmp, '"', '') : smarty_modifier_replace($_tmp, '"', '')))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
		<meta property="og:description" content="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['web']['pageDescription'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('replace', true, $_tmp, '"', '') : smarty_modifier_replace($_tmp, '"', '')))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
		<meta property="og:image" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>
<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
countries/<?php echo $this->_tpl_vars['ctrAr']['active']['image']; ?>
" />
		
		<link rel="icon" href="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
favicon.png">
		<link rel="shortcut icon" href="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
favicon.png">
		<?php if ($this->_tpl_vars['ctrAr']['active']['webmasters']): ?><meta name="google-site-verification" content="<?php echo $this->_tpl_vars['ctrAr']['active']['webmasters']; ?>
" /><?php endif; ?>
		<?php echo $this->_tpl_vars['PAGE_HEAD_TEMPLATE']; ?>

	</head>
	
	<!--[if lt IE 9 ]>    <body class="ie8"> <![endif]-->
	<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->
	
		<?php if ($this->_tpl_vars['ctrAr']['active']['google_analytics']): ?>
			 
			 <!-- Google Tag Manager -->
			<noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?php echo $this->_tpl_vars['ctrAr']['active']['google_analytics']; ?>
"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','<?php echo $this->_tpl_vars['ctrAr']['active']['google_analytics']; ?>
');</script>
			<!-- End Google Tag Manager -->
				
		<?php endif; ?>
	
		<div class="ver_1024"></div>
		<div class="ver_768"></div>
		<div class="ver_320"></div>
		
		<?php echo $this->_tpl_vars['PAGE_CONTACT_TEMPLATE']; ?>

		<section id="page">
			<?php echo $this->_tpl_vars['PAGE_HEADER_TEMPLATE']; ?>

			<?php echo $this->_tpl_vars['PAGE_BODY_TEMPLATE']; ?>

			<div class="footer_push"></div>
		</section>
		<?php echo $this->_tpl_vars['PAGE_FOOTER_TEMPLATE']; ?>

		
		
		<script src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
<?php echo $this->_tpl_vars['web']['country']; ?>
/bootstrap.min.js"></script>
		<script src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
<?php echo $this->_tpl_vars['web']['country']; ?>
/bootstrap-select.min.js"></script>
		<script src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
<?php echo $this->_tpl_vars['web']['country']; ?>
/jquery.touchSwipe.min.js"></script>
		<script src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
<?php echo $this->_tpl_vars['web']['country']; ?>
/js.js"></script>
		<script src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
functions.js"></script>
		<script src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
<?php echo $this->_tpl_vars['web']['country']; ?>
/order.js"></script>
		<script src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
<?php echo $this->_tpl_vars['web']['country']; ?>
/profile.js"></script>
		<!-- begin SnapEngage code -->
		<script type="text/javascript">
		(function() {
		var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
		se.src = '//commondatastorage.googleapis.com/code.snapengage.com/js/95dd8765-1c75-417b-9095-4364b43dc98b.js';
		var done = false;
		se.onload = se.onreadystatechange = function() {
		if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
		done = true;
		// Place your SnapEngage JS API code below
		// SnapEngage.allowChatSound(true); // Example JS API: Enable sounds for Visitors. 
		
		SnapEngage.hideButton(); 
		}
		};
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
		})();
		</script>
		<!-- end SnapEngage code -->
	</body>	
</html>