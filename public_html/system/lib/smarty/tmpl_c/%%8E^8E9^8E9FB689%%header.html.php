<?php /* Smarty version 2.6.26, created on 2015-07-14 09:48:05
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/out/orders/tmpl/header.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/orders/tmpl/header.html', 2, false),)), $this); ?>
<section class="col1a">
	<h1><?php echo ((is_array($_tmp='order_H1Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Put your first order in') : gL($_tmp, 'Put your first order in')); ?>
</h1>
	<h2><?php echo ((is_array($_tmp='order_H2Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Only custom-written papers, professional writers, always on-time delivery') : gL($_tmp, 'Only custom-written papers, professional writers, always on-time delivery')); ?>
</h2>
	<div class="form_steps clearfix">
		<?php $_from = $this->_tpl_vars['order']['config']['steps']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['ordersteps'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['ordersteps']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['ordersteps']['iteration']++;
?>
		<div class="step clearfix <?php if ($this->_tpl_vars['order']['order']['step'] >= $this->_tpl_vars['k'] || ( ! $this->_tpl_vars['order']['order'] && $this->_foreach['ordersteps']['iteration'] == 1 )): ?>active<?php endif; ?>">
			<div class="nr"><?php echo $this->_tpl_vars['k']; ?>
</div>
			<div class="text"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
		</div>
		<?php endforeach; endif; unset($_from); ?>
	</div>
</section>