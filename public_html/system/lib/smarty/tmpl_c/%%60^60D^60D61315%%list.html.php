<?php /* Smarty version 2.6.26, created on 2014-07-18 15:08:36
         compiled from /home/papersst/public_html/system/config/../../system/app/out/news/tmpl/list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/news/tmpl/list.html', 7, false),array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/news/tmpl/list.html', 14, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/out/news/tmpl/list.html', 14, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/out/news/tmpl/list.html', 24, false),)), $this); ?>

<?php if ($this->_tpl_vars['web']['image']): ?>
<section class="topimg css3"><img src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
content/<?php echo $this->_tpl_vars['web']['image']; ?>
" alt="<?php if ($this->_tpl_vars['web']['image_alt']): ?><?php echo $this->_tpl_vars['web']['image_alt']; ?>
<?php else: ?><?php echo $this->_tpl_vars['web']['title']; ?>
<?php endif; ?>" /></section>
<?php endif; ?>
<section class="w850 <?php if ($this->_tpl_vars['web']['image']): ?>has_top_img<?php endif; ?>" data-equalcol="true">	
<h1 class="h1" id="h1"><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
<?php if (count($this->_tpl_vars['news']) > 0): ?>
<section class="dlist">
	<?php $_from = $this->_tpl_vars['news']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['item'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['item']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['news']):
        $this->_foreach['item']['iteration']++;
?>
	<section class="item clearfix">
		
			<?php if ($this->_tpl_vars['news']['date_from']): ?>
			<section class="date">
				<?php echo ((is_array($_tmp='createdTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Created') : gL($_tmp, 'Created')); ?>
 - <?php echo ((is_array($_tmp=$this->_tpl_vars['news']['date_from'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d.%m.%Y") : smarty_modifier_date_format($_tmp, "%d.%m.%Y")); ?>
, <?php echo ((is_array($_tmp='updatedTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Modified') : gL($_tmp, 'Modified')); ?>
 - 
					<?php if ($this->_tpl_vars['news']['date_from'] > $this->_tpl_vars['news']['updated']): ?>	
					<?php echo ((is_array($_tmp=$this->_tpl_vars['news']['date_from'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d.%m.%Y") : smarty_modifier_date_format($_tmp, "%d.%m.%Y")); ?>

					<?php else: ?>
					<?php echo ((is_array($_tmp=$this->_tpl_vars['news']['updated'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d.%m.%Y") : smarty_modifier_date_format($_tmp, "%d.%m.%Y")); ?>

					<?php endif; ?>
			</section>
			<?php endif; ?>
		
		<?php if ($this->_tpl_vars['news']['lead_image']): ?>
			<a href="<?php echo $this->_tpl_vars['news']['page_url']; ?>
.html" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['news']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" class="image">
				<img src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['newsConfig']['uploadFolder']; ?>
small/<?php echo $this->_tpl_vars['news']['lead_image']; ?>
" alt="<?php if ($this->_tpl_vars['news']['lead_image_alt']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['lead_image_alt'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
<?php else: ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
<?php endif; ?>" />
			</a>
		<?php endif; ?>
		<h2>
			<a href="<?php echo $this->_tpl_vars['news']['page_url']; ?>
.html" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['news']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
">
				<?php echo ((is_array($_tmp=$this->_tpl_vars['news']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>

			</a>
		</h2>
		<p><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['lead'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</p>
		<?php if ($this->_tpl_vars['news']['text']): ?>
		<a href="<?php echo $this->_tpl_vars['news']['page_url']; ?>
.html" title="<?php echo ((is_array($_tmp='more')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Learn more') : gL($_tmp, 'Learn more')); ?>
" class="readmore"><?php echo ((is_array($_tmp='more')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Learn more') : gL($_tmp, 'Learn more')); ?>
</a>
		<?php endif; ?>
	</section>
	<?php endforeach; endif; unset($_from); ?>
</section>
 
<?php if ($this->_tpl_vars['pager']['previous'] || $this->_tpl_vars['pager']['next']): ?>
<section class="pager clearfix">
	<?php if ($this->_tpl_vars['pager']['previous']): ?>
		<a href="<?php echo $this->_tpl_vars['web']['url']; ?>
page:<?php echo $this->_tpl_vars['pager']['previous']; ?>
/" title="<?php echo ((is_array($_tmp='pagerPrevious')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Previous') : gL($_tmp, 'Previous')); ?>
" class="prev"><?php echo ((is_array($_tmp='pagerPrevious')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Previous') : gL($_tmp, 'Previous')); ?>
</a>
	<?php endif; ?>
	<div class="numbers">
	<?php unset($this->_sections['pagination']);
$this->_sections['pagination']['name'] = 'pagination';
$this->_sections['pagination']['loop'] = is_array($_loop=$this->_tpl_vars['pager']['total_pages']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['pagination']['show'] = true;
$this->_sections['pagination']['max'] = $this->_sections['pagination']['loop'];
$this->_sections['pagination']['step'] = 1;
$this->_sections['pagination']['start'] = $this->_sections['pagination']['step'] > 0 ? 0 : $this->_sections['pagination']['loop']-1;
if ($this->_sections['pagination']['show']) {
    $this->_sections['pagination']['total'] = $this->_sections['pagination']['loop'];
    if ($this->_sections['pagination']['total'] == 0)
        $this->_sections['pagination']['show'] = false;
} else
    $this->_sections['pagination']['total'] = 0;
if ($this->_sections['pagination']['show']):

            for ($this->_sections['pagination']['index'] = $this->_sections['pagination']['start'], $this->_sections['pagination']['iteration'] = 1;
                 $this->_sections['pagination']['iteration'] <= $this->_sections['pagination']['total'];
                 $this->_sections['pagination']['index'] += $this->_sections['pagination']['step'], $this->_sections['pagination']['iteration']++):
$this->_sections['pagination']['rownum'] = $this->_sections['pagination']['iteration'];
$this->_sections['pagination']['index_prev'] = $this->_sections['pagination']['index'] - $this->_sections['pagination']['step'];
$this->_sections['pagination']['index_next'] = $this->_sections['pagination']['index'] + $this->_sections['pagination']['step'];
$this->_sections['pagination']['first']      = ($this->_sections['pagination']['iteration'] == 1);
$this->_sections['pagination']['last']       = ($this->_sections['pagination']['iteration'] == $this->_sections['pagination']['total']);
?> 
		<a href="<?php echo $this->_tpl_vars['web']['url']; ?>
page:<?php echo $this->_sections['pagination']['iteration']; ?>
/" title="<?php echo $this->_sections['pagination']['iteration']; ?>
" class="<?php if ($this->_tpl_vars['pager']['current_page'] == $this->_sections['pagination']['iteration']): ?> active<?php endif; ?>"><?php echo $this->_sections['pagination']['iteration']; ?>
</a>
	<?php endfor; endif; ?>
	</div>

	<?php if ($this->_tpl_vars['pager']['next']): ?>
		<a href="<?php echo $this->_tpl_vars['web']['url']; ?>
page:<?php echo $this->_tpl_vars['pager']['next']; ?>
/" title="<?php echo ((is_array($_tmp='pagerNext')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Next') : gL($_tmp, 'Next')); ?>
" class="next"><?php echo ((is_array($_tmp='pagerNext')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Next') : gL($_tmp, 'Next')); ?>
</a>
	<?php endif; ?>
	
</section>
<?php endif; ?>

<?php endif; ?>
</section>