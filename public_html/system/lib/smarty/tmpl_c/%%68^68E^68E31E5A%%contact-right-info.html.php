<?php /* Smarty version 2.6.26, created on 2014-07-14 15:48:30
         compiled from /home/papersst/public_html/system/config/../../system/app/out/content/tmpl/writers/contact-right-info.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/writers/contact-right-info.html', 7, false),)), $this); ?>
<section class="contacts_block">
	<div class="button tel css3">
		<a href="#" class="icon"></a>
		<div class="sub css3">
			<div class="cont">
				<div class="cell">
					<?php echo ((is_array($_tmp='contacts_RightBlockPhone')) ? $this->_run_mod_handler('gL', true, $_tmp, '
					<div class="country">United Kingdom</div>
					<div class="phone_nr">44-203-670-0050</div>
					<div class="country">Australia</div>
					<div class="phone_nr">61-283-550-175</div>') : gL($_tmp, '
					<div class="country">United Kingdom</div>
					<div class="phone_nr">44-203-670-0050</div>
					<div class="country">Australia</div>
					<div class="phone_nr">61-283-550-175</div>')); ?>

				</div>
			</div>
		</div>
	</div>
	<div class="button msg">
		<a href="#" class="icon"></a>
		<div class="sub css3">
			<div class="cont">
				<div class="cell">
					<?php echo ((is_array($_tmp='contacts_RightBlockEmail')) ? $this->_run_mod_handler('gL', true, $_tmp, '
					<div class="mailus">Feel free to mail us!</div>
					<a href="mailto:info_at_paperstock.com" class="mail">info_at_paperstock.com</a>
					') : gL($_tmp, '
					<div class="mailus">Feel free to mail us!</div>
					<a href="mailto:info_at_paperstock.com" class="mail">info_at_paperstock.com</a>
					')); ?>

				</div>
			</div>
		</div>
	</div>
	<div class="button chat css3">
		<a href="#" class="icon"></a>
		<div class="sub css3">
			<div class="cont">
				<div class="cell">
					<div class="call_ico"></div>
					<?php echo ((is_array($_tmp='contacts_RightBlockChat')) ? $this->_run_mod_handler('gL', true, $_tmp, '
					<div class="online_chat">Online chat</div>
					<a href="#" onclick="return SnapABug.startLink();" class="btn1">Start conversation</a>
					') : gL($_tmp, '
					<div class="online_chat">Online chat</div>
					<a href="#" onclick="return SnapABug.startLink();" class="btn1">Start conversation</a>
					')); ?>

				</div>
			</div>
		</div>
	</div>
</section>