<?php /* Smarty version 2.6.26, created on 2014-08-11 21:53:08
         compiled from /home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html', 4, false),array('modifier', 'cat', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html', 8, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html', 30, false),array('modifier', 'ceil', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html', 43, false),array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html', 56, false),array('modifier', 'getLM', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html', 82, false),array('function', 'math', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/4step-ajax.html', 5, false),)), $this); ?>
<section class="col1a">
	<section class="form_summary with_form">
		<section class="wrap_onlymob clearfix">
					<?php if (count($this->_tpl_vars['order']['order']['3step']) > 0): ?>
					<?php echo smarty_function_math(array('assign' => 'coldiv2','equation' => "x / y",'x' => count($this->_tpl_vars['order']['order']['3step']),'y' => 2), $this);?>

					<?php $_from = $this->_tpl_vars['order']['order']['3step']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
						
						<?php $this->assign('ID', ((is_array($_tmp='mob_')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['k']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['k']))); ?>
						<?php if ($this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]): ?>
							
							<?php $this->assign('IDVAL', $this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]); ?>
							
							<?php if (($this->_foreach['orderdata']['iteration'] <= 1)): ?>
							<div class="col first">		
							<?php endif; ?>
							
							<div class="field clearfix">
								<div class="name"><?php echo $this->_tpl_vars['i']['name']; ?>
:</div>
								<div class="value">
								<?php if ($this->_tpl_vars['i']['values']): ?>
									<?php if ($this->_tpl_vars['k'] == 17): ?>
										<?php echo $_SESSION['order']['fields']['number_of_pages']; ?>
, <?php echo $this->_tpl_vars['i']['values'][$this->_tpl_vars['IDVAL']]['name']; ?>
 
									<?php else: ?>
										<?php echo $this->_tpl_vars['i']['values'][$this->_tpl_vars['IDVAL']]['name']; ?>

									<?php endif; ?>	
								<?php else: ?>
								
								<?php if ($this->_tpl_vars['k'] == 18): ?>
								<?php $this->assign('TIMETOSHOW', ($this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]+time()+$this->_tpl_vars['order']['order']['deadlinedays'])); ?>
								<?php echo ((is_array($_tmp=$this->_tpl_vars['TIMETOSHOW'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %I %p") : smarty_modifier_date_format($_tmp, "%e %B %I %p")); ?>

								<?php else: ?>
								<?php echo $this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]; ?>

								<?php endif; ?>
			
								<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						
						
						<?php if (($this->_foreach['orderdata']['iteration'] == $this->_foreach['orderdata']['total'])): ?>
							</div>	
						<?php elseif ($this->_foreach['orderdata']['iteration'] == ceil($this->_tpl_vars['coldiv2'])): ?>
							</div>
							<div class="col">
						<?php endif; ?>
						
					<?php endforeach; endif; unset($_from); ?>
				<?php endif; ?>
		</section>
	</section>
	
	<section class="order_form with_summary">
		<section class="wrap_onlymob clearfix">
			<div class="explanation">
				<h2><?php echo ((is_array($_tmp='order_MakePayment_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Make a secure payment') : gL($_tmp, 'Make a secure payment')); ?>
</h2>
			</div>
			<div class="field clearfix">
				<div class="col1h">
					<div class="c"></div>
					<div class="note">
						<p><?php echo ((is_array($_tmp='order_MakePayment_Note')) ? $this->_run_mod_handler('gL', true, $_tmp, 'When you click Continue, you’ll be temporarily redirected to the PayPal site to log in and confirm your payment. You’ll then be returned to complete your order!') : gL($_tmp, 'When you click Continue, you’ll be temporarily redirected to the PayPal site to log in and confirm your payment. You’ll then be returned to complete your order!')); ?>
</p>
					</div>
				</div>
				<div class="col2h clearfix">
					<div class="security clearfix css3">
						<div class="title"><?php echo ((is_array($_tmp='order_MakePaymentSecure_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Secure payments by') : gL($_tmp, 'Secure payments by')); ?>
</div>
						<br>
						<img alt="Paypal" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_paypal.png">
						<br class="no_mob">
						<img alt="MasterCard" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_mastercard.png">
						<img alt="Visa" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_visa.png">
						<img alt="America Express" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_aexpress.png">
						<img alt="Discover" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_discover.png">
					</div>
				</div>
			</div>
			<hr>
			<div class="summary has_back clearfix">
				<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back dt"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a>
				<div class="price set"><?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price $') : gL($_tmp, 'Total price $')); ?>
<span id="priceTotal"></span></div>
				<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_order_payment_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" title="<?php echo ((is_array($_tmp='order_MakePayment_Button')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Make payment') : gL($_tmp, 'Make payment')); ?>
" class="btn3 css3"><?php echo ((is_array($_tmp='order_MakePayment_Button')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Make payment') : gL($_tmp, 'Make payment')); ?>
</a>
				<div class="back_cont mob"><a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a></div>
			</div>
		</section>
	</section>
	
</section>
<script>
$(document).ready(function(){
	order.calculatePrice();
});	
</script>