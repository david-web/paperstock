<?php /* Smarty version 2.6.26, created on 2015-01-14 09:26:34
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'getLM', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 4, false),array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 4, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 18, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 52, false),array('modifier', 'ceil', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 82, false),array('modifier', 'implode', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 109, false),array('modifier', 'pathinfo', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 131, false),array('modifier', 'md5', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 138, false),array('function', 'math', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-open-1.html', 53, false),)), $this); ?>
<section class="green_title order sp30-26-20">
	<section class="wrap">
		<div class="btn_cont ver_1024 ver_768">
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_inquiry_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="btn6 ver_1024" title="<?php echo ((is_array($_tmp='header_FreeInquiryTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Free inquiry') : gL($_tmp, 'Free inquiry')); ?>
"><?php echo ((is_array($_tmp='header_FreeInquiryTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Free inquiry') : gL($_tmp, 'Free inquiry')); ?>
</a>
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_order_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="btn5 ver_1024" title="<?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
"><?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
</a>
		</div>
		<h1><?php echo ((is_array($_tmp='order')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order') : gL($_tmp, 'Order')); ?>
 <?php echo $this->_tpl_vars['profile']['data']['id']; ?>
</h1>
	</section>
</section>
<section class="wrap mob_nowrap clearfix">
	<div class="col1k">
		<div class="order_status wrap_onlymob css3 clearfix">
			<div class="time_header ver_1024 ver_320"><?php echo ((is_array($_tmp='order_TimeLeft')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Time left') : gL($_tmp, 'Time left')); ?>
</div>
			<div class="time_block">
				<div class="days"><div class="nr"><?php echo $this->_tpl_vars['profile']['data']['diffDays']; ?>
</div><div class="text"><?php echo ((is_array($_tmp='order_Days')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div></div>
				<div class="hours"><div class="nr"><?php echo $this->_tpl_vars['profile']['data']['diffHours']; ?>
</div><div class="text"><?php echo ((is_array($_tmp='order_Hours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div></div>
				<div class="clearfix"></div>
				<div class="deadline"><?php echo ((is_array($_tmp='order_Deadline')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Deadline') : gL($_tmp, 'Deadline')); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['data']['first_draft_deadline'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>
</div>
			</div>
			<div class="status <?php if (! $this->_tpl_vars['profile']['data']['paid']): ?>notpaid<?php endif; ?>">
			
				<?php if ($this->_tpl_vars['profile']['data']['writer_appointed']): ?>
				<a href="#" class="btn_status css3">
					<?php echo ((is_array($_tmp='order_Status_Pending')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Pending...') : gL($_tmp, 'Pending...')); ?>

					<?php echo ((is_array($_tmp='order_Status_WriterAppointed')) ? $this->_run_mod_handler('gL', true, $_tmp, '<span class="no_mob"><br>W</span><span class="mob_only">, w</span>riter appointed: YES') : gL($_tmp, '<span class="no_mob"><br>W</span><span class="mob_only">, w</span>riter appointed: YES')); ?>

				</a>
				<?php else: ?>
				<a href="#" class="btn_status css3"><?php echo ((is_array($_tmp='order_Status_Pending')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Pending...') : gL($_tmp, 'Pending...')); ?>
</a>
				<?php endif; ?>
				
				<a href="#" class="btn_pay <?php if (! $this->_tpl_vars['profile']['data']['paid']): ?>notpaid<?php endif; ?> css3"><?php if ($this->_tpl_vars['profile']['data']['paid']): ?><?php echo ((is_array($_tmp='order_Paid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Paid') : gL($_tmp, 'Paid')); ?>
<?php else: ?><?php echo ((is_array($_tmp='order_NotPaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Not paid') : gL($_tmp, 'Not paid')); ?>
<?php endif; ?></a>
				<div class="price">
					<?php if (! $this->_tpl_vars['profile']['data']['paid']): ?>
					<?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price') : gL($_tmp, 'Total price')); ?>
 
					<?php else: ?>
					<?php echo ((is_array($_tmp='order_TotalPricePaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price paid') : gL($_tmp, 'Total price paid')); ?>
 
					<?php endif; ?>
					<?php echo $this->_tpl_vars['profile']['data']['price']; ?>

				</div>
				
				<?php if (! $this->_tpl_vars['profile']['data']['paid']): ?>
				<div class="clearfix"></div>
				<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
id:<?php echo $this->_tpl_vars['profile']['data']['id']; ?>
/action:pay/" class="btn3"><?php echo ((is_array($_tmp='order_Pay')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Pay') : gL($_tmp, 'Pay')); ?>
</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="col2k">
		<div class="order_info wrap_onlymob css3">
			<div class="inner">
				<div class="twocols clearfix">
					<?php if (count($this->_tpl_vars['profile']['order']['3step']) > 0): ?>
						<?php echo smarty_function_math(array('assign' => 'coldiv2','equation' => "x / y",'x' => count($this->_tpl_vars['profile']['order']['3step']),'y' => 2), $this);?>

						<?php $_from = $this->_tpl_vars['profile']['order']['3step']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
							
							<?php $this->assign('ID', $this->_tpl_vars['i']['db_field']); ?>
							
							<?php if ($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]): ?>
							
								<?php if (($this->_foreach['orderdata']['iteration'] <= 1)): ?>
								<div class="col first">		
								<?php endif; ?>
								
								<div class="line clearfix">
									<div class="col1z"><?php echo $this->_tpl_vars['i']['name']; ?>
: </div>
									<div class="col2z">
									<?php if ($this->_tpl_vars['ID'] == 'first_draft_deadline'): ?>
									<?php $this->assign('TIMETOSHOW', ($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']])); ?>
									<?php echo ((is_array($_tmp=$this->_tpl_vars['TIMETOSHOW'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>

									<?php elseif ($this->_tpl_vars['ID'] == 'pages_type'): ?>
									<?php echo $this->_tpl_vars['profile']['data']['number_of_pages']; ?>
, <?php echo $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]; ?>

									<?php else: ?>
									<?php echo $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]; ?>

									<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
							
							
							<?php if (($this->_foreach['orderdata']['iteration'] == $this->_foreach['orderdata']['total'])): ?>
								</div>	
							<?php elseif ($this->_foreach['orderdata']['iteration'] == ceil($this->_tpl_vars['coldiv2'])): ?>
								</div>
								<div class="col">
							<?php endif; ?>
							
						<?php endforeach; endif; unset($_from); ?>
					<?php endif; ?>
				</div>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_TopicInFull')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Topic in full') : gL($_tmp, 'Topic in full')); ?>
:</h2>
					<p>
						<?php echo $this->_tpl_vars['profile']['data']['topic']; ?>

					</p>
				</div>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_Details')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Details') : gL($_tmp, 'Details')); ?>
:</h2>
					<p>
						<?php echo $this->_tpl_vars['profile']['data']['work_details']; ?>

					</p>
				</div>
				<?php if (count($this->_tpl_vars['profile']['data']['additional_extras']) > 0): ?>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_Extras')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Extras') : gL($_tmp, 'Extras')); ?>
:</h2>
					<p class="bolded">
						<?php echo implode(",", $this->_tpl_vars['profile']['data']['additional_extras']); ?>

					</p>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="order_collapsible open css3">
			<div class="trigger_line"><div class="inner wrap_onlymob"><?php echo ((is_array($_tmp='order_Files')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Files') : gL($_tmp, 'Files')); ?>
</div></div>
			<div class="collapsible"><div class="inner wrap_onlymob">
				<div class="files">
					<a href="javascript:;"  onclick="$('#fileForm').show();$('#newFile').hide();" id="newFile" class="btn5 m10b"><?php echo ((is_array($_tmp='order_AddNewFile')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Add new file') : gL($_tmp, 'Add new file')); ?>
</a>
					<div id="fileForm" style="display:none;">
						
						<a href="javascript:;" id="uploadButton" class="btn5"><?php echo ((is_array($_tmp='order_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
</a>
						<input type="text" value="" id="uploadButtonFile">
						<br /><br />
						
						<a href="javascript:;" onclick="profile.uploadFile();" class="btn5"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
						<a href="javascript:;" onclick="$('#uploadButtonFile').val('');$('#fileForm').hide();$('#newFile').show();" class="btn7" style="width:120px;"><?php echo ((is_array($_tmp='order_Cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
					</div>
					
					<?php $_from = $this->_tpl_vars['profile']['data']['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
					<?php $this->assign('pathinfo', pathinfo($this->_tpl_vars['i']['filename'])); ?>
					<div class="file clearfix css3">
						<div class="col1z">
							<div class="ico <?php if ($this->_tpl_vars['pathinfo']['extension'] == 'jpg' || $this->_tpl_vars['pathinfo']['extension'] == 'gif' || $this->_tpl_vars['pathinfo']['extension'] == 'png' || $this->_tpl_vars['pathinfo']['extension'] == 'jpeg'): ?>jpg<?php else: ?>docx<?php endif; ?>"></div>
							<div class="type"><?php echo $this->_tpl_vars['pathinfo']['extension']; ?>
</div>
						</div>
						<div class="col2z">
							<div class="link"><a href="/download.php?file=orders/<?php echo md5($this->_tpl_vars['profile']['data']['id']); ?>
/<?php echo $this->_tpl_vars['i']['filename']; ?>
"><?php echo $this->_tpl_vars['i']['filename']; ?>
</a></div>
							<div class="author"><?php if ($this->_tpl_vars['i']['user_id']): ?><?php echo ((is_array($_tmp='order_MessagesYou')) ? $this->_run_mod_handler('gL', true, $_tmp, 'You') : gL($_tmp, 'You')); ?>
<?php else: ?><?php echo ((is_array($_tmp='order_MessagesAdministrator')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Administrator') : gL($_tmp, 'Administrator')); ?>
<?php endif; ?>, <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>
</div>
						</div>
					</div>
					<?php endforeach; endif; unset($_from); ?>
				</div>
			</div></div>
		</div>
		<div class="order_collapsible open css3">
			<div class="trigger_line"><div class="inner wrap_onlymob"><?php echo ((is_array($_tmp='order_Messages')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Messages') : gL($_tmp, 'Messages')); ?>
</div></div>
			<div class="collapsible"><div class="inner wrap_onlymob">
				<div class="messages">
					<a href="javascript:;"  onclick="$('#msgBlock').show();$('#newMsg').hide();" id="newMsg" class="btn5 m10mob"><?php echo ((is_array($_tmp='order_NewMessage')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Create new message') : gL($_tmp, 'Create new message')); ?>
</a>
					<div class="new_msg clearfix" id="msgBlock" style="display:none;">
						<div class="cinput default"><textarea id="message" data-default=""></textarea></div>
						<a href="javascript:;" onclick="profile.orderNewMessage();" class="btn5"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
						<a href="javascript:;" onclick="$('#msgBlock').hide();$('#newMsg').show();" class="btn7"><?php echo ((is_array($_tmp='order_Cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
					</div>
					<?php $_from = $this->_tpl_vars['profile']['data']['messages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
					<div class="msg css3<?php if ($this->_tpl_vars['i']['user_id']): ?> self<?php endif; ?>"><div class="inner">
						<?php echo $this->_tpl_vars['i']['message']; ?>

						<div class="author"><?php if ($this->_tpl_vars['i']['user_id']): ?><?php echo ((is_array($_tmp='order_MessagesYou')) ? $this->_run_mod_handler('gL', true, $_tmp, 'You') : gL($_tmp, 'You')); ?>
<?php else: ?><?php echo ((is_array($_tmp='order_MessagesAdministrator')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Administrator') : gL($_tmp, 'Administrator')); ?>
<?php endif; ?>, <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>
</div>
					</div></div>
					<?php endforeach; endif; unset($_from); ?>
				</div>
			</div></div>
		</div>
		<div class="backlink_cont wrap_onlymob">
			<a href="javascript:;" onclick="history.go(-1);" class="backlink"><?php echo ((is_array($_tmp='order_Back')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to order list') : gL($_tmp, 'Back to order list')); ?>
</a>
		</div>
	</div>
</section>
<script type="text/javascript">
       
$(document).ready(function() {

	loadUploadButton('orders/temp/', 'uploadButton');

});

</script>