<?php /* Smarty version 2.6.26, created on 2014-07-31 23:05:28
         compiled from /home/papersst/public_html/system/config/../../system/app/in/users/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/users/tmpl/edit.html', 9, false),)), $this); ?>
<td id="editForma" colspan="9" class="open">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='general_data')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'General data') : gLA($_tmp, 'General data')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_username')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Username') : gLA($_tmp, 'Username')); ?>
:</td>
			            <td><input type="text" class="long" name="username" id="username" value="<?php echo $this->_tpl_vars['edit']['username']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_password')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Password') : gLA($_tmp, 'Password')); ?>
:</td>
			            <td><input type="password" class="long" name="password" id="password" value="" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_password2')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Confirm password') : gLA($_tmp, 'Confirm password')); ?>
:</td>
			            <td><input type="password" class="long" name="password2" id="password2" value="" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Name') : gLA($_tmp, 'Name')); ?>
:</td>
			            <td><input type="text" class="long" name="name" id="name" value="<?php echo $this->_tpl_vars['edit']['name']; ?>
" /></td>
			        </tr>
					<?php if ($this->_tpl_vars['groups']): ?>
					<tr id="user_group">
			            <td><?php echo ((is_array($_tmp='m_group')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'User group') : gLA($_tmp, 'User group')); ?>
:</td>
			            <td>
							<select onchange="getRolesGroup();" class="long" name="type" id="group_id">
								<option value=""></option>
								<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['gr']):
?>
								<option value="<?php echo $this->_tpl_vars['gr']['id']; ?>
" <?php if ($this->_tpl_vars['gr']['id'] == $this->_tpl_vars['edit']['group_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['gr']['name']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>
						</td>
			        </tr>
					<?php endif; ?>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_enable')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enable') : gLA($_tmp, 'Enable')); ?>
:</td>
			            <td><input type="checkbox" name="enable" id="enable" value="1" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_admin')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Admin') : gLA($_tmp, 'Admin')); ?>
:</td>
			            <td><input type="checkbox" onchange="checkForAdmin();" name="admin" id="admin" value="1" <?php if ($this->_tpl_vars['edit']['admin']): ?> checked="checked" <?php endif; ?> /></td>
			        </tr>
    			</table>
    		</td>
			<td>
    			<table id="userModules" class="inner-table">
			        <tr>
			            <td colspan="4"><b><?php echo ((is_array($_tmp='modules_list')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Modules list') : gLA($_tmp, 'Modules list')); ?>
</b></td>
			        </tr>
			        <tr>
                        <td>
                        	<div>
	                        	<?php $_from = $this->_tpl_vars['modules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['m']):
?>
								<div class="block-holder<?php if (( $this->_tpl_vars['k'] == 0 )): ?> open<?php endif; ?>">
	                                <h4><a href="#"><?php echo $this->_tpl_vars['m']['name']; ?>
</a></h4>
	                                <div class="inner-block">
                                    	<?php $_from = $this->_tpl_vars['roles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rk'] => $this->_tpl_vars['r']):
?>
                                    	<label for="<?php echo $this->_tpl_vars['r']; ?>
_<?php echo $this->_tpl_vars['m']['id']; ?>
"><?php echo ((is_array($_tmp="role_".($this->_tpl_vars['r']))) ? $this->_run_mod_handler('gLA', true, $_tmp, ($this->_tpl_vars['r'])) : gLA($_tmp, ($this->_tpl_vars['r']))); ?>
:</label> <input class="roles" type="checkbox" value="1" id="<?php echo $this->_tpl_vars['r']; ?>
_<?php echo $this->_tpl_vars['m']['id']; ?>
" <?php if ($this->_tpl_vars['edit']['roles'][$this->_tpl_vars['m']['id']][$this->_tpl_vars['r']]): ?> checked="checked"<?php endif; ?> />
							            <?php endforeach; endif; unset($_from); ?>
	                                </div>
	                            </div>
								<?php endforeach; endif; unset($_from); ?>
							</div>
                        </td>
                    </tr>	        
				</table>
			</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save_and_close')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and close') : gLA($_tmp, 'Save and close')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="$('#edit_<?php echo $this->_tpl_vars['edit']['id']; ?>
').html('');"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>
    
    <script type="text/javascript">
    $(function () {

    	checkForAdmin();
        
		$('div.block-holder h4').click(function() {
			$(this).parent().parent().children('.open').removeClass('open');
			$(this).parent().addClass('open');
			
			return false;
		});

		var tabindex = 1;
		$('#editForma :input,select').each(function() {
			if (this.type != "hidden" && this.id != "cmsLang") {
				var $input = $(this);
				$input.attr("tabindex", tabindex);
				tabindex++;
			}
		});
    });	
	</script>
</td>