<?php /* Smarty version 2.6.26, created on 2015-07-16 10:14:34
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/in/promoblocks/tmpl/promoblocks.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/in/promoblocks/tmpl/promoblocks.html', 5, false),)), $this); ?>
<div class="content">
    <table class="search-table" style="margin-bottom: 4px;">
        <tr>
            <td>
                <strong><?php echo ((is_array($_tmp='m_promo_blocks')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Promo blocks') : gLA($_tmp, 'Promo blocks')); ?>
</strong>
            </td>
        </tr>
    </table>
    <table class="search-table" style="margin-bottom: 4px;">
        <tr>
        	<td style="width: 150px;"><?php echo ((is_array($_tmp='m_country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</td>
            <td>
                <select class="long" id="country" onchange="changeFilter(); return false;">
                    <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
                    <?php echo $this->_tpl_vars['countries']; ?>

                </select>
            </td>
            <td style="width: 150px;"><?php echo ((is_array($_tmp='m_lang')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Language') : gLA($_tmp, 'Language')); ?>
:</td>
            <td>
                <select class="long" id="lang" onchange="changeFilter(); return false;">
                    <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
                    <?php echo $this->_tpl_vars['langs']; ?>

                </select>
            </td>
            <td class="bttns">
                <div class="btn">
                    <a href="javascript: void(0);" onclick="changeFilter(); return false;">
                        <span><?php echo ((is_array($_tmp='m_search')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Search') : gLA($_tmp, 'Search')); ?>
</span>
                    </a>
                </div>
                <div class="btn" style="margin-left: 4px;">
                    <a href="javascript: void(0);" onclick="clearFilter(); return false;">
                        <span><?php echo ((is_array($_tmp='clear')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Clear') : gLA($_tmp, 'Clear')); ?>
</span>
                    </a>
                </div>
            </td>
        </tr>
    </table>
    <?php echo $this->_tpl_vars['tTable']; ?>

    <div id="modulePath"></div>
    <?php echo $this->_tpl_vars['bTable']; ?>

</div>
<script type="text/javascript">
    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
    moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
    moduleTable.usePaging = true;
    <?php if ($this->_tpl_vars['MODULE_FROM']): ?>
    moduleTable.from = <?php echo $this->_tpl_vars['MODULE_FROM']; ?>
;
    <?php endif; ?>
    
    $(function() {
        updateModule();
        updateFilterValues();
    });
</script>