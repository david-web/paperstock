<?php /* Smarty version 2.6.26, created on 2014-07-31 10:37:29
         compiled from /home/papersst/public_html/system/config/../../system/app/in/promoblocks/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/promoblocks/tmpl/edit.html', 8, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/promoblocks/tmpl/edit.html', 35, false),array('modifier', 'count_characters', '/home/papersst/public_html/system/config/../../system/app/in/promoblocks/tmpl/edit.html', 36, false),array('modifier', 'hsc', '/home/papersst/public_html/system/config/../../system/app/in/promoblocks/tmpl/edit.html', 78, false),)), $this); ?>
<div class="content">
    <form id="edit_form">
        <input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
        <input type="hidden" name="country" id="country" value="<?php echo $this->_tpl_vars['edit']['country']; ?>
" />
        <table class="search-table" style="margin-bottom: 4px;">
            <tr>
                <td>
                    <strong><?php echo ((is_array($_tmp='m_promo_add_edit')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add/edit promo bloc') : gLA($_tmp, 'Add/edit promo bloc')); ?>
</strong>
                </td>
            </tr>
        </table>
        <div class="error-msg hidden" id="errorBlock"></div>
        <div class="ok-msg hidden" id="okBlock"><?php echo ((is_array($_tmp='m_data_saved')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Data has been saved') : gLA($_tmp, 'Data has been saved')); ?>
</div>
        <table class="holder-table">
            <tr>
                <td>* <?php echo ((is_array($_tmp='m_language')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Language') : gLA($_tmp, 'Language')); ?>
:</td>
                <td>
                    <select name="lang" id="lang" class="long">
                        <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
                        <?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['langs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
	                		<option <?php if ($this->_tpl_vars['langs'][$this->_sections['item']['index']]['lang'] == $this->_tpl_vars['edit']['lang']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['langs'][$this->_sections['item']['index']]['lang']; ?>
"><?php echo $this->_tpl_vars['langs'][$this->_sections['item']['index']]['title']; ?>
</option>
	                	<?php endfor; endif; ?> 
                    </select>
                </td>
            </tr>
            <tr>
                <td><?php echo ((is_array($_tmp='m_with_text')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'With text') : gLA($_tmp, 'With text')); ?>
:</td>
                <td>
                    <input type="checkbox" class="simple" name="with_text" id="with_text" value="1" <?php if ($this->_tpl_vars['edit']['with_text']): ?> checked="checked"<?php endif; ?> />
                </td>
            </tr>
            <tr>
                <td><?php echo ((is_array($_tmp='m_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
:</td>
                <td>
                    <input type="text" maxlength="55" class="long simple" name="title" id="title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" style="width: 60%;" maxlength="255" />
                    <p class="count" style="margin-bottom: 0; margin-right: 1%; float: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['title'])) ? $this->_run_mod_handler('count_characters', true, $_tmp, true) : smarty_modifier_count_characters($_tmp, true)); ?>
 / 50</p>
                    <div class="clr"><!-- clear --></div>
                </td>
            </tr>
            <tr>
                <td><?php echo ((is_array($_tmp='m_target_url')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Target URL') : gLA($_tmp, 'Target URL')); ?>
:</td>
                <td>
                    <input type="text" id="url" name="url" value="<?php echo $this->_tpl_vars['edit']['url']; ?>
" class="simple" />
					<input type="hidden" id="url_id" name="url_id" onchange="getDocumentsList('url_id', 'doc_id'); return false;" value="<?php echo $this->_tpl_vars['edit']['url_id']; ?>
" class="simple" />
					<a href="#" onclick="openSiteMapDialog('url_id', 'url', ''); return false;" class="select-btn">Select</a>
					<select id="doc_id" name="doc_id" class="simple">
						<option value="0">Select</option>
					</select>
                </td>
            </tr>
            <tr>
                <td><?php echo ((is_array($_tmp='m_target_location')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'How to open link') : gLA($_tmp, 'How to open link')); ?>
:</td>
                <td>
                    <select name="target" id="target" class="long simple">
                        <option value="_blank"<?php if ($this->_tpl_vars['edit']['target'] == '_blank' || ! $this->_tpl_vars['edit']['id']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='m_target_blank')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'New window') : gLA($_tmp, 'New window')); ?>
</option>
                        <option value="_self"<?php if ($this->_tpl_vars['edit']['target'] == '_self'): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='m_target_self')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'This window') : gLA($_tmp, 'This window')); ?>
</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>* <?php echo ((is_array($_tmp='m_image')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Image') : gLA($_tmp, 'Image')); ?>
 (1400x500):</td>
                <td>
                    <div style="width: 98%; padding: 5px;">
                        <div id="imageDiv">
                            <?php if ($this->_tpl_vars['edit']['image']): ?>
                                <img width="100" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['edit']['uploadFolder']; ?>
<?php echo $this->_tpl_vars['edit']['image']; ?>
" />
                                <a href="javascript: void(0);" onclick="emptyUploadFile('image'); return false;"><?php echo ((is_array($_tmp='m_delete')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Delete') : gLA($_tmp, 'Delete')); ?>
</a>
                                <input type="hidden" class="simple" name="image" id="image" value="<?php echo $this->_tpl_vars['edit']['image']; ?>
" />
                            <?php endif; ?>
                        </div>
                        <a href="javascript: void(0);" id="imageButton" class="select-btn  img-required"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
                    </div>
                </td>
            </tr>
            <tr>
                <td><?php echo ((is_array($_tmp='m_alt_text')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Alternative text') : gLA($_tmp, 'Alternative text')); ?>
:</td>
                <td>
                    <input type="text" class="long simple" name="alt" id="alt" value="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['edit']['alt'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)))) ? $this->_run_mod_handler('hsc', true, $_tmp) : hsc($_tmp)); ?>
" style="width: 60%;" maxlength="255" />
                </td>
            </tr>
            <tr>
                <td><?php echo ((is_array($_tmp='m_lead_text')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Description') : gLA($_tmp, 'Description')); ?>
:</td>
                <td>
                    <textarea rows="4" maxlength="150" cols="100" class="simple lead" id="lead" name="lead" style="width: 99%;"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['lead'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>                    
                    <p class="count" style="margin-bottom: 0; margin-right: 1%; float: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['lead'])) ? $this->_run_mod_handler('count_characters', true, $_tmp, true) : smarty_modifier_count_characters($_tmp, true)); ?>
 / 150</p>
                    <div class="clr"><!-- clear --></div>
                </td>
            </tr>
            <tr>
                <td>* <?php echo ((is_array($_tmp='m_seconds')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Slide time in seconds') : gLA($_tmp, 'Slide time in seconds')); ?>
:</td>
                <td>
                    <input type="text" class="long simple" name="seconds" id="seconds" value="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['edit']['seconds'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)))) ? $this->_run_mod_handler('hsc', true, $_tmp) : hsc($_tmp)); ?>
" style="width: 60%;" maxlength="4" />
                </td>
            </tr>
        </table>
        <table class="bttns-table">
            <tr>
                <td>
                    <?php if ($this->_tpl_vars['edit']['id']): ?>
                        <div class="btn orange">
                            <a href="javascript: void(0);" onclick="checkFields('apply'); return false;">
                                <span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="btn">
                        <a href="javascript: void(0);" onclick="checkFields('save'); return false;">
                            <span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span>
                        </a>
                    </div>
                    <div class="btn cancel">
                        <a href="javascript: void(0);" onclick="window.location.href = moduleTable.getRequestUrl();">
                            <span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span>
                        </a>
                    </div>
                </td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">
    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
    moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
    
    $id = parseInt('<?php echo $this->_tpl_vars['edit']['id']; ?>
');
    
    $(function() {
        $('textarea').keydown(function() {
            symbolcount(this);
        }).keyup(function() {
            symbolcount(this);
        }).focus(function() {
            symbolcount(this);
        });
        
        $('#title').keydown(function() {
            symbolcount(this);
        }).keyup(function() {
            symbolcount(this);
        }).focus(function() {
            symbolcount(this);
        });

        
        <?php if ($this->_tpl_vars['edit']['doc_id']): ?>
        getDocumentsList('url_id', 'doc_id', '<?php echo $this->_tpl_vars['edit']['doc_id']; ?>
');
		<?php endif; ?>
        
        symbolcount = function(e) {
            
        	$symbolmax = parseInt($(e).attr('maxlength'));
        	
        	$text = $(e).val();
            if($text.length > $symbolmax) {
                $(e).val($text.substr(0, $symbolmax));                
            } else {
                $(e).parent().children('.count').html($(e).val().length + ' / ' + $symbolmax);
            }
        }
        
        var config = {};
        config['max_width'] = 1400;
        config['max_height'] = 500;
        config['min_width'] = 1400;
        config['min_height'] = 500;
        config['allowed_types'] = 'jpg|jpeg|gif|png';
        loadUploadImgButton('<?php echo $this->_tpl_vars['edit']['uploadFolder']; ?>
', 'image', config);
    });
</script>
</div>