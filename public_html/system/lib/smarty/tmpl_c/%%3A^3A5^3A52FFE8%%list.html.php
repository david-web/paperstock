<?php /* Smarty version 2.6.26, created on 2015-01-06 21:52:43
         compiled from /home/papersst/public_html/system/config/../../system/app/out/promoblocks/tmpl/list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/promoblocks/tmpl/list.html', 1, false),)), $this); ?>
<?php if (count($this->_tpl_vars['promoblock']) > 0): ?>
<section id="promo">
	<div class="bg">
		<?php $_from = $this->_tpl_vars['promoblock']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['promoblock'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['promoblock']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['block']):
        $this->_foreach['promoblock']['iteration']++;
?>
		<?php if ($this->_tpl_vars['block']['url']): ?>
			<a href="<?php echo $this->_tpl_vars['block']['url']; ?>
" title="<?php echo $this->_tpl_vars['block']['title']; ?>
" target="<?php echo $this->_tpl_vars['block']['target']; ?>
">
				<img <?php if (($this->_foreach['promoblock']['iteration'] <= 1)): ?> class="active"<?php endif; ?> alt="<?php if ($this->_tpl_vars['block']['alt']): ?><?php echo $this->_tpl_vars['block']['alt']; ?>
<?php else: ?><?php echo $this->_tpl_vars['block']['title']; ?>
<?php endif; ?>" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['promoblockUploadFolder']; ?>
<?php echo $this->_tpl_vars['block']['image']; ?>
" data-img="<?php echo $this->_foreach['promoblock']['iteration']; ?>
" data-width="1400" data-height="500">
			</a>
		<?php else: ?>
			<img <?php if (($this->_foreach['promoblock']['iteration'] <= 1)): ?> class="active"<?php endif; ?> alt="<?php if ($this->_tpl_vars['block']['alt']): ?><?php echo $this->_tpl_vars['block']['alt']; ?>
<?php else: ?><?php echo $this->_tpl_vars['block']['title']; ?>
<?php endif; ?>" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['promoblockUploadFolder']; ?>
<?php echo $this->_tpl_vars['block']['image']; ?>
" data-img="<?php echo $this->_foreach['promoblock']['iteration']; ?>
" data-width="1400" data-height="500">
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
	</div>
	<section class="wrap">
		<div class="cont">
			<div class="items">
				<?php $_from = $this->_tpl_vars['promoblock']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['promoblock'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['promoblock']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['block']):
        $this->_foreach['promoblock']['iteration']++;
?>
				<?php if ($this->_tpl_vars['block']['url']): ?>
				<a href="<?php echo $this->_tpl_vars['block']['url']; ?>
" title="<?php echo $this->_tpl_vars['block']['title']; ?>
" target="<?php echo $this->_tpl_vars['block']['target']; ?>
">
					<div class="item <?php if (($this->_foreach['promoblock']['iteration'] <= 1)): ?> active<?php endif; ?>" data-item="<?php echo $this->_foreach['promoblock']['iteration']; ?>
">
						<?php echo $this->_tpl_vars['block']['title']; ?>

					</div>
				</a>	
				<?php else: ?>
					<div class="item <?php if (($this->_foreach['promoblock']['iteration'] <= 1)): ?> active<?php endif; ?>" data-item="<?php echo $this->_foreach['promoblock']['iteration']; ?>
">
						<?php echo $this->_tpl_vars['block']['title']; ?>

					</div>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
			</div>
			<div class="button_cont">
				<div class="buttons">
					<?php $_from = $this->_tpl_vars['promoblock']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['promoblock'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['promoblock']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['block']):
        $this->_foreach['promoblock']['iteration']++;
?>
					<div class="button <?php if (($this->_foreach['promoblock']['iteration'] <= 1)): ?> active<?php endif; ?>" data-img="<?php echo $this->_foreach['promoblock']['iteration']; ?>
" data-item="<?php echo $this->_foreach['promoblock']['iteration']; ?>
"></div>
					<?php endforeach; endif; unset($_from); ?>
				</div>
			</div>
		</div>
	</section>
</section>
<?php endif; ?>