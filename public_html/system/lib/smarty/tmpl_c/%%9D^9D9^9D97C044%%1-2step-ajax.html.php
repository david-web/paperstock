<?php /* Smarty version 2.6.26, created on 2015-07-14 09:48:04
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/out/orders/tmpl/1-2step-ajax.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'is_array', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/orders/tmpl/1-2step-ajax.html', 31, false),array('modifier', 'count', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/orders/tmpl/1-2step-ajax.html', 31, false),array('modifier', 'implode', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/orders/tmpl/1-2step-ajax.html', 31, false),array('modifier', 'gL', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/orders/tmpl/1-2step-ajax.html', 56, false),array('modifier', 'in_array', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/orders/tmpl/1-2step-ajax.html', 334, false),)), $this); ?>
<section class="order_form">
	<?php $_from = $this->_tpl_vars['order']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
	<?php if ($this->_tpl_vars['i']['type'] == 'tabselect'): ?>
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b">
			<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
				<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
				<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
					<?php if ($this->_tpl_vars['v']['hint']): ?>
					<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
					<?php endif; ?>
					<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
 </div>
				</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
			<div class="horizontal_dropdown">
				<select class="selectpicker <?php if ($this->_tpl_vars['i']['price']): ?> calcprice<?php endif; ?>" <?php if ($this->_tpl_vars['i']['price']): ?> onchange="order.calculatePrice();" <?php endif; ?> data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" <?php if ($this->_tpl_vars['i']['hiddenData']): ?> data-hide="<?php echo $this->_tpl_vars['i']['hiddenData']; ?>
" <?php endif; ?> >
					<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
					<option <?php if (((is_array($_tmp=$this->_tpl_vars['v']['show_fields'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp)) && count($this->_tpl_vars['v']['show_fields']) > 0): ?> data-show=".show_<?php echo ((is_array($_tmp=' .show_')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['v']['show_fields']) : implode($_tmp, $this->_tpl_vars['v']['show_fields'])); ?>
" <?php endif; ?> value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>>
						<?php echo $this->_tpl_vars['v']['name']; ?>
 
					</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>
		</div>
	</div>
	<?php elseif ($this->_tpl_vars['i']['type'] == 'select'): ?>
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b">
			<div class="select_cont">
				<select class="selectpicker <?php if ($this->_tpl_vars['i']['price']): ?> calcprice<?php endif; ?>" <?php if ($this->_tpl_vars['i']['price']): ?> onchange="order.calculatePrice();" <?php endif; ?> data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
"  <?php if ($this->_tpl_vars['i']['hiddenData']): ?> data-hide="<?php echo $this->_tpl_vars['i']['hiddenData']; ?>
" <?php endif; ?>>
					<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='order_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
					<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
					
						<?php if ($this->_tpl_vars['v']['parent']): ?>
							
							<?php if ($this->_tpl_vars['optgroupOpened']): ?>
								</optgroup>	
							<?php endif; ?>
							
							<?php $this->assign('optgroupOpened', $this->_tpl_vars['v']['id']); ?>
							
							<optgroup label="<?php echo $this->_tpl_vars['v']['name']; ?>
">
						<?php else: ?>	
							<option <?php if ($this->_tpl_vars['optgroupOpened']): ?>data-groupid="<?php echo $this->_tpl_vars['optgroupOpened']; ?>
"<?php endif; ?> <?php if (((is_array($_tmp=$this->_tpl_vars['v']['show_fields'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp)) && count($this->_tpl_vars['v']['show_fields']) > 0): ?> data-show=".show_<?php echo ((is_array($_tmp=' .show_')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['v']['show_fields']) : implode($_tmp, $this->_tpl_vars['v']['show_fields'])); ?>
" <?php endif; ?> value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>	
						<?php endif; ?>
						
						<?php if (($this->_foreach['orderdatavalues']['iteration'] == $this->_foreach['orderdatavalues']['total']) && $this->_tpl_vars['optgroupOpened']): ?>
							</optgroup>	
						<?php endif; ?>	
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>
			<?php if ($this->_tpl_vars['i']['hint_below']): ?>
			<div class="note">
				<p>
					<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b">
			<div class="cinput css3">
				<input type="text" <?php if ($this->_tpl_vars['i']['price']): ?> class="calcprice" onchange="order.calculatePrice();"<?php endif; ?> name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
			</div>
		</div>
	</div>
	<?php elseif ($this->_tpl_vars['i']['type'] == 'short_text'): ?>
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b">
			<div class="cinput css3 input1">
				<input type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
			</div>
		</div>
	</div>
	<?php elseif ($this->_tpl_vars['i']['type'] == 'textarea'): ?>
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b">
			<div class="cinput css3">
				<textarea name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['selected']; ?>
</textarea>
			</div>
		</div>
	</div>
	<?php elseif ($this->_tpl_vars['i']['type'] == 'pages'): ?>
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php if ($_SESSION['order']['fields']['mob_2'] == 1221 || $_SESSION['order']['fields']['mob_2'] == 1222): ?>
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php elseif ($_SESSION['order']['fields']['mob_2'] == 1223): ?>
					<?php echo ((is_array($_tmp='order_NumberOfQuestions')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Number of questions') : gL($_tmp, 'Number of questions')); ?>

					<?php elseif ($_SESSION['order']['fields']['mob_2'] == 1224): ?>
					<?php echo ((is_array($_tmp='order_NumberOfProblems')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Number of problems') : gL($_tmp, 'Number of problems')); ?>

					<?php endif; ?>
					
					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b clearfix">
			<div class="col1c">
				<div class="cinput css3">
					<input type="text" <?php if ($this->_tpl_vars['i']['price']): ?> class="calcprice" onchange="order.calculatePrice();" <?php endif; ?> name="number_of_pages" id="number_of_pages" value="<?php if ($_SESSION['order']['fields']['number_of_pages']): ?><?php echo $_SESSION['order']['fields']['number_of_pages']; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']['default_val']; ?>
<?php endif; ?>">
				</div>
			</div>
			
			<?php if ($_SESSION['order']['fields']['mob_2'] == 1221 || $_SESSION['order']['fields']['mob_2'] == 1222): ?>
			
			<div class="col2c clearfix">
				<div class="col1d">
					<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
						<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
						<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 2 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
							<?php if ($this->_tpl_vars['v']['hint']): ?>
							<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
							<?php endif; ?>
							<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
						</div>
						<?php endforeach; endif; unset($_from); ?>
					</div>
					<div class="horizontal_dropdown">
						<select class="selectpicker <?php if ($this->_tpl_vars['i']['price']): ?> calcprice <?php endif; ?>" <?php if ($this->_tpl_vars['i']['price']): ?> onchange="order.calculatePrice();" <?php endif; ?> data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-show=".show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 2 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
							<?php endforeach; endif; unset($_from); ?>
						</select>
					</div>
				</div>
				<div class="col2d">
					<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
					<?php if ($this->_tpl_vars['v']['hint']): ?>
					<div class="note2 hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if (( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 ) || ( $this->_tpl_vars['i']['selected'] && $this->_tpl_vars['v']['id'] != $this->_tpl_vars['i']['selected'] )): ?>style="display:none;"<?php endif; ?>>
						<?php echo $this->_tpl_vars['v']['hint']; ?>

					</div>
					<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?>
				</div>
			</div>
			
			<?php endif; ?>
		</div>
	</div>
	<?php elseif ($this->_tpl_vars['i']['type'] == 'hourdate'): ?>
	
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b clearfix select_group" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
			<div class="col1c">
				<div class="special1 css3 hours"><?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
				<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
					<div class="option <?php if (((is_array($_tmp='orders_FormsFieldHours_9')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>has_tip<?php endif; ?> <?php if (32400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="32400">
						<?php if (((is_array($_tmp='orders_FormsFieldHours_9')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3"><?php echo ((is_array($_tmp='orders_FormsFieldHours_9')) ? $this->_run_mod_handler('gL', true, $_tmp, '9 Hours') : gL($_tmp, '9 Hours')); ?>
</div>
						<?php endif; ?>
						<div class="text">9</div>
					</div>
					<div class="option <?php if (((is_array($_tmp='orders_FormsFieldHours_24')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>has_tip<?php endif; ?> <?php if (86400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="86400">
						<?php if (((is_array($_tmp='orders_FormsFieldHours_24')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3"><?php echo ((is_array($_tmp='orders_FormsFieldHours_24')) ? $this->_run_mod_handler('gL', true, $_tmp, '24 Hours') : gL($_tmp, '24 Hours')); ?>
</div>
						<?php endif; ?>
						<div class="text">24</div>
					</div>
				</div>
			</div>
			<div class="col2c">
				<div class="special1 css3 days"><?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
				<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
					<div class="option <?php if (((is_array($_tmp='orders_FormsFieldDays_2')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>has_tip<?php endif; ?>  <?php if (172800 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="172800">
						<?php if (((is_array($_tmp='orders_FormsFieldDays_2')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3"><?php echo ((is_array($_tmp='orders_FormsFieldDays_2')) ? $this->_run_mod_handler('gL', true, $_tmp, '2 Days') : gL($_tmp, '2 Days')); ?>
</div>
						<?php endif; ?>
						<div class="text">2</div>
					</div>
					<div class="option <?php if (((is_array($_tmp='orders_FormsFieldDays_3')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>has_tip<?php endif; ?> <?php if (259200 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="259200">
						<?php if (((is_array($_tmp='orders_FormsFieldDays_3')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3"><?php echo ((is_array($_tmp='orders_FormsFieldDays_3')) ? $this->_run_mod_handler('gL', true, $_tmp, '3 Days') : gL($_tmp, '3 Days')); ?>
</div>
						<?php endif; ?>
						<div class="text">3</div>
					</div>
					<div class="option <?php if (((is_array($_tmp='orders_FormsFieldDays_6')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>has_tip<?php endif; ?> <?php if (518400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="518400">
						<?php if (((is_array($_tmp='orders_FormsFieldDays_6')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3"><?php echo ((is_array($_tmp='orders_FormsFieldDays_6')) ? $this->_run_mod_handler('gL', true, $_tmp, '6 Days') : gL($_tmp, '6 Days')); ?>
</div>
						<?php endif; ?>
						<div class="text">6</div>
					</div>
					<div class="option <?php if (((is_array($_tmp='orders_FormsFieldDays_10')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>has_tip<?php endif; ?> <?php if (864000 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="864000">
						<?php if (((is_array($_tmp='orders_FormsFieldDays_10')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3"><?php echo ((is_array($_tmp='orders_FormsFieldDays_10')) ? $this->_run_mod_handler('gL', true, $_tmp, '10 Days') : gL($_tmp, '10 Days')); ?>
</div>
						<?php endif; ?>
						<div class="text">10</div>
					</div>
					<div class="option <?php if (((is_array($_tmp='orders_FormsFieldDays_14')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>has_tip<?php endif; ?> <?php if (1209600 == $this->_tpl_vars['i']['selected'] || ! $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="1209600">
						<?php if (((is_array($_tmp='orders_FormsFieldDays_14')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3"><?php echo ((is_array($_tmp='orders_FormsFieldDays_14')) ? $this->_run_mod_handler('gL', true, $_tmp, '14 Days') : gL($_tmp, '14 Days')); ?>
</div>
						<?php endif; ?>
						<div class="text">14</div>
					</div>
				</div>
			</div>
			<div class="horizontal_dropdown">
				<select class="selectpicker <?php if ($this->_tpl_vars['i']['price']): ?> calcprice <?php endif; ?>" <?php if ($this->_tpl_vars['i']['price']): ?> onchange="order.calculatePrice(); order.changeDeadlineNotice();" <?php endif; ?> data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
					<option value="32400" <?php if (32400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>9 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
					<option value="86400" <?php if (86400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
					<option value="172800" <?php if (172800 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
					<option value="259200" <?php if (259200 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
					<option value="518400" <?php if (518400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
					<option value="864000" <?php if (864000 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
					<option value="1209600" <?php if (1209600 == $this->_tpl_vars['i']['selected'] || ! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
				</select>
			</div>
			<div class="c"></div>
			<div class="note3">

				<div><?php echo ((is_array($_tmp='order_DeadLineNotice_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'The deadline for the first draft is') : gL($_tmp, 'The deadline for the first draft is')); ?>

					<div <?php if (((is_array($_tmp='order_DeadLineNoticeHint_1')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>class="has_tip"<?php endif; ?>>
						<span id="deadline1"></span>
						<?php if (((is_array($_tmp='order_DeadLineNoticeHint_1')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3">
							<?php echo ((is_array($_tmp='order_DeadLineNoticeHint_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint 1') : gL($_tmp, 'Hint 1')); ?>

						</div>
						<?php endif; ?>
					</div>
				</div>
				<div>
					<?php echo ((is_array($_tmp='order_DeadLineNotice_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'We estimate that your final submission deadline is') : gL($_tmp, 'We estimate that your final submission deadline is')); ?>

					<div <?php if (((is_array($_tmp='order_DeadLineNoticeHint_2')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>class="has_tip"<?php endif; ?>>
						<span id="deadline2"></span>
						<?php if (((is_array($_tmp='order_DeadLineNoticeHint_2')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp))): ?>
						<div class="tip css3">
							<?php echo ((is_array($_tmp='order_DeadLineNoticeHint_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint 2') : gL($_tmp, 'Hint 2')); ?>

						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php elseif ($this->_tpl_vars['i']['type'] == 'extra'): ?>
	<div class="field clearfix <?php if ($this->_tpl_vars['i']['hidden']): ?> show_<?php echo $this->_tpl_vars['i']['id']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['hidden']): ?> style="display:none;" <?php endif; ?>>
		<div class="col1b">
			<div class="label_cont"><div class="label_center">
				<label class="has_tip">
					<?php echo $this->_tpl_vars['i']['name']; ?>

					<?php if ($this->_tpl_vars['i']['hint_over']): ?>
					<div class="tip css3">
						<?php echo $this->_tpl_vars['i']['hint_over']; ?>

					</div>
					<?php endif; ?>
				</label>
			</div></div>
		</div>
		<div class="col2b">
			<div class="checkbox_block css3 clearfix">
				<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
				<div class="option <?php if (is_array($_SESSION['order']['fields']['additional_extras']) && in_array($this->_tpl_vars['v']['id'], $_SESSION['order']['fields']['additional_extras'])): ?>active<?php endif; ?>">
					<input type="checkbox" <?php if (is_array($_SESSION['order']['fields']['additional_extras']) && in_array($this->_tpl_vars['v']['id'], $_SESSION['order']['fields']['additional_extras'])): ?>checked="checked"<?php endif; ?> id="<?php echo $this->_tpl_vars['v']['id']; ?>
" class="checkbox extra" onchange="order.calculatePrice();" name="additional_extras" value="<?php echo $this->_tpl_vars['v']['name']; ?>
" />
					<div class="text">
						<span class="has_tip"><?php echo $this->_tpl_vars['v']['name']; ?>

							<?php if ($this->_tpl_vars['v']['hint']): ?>
							<span class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</span>
							<?php endif; ?>
						</span>
					</div>
					<div class="price">$<?php echo $this->_tpl_vars['v']['price']; ?>
</div>
				</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>
	
	<hr>
	<?php if ($this->_tpl_vars['order']['order']['prevStep']): ?>
	<div class="summary has_back clearfix">
		<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back dt"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a>
		<div class="price set"><?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price $') : gL($_tmp, 'Total price $')); ?>
<span id="priceTotal"></span></div>
		<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
);" class="btn3 css3"><?php echo ((is_array($_tmp='order_GoToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Go to step') : gL($_tmp, 'Go to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
</a>
		<div class="back_cont mob"><a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a></div>
	</div>
	<?php else: ?>
	<div class="summary clearfix">
		<div class="price"><?php echo ((is_array($_tmp='order_ApproxPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Approximate price $') : gL($_tmp, 'Approximate price $')); ?>
<span id="priceTotal"></span></div>
		<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
);" class="btn3 css3"><?php echo ((is_array($_tmp='order_GoToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Go to step') : gL($_tmp, 'Go to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
</a>
	</div>
	<?php endif; ?>
</section>
<script>
$(document).ready(function(){
	
    if ($("#mob_2").length) {
        if ($("#mob_2").val() == 1221 || $("#mob_2").val() == 1222) {
        	$(".show_4").show();
        }
    }
    
    if ($("#number_of_pages").length) {
        $("#number_of_pages").change();
    }
    
    if ($("#mob_18").length) {
        $("#mob_18").change();
    }
    
    order.calculatePrice();
});
</script>