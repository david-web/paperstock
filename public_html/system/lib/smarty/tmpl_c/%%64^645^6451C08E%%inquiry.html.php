<?php /* Smarty version 2.6.26, created on 2015-07-16 09:05:45
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/out/profile/tmpl/1/inquiry.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'getLM', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/profile/tmpl/1/inquiry.html', 4, false),array('modifier', 'gL', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/profile/tmpl/1/inquiry.html', 4, false),)), $this); ?>
<section class="green_title order sp30-40-0">
	<section class="wrap">
		<div class="btn_cont ver_1024 ver_768">
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_order_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="btn5 ver_1024" title="<?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
"><?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
</a>
		</div>
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
	</section>
</section>
<section class="wrap mob_nowrap clearfix">
	<section class="col1a">
		<div class="form_bg">
			<section class="order_form wrap_onlymob">
				<?php $_from = $this->_tpl_vars['order']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
				<?php if ($this->_tpl_vars['i']['type'] == 'tabselect'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
								<?php if ($this->_tpl_vars['v']['hint']): ?>
								<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
								<?php endif; ?>
								<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
 </div>
							</div>
							<?php endforeach; endif; unset($_from); ?>
						</div>
						<div class="horizontal_dropdown">
							<select class="selectpicker inquiry" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" >
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<option  value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>>
									<?php echo $this->_tpl_vars['v']['name']; ?>
 
								</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'select'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="select_cont">
							<select class="selectpicker inquiry" data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='order_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								
									<?php if ($this->_tpl_vars['v']['parent']): ?>
										
										<?php if ($this->_tpl_vars['optgroupOpened']): ?>
											</optgroup>	
										<?php endif; ?>
										
										<?php $this->assign('optgroupOpened', $this->_tpl_vars['v']['id']); ?>
										
										<optgroup label="<?php echo $this->_tpl_vars['v']['name']; ?>
">
									<?php else: ?>	
										<option <?php if ($this->_tpl_vars['optgroupOpened']): ?>data-groupid="<?php echo $this->_tpl_vars['optgroupOpened']; ?>
"<?php endif; ?> value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>	
									<?php endif; ?>
									
									<?php if (($this->_foreach['orderdatavalues']['iteration'] == $this->_foreach['orderdatavalues']['total']) && $this->_tpl_vars['optgroupOpened']): ?>
										</optgroup>	
									<?php endif; ?>	
								<?php endforeach; endif; unset($_from); ?>
							</select>
						</div>
						<?php if ($this->_tpl_vars['i']['hint_below']): ?>
						<div class="note">
							<p>
								<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
							</p>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="cinput css3">
							<input type="text" class="inquiry" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'short_text'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="cinput css3 input1">
							<input type="text" class="inquiry" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'textarea'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="cinput css3">
							<textarea name="<?php echo $this->_tpl_vars['i']['id']; ?>
" class="inquiry" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['selected']; ?>
</textarea>
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'pages'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								
								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix">
						<div class="col1c">
							<div class="cinput css3">
								<input type="text" class="inquiry" name="number_of_pages" id="number_of_pages" value="<?php if ($_SESSION['order']['fields']['number_of_pages']): ?><?php echo $_SESSION['order']['fields']['number_of_pages']; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']['default_val']; ?>
<?php endif; ?>">
							</div>
						</div>
						
						<div class="col2c clearfix">
							<div class="col1d">
								<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
										<?php if ($this->_tpl_vars['v']['hint']): ?>
										<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
										<?php endif; ?>
										<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
									</div>
									<?php endforeach; endif; unset($_from); ?>
								</div>
								<div class="horizontal_dropdown">
									<select class="selectpicker inquiry" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
										<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-show=".show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
										<?php endforeach; endif; unset($_from); ?>
									</select>
								</div>
							</div>
							<div class="col2d">
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<?php if ($this->_tpl_vars['v']['hint']): ?>
								<div class="note2 hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_foreach['orderdatavalues']['iteration'] != 1): ?>style="display:none;"<?php endif; ?>>
									<?php echo $this->_tpl_vars['v']['hint']; ?>

								</div>
								<?php endif; ?>
								<?php endforeach; endif; unset($_from); ?>
							</div>
						</div>
						
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'hourdate'): ?>
				
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix select_group" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
						<div class="col1c">
							<div class="special1 css3 hours"><?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
							<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<div class="option has_tip <?php if (32400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="32400">
									<div class="tip css3">9 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
									<div class="text">9</div>
								</div>
								<div class="option has_tip <?php if (86400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="86400">
									<div class="tip css3">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
									<div class="text">24</div>
								</div>
							</div>
						</div>
						<div class="col2c">
							<div class="special1 css3 days"><?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
							<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<div class="option has_tip  <?php if (172800 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="172800">
									<div class="tip css3">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">2</div>
								</div>
								<div class="option has_tip <?php if (259200 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="259200">
									<div class="tip css3">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">3</div>
								</div>
								<div class="option has_tip <?php if (518400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="518400">
									<div class="tip css3">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">6</div>
								</div>
								<div class="option has_tip <?php if (864000 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="864000">
									<div class="tip css3">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">10</div>
								</div>
								<div class="option has_tip <?php if (1209600 == $this->_tpl_vars['i']['selected'] || ! $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="1209600">
									<div class="tip css3">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">14</div>
								</div>
							</div>
						</div>
						<div class="horizontal_dropdown">
							<select class="selectpicker inquiry" onchange="order.changeDeadlineNotice();" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<option value="32400" <?php if (32400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>9 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
								<option value="86400" <?php if (86400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
								<option value="172800" <?php if (172800 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="259200" <?php if (259200 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="518400" <?php if (518400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="864000" <?php if (864000 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="1209600" <?php if (1209600 == $this->_tpl_vars['i']['selected'] || ! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
							</select>
						</div>
						<div class="c"></div>
						<div class="note3">
			
							<div><?php echo ((is_array($_tmp='order_DeadLineNotice_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'The deadline for the first draft is') : gL($_tmp, 'The deadline for the first draft is')); ?>

								<div class="has_tip">
									<span id="deadline1"></span>
									<div class="tip css3">
										<?php echo ((is_array($_tmp='order_DeadLineNoticeHint_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint 1') : gL($_tmp, 'Hint 1')); ?>

									</div>
								</div>
							</div>
							<div>
								<?php echo ((is_array($_tmp='order_DeadLineNotice_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'We estimate that your final submission deadline is') : gL($_tmp, 'We estimate that your final submission deadline is')); ?>

								<div class="has_tip">
									<span id="deadline2"></span>
									<div class="tip css3">
										<?php echo ((is_array($_tmp='order_DeadLineNoticeHint_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint 2') : gL($_tmp, 'Hint 2')); ?>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				<hr class="free">
				<div class="summary free2 clearfix">
					<a href="javascript:;" onclick="profile.registerInquiry();" class="btn3 css3" title="<?php echo ((is_array($_tmp='inquiry_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit inquiry') : gL($_tmp, 'Submit inquiry')); ?>
"><?php echo ((is_array($_tmp='inquiry_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit inquiry') : gL($_tmp, 'Submit inquiry')); ?>
</a>
					<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['mainpageId'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="cancel" title="<?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
"><?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
				</div>
			</section>
		</div>
	</section>
</section>
<script>
$(document).ready(function(){
	
	order.changeDeadlineNotice();
});
</script>	