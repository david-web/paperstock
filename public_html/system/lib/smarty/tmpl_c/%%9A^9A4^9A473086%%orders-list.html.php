<?php /* Smarty version 2.6.26, created on 2015-07-14 09:47:08
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/out/profile/tmpl/1/orders-list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'getLM', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/profile/tmpl/1/orders-list.html', 4, false),array('modifier', 'gL', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/profile/tmpl/1/orders-list.html', 4, false),array('modifier', 'count', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/profile/tmpl/1/orders-list.html', 88, false),)), $this); ?>
<section class="green_title order sp26-26-0">
	<section class="wrap">
		<div class="btn_cont">
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_inquiry_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="btn6 ver_1024" title="<?php echo ((is_array($_tmp='header_FreeInquiryTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Free inquiry') : gL($_tmp, 'Free inquiry')); ?>
"><?php echo ((is_array($_tmp='header_FreeInquiryTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Free inquiry') : gL($_tmp, 'Free inquiry')); ?>
</a>
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_order_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="btn5 ver_1024" title="<?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
"><?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
</a>
			<div class="orders ver_320">
				<select class="selectpicker" data-width="100%"  onchange="orderFilter(this, '<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
');">
					<?php $_from = $this->_tpl_vars['profile']['config']['status']['profile']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderstatus'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderstatus']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderstatus']['iteration']++;
?>
					<option <?php if (! $_GET['status'] || $_GET['status'] == $this->_tpl_vars['k']): ?>selected="selected"<?php endif; ?> value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>
		</div>
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
	</section>
</section>

<?php if ($this->_tpl_vars['SHOW_PAYMENT_ERROR']): ?>
<section class="list_message wrap mob_nowrap css3">
	<div class="wrap_onlymob">
		<h2><?php echo ((is_array($_tmp='profile_ErrorPaymentTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Error! Order payment error!') : gL($_tmp, 'Error! Order payment error!')); ?>
</h2>
		<p><?php echo ((is_array($_tmp='profile_ErrorPaymentText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Error! Order payment error! Error! Order payment error! Error! Order payment error! Error! Order payment error! Error! Order payment error! ') : gL($_tmp, 'Error! Order payment error! Error! Order payment error! Error! Order payment error! Error! Order payment error! Error! Order payment error! ')); ?>
</p>
	</div>
</section>
<?php endif; ?>

<?php if ($this->_tpl_vars['SHOW_INQUIRY_MESSAGE']): ?>
<section class="list_message wrap mob_nowrap css3">
	<div class="wrap_onlymob">
		<h2><?php echo ((is_array($_tmp='profile_InquiryMessageTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Inquiry title!') : gL($_tmp, 'Inquiry title!')); ?>
</h2>
		<p><?php echo ((is_array($_tmp='profile_InquiryMessageText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Inquiry message! ') : gL($_tmp, 'Inquiry message! ')); ?>
</p>
	</div>
</section>
<?php endif; ?>

<?php if ($this->_tpl_vars['SHOW_REGISTRATION_MESSAGE']): ?>
<section class="list_message wrap mob_nowrap css3">
	<div class="wrap_onlymob">
		<h2><?php echo ((is_array($_tmp='profile_RegistrationMessageTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Registration title!') : gL($_tmp, 'Registration title!')); ?>
</h2>
		<p><?php echo ((is_array($_tmp='profile_RegistrationMessageText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Registration message! ') : gL($_tmp, 'Registration message! ')); ?>
</p>
	</div>
</section>
<?php endif; ?>

<?php if ($this->_tpl_vars['SHOW_ORDER_MESSAGE']): ?>
<section class="list_message wrap mob_nowrap css3">
	<div class="wrap_onlymob">
		<h2><?php echo ((is_array($_tmp='profile_OrderMessageTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order title!') : gL($_tmp, 'Order title!')); ?>
</h2>
		<p><?php echo ((is_array($_tmp='profile_OrderMessageText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order message! ') : gL($_tmp, 'Order message! ')); ?>
</p>
	</div>
</section>
<?php endif; ?>

<section class="wrap mob_nowrap clearfix">
	<nav class="list_menu ver_1024 ver_768 clearfix">
		<ul class="show ver_1024">
			<?php $_from = $this->_tpl_vars['profile']['config']['status']['profile']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderstatus'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderstatus']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderstatus']['iteration']++;
?>
			
			<?php if (( ! $_GET['status'] && $this->_tpl_vars['k'] == 0 ) || $_GET['status'] == $this->_tpl_vars['k']): ?>
			<li  class="active">
				<div class="item"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
			</li>
			<?php else: ?>
			<li>
				<a href="<?php if ($this->_tpl_vars['k'] == 0): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
<?php else: ?><?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
status:<?php echo $this->_tpl_vars['k']; ?>
/<?php endif; ?>" class="item">
					<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</a>
			</li>
			<?php endif; ?>
			
			<?php endforeach; endif; unset($_from); ?>
		</ul>
		<div class="show2 ver_768">
			<select class="selectpicker" data-width="100%"  onchange="orderFilter(this, '<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
');">
				<?php $_from = $this->_tpl_vars['profile']['config']['status']['profile']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderstatus'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderstatus']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderstatus']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if (! $_GET['status'] || $_GET['status'] == $this->_tpl_vars['k']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</div>
		<div class="sort">
			<select class="selectpicker" id="ordersSort" data-width="100%" onchange="ordersSort('<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
');">
				<option <?php if ($_GET['sort'] == 'deadline' || ! $_GET['sort']): ?>selected="selected"<?php endif; ?> value="deadline"><?php echo ((is_array($_tmp='profile_SortByDeadline')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sort by deadline') : gL($_tmp, 'Sort by deadline')); ?>
</option>
				<option <?php if ($_GET['sort'] == 'created'): ?>selected="selected"<?php endif; ?> value="created"><?php echo ((is_array($_tmp='profile_SortByCreated')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sort by created') : gL($_tmp, 'Sort by created')); ?>
</option>
			</select>
		</div>
	</nav>
	
	<?php if (count($this->_tpl_vars['profile']['data']) > 0): ?>
	
	<div class="order_list ver_1024 ver_768">
		<div class="header css3 clearfix">
			<div class="col col1"><?php echo ((is_array($_tmp='profile_OrdersTable_ID')) ? $this->_run_mod_handler('gL', true, $_tmp, 'ID') : gL($_tmp, 'ID')); ?>
</div>
			<div class="col col2"><?php echo ((is_array($_tmp='profile_OrdersTable_Price')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Price') : gL($_tmp, 'Price')); ?>
</div>
			<div class="col col3"><?php echo ((is_array($_tmp='profile_OrdersTable_ItemsCount')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Items count') : gL($_tmp, 'Items count')); ?>
</div>
			<div class="col col4"><?php echo ((is_array($_tmp='profile_OrdersTable_TypeOfPaper')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Type of paper') : gL($_tmp, 'Type of paper')); ?>
</div>
			<div class="col col5"><?php echo ((is_array($_tmp='profile_OrdersTable_Subject')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Subject') : gL($_tmp, 'Subject')); ?>
</div>
			<div class="col col6"><?php echo ((is_array($_tmp='profile_OrdersTable_Topic')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Topic') : gL($_tmp, 'Topic')); ?>
</div>
			<div class="col col7"><?php echo ((is_array($_tmp='profile_OrdersTable_Status')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Status') : gL($_tmp, 'Status')); ?>
</div>
			<div class="col col8"><?php echo ((is_array($_tmp='profile_OrdersTable_Deadline')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Deadline') : gL($_tmp, 'Deadline')); ?>
</div>
			<div class="col col9"><?php echo ((is_array($_tmp='profile_OrdersTable_OrderStatus')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order stat<span class="no_mob">us</span><span class="no_dt">.</span>') : gL($_tmp, 'Order stat<span class="no_mob">us</span><span class="no_dt">.</span>')); ?>
</div>
		</div>
		<?php $_from = $this->_tpl_vars['profile']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
		<div class="line">
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
id:<?php echo $this->_tpl_vars['i']['id']; ?>
/" class="line_link <?php if ($this->_tpl_vars['i']['status'] == 1): ?>bolded<?php endif; ?> clearfix">
				<div class="col col1"><?php echo $this->_tpl_vars['i']['id']; ?>
</div>
				<div class="col col2"><?php echo $this->_tpl_vars['i']['price']; ?>
</div>
				<div class="col col3"><?php echo $this->_tpl_vars['i']['number_of_pages']; ?>
</div>
				<div class="col col4"><?php echo $this->_tpl_vars['i']['type_of_work']; ?>
</div>
				<div class="col col5"><?php echo $this->_tpl_vars['i']['subject']; ?>
</div>
				<div class="col col6"><?php echo $this->_tpl_vars['i']['topic']; ?>
</div>
				<div class="col col7"><?php if ($this->_tpl_vars['i']['paid']): ?><?php echo ((is_array($_tmp='profile_OrdersPaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Paid') : gL($_tmp, 'Paid')); ?>
<?php else: ?><?php echo ((is_array($_tmp='profile_OrdersNotPaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Not Paid') : gL($_tmp, 'Not Paid')); ?>
<?php endif; ?></div>
				<div class="col col8"><?php echo $this->_tpl_vars['i']['diffDays']; ?>
d <?php echo $this->_tpl_vars['i']['diffHours']; ?>
h</div>
				<div class="col col9">
					<?php if ($this->_tpl_vars['i']['status'] == 1): ?>
					<?php echo ((is_array($_tmp='profile_OrdersStatus_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Pending') : gL($_tmp, 'Pending')); ?>
	
					<?php elseif ($this->_tpl_vars['i']['status'] == 2): ?>
					<?php echo ((is_array($_tmp='profile_OrdersStatus_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Approved') : gL($_tmp, 'Approved')); ?>

					<?php elseif ($this->_tpl_vars['i']['status'] == 3): ?>
					<?php echo ((is_array($_tmp='profile_OrdersStatus_3')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Finished') : gL($_tmp, 'Finished')); ?>

					<?php elseif ($this->_tpl_vars['i']['status'] == 4): ?>
					<?php echo ((is_array($_tmp='profile_OrdersStatus_4')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Canceled') : gL($_tmp, 'Canceled')); ?>

					<?php endif; ?>
				</div>
				<div class="col col10"></div>
			</a>
			<?php if (! $this->_tpl_vars['i']['paid']): ?>
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
id:<?php echo $this->_tpl_vars['i']['id']; ?>
/action:pay/" class="btn8 css3 green">
				<?php echo ((is_array($_tmp='profile_OrdersPay')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Pay') : gL($_tmp, 'Pay')); ?>

			</a>
			<?php elseif ($this->_tpl_vars['i']['status'] == 3): ?>
				<?php if ($this->_tpl_vars['i']['rated']): ?>
					<a href="javascript:;" class="btn8 css3"><?php echo ((is_array($_tmp='profile_OrdersRated')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Rated') : gL($_tmp, 'Rated')); ?>
</a>
				<?php else: ?>
					<a href="javascript:;" onclick="profile.getOrderRatesForm(<?php echo $this->_tpl_vars['i']['id']; ?>
);" class="btn8 css3"><?php echo ((is_array($_tmp='profile_OrdersRate')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Rate') : gL($_tmp, 'Rate')); ?>
</a>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php if (count($this->_tpl_vars['i']['messages']) > 0 || count($this->_tpl_vars['i']['files']) > 0): ?>
			<div class="icons">
				<?php if (count($this->_tpl_vars['i']['messages']) > 0): ?>
				<a href="#" class="icon green"></a>
				<?php endif; ?>
				<?php if (count($this->_tpl_vars['i']['files']) > 0): ?>
				<a href="#" class="icon yellow"><?php echo count($this->_tpl_vars['i']['files']); ?>
</a>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endforeach; endif; unset($_from); ?>
		
	</div>
	<div class="order_list2 ver_320">
			<?php $_from = $this->_tpl_vars['profile']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
			<div class="item">
				<div class="wrap_onlymob">
					<div class="info1"><a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
id:<?php echo $this->_tpl_vars['i']['id']; ?>
/"><?php echo ((is_array($_tmp='profile_OrdersTable_ID')) ? $this->_run_mod_handler('gL', true, $_tmp, 'ID') : gL($_tmp, 'ID')); ?>
: <?php echo $this->_tpl_vars['i']['id']; ?>
</a></div>
					<div class="info2 clearfix">
						<div class="col1">
							<div class="title"><?php echo ((is_array($_tmp='profile_OrdersTable_TypeOfPaper')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Type of paper') : gL($_tmp, 'Type of paper')); ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['i']['type_of_work']; ?>
</div>
						</div>
						<div class="col2">
							<div class="title"><?php echo ((is_array($_tmp='profile_OrdersTable_Subject')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Subject') : gL($_tmp, 'Subject')); ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['i']['subject']; ?>
</div>
						</div>
					</div>
					<div class="info3">
						<div class="title"><?php echo ((is_array($_tmp='profile_OrdersTable_Topic')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Topic') : gL($_tmp, 'Topic')); ?>
:</div>
						<div class="value"><?php echo $this->_tpl_vars['i']['topic']; ?>
</div>
					</div>
				</div>
				<div class="line">
					<div class="wrap_onlymob clearfix">
						<div class="col col1 bolded"><?php echo $this->_tpl_vars['i']['price']; ?>
</div>
						<div class="col col2"><?php if ($this->_tpl_vars['i']['paid']): ?><?php echo ((is_array($_tmp='profile_OrdersPaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Paid') : gL($_tmp, 'Paid')); ?>
<?php else: ?><?php echo ((is_array($_tmp='profile_OrdersNotPaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Not Paid') : gL($_tmp, 'Not Paid')); ?>
<?php endif; ?></div>
						<div class="col col3"><?php echo $this->_tpl_vars['i']['deadline']; ?>
</div>
						<div class="col col4">
							<?php if ($this->_tpl_vars['i']['status'] == 1): ?>
							<?php echo ((is_array($_tmp='profile_OrdersStatus_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Pending') : gL($_tmp, 'Pending')); ?>
	
							<?php elseif ($this->_tpl_vars['i']['status'] == 2): ?>
							<?php echo ((is_array($_tmp='profile_OrdersStatus_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Approved') : gL($_tmp, 'Approved')); ?>

							<?php elseif ($this->_tpl_vars['i']['status'] == 3): ?>
							<?php echo ((is_array($_tmp='profile_OrdersStatus_3')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Finished') : gL($_tmp, 'Finished')); ?>

							<?php elseif ($this->_tpl_vars['i']['status'] == 4): ?>
							<?php echo ((is_array($_tmp='profile_OrdersStatus_4')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Canceled') : gL($_tmp, 'Canceled')); ?>

							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; endif; unset($_from); ?>
			
		</div>
	
	<?php else: ?>
	
	<div class="order_list_empty wrap mob_nowrap css3">
		<div class="wrap_onlymob">
			<h2><?php echo ((is_array($_tmp='profile_OrdersEmptyTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'No orders yet') : gL($_tmp, 'No orders yet')); ?>
</h2>
			<?php echo ((is_array($_tmp='profile_OrdersEmptyText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
			<br>
			<p>Praesent in porta magna. Nunc hendrerit libero sed justo pharetra, eu tincidunt tellus tempus.</p>') : gL($_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
			<br>
			<p>Praesent in porta magna. Nunc hendrerit libero sed justo pharetra, eu tincidunt tellus tempus.</p>')); ?>

			
		</div>
	</div>
	
	<?php endif; ?>
</section>