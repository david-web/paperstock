<?php /* Smarty version 2.6.26, created on 2014-09-17 08:43:43
         compiled from /home/papersst/public_html/system/config/../../system/app/out/content/tmpl/writers/startpage.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/writers/startpage.html', 6, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/writers/startpage.html', 8, false),array('modifier', 'getLM', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/writers/startpage.html', 15, false),)), $this); ?>
<section id="content">
	<?php echo $this->_tpl_vars['PROMOBLOCK_CONTENT']; ?>

	<section class="startpage_slider">
		<section class="wrap">
			<section class="calc_cont css3 clearfix">
				<h2><?php echo ((is_array($_tmp='startpage_ApplyFormTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'More than 34 jobs are awaiting for you!') : gL($_tmp, 'More than 34 jobs are awaiting for you!')); ?>
</h2>
				<div class="slider_cont">
					<?php if (count($this->_tpl_vars['apply']['orders']) > 4): ?>
					<div class="arrow left"></div>
					<div class="arrow right"></div>
					<?php endif; ?>
					<div class="slider_actual">
						<div class="inner">
							<?php $_from = $this->_tpl_vars['apply']['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['apply'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['apply']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['apply']['iteration']++;
?>
							<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_signup_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="item css3">
								<div class="text"><?php echo $this->_tpl_vars['i']['topic']; ?>
</div>
								<div class="stats">
									<div class="words css3">
										<?php echo $this->_tpl_vars['i']['number_of_pages']; ?>

										<?php if ($this->_tpl_vars['i']['type_of_services'] == 'Problem solving' || $this->_tpl_vars['i']['type_of_services'] == 'Multiple Choices'): ?>
										<?php echo ((is_array($_tmp='order_Questions')) ? $this->_run_mod_handler('gL', true, $_tmp, 'questions') : gL($_tmp, 'questions')); ?>
 
										<?php else: ?>
										<?php echo ((is_array($_tmp='order_Pages')) ? $this->_run_mod_handler('gL', true, $_tmp, 'pages') : gL($_tmp, 'pages')); ?>

										<?php endif; ?>
									</div>
									<div class="price css3">$<?php echo $this->_tpl_vars['i']['price']; ?>
</div>
								</div>
							</a>
							<?php endforeach; endif; unset($_from); ?>
						</div>
					</div>
				</div>
				<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_signup_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="apply"><?php echo ((is_array($_tmp='startpage_ApplyNow')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Apply now') : gL($_tmp, 'Apply now')); ?>
</a>
			</section>
		</section>
	</section>
	<section class="whyus">
		<section class="wrap clearfix">
			<div class="extra"><div class="h2"><?php echo ((is_array($_tmp='startpage_WhyChooseUsTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Why choose us?') : gL($_tmp, 'Why choose us?')); ?>
</div></div>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item1_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item1">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item1_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, '345 professionals from UK and US') : gL($_tmp, '345 professionals from UK and US')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item2_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item2">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item2_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Support team available 24/7') : gL($_tmp, 'Support team available 24/7')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item3_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item3">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item3_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Save your time! Delivery in 8h') : gL($_tmp, 'Save your time! Delivery in 8h')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item4_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item4">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item4_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order under your complete control') : gL($_tmp, 'Order under your complete control')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item5_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item5">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item5_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Discounts for returning customers') : gL($_tmp, 'Discounts for returning customers')); ?>
</div>
			</a>
		</section>
	</section>
	<section class="how_it_works">
		<div class="bg">
			<img alt="<?php echo ((is_array($_tmp='startpage_HowItWorksTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'How it works?') : gL($_tmp, 'How it works?')); ?>
" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
how_it_works.jpg">
		</div>
		<section class="wrap">
			<h2><?php echo ((is_array($_tmp='startpage_HowItWorksTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'How it works?') : gL($_tmp, 'How it works?')); ?>
</h2>
			<div class="cont">
				<div class="link onetwo"></div>
				<div class="link twothree"></div>
				<div class="link threefour"></div>
				<div class="link fourfive"></div>
				<div class="link fivesix"></div>
				<div class="step step1 css3">
					<div class="nr">1</div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step1')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
				</div>
				<div class="step step2 css3">
					<div class="nr">2</div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step2')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
				</div>
				<div class="step step3 css3">
					<div class="nr">3</div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step3')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
				</div>
				<div class="step step4 css3">
					<div class="nr">4</div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step4')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
				</div>
				<div class="step step5 css3">
					<div class="nr">5</div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step5')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
				</div>
				<div class="step step6 css3">
					<div class="nr">6</div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step6')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
				</div>
			</div>
		</section>
	</section>
	<section class="safety">
		<section class="wrap clearfix">
			<div class="col">
				<div class="cont">
					<div class="cont2">
						<div class="text"><?php echo ((is_array($_tmp='startpage_WhyFeelSafeTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Why will you feel safe with us?') : gL($_tmp, 'Why will you feel safe with us?')); ?>
</div>
					</div>
				</div>
				<?php echo ((is_array($_tmp='startpage_WhyFeelSafeBlock')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

			</div>
		</section>
	</section>
	<section class="statistics">
		<section class="wrap">
			<h2><?php echo ((is_array($_tmp='startpage_OurLiveStatsTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Our LIVE stats') : gL($_tmp, 'Our LIVE stats')); ?>
</h2>
			<div class="list_cont">
				<div class="list clearfix">
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_1.png">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_AvgPricePerOrder_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '20') : gL($_tmp, '20')); ?>
">0</span>$</div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_AvgPricePerOrder_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Average price per order') : gL($_tmp, 'Average price per order')); ?>
</div>
					</div>
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_2.png">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_ActiveWriters_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '50') : gL($_tmp, '50')); ?>
">0</span></div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_ActiveWriters_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Active writers') : gL($_tmp, 'Active writers')); ?>
</div>
					</div>
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_3.png">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
">0</span></div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Orders delivered') : gL($_tmp, 'Orders delivered')); ?>
</div>
					</div>
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_4.png">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_ReturningCustomers_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '99') : gL($_tmp, '99')); ?>
">0</span>%</div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_ReturningCustomers_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Returning customers') : gL($_tmp, 'Returning customers')); ?>
</div>
					</div>
				</div>
				<div class="button_cont">
					<div class="buttons">
						<div class="button css3"></div>
						<div class="button css3"></div>
						<div class="button css3"></div>
						<div class="button css3"></div>
					</div>
				</div>
			</div>
		</section>
	</section>
	<section class="aboutus">
		<section class="wrap">
			<div class="cols userinput clearfix">
				<div class="col">
					<?php echo ((is_array($_tmp='startpage_AboutUsBlock1')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</div>
				<div class="col">
					<?php echo ((is_array($_tmp='startpage_AboutUsBlock2')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</div>
				<div class="col">
					<?php echo ((is_array($_tmp='startpage_AboutUsBlock3')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</div>
			</div>
		</section>
	</section>
</section>