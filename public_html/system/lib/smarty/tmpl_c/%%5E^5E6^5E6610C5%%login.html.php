<?php /* Smarty version 2.6.26, created on 2014-07-08 23:49:01
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/login.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/login.html', 5, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/login.html', 87, false),)), $this); ?>
<section class="wrap clearfix">
		<section class="col1a">
			<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
			<h2 class="notlogged">
				<?php echo ((is_array($_tmp='profile_SignIn_LeadText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Condimentum tristique. Not a member? <a href="#">Join Papersstock</a>!') : gL($_tmp, 'Condimentum tristique. Not a member? <a href="#">Join Papersstock</a>!')); ?>

			</h2>
		</section>
	</section>
	
	<section class="form_bg">
		<section class="wrap clearfix">
			<section class="col1a">
				<section class="order_form">
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip"><?php echo ((is_array($_tmp='order_LoginTab_Fields_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>

									<?php if (((is_array($_tmp='order_LoginTab_Fields_EmailHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint'))): ?>
									<div class="tip css3"><?php echo ((is_array($_tmp='order_LoginTab_Fields_EmailHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint')); ?>
</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix">
							<div class="col1e">
								<div class="cinput css3">
									<input type="text" class="login" name="email" id="email" value="" />
								</div>
							</div>
						</div>
					</div>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip"><?php echo ((is_array($_tmp='order_LoginTab_Fields_Password')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Password') : gL($_tmp, 'Password')); ?>

									<?php if (((is_array($_tmp='order_LoginTab_Fields_PasswordHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint'))): ?>
									<div class="tip css3"><?php echo ((is_array($_tmp='order_LoginTab_Fields_PasswordHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint')); ?>
</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix">
							<div class="col1e">
								<div class="cinput css3">
									<input type="password" class="login" name="password" id="password" value="" />
								</div>
							</div>
							<div class="col2e clearfix">
								<a href="#" class="forgot"><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Forgot your password?') : gL($_tmp, 'Forgot your password?')); ?>
</a>
							</div>
						</div>
					</div>
					<div class="forgot_pw_block" style="display: none;">
						<hr>
						<div class="explanation">
							<h2><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Forgot your password?') : gL($_tmp, 'Forgot your password?')); ?>
</h2>
							<p><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.') : gL($_tmp, 'Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.')); ?>
</p>
						</div>
						<div class="info_submitted_msg css3" id="pswdOkMsg" style="display:none;"><?php echo ((is_array($_tmp='profile_ForgotPasswordOkMsg')) ? $this->_run_mod_handler('gL', true, $_tmp, 'New password has been sent to your email! <a href="#">Sign in with a new password</a>') : gL($_tmp, 'New password has been sent to your email! <a href="#">Sign in with a new password</a>')); ?>
</div>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label><?php echo ((is_array($_tmp='order_LoginTab_Fields_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>
</label>
								</div></div>
							</div>
							<div class="col2b clearfix">
								<div class="col1e">
									<div class="cinput css3">
										<input type="text" id="password_reminder">
									</div>
								</div>
								<div class="col2e to_the_left clearfix">
									<a href="javascript:;" onclick="profile.passwordReminder();" class="btn4 css3"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
								</div>
							</div>
						</div>
					</div>
					
					<hr>
					<div class="authorize_cont">
						<a href="javascript:;" onclick="profile.profileLogin();" class="btn3 css3"><?php echo ((is_array($_tmp='profile_LoginButton')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Login') : gL($_tmp, 'Login')); ?>
</a>
					</div>
				</section>
			</section>
		<section class="col2a">
			<section class="side_safety css3">
				<?php if (count($this->_tpl_vars['menu']['ORDERS_RIGHT']) > 0): ?>	
						<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['menu']['ORDERS_RIGHT']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
						<div><a href="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
"><?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
</a></div>	
						<?php endfor; endif; ?>
					<?php endif; ?>
			</section>
			<section class="small_stats clearfix">
				<img alt="<?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats.png">
				<div class="nr"><?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
</div>
				<div class="text"><?php echo ((is_array($_tmp='news_OurLiveStats_OrdersDelivered_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Orders<br>delivered') : gL($_tmp, 'Orders<br>delivered')); ?>
</div>
			</section>
		</section>
	</section>
</section>
<script type="text/javascript">

	$(function () { 

		$('#email, #password').keyup(function(e){
			if (!e) var e = window.event;
			var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : null;
			if (keyCode == 13) {
				profile.profileLogin();
			}
			return false;	
		});
		
		$('#password_reminder').keyup(function(e){
			if (!e) var e = window.event;
			var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : null;
			if (keyCode == 13) {
				profile.passwordReminder();
			}
			return false;	
		});

		
	});	
</script>