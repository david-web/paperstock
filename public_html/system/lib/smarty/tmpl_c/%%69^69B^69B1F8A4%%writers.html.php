<?php /* Smarty version 2.6.26, created on 2014-07-15 10:37:17
         compiled from /home/papersst/public_html/system/config/../../system/app/in/writers/tmpl/writers.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/writers/tmpl/writers.html', 7, false),)), $this); ?>
<div class="content">
	<form class="forma" method="post" action="" onsubmit="return false;">
		<fieldset>
			<table class="search-table" cellpadding="0" cellspacing="0">
			    <tr>
			        <td>
			            <label for="filterID"><?php echo ((is_array($_tmp='id')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client ID') : gLA($_tmp, 'Client ID')); ?>
:</label>
			            <input class="filter" type="text" id="filterID" name="filterID" />
			        </td>
			        <td>
			            <label for="filterName"><?php echo ((is_array($_tmp='name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client Name') : gLA($_tmp, 'Client Name')); ?>
:</label>
			            <input class="filter" type="text" id="filterName" name="filterName" />
			        </td>
			        <td>
			            <label for="filterLastName"><?php echo ((is_array($_tmp='last_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client last Name') : gLA($_tmp, 'Client last Name')); ?>
:</label>
			            <input class="filter" type="text" id="filterLastName" name="filterLastName" />
			        </td>
			        <td>
			            <label for="filterEmail"><?php echo ((is_array($_tmp='email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client email') : gLA($_tmp, 'Client email')); ?>
:</label>
			            <input class="filter" type="text" id="filterEmail" name="filterEmail" />
			        </td>
			        <td>
			            <label for="filterPhone"><?php echo ((is_array($_tmp='phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client phone') : gLA($_tmp, 'Client phone')); ?>
:</label>
			            <input class="filter" type="text" id="filterPhone" name="filterPhone" />
			        </td>
			        <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="changeFilter(); return false;"><span><?php echo ((is_array($_tmp='m_search')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Search') : gLA($_tmp, 'Search')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
	               <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="clearFilter(); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
			    </tr>
			</table>
			
			<?php echo $this->_tpl_vars['tTable']; ?>

			
			<div id="modulePath">
			</div>
			
			<?php echo $this->_tpl_vars['bTable']; ?>

	
		</fieldset>
	</form>
	
	<script type="text/javascript">
		moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
		moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
		moduleTable.usePaging = true;
		<?php if ($this->_tpl_vars['MODULE_FROM']): ?>
	    moduleTable.from = <?php echo $this->_tpl_vars['MODULE_FROM']; ?>
;
	    <?php endif; ?>

		$(document).ready(function() {
			
			updateModule();	
			
		});
	</script>
</div>	