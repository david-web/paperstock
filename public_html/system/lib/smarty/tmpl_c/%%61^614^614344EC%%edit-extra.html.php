<?php /* Smarty version 2.6.26, created on 2014-08-05 21:33:45
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/edit-extra.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/edit-extra.html', 6, false),)), $this); ?>
<div class="popup_bg" style="block;"></div>
<div class="popup small popup_changeinfo" style="display:block;">
	<div class="close"></div>
	<div class="top css3">
		<div class="wrap_onlymob">
			<h2><?php echo ((is_array($_tmp='profile_EditAdditionalInfo')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Edit additional information') : gL($_tmp, 'Edit additional information')); ?>
</h2>
			<hr>
			<?php $_from = $this->_tpl_vars['profile']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['register'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['register']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['register']['iteration']++;
?>
			<?php if ($this->_tpl_vars['i']['type'] == 'select'): ?>
			<div class="field">
				<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
				<select class="selectpicker extrainfo" name="<?php echo $this->_tpl_vars['i']['db_field']; ?>
" id="<?php echo $this->_tpl_vars['i']['db_field']; ?>
" data-width="100%">
					<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='profile_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
					<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['datavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['datavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['datavalues']['iteration']++;
?>
					<option value="<?php echo $this->_tpl_vars['v']['name']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>
			<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
			<div class="field">
				<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
				<div class="cinput"><input class="extrainfo" type="text" name="<?php echo $this->_tpl_vars['i']['db_field']; ?>
" id="<?php echo $this->_tpl_vars['i']['db_field']; ?>
"></div>
			</div>
			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</div>
	</div>
	<div class="btm css3 clearfix">
		<div class="wrap_onlymob">
			<a href="javascript:;" onclick="profile.saveExtraInfo();" class="btn1"><?php echo ((is_array($_tmp='profile_EditAdditionalInfo_MakeChanges')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Make changes') : gL($_tmp, 'Make changes')); ?>
</a>
		</div>
	</div>
</div>
<script>
$('.popup_bg,.popup .close').click(function(){
	$('.popup_bg,.popup').remove();
});
</script>