<?php /* Smarty version 2.6.26, created on 2014-07-22 13:25:50
         compiled from messages_form.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'clear', 'messages_form.html', 7, false),array('modifier', 'date_format', 'messages_form.html', 8, false),array('modifier', 'gLA', 'messages_form.html', 18, false),)), $this); ?>
<div id="messages_form" class="main-tab">
	<table class="inner-table">
		
		<?php $_from = $this->_tpl_vars['edit']['messages']['1']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
		<tr>
			<td colspan="2">
				<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['message'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>

				<p><?php if ($this->_tpl_vars['i']['user_id']): ?>Customer<?php else: ?>Administrator<?php endif; ?>, <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p")); ?>
</p>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
        <tr>
            <td colspan="2"><b>Add message:</b></td>
        </tr>
        <tr>
            <td colspan="2">
            	<textarea class="message" rel="1" id="message" name="message" rows="5" style="width:500px"></textarea>
            	<p class="wys"><a href="#" onclick="openCkEditor('message', 'advanced'); return false;"><?php echo ((is_array($_tmp='wysiwyg')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'WYSIWYG') : gLA($_tmp, 'WYSIWYG')); ?>
</a></p>
			</td>
        </tr>
	</table>
</div>