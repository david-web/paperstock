<?php /* Smarty version 2.6.26, created on 2015-07-14 18:18:44
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/in/content/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/in/content/tmpl/edit.html', 5, false),array('modifier', 'clear', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/in/content/tmpl/edit.html', 19, false),array('modifier', 'count', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/in/content/tmpl/edit.html', 204, false),)), $this); ?>
<form class="forma" id="editForma" method="post" action="" onsubmit="return false;">
	<input type="hidden" class="notInsert" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<ul class="tabs">
		<li><a href="#"><?php echo ((is_array($_tmp='content')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Content') : gLA($_tmp, 'Content')); ?>
</a></li>
		<li class="active"><a href="#"><?php echo ((is_array($_tmp='settings')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Settings') : gLA($_tmp, 'Settings')); ?>
</a></li>
		<li><a href="#"><?php echo ((is_array($_tmp='settings_advanced')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Advanced settings') : gLA($_tmp, 'Advanced settings')); ?>
</a></li>
	</ul>
	
	<div class="areaBlock">
		<div class="tabs-block">
			<div class="btn"><a href="#" onclick="checkFields(); return false;"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
			<div class="btn cancel"><a href="#" onclick="$('#contentData').html(''); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
			<div class="clr"><!-- clear --></div>
			<div class="fieldset">
				<ul>
					<li>
						<label for="content"><?php echo ((is_array($_tmp='content')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Content') : gLA($_tmp, 'Content')); ?>
</label>
						 <textarea name="content" class="inherit" id="content" cols="20" rows="7" <?php if ($this->_tpl_vars['edit']['template'] > 1): ?> disabled<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['content'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>
                         <p class="wys"><a href="#" onclick="openCkEditor('content', 'advanced'); return false;"><?php echo ((is_array($_tmp='wysiwyg')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'WYSIWYG') : gLA($_tmp, 'WYSIWYG')); ?>
</a></p>
                         <div class="clr"><!-- clear --></div>
					</li>
					<li>
			            <label for="image"><?php echo ((is_array($_tmp='m_image')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Image') : gLA($_tmp, 'Image')); ?>
</label>
			            <div>
			            	<div id="imageDiv">
			            	<?php if ($this->_tpl_vars['edit']['image']): ?>
			            	<img width="100" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['edit']['uploadFolder']; ?>
<?php echo $this->_tpl_vars['edit']['image']; ?>
"><a href="#" onclick="emptyUploadFile('image'); return false;"><?php echo ((is_array($_tmp='m_delete')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Delete') : gLA($_tmp, 'Delete')); ?>
</a><input type="hidden" class="simple inherit" name="image" id="image" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['image'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
">
			            	<?php endif; ?>
			            	</div>
			            	<a href="#" id="imageButton" class="select-btn"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
		            	</div>
			        </li>
			        <li>
						<label for="page_title"><?php echo ((is_array($_tmp='m_image_alt')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Image Alt text') : gLA($_tmp, 'Image Alt text')); ?>
</label>
						<input type="text" class="inherit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['image_alt'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="image_alt" name="image_alt" />
					</li>
					<li>
						<label for="page_title"><?php echo ((is_array($_tmp='page_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Page title') : gLA($_tmp, 'Page title')); ?>
</label>
						<input type="text" class="inherit" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['page_title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="page_title" name="page_title" />
					</li>
					<li>
						<label for="description"><?php echo ((is_array($_tmp='m_description')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Description') : gLA($_tmp, 'Description')); ?>
</label>
						 <textarea class="inherit" name="description" id="description" cols="20" rows="5"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['description'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>
                         <p class="wys"><!-- <a href="#"><?php echo ((is_array($_tmp='wysiwyg')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'WYSIWYG') : gLA($_tmp, 'WYSIWYG')); ?>
</a>  --></p>
                         <div class="clr"><!-- clear --></div>
					</li>
					<li>
						<label for="keywords"><?php echo ((is_array($_tmp='keywords')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Keywords') : gLA($_tmp, 'Keywords')); ?>
</label>
						<input class="inherit" type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['keywords'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="keywords" name="keywords" />
					</li>
				</ul>
			</div>
			<div class="btn"><a href="#" onclick="checkFields(); return false;"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
			<div class="btn cancel"><a href="#" onclick="$('#contentData').html(''); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
			<div class="clr"><!-- clear --></div>    
		</div>
		<div class="tabs-block open">
			<div class="btn"><a href="#" onclick="checkFields(); return false;"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
			<div class="btn cancel"><a href="#" onclick="$('#contentData').html(''); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
			<div class="clr"><!-- clear --></div>
			<div class="fieldset">
				<h3><?php echo ((is_array($_tmp='public_info')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Public info') : gLA($_tmp, 'Public info')); ?>
</h3>
				<ul>
					<?php if ($this->_tpl_vars['edit']['id']): ?>
					<li>
						<label for="title">ID:</label>
						<span id="currentPageId"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['id'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</span>
					</li>
					<?php endif; ?>
					<li>
						<label for="title"><?php echo ((is_array($_tmp='m_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
</label>
						<input type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="title" name="title" onblur="createUrl(); return false;" />
					</li>
                    <li>
						<label for="url"><?php echo ((is_array($_tmp='url')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'URL') : gLA($_tmp, 'URL')); ?>
</label>
						<input type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['url'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="url" name="url" />
					</li>
				</ul>
			</div>
			<div class="fieldset">
				<h3><?php echo ((is_array($_tmp='place_and_type')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Place and type') : gLA($_tmp, 'Place and type')); ?>
</h3>
				<ul>
					<li>
						<label for="country"><?php echo ((is_array($_tmp='country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</label>
		                <select id="country" name="country" onchange="return false;">
		                	<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['edit']['countries']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
		                		<option <?php if ($this->_tpl_vars['edit']['countries']['data'][$this->_sections['item']['index']]['id'] == $this->_tpl_vars['edit']['countries']['sel']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['edit']['countries']['data'][$this->_sections['item']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['edit']['countries']['data'][$this->_sections['item']['index']]['title']; ?>
</option>
		                	<?php endfor; endif; ?> 
		                </select>
					</li>
					<li>
						<label for="lang"><?php echo ((is_array($_tmp='lang')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Language') : gLA($_tmp, 'Language')); ?>
</label>
						<select id="lang" name="lang">
							<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['edit']['languages']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
	                			<option <?php if ($this->_tpl_vars['edit']['languages']['data'][$this->_sections['item']['index']]['lang'] == $this->_tpl_vars['edit']['languages']['sel']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['edit']['languages']['data'][$this->_sections['item']['index']]['lang']; ?>
"><?php echo $this->_tpl_vars['edit']['languages']['data'][$this->_sections['item']['index']]['title']; ?>
</option>
	                		<?php endfor; endif; ?>
						</select>
					</li>
					<?php if (! $this->_tpl_vars['edit']['url'] || ( $this->_tpl_vars['edit']['url'] != $this->_tpl_vars['edit']['lang'] )): ?>
					<li>
						<label for="parent_idTitle"><?php echo ((is_array($_tmp='parent_page')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Parent page') : gLA($_tmp, 'Parent page')); ?>
</label>
						<input type="hidden" name="parent_id" id="parent_id" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['parent_id'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" />
						<input type="text" class="notInsert" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['parent_idTitle'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="parent_idTitle" name="parent_idTitle" readonly="readonly" />
						<a href="#" onclick="openSiteMapDialog('parent_id', 'parent_idTitle', '','<?php echo $this->_tpl_vars['edit']['languages']['sel']; ?>
'); return false;" class="select-btn"><?php echo ((is_array($_tmp='select')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select') : gLA($_tmp, 'Select')); ?>
</a>
						<a href="#" onclick="$('#parent_id').val(''); $('#parent_idTitle').val(''); return false;"><?php echo ((is_array($_tmp='clear')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Clear') : gLA($_tmp, 'Clear')); ?>
</a>
					</li>
					<?php endif; ?>
					<li>
						<label for="template"><?php echo ((is_array($_tmp='template')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Template') : gLA($_tmp, 'Template')); ?>
</label>
						<select id="template" name="template">
							<option value=""><?php echo ((is_array($_tmp='select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
							<?php echo $this->_tpl_vars['edit']['templateList']; ?>

						</select>
					</li>
					<li>
						<label for="layout"><?php echo ((is_array($_tmp='layout')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Layout') : gLA($_tmp, 'Layout')); ?>
</label>
						<select id="layout" name="layout">
							<option value=""><?php echo ((is_array($_tmp='select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
							<?php echo $this->_tpl_vars['edit']['layoutList']; ?>

						</select>
					</li>
					<li>
						<label for="menu_id"><?php echo ((is_array($_tmp='menu')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Menu') : gLA($_tmp, 'Menu')); ?>
</label>
						<select multiple="multiple" id="menu_id" name="menu_id">
							<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['edit']['menuList']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
		               			<option value="<?php echo $this->_tpl_vars['edit']['menuList'][$this->_sections['item']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['edit']['menuList'][$this->_sections['item']['index']]['sel']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['edit']['menuList'][$this->_sections['item']['index']]['name']; ?>
</option>
		               		<?php endfor; endif; ?>
							<?php echo $this->_tpl_vars['edit']['menuList']; ?>

						</select>
					</li>
				</ul>
			</div>
			<div class="fieldset last">
				<h3><?php echo ((is_array($_tmp='status')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Status') : gLA($_tmp, 'Status')); ?>
</h3>
				<ul>
					<li>
						<label for="active"><?php echo ((is_array($_tmp='enabled')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enabled') : gLA($_tmp, 'Enabled')); ?>
</label>
						<input type="checkbox" value="1" id="active" class="active" <?php if ($this->_tpl_vars['edit']['active']): ?> checked="checked" <?php endif; ?> />
					</li>
					<li>
						<label for="enable"><?php echo ((is_array($_tmp='visible')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Visible') : gLA($_tmp, 'Visible')); ?>
</label>
						<input type="checkbox" value="1" id="enable" class="enable" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> />
					</li>
					<li>
						<label for="ssl"><?php echo ((is_array($_tmp='ssl')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'SSL') : gLA($_tmp, 'SSL')); ?>
</label>
						<input type="checkbox" value="1" id="ssl" class="ssl" <?php if ($this->_tpl_vars['edit']['ssl']): ?> checked="checked" <?php endif; ?> />
					</li>
				</ul>
			</div>
			<div class="btn"><a href="#" onclick="checkFields(); return false;"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
			<div class="btn cancel"><a href="#" onclick="$('#contentData').html(''); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
			<div class="clr"><!-- clear --></div>   
		</div>
		<div class="tabs-block">
			<div class="btn"><a href="#" onclick="checkFields(); return false;"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
			<div class="btn cancel"><a href="#" onclick="$('#contentData').html(''); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
			<div class="clr"><!-- clear --></div>
			<div class="fieldset">
				<h3><?php echo ((is_array($_tmp='dependend_content')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Dependend content') : gLA($_tmp, 'Dependend content')); ?>
</h3>
				<ul>
					<li>
						<label for="type"><?php echo ((is_array($_tmp='type')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Content type') : gLA($_tmp, 'Content type')); ?>
</label>
						<div class="check">
							<div><input class="check" type="radio" value="s" name="type" onchange="inheritChecker();" <?php if ($this->_tpl_vars['edit']['type'] == 's' || ! $this->_tpl_vars['edit']['type']): ?> checked="checked" <?php endif; ?> /> <?php echo ((is_array($_tmp='independend_page')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Independend page') : gLA($_tmp, 'Independend page')); ?>
</div>
							<div><input class="check" type="radio" value="r" name="type" onchange="inheritChecker();" <?php if ($this->_tpl_vars['edit']['type'] == 'r'): ?> checked="checked" <?php endif; ?> /> <?php echo ((is_array($_tmp='redirect_page')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Redirect page') : gLA($_tmp, 'Redirect page')); ?>
</div>
							<div><input class="check" type="radio" value="i" name="type" id="inherit" onchange="inheritChecker();" <?php if ($this->_tpl_vars['edit']['type'] == 'i'): ?> checked="checked" <?php endif; ?> /> <?php echo ((is_array($_tmp='inherit_page')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Inherit content from another page') : gLA($_tmp, 'Inherit content from another page')); ?>
</div>
						</div>	
					</li>
					<li>
						<label for="target"><?php echo ((is_array($_tmp='target')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Target') : gLA($_tmp, 'Target')); ?>
</label>
						<input type="hidden" name="target" id="target" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['target'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" />	
						<input type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['targetTitle'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="targetTitle" name="targetTitle" <?php if ($this->_tpl_vars['edit']['targetReadOnly']): ?> readonly="readonly" <?php endif; ?> />
						<a href="#" onclick="openSiteMapDialog('target', 'targetTitle', ''); return false;" class="select-btn"><?php echo ((is_array($_tmp='select')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select') : gLA($_tmp, 'Select')); ?>
</a>
						<a href="#" onclick="$('#target').val(''); $('#targetTitle').val(''); return false;"><?php echo ((is_array($_tmp='clear')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Clear') : gLA($_tmp, 'Clear')); ?>
</a>
					</li>
				</ul>
			</div>
			<div class="fieldset">
				<h3><?php echo ((is_array($_tmp='other_settings')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Other settings') : gLA($_tmp, 'Other settings')); ?>
</h3>
				<ul>
					<li>
						<label for="active"><?php echo ((is_array($_tmp='content_sitemap')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Show in sitemap') : gLA($_tmp, 'Show in sitemap')); ?>
</label>
						<input type="checkbox" value="1" id="sitemap" class="sitemap" <?php if ($this->_tpl_vars['edit']['sitemap']): ?> checked="checked" <?php endif; ?> />
						<select name="changefreq" id="changefreq">
							<?php echo $this->_tpl_vars['edit']['changefreq']; ?>

						</select>
					</li>
					<li>
						<label for="active"><?php echo ((is_array($_tmp='cache')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cache') : gLA($_tmp, 'Cache')); ?>
</label>
						<input type="checkbox" value="1" id="cache" class="cache" <?php if ($this->_tpl_vars['edit']['cache']): ?> checked="checked" <?php endif; ?> />
					</li>
                    <li>
						<label for="mirror_id"><?php echo ((is_array($_tmp='mirrors')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Mirrors') : gLA($_tmp, 'Mirrors')); ?>
</label>
						<input type="hidden" name="mirror_id" id="mirror_id" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['mirror_id'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" />	
						<input type="text" class="notInsert" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['mirror_idTitle'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="mirror_idTitle" name="mirror_idTitle" readonly="readonly" />
						<a href="#" onclick="openSiteMapDialog('mirror_id', 'mirror_idTitle', ''); return false;" class="select-btn"><?php echo ((is_array($_tmp='select')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select') : gLA($_tmp, 'Select')); ?>
</a>
						<a href="#" onclick="$('#mirror_id').val(''); $('#mirror_idTitle').val(''); return false;"><?php echo ((is_array($_tmp='clear')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Clear') : gLA($_tmp, 'Clear')); ?>
</a>
					</li>
				</ul>
			</div>
			<div class="fieldset last">
				<h3><?php echo ((is_array($_tmp='modules')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Show modules on page') : gLA($_tmp, 'Show modules on page')); ?>
</h3>
				<?php if (count($this->_tpl_vars['edit']['modules']) > 0): ?>
				<ul>
					<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['edit']['modules']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
               			<li>
							<label for="module_<?php echo $this->_tpl_vars['edit']['modules'][$this->_sections['item']['index']]['id']; ?>
">
								<a href="<?php echo $this->_tpl_vars['AD_CMS_WEB_FOLDER']; ?>
modules/<?php echo $this->_tpl_vars['edit']['modules'][$this->_sections['item']['index']]['name']; ?>
/#content_id:<?php echo $this->_tpl_vars['edit']['id']; ?>
/" title="<?php echo $this->_tpl_vars['edit']['modules'][$this->_sections['item']['index']]['title']; ?>
"><?php echo $this->_tpl_vars['edit']['modules'][$this->_sections['item']['index']]['title']; ?>
</a>
							</label>
							<input type="checkbox" value="1" id="module_<?php echo $this->_tpl_vars['edit']['modules'][$this->_sections['item']['index']]['id']; ?>
" name="module_<?php echo $this->_tpl_vars['edit']['modules'][$this->_sections['item']['index']]['id']; ?>
" class="active" <?php if ($this->_tpl_vars['edit']['modules'][$this->_sections['item']['index']]['sel']): ?> checked="checked" <?php endif; ?> />
						</li>
               		<?php endfor; endif; ?>
				</ul>
				<?php endif; ?>
			</div>
			<div class="btn"><a href="#" onclick="checkFields(); return false;"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
			<div class="btn cancel"><a href="#" onclick="$('#contentData').html(''); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
			<div class="clr"><!-- clear --></div> 
		</div>
	</div>                 
</form>
<h3><?php echo ((is_array($_tmp='log_info')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Log information') : gLA($_tmp, 'Log information')); ?>
</h3>
<table class="log-info">
	<tr>
		<td style="width:110px;"><?php echo ((is_array($_tmp='created')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Created date') : gLA($_tmp, 'Created date')); ?>
:</td>
		<td><?php echo $this->_tpl_vars['edit']['createdInfo']; ?>
</td>
	</tr>
	<tr>
		<td><?php echo ((is_array($_tmp='edited')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Edit date') : gLA($_tmp, 'Edit date')); ?>
:</td>
		<td><?php echo $this->_tpl_vars['edit']['editedInfo']; ?>
</td>
	</tr>
</table>

<script type="text/javascript">

$(document).ready(function() {
	$('ul.tabs a').click(function() {
		var curChildIndex = $(this).parent().prevAll().length + 1;
		$(this).parent().parent().children('.active').removeClass('active');
		$(this).parent().addClass('active');
		$(this).parent().parent().next('.areaBlock').children('.open').fadeOut('fast',function() {
			$(this).removeClass('open');
			$(this).parent().children('div:nth-child('+curChildIndex+')').fadeIn('normal',function() {
				$(this).addClass('open');
			});
		});
		return false;
	});

	var tabindex = 1;
	$('#edit :input').each(function() {
		if (this.type != "hidden") {
			var $input = $(this);
			$input.attr("tabindex", tabindex);
			tabindex++;
		}
	});

	loadUploadImgButton('<?php echo $this->_tpl_vars['edit']['uploadFolder']; ?>
', 'image');

});

	
</script>