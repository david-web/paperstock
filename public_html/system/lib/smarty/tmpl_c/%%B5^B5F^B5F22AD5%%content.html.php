<?php /* Smarty version 2.6.26, created on 2014-07-08 23:49:26
         compiled from /home/papersst/public_html/system/config/../../system/app/in/content/tmpl/content.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/content/tmpl/content.html', 2, false),)), $this); ?>
<div id="sidebar">
	<div id="close"><a href="javascript:;" onclick="$('#sidebar').toggleClass('closed'); $('.sb-forma').toggle(); ($('.sb-forma').is(':visible')) ? $('.contentTree').show() : $('.contentTree').hide();"><span><?php echo ((is_array($_tmp='close_sidebar')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Close sidebar') : gLA($_tmp, 'Close sidebar')); ?>
</span></a></div>
	
	<form class="sb-forma" method="post" action="">
	    <fieldset>
	        <legend>Add form</legend>
	        <ul class="add-list">
	        	<?php if (count ( $this->_tpl_vars['countries'] ) > 1): ?>
				<li>
	                <label for="countryF"><?php echo ((is_array($_tmp='country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Site') : gLA($_tmp, 'Site')); ?>
:</label>
	                <select id="countryF" name="countryF" onchange="changeCountry(); return false;">
	                	<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['countries']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
	                		<option <?php if ($this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['id'] == $this->_tpl_vars['countries']['sel']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['title']; ?>
</option>
	                	<?php endfor; endif; ?> 
	                </select>
	            </li>                  
				<?php endif; ?>
	            <li>
	                <label for="language"><?php echo ((is_array($_tmp='lang')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Language') : gLA($_tmp, 'Language')); ?>
:</label>
	                <select id="language" name="language" onchange="changeLang(); return false;">
	                    <?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['languages']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
	                		<option <?php if ($this->_tpl_vars['languages']['data'][$this->_sections['item']['index']]['lang'] == $this->_tpl_vars['languages']['sel']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['languages']['data'][$this->_sections['item']['index']]['lang']; ?>
"><?php echo $this->_tpl_vars['languages']['data'][$this->_sections['item']['index']]['title']; ?>
</option>
	                	<?php endfor; endif; ?>
	                </select>
	            </li>
	            <li>
	                <div class="btn"><a href="#" onclick="moduleEdit('', ''); return false;"><span><?php echo ((is_array($_tmp='content_add')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add New Content') : gLA($_tmp, 'Add New Content')); ?>
</span></a></div>
	            </li>
	        </ul>
	    </fieldset>
	</form>
            
	<div class="contentTree" id="modulePath">           
	</div>
</div>

<div class="content" id="contentData">
</div>  

<div class="clr"><!-- clear --></div>
<script type="text/javascript">

	moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
	moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
	moduleTable.usePaging = false;

	$(function () { 
		$("#dialog").dialog("destroy");

		updateModule();

		setTimeout('checkForSelectedId()', 500);
	});	
	
</script>