<?php /* Smarty version 2.6.26, created on 2015-07-14 18:17:41
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../admin/tmpl/helpers/moduleTable.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../admin/tmpl/helpers/moduleTable.html', 6, false),)), $this); ?>
<div class="table-nav">
	<?php if ($this->_tpl_vars['add'] || $this->_tpl_vars['bulk']): ?>
	<ul class="action-nav">
	    <?php if ($this->_tpl_vars['add']): ?>
	    <li>
	        <div class="btn addNew"><a href="#addNew" onclick="return moduleEdit('');"><span><?php echo ((is_array($_tmp='add_new')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add new', '', true) : gLA($_tmp, 'Add new', '', true)); ?>
</span></a></div>
	    </li>
	    <?php endif; ?>
	    
	    <?php if ($this->_tpl_vars['bulk']): ?>
	    <li class="select">
	        <label for="multiaction"><?php echo ((is_array($_tmp='bulk_actions')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Bulk actions', '', true) : gLA($_tmp, 'Bulk actions', '', true)); ?>
:</label>
	        <select onchange="filterAction('<?php echo $this->_tpl_vars['type']; ?>
');" name="multiaction<?php echo $this->_tpl_vars['type']; ?>
" id="multiaction<?php echo $this->_tpl_vars['type']; ?>
">
	        	<option value=""><?php echo ((is_array($_tmp='select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>       
				<option value="enable"><?php echo ((is_array($_tmp='m_enable')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enable', '', true) : gLA($_tmp, 'Enable', '', true)); ?>
</option>
				<option value="disable"><?php echo ((is_array($_tmp='m_disable')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Disable', '', true) : gLA($_tmp, 'Disable', '', true)); ?>
</option>
				<option value="delete"><?php echo ((is_array($_tmp='m_delete')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Delete', '', true) : gLA($_tmp, 'Delete', '', true)); ?>
</option>
                <?php if ($this->_tpl_vars['copy']): ?>
                    <option value="copy"><?php echo ((is_array($_tmp='m_copy')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Copy', '', true) : gLA($_tmp, 'Copy', '', true)); ?>
</option>
                <?php endif; ?>
	        </select>
	    </li>
	    <li class="check">
	        <input type="checkbox" onclick="setAll(true, '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
');" value="" id="selAll<?php echo $this->_tpl_vars['type']; ?>
" class="radio" />
	        <label for="selAll<?php echo $this->_tpl_vars['type']; ?>
"><?php echo ((is_array($_tmp='select_all')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select All', '', true) : gLA($_tmp, 'Select All', '', true)); ?>
</label>
	    </li>
	    <li class="check">
	        <input type="checkbox" onclick="setAll(false, '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
'); return true;" value="" id="disAll<?php echo $this->_tpl_vars['type']; ?>
" class="radio" />
	        <label for="disAll<?php echo $this->_tpl_vars['type']; ?>
"><?php echo ((is_array($_tmp='deselect_all')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Deselect All', '', true) : gLA($_tmp, 'Deselect All', '', true)); ?>
</label>
	    </li>
	    <?php endif; ?>    
	</ul>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['pager']): ?>
	<div id="pager<?php echo $this->_tpl_vars['type']; ?>
">
	</div>
	<div class="limit_to" style="float: right; margin-top: 4px;">
        <label><?php echo ((is_array($_tmp='limit_to')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Rows per page', '', true) : gLA($_tmp, 'Rows per page', '', true)); ?>
:</label>
        <select name="limit_to" id="limit_to<?php echo $this->_tpl_vars['type']; ?>
">
            <option value="25">25</option>
            <option value="40">40</option>
            <option value="100">100</option>
            <option value="150">150</option>
            <option value="200">200</option>
            <option value="250">250</option>
            <option value="300">300</option>
        </select>
    </div>
	<?php endif; ?>	
	<div class="clr"><!-- clear --></div>
</div>
<a name="addNew"></a>