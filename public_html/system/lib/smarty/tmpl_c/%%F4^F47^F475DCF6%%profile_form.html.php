<?php /* Smarty version 2.6.26, created on 2014-07-22 13:25:50
         compiled from profile_form.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', 'profile_form.html', 8, false),)), $this); ?>
<div id="profile_form" class="main-tab">
	<table class="inner-table">

        <tr>
			<td colspan="2"><b>Basic information</b></td>
		</tr>
        <tr>
            <td><?php echo ((is_array($_tmp='email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Email') : gLA($_tmp, 'Email')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['email']; ?>
</td>
        </tr>
		<tr>
            <td><?php echo ((is_array($_tmp='first_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'First name') : gLA($_tmp, 'First name')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['first_name']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='last_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Last name') : gLA($_tmp, 'Last name')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['last_name']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='profile_country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['country']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Phone') : gLA($_tmp, 'Phone')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['phone']; ?>
</td>
        </tr>
        <tr>
			<td colspan="2"><b>Additional information</b></td>
		</tr>
        <tr>
            <td><?php echo ((is_array($_tmp='night_calls')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Night calls') : gLA($_tmp, 'Night calls')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['night_calls']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='alternative_email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Alternative email') : gLA($_tmp, 'Alternative email')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['alternative_email']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='alternative_phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Alternative phone') : gLA($_tmp, 'Alternative phone')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['alternative_phone']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='preferred_language')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Preferred language') : gLA($_tmp, 'Preferred language')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['preferred_language']; ?>
</td>
        </tr>
	</table>
</div>