<?php /* Smarty version 2.6.26, created on 2014-07-22 23:06:31
         compiled from /home/papersst/public_html/system/config/../../system/app/in/payments/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/payments/tmpl/edit.html', 13, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/in/payments/tmpl/edit.html', 27, false),)), $this); ?>
<div class="content">
	<div id="modulePath">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
						<td colspan="2"><b>Payment details</b></td>
					</tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='id')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'ID') : gLA($_tmp, 'ID')); ?>
:</td>
			            <td><?php echo $this->_tpl_vars['edit']['id']; ?>
</td>
			        </tr>
			        <tr>
			            <td>Payment amount:</td>
			            <td><?php echo $this->_tpl_vars['edit']['price']; ?>
</td>
			        </tr>
			        
			        <tr>
			            <td>Payment type:</td>
			            <td><?php echo $this->_tpl_vars['edit']['payment_type']; ?>
</td>
			        </tr>
			        <tr>
			            <td>Created:</td>
			            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d %b %Y") : smarty_modifier_date_format($_tmp, "%d %b %Y")); ?>
</td>
			        </tr>
			        <tr>
			            <td>Status:</td>
			            <td><?php echo $this->_tpl_vars['edit']['paid']; ?>
</td>
			        </tr>
			        <tr>
						<td colspan="2"><b>Client information</b></td>
					</tr>
					<tr>
			            <td><?php echo ((is_array($_tmp='email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Email') : gLA($_tmp, 'Email')); ?>
:</td>
			            <td><?php echo $this->_tpl_vars['edit']['email']; ?>
</td>
			        </tr>
					<tr>
			            <td><?php echo ((is_array($_tmp='first_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'First name') : gLA($_tmp, 'First name')); ?>
:</td>
			            <td><?php echo $this->_tpl_vars['edit']['first_name']; ?>
</td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='last_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Last name') : gLA($_tmp, 'Last name')); ?>
:</td>
			            <td><?php echo $this->_tpl_vars['edit']['last_name']; ?>
</td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Phone') : gLA($_tmp, 'Phone')); ?>
:</td>
			            <td><?php echo $this->_tpl_vars['edit']['phone']; ?>
</td>
			        </tr>
			        <tr>
						<td colspan="2"><b>Order information:</b></td>
					</tr>
    			</table>
    			<?php echo $this->_tpl_vars['edit']['order']['html']; ?>

    		</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
                <div class="btn cancel"><a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl();"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
	    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
        moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
        
        $(document).ready(function() {

                var tabindex = 1;
                $('#editForma :input,select').each(function() {
                        if (this.type != "hidden") {
                                var $input = $(this);
                                $input.attr("tabindex", tabindex);
                                tabindex++;
                        }
                });
        });
			
	</script>
	</div>
</div>