<?php /* Smarty version 2.6.26, created on 2014-07-31 15:20:46
         compiled from /home/papersst/public_html/system/config/../../system/app/in/countries/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/countries/tmpl/edit.html', 9, false),)), $this); ?>
<td id="editForma" colspan="9" class="open">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td>
				<table class="inner-table">
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
:</td>
			            <td><input type="text" class="long" name="title" id="title" value="<?php echo $this->_tpl_vars['edit']['title']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_logo')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Logo') : gLA($_tmp, 'Logo')); ?>
:</td>
			            <td>
			            	<div id="imageDiv">
			            	<?php if ($this->_tpl_vars['edit']['image']): ?>
			            	<img width="100" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['edit']['uploadFolder']; ?>
<?php echo $this->_tpl_vars['edit']['image']; ?>
"><a href="#" onclick="emptyUploadFile('image'); return false;"><?php echo ((is_array($_tmp='m_delete')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Delete') : gLA($_tmp, 'Delete')); ?>
</a>
							<input type="hidden" class="simple" name="image" id="image" value="<?php echo $this->_tpl_vars['edit']['image']; ?>
">
			            	<?php endif; ?>
			            	</div>
			            	<a href="#" id="imageButton" class="select-btn"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_gAnalytics')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Google Analytics') : gLA($_tmp, 'Google Analytics')); ?>
:</td>
			            <td><input type="text" class="long" name="google_analytics" id="google_analytics" value="<?php echo $this->_tpl_vars['edit']['google_analytics']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_webmasters')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Webmasters') : gLA($_tmp, 'Webmasters')); ?>
:</td>
			            <td><input type="text" class="long" name="google_webmasters" id="google_webmasters" value="<?php echo $this->_tpl_vars['edit']['google_webmasters']; ?>
" /></td>
			        </tr>			         
			        <tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='domain_names')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Domain names') : gLA($_tmp, 'Domain names')); ?>
</b></td>
			        </tr>
			        <tr>
			        	<td><?php echo ((is_array($_tmp='domain')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Domain') : gLA($_tmp, 'Domain')); ?>
</td>
			        	<td><?php echo ((is_array($_tmp='is_default_domain')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Default') : gLA($_tmp, 'Default')); ?>
</td>
			        </tr>
			        <?php $_from = $this->_tpl_vars['edit']['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['domain']):
?>
					<tr id="domain_row_<?php echo $this->_tpl_vars['k']; ?>
">
			            <td>
			            	<input type="text" class="long" name="domain_<?php echo $this->_tpl_vars['domain']['id']; ?>
" id="domain_<?php echo $this->_tpl_vars['domain']['id']; ?>
" value="<?php echo $this->_tpl_vars['domain']['url']; ?>
" />
			            </td>
			            <td>
			            	<input type="radio" name="default_domain" id="default_domain" value="<?php echo $this->_tpl_vars['domain']['id']; ?>
" title="<?php echo ((is_array($_tmp='is_default_domain')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Default') : gLA($_tmp, 'Default')); ?>
" <?php if ($this->_tpl_vars['domain']['default']): ?>checked="checked"<?php endif; ?>/>
			        	</td>
			        </tr>
				  	<?php endforeach; endif; unset($_from); ?>
				  	<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='languages')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Languages') : gLA($_tmp, 'Languages')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo $this->_tpl_vars['_lang_name']; ?>
</td>
			            <td><?php echo ((is_array($_tmp='lang_enable')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enable') : gLA($_tmp, 'Enable')); ?>
</td>
			            <td><?php echo ((is_array($_tmp='lang_default')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Default') : gLA($_tmp, 'Default')); ?>
</td>
			            <td><?php echo ((is_array($_tmp='main_page')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Main page') : gLA($_tmp, 'Main page')); ?>
</td>
			        </tr>
			        <?php $_from = $this->_tpl_vars['edit']['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['lang']):
?>
					<tr>
			            <td><?php echo $this->_tpl_vars['lang']['title']; ?>
:</td>
			            <td><input type="checkbox" name="langs_<?php echo $this->_tpl_vars['lang']['id']; ?>
" id="langs_<?php echo $this->_tpl_vars['lang']['id']; ?>
" value="1" <?php if ($this->_tpl_vars['lang']['info']): ?> checked="checked" <?php endif; ?> /></td>
			            <td><input type="radio" name="default" id="default_<?php echo $this->_tpl_vars['lang']['id']; ?>
" value="<?php echo $this->_tpl_vars['lang']['id']; ?>
" <?php if ($this->_tpl_vars['lang']['info']['default']): ?> checked="checked" <?php endif; ?> /></td>
			            <td>
			            	<input type="hidden" name="main_id_<?php echo $this->_tpl_vars['lang']['id']; ?>
" id="main_id_<?php echo $this->_tpl_vars['lang']['id']; ?>
" value="<?php echo $this->_tpl_vars['lang']['info']['main_id']; ?>
" />
			            	<input type="text" class="short" name="main_title_<?php echo $this->_tpl_vars['lang']['id']; ?>
" id="main_title_<?php echo $this->_tpl_vars['lang']['id']; ?>
" value="<?php echo $this->_tpl_vars['lang']['info']['main_title']; ?>
" />
			            	<a href="#" onclick="openSiteMapDialog('main_id_<?php echo $this->_tpl_vars['lang']['id']; ?>
', 'main_title_<?php echo $this->_tpl_vars['lang']['id']; ?>
', ''); return false;" class="select-btn"><?php echo ((is_array($_tmp='select')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select') : gLA($_tmp, 'Select')); ?>
</a>
							<a href="#" onclick="$('#main_id_<?php echo $this->_tpl_vars['lang']['id']; ?>
').val(''); $('#main_title_<?php echo $this->_tpl_vars['lang']['id']; ?>
').val(''); return false;"><?php echo ((is_array($_tmp='clear')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Clear') : gLA($_tmp, 'Clear')); ?>
</a>
			            </td>
			        </tr>
				  	<?php endforeach; endif; unset($_from); ?>
			        
    			</table>
    		</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('next');"><span><?php echo ((is_array($_tmp='save_and_next')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and next') : gLA($_tmp, 'Save and next')); ?>
</span></a></div>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save_and_close')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and close') : gLA($_tmp, 'Save and close')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="$('#edit_<?php echo $this->_tpl_vars['edit']['id']; ?>
').html('');"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>
    
    <script type="text/javascript">

		var tabindex = 1;
		$('#editForma :input,select').each(function() {
			if (this.type != "hidden" && this.id != "cmsLang") {
				var $input = $(this);
				$input.attr("tabindex", tabindex);
				tabindex++;
			}
		});
        
        priew_id = $('input[name=default]:checked').attr('id');
        
        $('input[name=default]').click(function(){
            id = $(this).attr('id').replace('default_', '');
           
            if(!$('#langs_'+id).is(":checked")){
                $('#errorBlock').html("<?php echo ((is_array($_tmp='m_default_lang_error')) ? $this->_run_mod_handler('gLA', true, $_tmp, "Can't set as default language, because this language is disabled!", '', '', true) : gLA($_tmp, "Can't set as default language, because this language is disabled!", '', '', true)); ?>
");
                $('#errorBlock').show();
                $('#default_'+id).attr('checked', '');
                $('#'+priew_id).attr('checked', 'checked');
            } else {
                $('#errorBlock').html('');
                $('#errorBlock').hide();
                priew_id = 'default_'+id;
            }
        });
            
        $('input[type=checkbox]').click(function(){
            id = $(this).attr('id').replace('langs_', '');
            if($('#default_'+id).is(":checked")){
                $('#errorBlock').html("<?php echo ((is_array($_tmp='m_default_lang_checkbox_error')) ? $this->_run_mod_handler('gLA', true, $_tmp, "Can't disable, because this language is set as default. Change default language!", '', '', true) : gLA($_tmp, "Can't disable, because this language is set as default. Change default language!", '', '', true)); ?>
");
                $('#errorBlock').show();
                $('#langs_'+id).attr('checked', 'checked');
            } else {
                $('#errorBlock').html('');
                $('#errorBlock').hide();
            }
        });
    
		loadUploadImgButton('<?php echo $this->_tpl_vars['edit']['uploadFolder']; ?>
', 'image');
		//loadUploadImgButton('<?php echo $this->_tpl_vars['edit']['uploadFolder']; ?>
', 'image_small');
	</script>
</td>