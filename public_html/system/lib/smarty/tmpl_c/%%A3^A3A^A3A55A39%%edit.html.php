<?php /* Smarty version 2.6.26, created on 2014-07-22 13:39:09
         compiled from /home/papersst/public_html/system/config/../../system/app/in/writers/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/writers/tmpl/edit.html', 10, false),)), $this); ?>
<div class="content">
	<div id="modulePath">
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	
	<div id="writersTabs">
		<ul>
				
			<li>
				<a rel="general_data_form" href="#general_data_form"  >
					<?php echo ((is_array($_tmp='general')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Main info') : gLA($_tmp, 'Main info')); ?>

				</a>
			</li>
			<li>
				<a rel="tests_form" href="#tests_form"  >
					Tests
				</a>
			</li>
			<li>
				<a rel="orders_form" href="#orders_form"  >
					Orders
				</a>
			</li>
				
		</ul>

		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "writers_data_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "tests_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "orders_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	</div>
	
	
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl();"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
	    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
        moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
        
        $(document).ready(function() {

        	$('ul.lang-tabs a').click(function() {
    			var curChildIndex = $(this).parent().prevAll().length + 1;
    			$(this).parent().parent().children('.active').removeClass('active');
    			$(this).parent().addClass('active');
    			$(this).parent().parent().next('.areaBlock').children('.open').fadeOut('fast',function() {
    				$(this).removeClass('open');
    				$(this).parent().children('div:nth-child('+curChildIndex+')').fadeIn('normal',function() {
    					$(this).addClass('open');
    				});
    			});
    			return false;
    		});

        	$('#writersTabs').tabs();	   
        	
        });
			
	</script>
	</div>
</div>