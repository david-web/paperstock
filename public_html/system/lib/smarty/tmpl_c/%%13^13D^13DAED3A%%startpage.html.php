<?php /* Smarty version 2.6.26, created on 2014-07-08 23:06:04
         compiled from /home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/startpage.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/startpage.html', 6, false),array('modifier', 'is_array', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/startpage.html', 24, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/startpage.html', 24, false),array('modifier', 'implode', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/startpage.html', 24, false),array('modifier', 'getLM', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/startpage.html', 62, false),)), $this); ?>
<section id="content">
	<?php echo $this->_tpl_vars['PROMOBLOCK_CONTENT']; ?>

	<section class="calculator">
		<section class="wrap">
			<section class="calc_cont css3 clearfix">
				<h2><?php echo ((is_array($_tmp='startpage_CalculateYouPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Calculate Your price') : gL($_tmp, 'Calculate Your price')); ?>
</h2>
				<?php $_from = $this->_tpl_vars['paper']['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
				<?php if ($this->_tpl_vars['i']['type'] == 'tabselect' || $this->_tpl_vars['i']['type'] == 'select'): ?>
				<div class="cols col<?php echo $this->_foreach['orderdata']['iteration']; ?>
">
					<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
					<select class="selectpicker css3 <?php if ($this->_tpl_vars['i']['price']): ?> calcprice<?php endif; ?>" <?php if ($this->_tpl_vars['i']['price']): ?> onchange="order.calculatePrice();" <?php endif; ?> data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
						<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
					
							<?php if ($this->_tpl_vars['v']['parent']): ?>
								
								<?php if ($this->_tpl_vars['optgroupOpened']): ?>
									</optgroup>	
								<?php endif; ?>
								
								<?php $this->assign('optgroupOpened', $this->_tpl_vars['v']['id']); ?>
								
								<optgroup label="<?php echo $this->_tpl_vars['v']['name']; ?>
">
							<?php else: ?>	
								<option <?php if ($this->_tpl_vars['optgroupOpened']): ?>data-groupid="<?php echo $this->_tpl_vars['optgroupOpened']; ?>
"<?php endif; ?> <?php if (((is_array($_tmp=$this->_tpl_vars['v']['show_fields'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp)) && count($this->_tpl_vars['v']['show_fields']) > 0): ?> data-show=".show_<?php echo ((is_array($_tmp=' .show_')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['v']['show_fields']) : implode($_tmp, $this->_tpl_vars['v']['show_fields'])); ?>
" <?php endif; ?> value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>	
							<?php endif; ?>
							
							<?php if (($this->_foreach['orderdatavalues']['iteration'] == $this->_foreach['orderdatavalues']['total']) && $this->_tpl_vars['optgroupOpened']): ?>
								</optgroup>	
							<?php endif; ?>	
						<?php endforeach; endif; unset($_from); ?>
					</select>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'pages'): ?>
				<div class="cols col<?php echo $this->_foreach['orderdata']['iteration']; ?>
">
					<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
					<div class="num_input">
						<div class="num minus css3"></div>
						<input type="text" value="<?php if ($_SESSION['order']['fields']['number_of_pages']): ?><?php echo $_SESSION['order']['fields']['number_of_pages']; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']['default_val']; ?>
<?php endif; ?>" <?php if ($this->_tpl_vars['i']['price']): ?> class="calcprice" onchange="order.calculatePrice();" <?php endif; ?> name="number_of_pages" id="number_of_pages"" >
						<div class="num plus css3"></div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'hourdate'): ?>
				<div class="cols col<?php echo $this->_foreach['orderdata']['iteration']; ?>
">
					<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
					<select class="selectpicker css3 <?php if ($this->_tpl_vars['i']['price']): ?> calcprice <?php endif; ?>" <?php if ($this->_tpl_vars['i']['price']): ?> onchange="order.calculatePrice();" <?php endif; ?> data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
						<option value="32400" <?php if (32400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>9 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
						<option value="86400" <?php if (86400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
						<option value="172800" <?php if (172800 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
						<option value="259200" <?php if (259200 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
						<option value="518400" <?php if (518400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
						<option value="864000" <?php if (864000 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
						<option value="1209600" <?php if (1209600 == $this->_tpl_vars['i']['selected'] || ! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
					</select>
				</div>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				<div class="cols col5 css3">
					<label><?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price $') : gL($_tmp, 'Total price $')); ?>
:</label>
					<div id="result" class="css3"><span id="priceTotal"></span></div>
				</div>
				<div class="cols col6">
					<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_order_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="order css3" title="<?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
"><?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
</a>
					<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_inquiry_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="free" title="<?php echo ((is_array($_tmp='startpage_OrderTryInquiry')) ? $this->_run_mod_handler('gL', true, $_tmp, 'or try FREE inquiry') : gL($_tmp, 'or try FREE inquiry')); ?>
"><?php echo ((is_array($_tmp='startpage_OrderTryInquiry')) ? $this->_run_mod_handler('gL', true, $_tmp, 'or try FREE inquiry') : gL($_tmp, 'or try FREE inquiry')); ?>
</a>
				</div>
			</section>
			<script>
			$(document).ready(function(){
				order.calculatePrice();
			});
			</script>
		</section>
	</section>
	<section class="services">
		<section class="wrap">
			<h2><?php echo ((is_array($_tmp='startpage_OurServicesTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Our services for your needs') : gL($_tmp, 'Our services for your needs')); ?>
</h2>
			<ul class="list clearfix">
				<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['menu']['MIDDLE']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
				<li>
					<a href="<?php echo $this->_tpl_vars['menu']['MIDDLE'][$this->_sections['item']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['menu']['MIDDLE'][$this->_sections['item']['index']]['title']; ?>
">
						<?php echo $this->_tpl_vars['menu']['MIDDLE'][$this->_sections['item']['index']]['title']; ?>

					</a>
				</li>
				<?php endfor; endif; ?>
			</ul>
		</section>
	</section>
	<section class="whyus">
		<section class="wrap clearfix">
			<div class="extra"><h2><?php echo ((is_array($_tmp='startpage_WhyChooseUsTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Why choose us?') : gL($_tmp, 'Why choose us?')); ?>
</h2></div>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item1_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item1">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item1_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, '345 professionals from UK and US') : gL($_tmp, '345 professionals from UK and US')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item2_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item2">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item2_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Support team available 24/7') : gL($_tmp, 'Support team available 24/7')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item3_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item3">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item3_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Save your time! Delivery in 8h') : gL($_tmp, 'Save your time! Delivery in 8h')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item4_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item4">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item4_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order under your complete control') : gL($_tmp, 'Order under your complete control')); ?>
</div>
			</a>
			<a href="<?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item5_Link')) ? $this->_run_mod_handler('gL', true, $_tmp, '') : gL($_tmp, '')); ?>
" class="item5">
				<div class="icon"></div>
				<div class="text"><?php echo ((is_array($_tmp='startpage_WhyChooseUs_Item5_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Discounts for returning customers') : gL($_tmp, 'Discounts for returning customers')); ?>
</div>
			</a>
		</section>
	</section>
	<section class="statistics">
		<section class="wrap">
			<h2><?php echo ((is_array($_tmp='startpage_OurLiveStatsTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Our LIVE stats') : gL($_tmp, 'Our LIVE stats')); ?>
</h2>
			<div class="list_cont">
				<div class="list clearfix">
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_1.jpg">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_AvgPricePerOrder_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '20') : gL($_tmp, '20')); ?>
">0</span>$</div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_AvgPricePerOrder_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Average price per order') : gL($_tmp, 'Average price per order')); ?>
</div>
					</div>
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_2.jpg">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_ActiveWriters_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '50') : gL($_tmp, '50')); ?>
">0</span></div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_ActiveWriters_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Active writers') : gL($_tmp, 'Active writers')); ?>
</div>
					</div>
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_3.jpg">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
">0</span></div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Orders delivered') : gL($_tmp, 'Orders delivered')); ?>
</div>
					</div>
					<div class="item">
						<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats_4.jpg">
						<div class="bigtext"><span class="animate_number not_done" data-realnumber="<?php echo ((is_array($_tmp='startpage_OurLiveStats_ReturningCustomers_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '99') : gL($_tmp, '99')); ?>
">0</span>%</div>
						<div class="smalltext"><?php echo ((is_array($_tmp='startpage_OurLiveStats_ReturningCustomers_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Returning customers') : gL($_tmp, 'Returning customers')); ?>
</div>
					</div>
				</div>
				<div class="button_cont">
					<div class="buttons">
						<div class="button css3"></div>
						<div class="button css3"></div>
						<div class="button css3"></div>
						<div class="button css3"></div>
					</div>
				</div>
			</div>
		</section>
	</section>
	<section class="how_it_works">
		<div class="bg">
			<img alt="<?php echo ((is_array($_tmp='startpage_HowItWorksTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'How it works?') : gL($_tmp, 'How it works?')); ?>
" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
how_it_works.jpg">
		</div>
		<section class="wrap">
			<h2><?php echo ((is_array($_tmp='startpage_HowItWorksTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'How it works?') : gL($_tmp, 'How it works?')); ?>
</h2>
			<div class="cont">
				<div class="link onetwo"></div>
				<div class="link twothree"></div>
				<div class="link threefour"></div>
				<div class="step step1 css3">
					<div class="nr"></div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit your order') : gL($_tmp, 'Submit your order')); ?>
</div>
				</div>
				<div class="step step2 css3">
					<div class="nr"></div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Make the payment') : gL($_tmp, 'Make the payment')); ?>
</div>
				</div>
				<div class="step step3 css3">
					<div class="nr"></div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step3')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order in prosess') : gL($_tmp, 'Order in prosess')); ?>
</div>
				</div>
				<div class="step step4 css3">
					<div class="nr"></div>
					<hr>
					<div class="text"><?php echo ((is_array($_tmp='startpage_HowItWorks_Step4')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Download!') : gL($_tmp, 'Download!')); ?>
</div>
				</div>
			</div>
		</section>
	</section>
	<section class="safety">
		<section class="wrap clearfix">
			<div class="col">
				<div class="cont">
					<div class="cont2">
						<div class="text"><?php echo ((is_array($_tmp='startpage_WhyFeelSafeTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Why will you feel safe with us?') : gL($_tmp, 'Why will you feel safe with us?')); ?>
</div>
					</div>
				</div>
				<?php echo ((is_array($_tmp='startpage_WhyFeelSafeBlock')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

			</div>
		</section>
	</section>
	<section class="aboutus">
		<section class="wrap">
			<div class="cols userinput clearfix">
				<div class="col">
					<?php echo ((is_array($_tmp='startpage_AboutUsBlock1')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</div>
				<div class="col">
					<?php echo ((is_array($_tmp='startpage_AboutUsBlock2')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</div>
				<div class="col">
					<?php echo ((is_array($_tmp='startpage_AboutUsBlock3')) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</div>
			</div>
		</section>
	</section>
</section>