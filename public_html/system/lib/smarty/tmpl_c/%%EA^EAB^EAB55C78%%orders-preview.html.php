<?php /* Smarty version 2.6.26, created on 2014-07-22 22:56:28
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-preview.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/orders-preview.html', 6, false),)), $this); ?>
<div class="popup_bg" style="block;"></div>
<div class="popup big css3" style="display:block;">
	<div class="close"></div>
	<div class="top">
		<div class="wrap_onlymob">
			<h2><?php echo ((is_array($_tmp='order_OrderPreview')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order preview') : gL($_tmp, 'Order preview')); ?>
</h2>
			<hr>
			<div class="block3">
				<p><?php echo ((is_array($_tmp='order_OrderPreviewText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Duis ut tempus lectus, eu volutpat tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In nec leo pharetra, pretium purus ac, egestas nulla.') : gL($_tmp, 'Duis ut tempus lectus, eu volutpat tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In nec leo pharetra, pretium purus ac, egestas nulla.')); ?>
</p>
				<div class="rules">
					<img alt="" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['filename']; ?>
">
				</div>
			</div>
		</div>
	</div>
	<div class="btm clearfix" id="pr_buttons">
		<div class="wrap_onlymob">
			<a href="javascript:;" onclick="profile.acceptOrder();" class="btn1 big"><?php echo ((is_array($_tmp='order_OrderPreviewApprove')) ? $this->_run_mod_handler('gL', true, $_tmp, 'I aproove the order') : gL($_tmp, 'I aproove the order')); ?>
</a>
			<a href="javascript:;" onclick="$('#pr_buttons').hide();$('#pr_comment').show();" class="btn5 big"><?php echo ((is_array($_tmp='order_OrderPreviewComments')) ? $this->_run_mod_handler('gL', true, $_tmp, 'I have comments') : gL($_tmp, 'I have comments')); ?>
</a>
		</div>
	</div>
	<div class="btm clearfix" id="pr_comment" style="display:none;">
		<div class="wrap_onlymob">
			<h2><?php echo ((is_array($_tmp='order_OrderPreviewAddComment')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Add a comment') : gL($_tmp, 'Add a comment')); ?>
</h2>
			<div class="cinput">
				<textarea name="preview_comment" id="preview_comment" data-default="<?php echo ((is_array($_tmp='order_OrderPreviewAddCommentDefaultText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'DefaultText') : gL($_tmp, 'DefaultText')); ?>
"></textarea>
			</div>
			<div class="clearfix"></div>
			<a href="javascript:;" onclick="profile.previewComment();" class="btn5"><?php echo ((is_array($_tmp='submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
			<a href="javascript:;" onclick="$('#pr_comment').hide();$('#pr_buttons').show();" class="btn7"><?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
		</div>
	</div>
</div>
<script type="text/javascript">
       
$(document).ready(function() {
	$(".close").click(function() {
		$('.popup').remove();
		$('.popup_bg').remove();
	});
});
</script>