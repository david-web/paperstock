<?php /* Smarty version 2.6.26, created on 2014-07-15 17:16:05
         compiled from /home/papersst/public_html/system/config/../../system/app/in/sitemap/tmpl/sitemap.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/sitemap/tmpl/sitemap.html', 7, false),)), $this); ?>
<form class="sb-forma" method="post" action="">
    <fieldset>
        <legend>Add form</legend>
        <ul class="add-list">
        	<?php if (count ( $this->_tpl_vars['countries'] ) > 1): ?>
			<li>
                <label for="sitemap-countryF"><?php echo ((is_array($_tmp='country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</label>
                <select id="sitemap-countryF" name="sitemap-countryF" onchange="changeCountrySiteMap(); return false;">
                	<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['countries']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
                		<option <?php if ($this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['id'] == $this->_tpl_vars['countries']['sel']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['countries']['data'][$this->_sections['item']['index']]['title']; ?>
</option>
                	<?php endfor; endif; ?> 
                </select>
            </li>                  
			<?php endif; ?>
            <li>
                <label for="sitemap-language"><?php echo ((is_array($_tmp='lang')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Language') : gLA($_tmp, 'Language')); ?>
:</label>
                <select id="sitemap-language" name="sitemap-language" onchange="changeLangSiteMap(); return false;">
                    <?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['languages']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
                		<option <?php if ($this->_tpl_vars['languages']['data'][$this->_sections['item']['index']]['lang'] == $this->_tpl_vars['languages']['sel']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['languages']['data'][$this->_sections['item']['index']]['lang']; ?>
"><?php echo $this->_tpl_vars['languages']['data'][$this->_sections['item']['index']]['title']; ?>
</option>
                	<?php endfor; endif; ?>
                </select>
            </li>
        </ul>
    </fieldset>
</form>
           
<div class="contentTree" id="siteMap">         
</div>

<div class="clr"><!-- clear --></div>

<script type="text/javascript">

	var id = "siteMap";
	var	moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
	var	mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
	var	ajaxAction = "moduleTable";
	var	additionalParms = "";
	var	idField = '<?php echo $this->_tpl_vars['idField']; ?>
';
	var	titleField = '<?php echo $this->_tpl_vars['titleField']; ?>
';
	var refreshTree = true;
	var returnFunction = <?php echo $this->_tpl_vars['func']; ?>
;

	$(function () { 
		updateSitemap();
		initTree();
	});	

	function getRequestParameters() {
		return additionalParms;
	}

	function getRequestUrl() {
		return mainUrl + moduleName + "/";
	}

	function updateSitemap() {

		$.post(getRequestUrl() + ajaxAction + "/", getRequestParameters(),
				function(data) {
								returnHtml = data["html"];
								extraField = data["filterLang"];
						
								if (extraField != null && extraField != '') {
									$('#sitemap-language').html(extraField);
								}
						
								$('#' + id).html(returnHtml);
								
								
								initTree();

							}, 
				"json");
				
	}

	function initTree() {		

		$("#siteMap").jstree({
			"themes" : {
				"theme" : "default",
				"dots" : true,
				"icons" : false
			},
			"plugins" : [ "themes", "html_data", "ui" ]
			
		}).bind("dblclick.jstree", function (event, data) {
			var node = $(event.target).closest("li");
			
			eval(returnFunction(node));
			$("#dialog").dialog('close');
			return false;
		   
		});
	}

	
	function defaultReturn(NODE) {
		id = $(NODE).attr("id").replace("node", "");
		$('#' + idField).val(id);
		$('#' + idField).change();
		$('#' + titleField).val($(NODE).attr("title"));
	}

	function changeCountrySiteMap() {
		additionalParms = "filterCountry=" + $('#sitemap-countryF').val();
		updateSitemap();
	}

	function changeLangSiteMap() {
		additionalParms = "filterLang=" + $('#sitemap-language').val() + "&filterCountry=" + $('#sitemap-countryF').val();
		updateSitemap();
	}
	
</script>