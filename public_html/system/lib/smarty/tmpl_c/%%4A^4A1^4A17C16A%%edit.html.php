<?php /* Smarty version 2.6.26, created on 2014-07-16 23:46:14
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'getLM', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/edit.html', 4, false),array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/1/edit.html', 4, false),)), $this); ?>
<section class="green_title order sp26-26-0">
	<section class="wrap">
		<div class="btn_cont">
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_inquiry_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="btn6 ver_1024" title="<?php echo ((is_array($_tmp='header_FreeInquiryTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Free inquiry') : gL($_tmp, 'Free inquiry')); ?>
"><?php echo ((is_array($_tmp='header_FreeInquiryTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Free inquiry') : gL($_tmp, 'Free inquiry')); ?>
</a>
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_order_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="btn5 ver_1024" title="<?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
"><?php echo ((is_array($_tmp='header_OrderNowTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order now') : gL($_tmp, 'Order now')); ?>
</a>
		</div>
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
	</section>
</section>
<section class="wrap mob_nowrap clearfix">
	<div class="profile_text wrap_onlymob">
		<p>
			<?php echo ((is_array($_tmp='profile_EditText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Text text text. Profile edit text.') : gL($_tmp, 'Text text text. Profile edit text.')); ?>

		</p>
	</div>
	<div class="profile_cols clearfix">
		<div class="col col1">
			<div class="inner wrap_onlymob">
				<div class="fields">
					<?php if ($this->_tpl_vars['userData']['id']): ?>
					<div class="line clearfix">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_ID')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cliend ID') : gL($_tmp, 'Cliend ID')); ?>
</div>
						<div class="value"><?php echo $this->_tpl_vars['userData']['id']; ?>
</div>
					</div>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['userData']['first_name']): ?>
					<div class="line clearfix">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Name')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Name') : gL($_tmp, 'Name')); ?>
</div>
						<div class="value"><?php echo $this->_tpl_vars['userData']['first_name']; ?>
</div>
					</div>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['userData']['last_name']): ?>
					<div class="line clearfix">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_LastName')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Last name') : gL($_tmp, 'Last name')); ?>
</div>
						<div class="value"><?php echo $this->_tpl_vars['userData']['last_name']; ?>
</div>
					</div>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['userData']['email']): ?>
					<div class="line clearfix">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>
</div>
						<div class="value"><?php echo $this->_tpl_vars['userData']['email']; ?>
</div>
					</div>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['userData']['country']): ?>
					<div class="line clearfix">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Country')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Country') : gL($_tmp, 'Country')); ?>
</div>
						<div class="value"><?php echo $this->_tpl_vars['userData']['country']; ?>
</div>
					</div>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['userData']['phone']): ?>
					<div class="line clearfix">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Phone')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Phone') : gL($_tmp, 'Phone')); ?>
</div>
						<div class="value"><?php echo $this->_tpl_vars['userData']['phone']; ?>
</div>
					</div>
					<?php endif; ?>
				</div>
				<a href="javascript:;" onclick="profile.changePassword();" class="link trigger_changepw"><?php echo ((is_array($_tmp='profile_EditChangePassword')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Change passoword') : gL($_tmp, 'Change passoword')); ?>
</a>
			</div>
		</div>
		<div class="col col2">
			<div class="inner wrap_onlymob">
				<div class="fields">
					<div class="line clearfix">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Night_calls')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Night calls<span class="no_320"> allowed</span>') : gL($_tmp, 'Night calls<span class="no_320"> allowed</span>')); ?>
</div>
						<div class="value"><?php if ($this->_tpl_vars['userData']['night_calls']): ?><?php echo $this->_tpl_vars['userData']['night_calls']; ?>
<?php else: ?><?php echo ((is_array($_tmp='profile_EditField_Night_calls_Default')) ? $this->_run_mod_handler('gL', true, $_tmp, 'No') : gL($_tmp, 'No')); ?>
<?php endif; ?></div>
					</div>
					<div class="line clearfix unspecified">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Alternative_email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Alternat<span class="ver_320i">.</span><span class="no_320">ive</span> email') : gL($_tmp, 'Alternat<span class="ver_320i">.</span><span class="no_320">ive</span> email')); ?>
</div>
						<div class="value"><?php if ($this->_tpl_vars['userData']['alternative_email']): ?><?php echo $this->_tpl_vars['userData']['alternative_email']; ?>
<?php else: ?><?php echo ((is_array($_tmp='profile_EditField_Alternative_email_Default')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Empty') : gL($_tmp, 'Empty')); ?>
<?php endif; ?></div>
					</div>
					<div class="line clearfix unspecified">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Alternative_phone')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Alternat<span class="no_320">ive</span> phone') : gL($_tmp, 'Alternat<span class="no_320">ive</span> phone')); ?>
</div>
						<div class="value"><?php if ($this->_tpl_vars['userData']['alternative_phone']): ?><?php echo $this->_tpl_vars['userData']['alternative_phone']; ?>
<?php else: ?><?php echo ((is_array($_tmp='profile_EditField_Alternative_phone_Default')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Empty') : gL($_tmp, 'Empty')); ?>
<?php endif; ?></div>
					</div>
					<div class="line clearfix unspecified">
						<div class="name"><?php echo ((is_array($_tmp='profile_EditField_Preferred_language')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Pref<span class="ver_320i">.</span><span class="no_320">erred</span> language') : gL($_tmp, 'Pref<span class="ver_320i">.</span><span class="no_320">erred</span> language')); ?>
</div>
						<div class="value"><?php if ($this->_tpl_vars['userData']['preferred_language']): ?><?php echo $this->_tpl_vars['userData']['preferred_language']; ?>
<?php else: ?><?php echo ((is_array($_tmp='profile_EditField_Preferred_language_Default')) ? $this->_run_mod_handler('gL', true, $_tmp, 'No preference') : gL($_tmp, 'No preference')); ?>
<?php endif; ?></div>
					</div>
				</div>
				<a href="javascript:;" onclick="profile.editAdditionalInfo();" class="link trigger_changeinfo"><?php echo ((is_array($_tmp='profile_EditAdditionalInfo')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Edit additional information') : gL($_tmp, 'Edit additional information')); ?>
</a>
			</div>
		</div>
	</div>
</section>