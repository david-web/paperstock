<?php /* Smarty version 2.6.26, created on 2015-01-11 13:11:24
         compiled from /home/papersst/public_html/system/config/../../system/app/in/orders/tmpl/orders.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/orders/tmpl/orders.html', 9, false),)), $this); ?>
<div class="content">
	<form class="forma" method="post" action="" onsubmit="return false;">
		<fieldset>
			<table class="search-table" cellpadding="0" cellspacing="0">
			    <tr>
			        <td>
			            <label for="filterStatus">Status:</label>
			            <select id="filterStatus" class="filter" name="filterStatus" onchange="changeFilter(); return false;">
							<option value="all"><?php echo ((is_array($_tmp='select_option_all')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- All Orders --') : gLA($_tmp, '-- All Orders --')); ?>
</option>
							<?php echo $this->_tpl_vars['sList']; ?>

						</select>
			        </td>
			        <td>
			            <label for="filterPaid">Paid:</label>
			            <select id="filterPaid" class="filter" name="filterPaid" onchange="changeFilter(); return false;">
							<option value=""><?php echo ((is_array($_tmp='select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
			        </td>
			        <td>
            	
		            	<label for="deadlineFrom">Deadline From:</label>
					    <input class="filter" type="text" id="deadlineFrom" name="deadlineFrom"  value="" />
		            </td>
		            <td>
		            	
		            	<label for="deadlineTo">Deadline To:</label>
					    <input class="filter" type="text" id="deadlineTo" name="deadlineTo"  value="" />
		            </td>
			        <td>
			            <label for="filterSearch"><?php echo ((is_array($_tmp='content')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Content') : gLA($_tmp, 'Content')); ?>
:</label>
			            <input class="long filter" type="text" id="filterSearch" name="filterSearch" />
			        </td>
			        <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="changeFilter(); return false;"><span><?php echo ((is_array($_tmp='m_search')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Search') : gLA($_tmp, 'Search')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
	               <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="clearFilter(); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
			    </tr>
			</table>
			
			<?php echo $this->_tpl_vars['tTable']; ?>

			
			<div id="modulePath">
			</div>
			
			<?php echo $this->_tpl_vars['bTable']; ?>

	
		</fieldset>
	</form>
	
	<script type="text/javascript">
		moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
		moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
		moduleTable.usePaging = true;
		<?php if ($this->_tpl_vars['MODULE_FROM']): ?>
	    moduleTable.from = <?php echo $this->_tpl_vars['MODULE_FROM']; ?>
;
	    <?php endif; ?>

		$(document).ready(function() {
			
			$('#deadlineFrom').datepicker({
	            dateFormat  : 'dd.mm.yy',
	        });

	        $('#deadlineTo').datepicker({
	            dateFormat  : 'dd.mm.yy',
	        });
			
			updateModule();	
			
		});
	</script>
</div>	