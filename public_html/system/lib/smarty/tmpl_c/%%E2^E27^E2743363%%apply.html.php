<?php /* Smarty version 2.6.26, created on 2014-07-15 10:31:42
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/apply.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/apply.html', 4, false),)), $this); ?>
<section class="wrap clearfix">
	<section class="col1a">
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
		<h2 class="notlogged"><?php echo ((is_array($_tmp='profile_SignUp_LeadText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Condimentum tristique  get sollicitudin. Already a member? <a href="#">Sign in</a>') : gL($_tmp, 'Condimentum tristique  get sollicitudin. Already a member? <a href="#">Sign in</a>')); ?>
</h2>
	</section>
</section>

<section class="form_bg">
	<section class="wrap clearfix">
		<section class="col1a">
			<section class="order_form">
				<?php $_from = $this->_tpl_vars['profile']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['register'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['register']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['register']['iteration']++;
?>
				<?php if ($this->_tpl_vars['i']['type'] == 'tabselect'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
								<?php if ($this->_tpl_vars['v']['hint']): ?>
								<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
								<?php endif; ?>
								<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
							</div>
							<?php endforeach; endif; unset($_from); ?>
						</div>
						<div class="horizontal_dropdown">
							<select class="selectpicker register" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'select'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="select_cont">
							<select class="selectpicker register" data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='order_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<option rel="<?php echo $this->_tpl_vars['v']['hint']; ?>
" value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>
						</div>
						<?php if ($this->_tpl_vars['i']['hint_below']): ?>
						<div class="note">
							<p>
								<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
							</p>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="cinput css3">
							<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'radio'): ?>
				<div class="field clearfix">
					<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
					<div class="option_list clearfix">
						<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
						<div class="col80">
							<div class="radio_option">
								<input type="radio" class="styled register" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>checked="checked"<?php endif; ?>>
								<label><?php echo $this->_tpl_vars['v']['name']; ?>
</label>
							</div>
						</div>
						<?php endforeach; endif; unset($_from); ?>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'textarea'): ?>
				<div class="field clearfix">
					<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
					<div class="c"></div>
					<div class="cinput css3 mt12">
						<textarea class="register" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['selected']; ?>
</textarea>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'checkbox'): ?>
				<div class="field clearfix">
					<label><?php echo $this->_tpl_vars['i']['name']; ?>
</label>
					<div class="option_list clearfix">
						<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<?php if (($this->_foreach['orderdatavalues']['iteration'] <= 1)): ?>
							<div class="col160">
							<?php endif; ?>
							
							<div class="check_option">
								<input type="checkbox" rel="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" class="styled register" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
								<label><?php echo $this->_tpl_vars['v']['name']; ?>
</label>
							</div>
						
							<?php if (($this->_foreach['orderdatavalues']['iteration'] == $this->_foreach['orderdatavalues']['total'])): ?>
							</div>
							<?php elseif ($this->_foreach['orderdatavalues']['iteration'] % 6 == 0): ?>
							</div>
							<div class="col160">
							<?php endif; ?>
						<?php endforeach; endif; unset($_from); ?>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'password'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix">
						<div class="col1e">
							<div class="cinput css3">
								<input class="register" type="password" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
							</div>
						</div>	
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'phone'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix">
						<div class="col1e clearfix">
							<div class="col1f">
								<div class="cinput_nochange css3"><span id="countryCode"></span></div>
							</div>
							<div class="col2f">
								<div class="cinput css3">
									<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				<div class="field clearfix">
					<div class="col1g">
						<input type="checkbox" class="styled register" name="agreement" id="agreement" value="1" />
					</div>
					<div class="col2g clearfix">
						<label>
							<?php echo ((is_array($_tmp='profile_RegisterAgreementText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>') : gL($_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>')); ?>

						</label>
					</div>
					<div class="invalid_msg"></div>
				</div>
				<hr>
				<div class="authorize_cont">
					<a href="javascript:;" onclick="profile.profileRegister();" class="btn3 css3"><?php echo ((is_array($_tmp='profile_ApplyButton')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Apply') : gL($_tmp, 'Apply')); ?>
</a>
				</div>
			</section>
		</section>
		<section class="col2a">
			<section class="side_reg css3">
				<div class="block userinput">
					<?php echo ((is_array($_tmp='apply_RightBlock1')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h3>To apply you need</h3>
					<ul>
						<li>1 Sample paper (>2750 words)</li>
						<li>Short test on grammar and vocabulary</li>
						<li>15 questions on formatting</li>
						<li>Higher education certificate</li>
					</ul>') : gL($_tmp, '<h3>To apply you need</h3>
					<ul>
						<li>1 Sample paper (>2750 words)</li>
						<li>Short test on grammar and vocabulary</li>
						<li>15 questions on formatting</li>
						<li>Higher education certificate</li>
					</ul>')); ?>

				</div>
				<div class="block userinput">
					<?php echo ((is_array($_tmp='apply_RightBlock2')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h3>Work with us</h3>
					<ul>
						<li>Work remotely, without having to leave your home</li>
						<li>Earn real money for your knowledge</li>
						<li>No shortage of orders</li>
					</ul>') : gL($_tmp, '<h3>Work with us</h3>
					<ul>
						<li>Work remotely, without having to leave your home</li>
						<li>Earn real money for your knowledge</li>
						<li>No shortage of orders</li>
					</ul>')); ?>

				</div>
				<div class="block userinput">
					<?php echo ((is_array($_tmp='apply_RightBlock3')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h3>Communication</h3>
					<ul>
						<li>Be appreciated for your work</li>
						<li>Be praised, not punished</li>
						<li>Receive friendly, helpful support in any matter</li>
					</ul>') : gL($_tmp, '<h3>Communication</h3>
					<ul>
						<li>Be appreciated for your work</li>
						<li>Be praised, not punished</li>
						<li>Receive friendly, helpful support in any matter</li>
					</ul>')); ?>

				</div>
				<div class="block userinput">
					<?php echo ((is_array($_tmp='apply_RightBlock4')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h3>Hard work always pays off</h3>
					<ul>
						<li>Receive bonuses for your high rating</li>
						<li>Become an advanced writer, and receive benefits</li>
						<li>Develop, perfect and improve your writing skills</li>
						<li>Become a valuable member of our community</li>
					</ul>') : gL($_tmp, '<h3>Hard work always pays off</h3>
					<ul>
						<li>Receive bonuses for your high rating</li>
						<li>Become an advanced writer, and receive benefits</li>
						<li>Develop, perfect and improve your writing skills</li>
						<li>Become a valuable member of our community</li>
					</ul>')); ?>

				</div>
				<div class="block userinput">
					<?php echo ((is_array($_tmp='apply_RightBlock5')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h3>Flexible &amp; fair conditions</h3>
					<ul>
						<li>Fair pricing for each order</li>
						<li>Deadlines you can meet</li>
						<li>Payment options suitable</li>
						<li>Constant improvement for better results</li>
					</ul>') : gL($_tmp, '<h3>Flexible &amp; fair conditions</h3>
					<ul>
						<li>Fair pricing for each order</li>
						<li>Deadlines you can meet</li>
						<li>Payment options suitable</li>
						<li>Constant improvement for better results</li>
					</ul>')); ?>

				</div>
			</section>
		</section>
	</section>
</section>
<script type="text/javascript">

	$(function () { 
		$("#mob_29, #mob_41").change(function() {
			$('#countryCode').html("+" + $('option:selected', this).attr('rel'));
		});
	});	
</script>