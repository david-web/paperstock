<?php /* Smarty version 2.6.26, created on 2014-08-23 12:27:33
         compiled from /home/papersst/public_html/system/config/../../system/app/in/messages_admin/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/messages_admin/tmpl/edit.html', 9, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/messages_admin/tmpl/edit.html', 13, false),array('modifier', 'count_characters', '/home/papersst/public_html/system/config/../../system/app/in/messages_admin/tmpl/edit.html', 58, false),)), $this); ?>
<td id="editForma" colspan="9" class="open">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='general_dataA')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'General data') : gLA($_tmp, 'General data')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
:</td>
			            <td><input type="text" class="long" name="name" id="name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['name'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_module')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Module') : gLA($_tmp, 'Module')); ?>
:</td>
			            <td>
			            	<select class="long" name="module_id" id="module_id">
								<?php echo $this->_tpl_vars['edit']['module_list']; ?>

							</select>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_enable')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enable') : gLA($_tmp, 'Enable')); ?>
:</td>
			            <td><input type="checkbox" name="enable" id="enable" value="1" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> /></td>
			        </tr>
					<tr>
			            <td><?php echo ((is_array($_tmp='m_js')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Js') : gLA($_tmp, 'Js')); ?>
:</td>
			            <td><input type="checkbox" name="Js" id="Js" value="1" <?php if ($this->_tpl_vars['edit']['js']): ?> checked="checked" <?php endif; ?> /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_description')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Description') : gLA($_tmp, 'Description')); ?>
:</td>
			            <td><textarea name="description" id="description" cols="20" rows="6" class="t-desc"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['description'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea></td>
			        </tr>
    			</table>
    		</td>
			<td>
    			<table class="inner-table">
			        <tr>
			            <td colspan="4"><b><?php echo ((is_array($_tmp='translations')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Translations') : gLA($_tmp, 'Translations')); ?>
</b></td>
			        </tr>
			        <tr>
                        <td>
							<div id="typeL" <?php if ($this->_tpl_vars['edit']['type'] == 'c'): ?> style="display:none;"<?php endif; ?>>
								<div class="block-holder open">
	                                <div class="inner-block">
	                                    <ul class="lang-tabs">
	                                    	<?php $_from = $this->_tpl_vars['langauges']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['langauges'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['langauges']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['langauges']['iteration']++;
?>
	                                    	<li<?php if (($this->_foreach['langauges']['iteration'] <= 1)): ?> class="active" <?php endif; ?>><a href="#"><?php echo $this->_tpl_vars['lng']['label']; ?>
</a></li>
								            <?php endforeach; endif; unset($_from); ?>
	                                    </ul>
	                                   
	                                    <div class="areaBlock">
		                                    <?php $_from = $this->_tpl_vars['langauges']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['langauges2'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['langauges2']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['langauges2']['iteration']++;
?>
		                                   	<div class="text-block<?php if (($this->_foreach['langauges2']['iteration'] <= 1)): ?> open<?php endif; ?>">
		                                        <textarea name="value_<?php echo $this->_tpl_vars['lk']; ?>
" id="value_<?php echo $this->_tpl_vars['lk']; ?>
" cols="20" rows="5"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['values'][$this->_tpl_vars['lk']])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>
		                                        <p class="wys"><a href="#" onclick="openCkEditor('value_<?php echo $this->_tpl_vars['lk']; ?>
', 'advanced'); return false;"><?php echo ((is_array($_tmp='wysiwyg')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'WYSIWYG') : gLA($_tmp, 'WYSIWYG')); ?>
</a></p>
		                                        <p class="count"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['values'][$this->_tpl_vars['lk']])) ? $this->_run_mod_handler('count_characters', true, $_tmp, true) : smarty_modifier_count_characters($_tmp, true)); ?>
 / 1000</p>
		                                        <div class="clr"><!-- clear --></div>
		                                    </div>
								            <?php endforeach; endif; unset($_from); ?>
							            </div>             
	                                </div>
	                            </div>
                            </div>
                        </td>
                    </tr>	        
				</table>
			</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('next');"><span><?php echo ((is_array($_tmp='save_and_next')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and next') : gLA($_tmp, 'Save and next')); ?>
</span></a></div>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save_and_close')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and close') : gLA($_tmp, 'Save and close')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="$('#edit_<?php echo $this->_tpl_vars['edit']['id']; ?>
').html('');"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>
    
    <script type="text/javascript">
    $(function () {
		$('ul.lang-tabs a').click(function() {
			var curChildIndex = $(this).parent().prevAll().length + 1;
			$(this).parent().parent().children('.active').removeClass('active');
			$(this).parent().addClass('active');
			$(this).parent().parent().next('.areaBlock').children('.open').fadeOut('fast',function() {
				$(this).removeClass('open');
				$(this).parent().children('div:nth-child('+curChildIndex+')').fadeIn('normal',function() {
					$(this).addClass('open');
				});
			});
			return false;
		});
		$('div.block-holder h4').click(function() {
			$(this).parent().parent().children('.open').removeClass('open');
			$(this).parent().addClass('open');
			
			return false;
		});

		$('.areaBlock textarea').keydown(function(){
			symbolcount(this);
		}).keyup(function(){
			symbolcount(this);
		}).focus(function(){
			symbolcount(this);
		});

		$symbolmax = 1000;

		symbolcount = function(e){
			$text = $(e).val();
			if ($text.length > $symbolmax - 1) {
				$(e).val($text.substr(0, $symbolmax));
				symbolcount(this);
			} else {
				$(e).parent().children('.count').html($(e).val().length + ' / ' + $symbolmax);
			}				
		}

		var tabindex = 1;
		$('#editForma :input,select').each(function() {
			if (this.type != "hidden" && this.id != "cmsLang") {
				var $input = $(this);
				$input.attr("tabindex", tabindex);
				tabindex++;
			}
		});

    });		
	</script>
</td>