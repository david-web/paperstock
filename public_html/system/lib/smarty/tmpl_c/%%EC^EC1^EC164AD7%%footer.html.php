<?php /* Smarty version 2.6.26, created on 2014-08-05 22:22:01
         compiled from /home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper-clients/footer.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper-clients/footer.html', 17, false),)), $this); ?>
<footer>
	<section class="wrap">
		
		<a href="#" class="to_the_top" data-goto="page"></a>
		<div class="c mob_clear"></div>
		<ul class="links1 clearfix">
			<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['menu']['BOTTOM_LINKS']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
			<li>
				<a href="<?php echo $this->_tpl_vars['menu']['BOTTOM_LINKS'][$this->_sections['item']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['menu']['BOTTOM_LINKS'][$this->_sections['item']['index']]['title']; ?>
">
					<?php echo $this->_tpl_vars['menu']['BOTTOM_LINKS'][$this->_sections['item']['index']]['title']; ?>

				</a>
			</li>
			<?php endfor; endif; ?>
		</ul>
		<div class="c"></div>
		<div class="security clearfix css3">
			<div class="title"><?php echo ((is_array($_tmp='footer_SecurePaymentsTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Secure payments by') : gL($_tmp, 'Secure payments by')); ?>
</div>
			<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_paypal.png">
			<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_mastercard.png" class="no_mob">
			<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_visa.png" class="no_mob">
			<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_aexpress.png" class="no_mob">
			<img alt="" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
pay_discover.png" class="no_mob">
		</div>
		
		<div class="copyright"><?php echo ((is_array($_tmp='copyright')) ? $this->_run_mod_handler('gL', true, $_tmp, '&copy; Copyright 2014 by Paperstock. All rights reserved') : gL($_tmp, '&copy; Copyright 2014 by Paperstock. All rights reserved')); ?>
</div>
	</section>
</footer>