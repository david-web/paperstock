<?php /* Smarty version 2.6.26, created on 2014-07-18 14:53:02
         compiled from /home/papersst/public_html/system/config/../../system/app/in/news/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/news/tmpl/edit.html', 11, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/news/tmpl/edit.html', 15, false),array('modifier', 'date', '/home/papersst/public_html/system/config/../../system/app/in/news/tmpl/edit.html', 23, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/in/news/tmpl/edit.html', 76, false),)), $this); ?>
<div class="content">
	<div id="modulePath">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<input type="hidden" name="content_id" class="simple" id="content_id" value="<?php echo $this->_tpl_vars['edit']['content_id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='general_data')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'General data') : gLA($_tmp, 'General data')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
*:</td>
			            <td><input type="text" class="long simple" name="title" id="title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
						<td><?php echo ((is_array($_tmp='enabled')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enabled') : gLA($_tmp, 'Enabled')); ?>
</td>
						<td><input type="checkbox" value="1" id="enable" class="active simple" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_date_from')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Date') : gLA($_tmp, 'Date')); ?>
:</td>
			            <td><input type="text" class="long simple" name="date" id="date" value="<?php if ($this->_tpl_vars['edit']['date']): ?><?php echo ((is_array($_tmp=((is_array($_tmp='d-m-Y')) ? $this->_run_mod_handler('date', true, $_tmp, $this->_tpl_vars['edit']['date']) : date($_tmp, $this->_tpl_vars['edit']['date'])))) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
<?php endif; ?>" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_lead')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Lead text') : gLA($_tmp, 'Lead text')); ?>
*:</td>
			            <td>
			            	<textarea name="lead" id="lead" class="simple" cols="100" rows="4"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['lead'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_lead_image')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Lead image') : gLA($_tmp, 'Lead image')); ?>
:</td>
			            <td>
			            	<div id="lead_imageDiv">
			            	<?php if ($this->_tpl_vars['edit']['lead_image']): ?>
			            	<img width="100" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['edit']['uploadFolder']['small']['upload_path']; ?>
<?php echo $this->_tpl_vars['edit']['lead_image']; ?>
"><a href="#" onclick="emptyUploadFile('lead_image'); return false;"><?php echo ((is_array($_tmp='m_delete')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Delete') : gLA($_tmp, 'Delete')); ?>
</a>
							<input type="hidden" class="simple" name="lead_image" id="lead_image" value="<?php echo $this->_tpl_vars['edit']['lead_image']; ?>
">
			            	<?php endif; ?>
			            	</div>
			            	<a href="#" id="lead_imageButton" class="select-btn"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_lead_image_alt')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Lead image alt text') : gLA($_tmp, 'Lead image alt text')); ?>
:</td>
			            <td><input type="text" class="long simple" name="lead_image_alt" id="lead_image_alt" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['lead_image_alt'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
					<tr>
			            <td><?php echo ((is_array($_tmp='m_text')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Text') : gLA($_tmp, 'Text')); ?>
:</td>
			            <td>
			            	<textarea name="text" id="text" class="simple" cols="100" rows="6"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['text'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>
			            	<p class="wys"><a href="#" onclick="openCkEditor('text', 'advanced'); return false;"><?php echo ((is_array($_tmp='wysiwyg')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'WYSIWYG') : gLA($_tmp, 'WYSIWYG')); ?>
</a></p>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_text_image')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Text image') : gLA($_tmp, 'Text image')); ?>
:</td>
			            <td>
			            	<div id="text_imageDiv">
			            	<?php if ($this->_tpl_vars['edit']['text_image']): ?>
			            	<img width="100" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['edit']['uploadFolder']['big']['upload_path']; ?>
<?php echo $this->_tpl_vars['edit']['text_image']; ?>
"><a href="#" onclick="emptyUploadFile('text_image'); return false;"><?php echo ((is_array($_tmp='m_delete')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Delete') : gLA($_tmp, 'Delete')); ?>
</a>
							<input type="hidden" class="simple" name="text_image" id="text_image" value="<?php echo $this->_tpl_vars['edit']['text_image']; ?>
">
			            	<?php endif; ?>
			            	</div>
			            	<a href="#" id="text_imageButton" class="select-btn"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_text_image_alt')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Text image alt text') : gLA($_tmp, 'Text image alt text')); ?>
:</td>
			            <td><input type="text" class="long simple" name="text_image_alt" id="text_image_alt" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['text_image_alt'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='files_block')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Files block') : gLA($_tmp, 'Files block')); ?>
</b></td>
			        </tr>
			         <tr>
			            <td><a href="#" onclick="addFilesBlock('filesBlock', '<?php echo $this->_tpl_vars['edit']['uploadFolder']['original']['upload_path']; ?>
'); return false;" title="<?php echo ((is_array($_tmp='add_block')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add block') : gLA($_tmp, 'Add block')); ?>
"><?php echo ((is_array($_tmp='add_block')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add block') : gLA($_tmp, 'Add block')); ?>
</a></td>
			            <td id="filesBlock">
			            <?php if ($this->_tpl_vars['edit']['files'] && count($this->_tpl_vars['edit']['files']) > 0): ?>
			            	<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['edit']['files']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>	
			            	<p class="files-block" id="files-block<?php echo $this->_sections['item']['index_next']; ?>
">
			            		<input class="files" type="text" id="fileTitle_<?php echo $this->_sections['item']['index_next']; ?>
" value="<?php echo $this->_tpl_vars['edit']['files'][$this->_sections['item']['index']]['fileTitle']; ?>
" />
			            		<input type="text" id="fileName_<?php echo $this->_sections['item']['index_next']; ?>
" value="<?php echo $this->_tpl_vars['edit']['files'][$this->_sections['item']['index']]['fileName']; ?>
" />
			            		<a href="#" id="fileButton_<?php echo $this->_sections['item']['index_next']; ?>
" class="select-btn"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
			            		<b>
			            			<a href="#" onclick="$('#files-block<?php echo $this->_sections['item']['index_next']; ?>
').remove(); return false" title="<?php echo ((is_array($_tmp='remove')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Remove') : gLA($_tmp, 'Remove')); ?>
"><?php echo ((is_array($_tmp='remove')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Remove') : gLA($_tmp, 'Remove')); ?>
</a>
			            		</b>
			            	</p>
			            	<script type="text/javascript">
			            		loadUploadButton('<?php echo $this->_tpl_vars['edit']['uploadFolder']['original']['upload_path']; ?>
', 'files-block' + <?php echo $this->_sections['item']['index_next']; ?>
, <?php echo $this->_sections['item']['index_next']; ?>
);
							</script>
			            	
			            	<?php endfor; endif; ?>
			            <?php endif; ?>
			            </td>
			        </tr>
			        <tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='links_block')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Links block') : gLA($_tmp, 'Links block')); ?>
</b></td>
			        </tr>
			         <tr>
			            <td><a href="#" onclick="addLinksBlock('linkBlock'); return false;" title="<?php echo ((is_array($_tmp='add_block')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add block') : gLA($_tmp, 'Add block')); ?>
"><?php echo ((is_array($_tmp='add_block')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add block') : gLA($_tmp, 'Add block')); ?>
</a></td>
			            <td id="linkBlock">
			            <?php if ($this->_tpl_vars['edit']['links'] && count($this->_tpl_vars['edit']['links']) > 0): ?>
			            	<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['edit']['links']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>	
			            	<p class="links-block" id="links-block<?php echo $this->_sections['item']['index_next']; ?>
">
			            		<input class="links" type="text" id="linkTitle_<?php echo $this->_sections['item']['index_next']; ?>
" value="<?php echo $this->_tpl_vars['edit']['links'][$this->_sections['item']['index']]['linkTitle']; ?>
" />
			            		<input type="text" id="linkUrl_<?php echo $this->_sections['item']['index_next']; ?>
" value="<?php echo $this->_tpl_vars['edit']['links'][$this->_sections['item']['index']]['linkUrl']; ?>
" />
			            		<input type="hidden" id="linkUrlId_<?php echo $this->_sections['item']['index_next']; ?>
" value="<?php echo $this->_tpl_vars['edit']['links'][$this->_sections['item']['index']]['linkUrlId']; ?>
" />
			            		<a href="#" onclick="openSiteMapDialog('linkUrlId_<?php echo $this->_sections['item']['index_next']; ?>
', 'linkUrl_<?php echo $this->_sections['item']['index_next']; ?>
', ''); return false;" class="select-btn"><?php echo ((is_array($_tmp='select')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select') : gLA($_tmp, 'Select')); ?>
</a>
			            		<select id="linkDocId_<?php echo $this->_sections['item']['index_next']; ?>
" name="linkDocId_<?php echo $this->_sections['item']['index_next']; ?>
">
									<option value="0">Select</option>
								</select>
			            		<select id="linkTarget_<?php echo $this->_sections['item']['index_next']; ?>
">select
			            			<option value="_blank" <?php if ($this->_tpl_vars['edit']['links'][$this->_sections['item']['index']]['linkTarget'] == '_blank'): ?> selected="selected"<?php endif; ?>>_blank</option>
			            			<option value="_self" <?php if ($this->_tpl_vars['edit']['links'][$this->_sections['item']['index']]['linkTarget'] == '_self'): ?> selected="selected"<?php endif; ?>>_self</option>
			            		</select>
			            		<b>
			            			<a href="#" onclick="$('#links-block<?php echo $this->_sections['item']['index_next']; ?>
').remove(); return false" title="<?php echo ((is_array($_tmp='remove')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Remove') : gLA($_tmp, 'Remove')); ?>
"><?php echo ((is_array($_tmp='remove')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Remove') : gLA($_tmp, 'Remove')); ?>
</a>
			            		</b>
			            	</p>
			            	
			            	<script type="text/javascript">
			            		<?php if ($this->_tpl_vars['edit']['links'][$this->_sections['item']['index']]['linkDocId']): ?>
			            		getDocumentsList('linkUrlId_<?php echo $this->_sections['item']['index_next']; ?>
', 'linkDocId_<?php echo $this->_sections['item']['index_next']; ?>
', '<?php echo $this->_tpl_vars['edit']['links'][$this->_sections['item']['index']]['linkDocId']; ?>
');
			            		<?php endif; ?>
							</script>
			            	<?php endfor; endif; ?>
			            <?php endif; ?>
			            </td>
			        </tr>
			        <tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='seo_block')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Seo block') : gLA($_tmp, 'Seo block')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_page_url')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Page url') : gLA($_tmp, 'Page url')); ?>
:</td>
			            <td><input type="text" class="long simple" name="page_url" id="page_url" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['page_url'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_page_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Page title') : gLA($_tmp, 'Page title')); ?>
:</td>
			            <td><input type="text" class="long simple" name="page_title" id="page_title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['page_title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_page_keywords')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Page keywords') : gLA($_tmp, 'Page keywords')); ?>
:</td>
			            <td><input type="text" class="long simple" name="page_keywords" id="page_keywords" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['page_keywords'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_page_description')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Page description') : gLA($_tmp, 'Page description')); ?>
:</td>
			            <td><input type="text" class="long simple" name="page_description" id="page_description" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['page_description'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
    			</table>
    		</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl() + '#content_id:<?php echo $this->_tpl_vars['edit']['content_id']; ?>
/';"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
	    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
            moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
            
            $(document).ready(function() {

                    var tabindex = 1;
                    $('#editForma :input,select').each(function() {
                            if (this.type != "hidden") {
                                    var $input = $(this);
                                    $input.attr("tabindex", tabindex);
                                    tabindex++;
                            }
                    });

                    $("#date").datepicker();

                    loadUploadImgButton('<?php echo $this->_tpl_vars['edit']['uploadFolder']['original']['upload_path']; ?>
', 'lead_image');
                    loadUploadImgButton('<?php echo $this->_tpl_vars['edit']['uploadFolder']['original']['upload_path']; ?>
', 'text_image');

            });
			
	</script>
	</div>
</div>