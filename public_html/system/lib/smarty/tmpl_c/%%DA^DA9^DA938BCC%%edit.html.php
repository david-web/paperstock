<?php /* Smarty version 2.6.26, created on 2014-07-27 12:38:17
         compiled from /home/papersst/public_html/system/config/../../system/app/in/profile/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/profile/tmpl/edit.html', 13, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/in/profile/tmpl/edit.html', 41, false),)), $this); ?>
<div class="content">
	<div id="modulePath">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
						<td colspan="2"><b>Basic information</b></td>
					</tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Email') : gLA($_tmp, 'Email')); ?>
*:</td>
			            <td><input type="text" class="long simple required" name="email" id="email" readonly="readonly" value="<?php echo $this->_tpl_vars['edit']['email']; ?>
" /></td>
			        </tr>
			        <tr>
						<td><?php echo ((is_array($_tmp='enabled')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enabled') : gLA($_tmp, 'Enabled')); ?>
</td>
						<td><input type="checkbox" value="1" id="enable" class="active simple" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
			            <td><?php echo ((is_array($_tmp='first_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'First name') : gLA($_tmp, 'First name')); ?>
:</td>
			            <td><input type="text" class="long simple" name="first_name" id="first_name" value="<?php echo $this->_tpl_vars['edit']['first_name']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='last_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Last name') : gLA($_tmp, 'Last name')); ?>
:</td>
			            <td><input type="text" class="long simple" name="last_name" id="last_name" value="<?php echo $this->_tpl_vars['edit']['last_name']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='profile_country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</td>
			            <td><input type="text" class="long simple" name="country" id="country" value="<?php echo $this->_tpl_vars['edit']['country']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Phone') : gLA($_tmp, 'Phone')); ?>
:</td>
			            <td><input type="text" class="long simple" name="phone" id="phone" value="<?php echo $this->_tpl_vars['edit']['phone']; ?>
" /></td>
			        </tr>
			        <tr>
						<td colspan="2"><b>Additional information</b></td>
					</tr>
					<tr>
			            <td><?php echo ((is_array($_tmp='created')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Created') : gLA($_tmp, 'Created')); ?>
:</td>
			            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d %b %Y") : smarty_modifier_date_format($_tmp, "%d %b %Y")); ?>
</td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='night_calls')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Night calls') : gLA($_tmp, 'Night calls')); ?>
:</td>
			            <td><input type="text" class="long simple" name="night_calls" id="night_calls" value="<?php echo $this->_tpl_vars['edit']['night_calls']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='alternative_email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Alternative email') : gLA($_tmp, 'Alternative email')); ?>
:</td>
			            <td><input type="text" class="long simple" name="alternative_email" id="alternative_email" value="<?php echo $this->_tpl_vars['edit']['alternative_email']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='alternative_phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Alternative phone') : gLA($_tmp, 'Alternative phone')); ?>
:</td>
			            <td><input type="text" class="long simple" name="alternative_phone" id="alternative_phone" value="<?php echo $this->_tpl_vars['edit']['alternative_phone']; ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='preferred_language')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Preferred language') : gLA($_tmp, 'Preferred language')); ?>
:</td>
			            <td><input type="text" class="long simple" name="preferred_language" id="preferred_language" value="<?php echo $this->_tpl_vars['edit']['preferred_language']; ?>
" /></td>
			        </tr>
    			</table>
    		</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl();"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
	    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
        moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
        
        $(document).ready(function() {

                var tabindex = 1;
                $('#editForma :input,select').each(function() {
                        if (this.type != "hidden") {
                                var $input = $(this);
                                $input.attr("tabindex", tabindex);
                                tabindex++;
                        }
                });
        });
			
	</script>
	</div>
</div>