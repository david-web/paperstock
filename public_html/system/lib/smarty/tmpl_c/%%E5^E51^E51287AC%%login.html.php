<?php /* Smarty version 2.6.26, created on 2014-07-08 23:06:19
         compiled from /home/papersst/public_html/system/config/../../admin/tmpl/login.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../admin/tmpl/login.html', 4, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->_tpl_vars['AD_CMS_LANGUAGE']; ?>
" lang="<?php echo $this->_tpl_vars['AD_CMS_LANGUAGE']; ?>
">
<head>
	<title><?php echo ((is_array($_tmp='title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS 2.5.5', '', true) : gLA($_tmp, 'Beattum CMS 2.5.5', '', true)); ?>
</title>
	<meta name="title" content="<?php echo ((is_array($_tmp='title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS 2.5.5', '', true) : gLA($_tmp, 'Beattum CMS 2.5.5', '', true)); ?>
" />
	<meta name="description" content="<?php echo ((is_array($_tmp='description')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS - Content managment system', '', true) : gLA($_tmp, 'Beattum CMS - Content managment system', '', true)); ?>
" />
	<meta name="keywords" content="<?php echo ((is_array($_tmp='keywords')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS', '', true) : gLA($_tmp, 'Beattum CMS', '', true)); ?>
" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta content="www.Beattum.lv" name="author" />
	<meta name="robots" content="noindex, nofollow" />
	<link type="image/ico" href="<?php echo $this->_tpl_vars['AD_CMS_IMAGE_FOLDER']; ?>
favicon.ico" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['AD_CMS_CSS_FOLDER']; ?>
main.css" media="screen, projection" />   
    <!--[if lte IE 7]>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['AD_CMS_CSS_FOLDER']; ?>
ie.css"/>
    <![endif]-->
    <!--[if IE 6]>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['AD_CMS_CSS_FOLDER']; ?>
ie6.css"/>
    <![endif]-->
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#username").focus();
		});

		function changeLanguage() {
			$("#action").val("language");
			$("#form").submit();
		}
	</script>
</head>
<body>
	<div id="wrapper" class="main">		
		
		<form id="form" action="<?php echo $this->_tpl_vars['url']; ?>
" method="post">
			<?php if ($this->_tpl_vars['denied']): ?>
			<p id="error"><?php echo ((is_array($_tmp='access_denied')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Access denied', '', true) : gLA($_tmp, 'Access denied', '', true)); ?>
</p>
			<?php endif; ?>
			<fieldset>
			<input type="hidden" name="action" value="login" id="action" />		
				<dl>
					<dt><label for="username"><?php echo ((is_array($_tmp='username')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Username', '', true) : gLA($_tmp, 'Username', '', true)); ?>
:</label></dt>
					<dd><input id="username" name="username" value="" /></dd>
					<dt><label for="password"><?php echo ((is_array($_tmp='password')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Password', '', true) : gLA($_tmp, 'Password', '', true)); ?>
:</label></dt>
					<dd><input id="password" type="password" name="password" value="" /></dd>
					<dt><label for="language"><?php echo ((is_array($_tmp='lang')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Language', '', true) : gLA($_tmp, 'Language', '', true)); ?>
:</label></dt>
					<dd>
						<select id="language" name="language" onchange="changeLanguage()">
							<?php echo $this->_tpl_vars['language_menu']; ?>

						</select>
					</dd>
					<dd><input type="submit" value="<?php echo ((is_array($_tmp='login')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Login', '', true) : gLA($_tmp, 'Login', '', true)); ?>
" id="button" /></dd>
				</dl>
			</fieldset>
		</form>
	</div>		
</body>
</html>