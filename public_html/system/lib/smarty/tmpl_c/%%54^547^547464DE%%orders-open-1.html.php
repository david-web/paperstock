<?php /* Smarty version 2.6.26, created on 2015-01-14 09:32:40
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-1.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-1.html', 3, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-1.html', 14, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-1.html', 32, false),array('modifier', 'implode', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-1.html', 54, false),array('modifier', 'ceil', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-1.html', 66, false),array('function', 'math', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-1.html', 33, false),)), $this); ?>
<section class="green_title order sp30-26-20">
	<section class="wrap">
		<h1><?php echo ((is_array($_tmp='order')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order') : gL($_tmp, 'Order')); ?>
 <?php echo $this->_tpl_vars['profile']['data']['id']; ?>
</h1>
	</section>
</section>
<section class="wrap mob_nowrap clearfix">
	<div class="col1k">
		<div class="order_status wrap_onlymob css3 clearfix">
			<div class="time_header ver_1024 ver_320"><?php echo ((is_array($_tmp='order_TimeLeft')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Time left') : gL($_tmp, 'Time left')); ?>
</div>
			<div class="time_block">
				<div class="days"><div class="nr"><?php echo $this->_tpl_vars['profile']['data']['diffDays']; ?>
</div><div class="text"><?php echo ((is_array($_tmp='order_Days')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div></div>
				<div class="hours"><div class="nr"><?php echo $this->_tpl_vars['profile']['data']['diffHours']; ?>
</div><div class="text"><?php echo ((is_array($_tmp='order_Hours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div></div>
				<div class="clearfix"></div>
				<div class="deadline"><?php echo ((is_array($_tmp='order_Deadline')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Deadline') : gL($_tmp, 'Deadline')); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['data']['first_draft_deadline'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>
</div>
			</div>
			<div class="status notpaid clearfix">
				<div class="status_cont">
					<a href="javascript:;" class="btn_status css3"><?php echo ((is_array($_tmp='order_Status_WriterAppointed')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Writer appointed: NO') : gL($_tmp, 'Writer appointed: NO')); ?>
</a>
					<a href="#" class="btn_pay notpaid css3"><?php echo ((is_array($_tmp='order_NotPaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Not paid') : gL($_tmp, 'Not paid')); ?>
</a>
				</div>
				<div class="price_cont withbtn">
					<div class="price"><?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price') : gL($_tmp, 'Total price')); ?>
 <?php echo $this->_tpl_vars['profile']['data']['price']; ?>
</div>
					<a href="#apply" class="btn3"><?php echo ((is_array($_tmp='profile_OrdersApply')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Apply') : gL($_tmp, 'Apply')); ?>
</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col2k">
		<div class="order_info wrap_onlymob css3">
			<div class="inner">
				<div class="twocols clearfix">
					<?php if (count($this->_tpl_vars['profile']['order']['3step']) > 0): ?>
						<?php echo smarty_function_math(array('assign' => 'coldiv2','equation' => "x / y",'x' => count($this->_tpl_vars['profile']['order']['3step']),'y' => 2), $this);?>

						<?php $_from = $this->_tpl_vars['profile']['order']['3step']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
							
							<?php $this->assign('ID', $this->_tpl_vars['i']['db_field']); ?>
							
							<?php if ($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']] && $this->_tpl_vars['ID'] != 'preferred_writer'): ?>
							
								<?php if (($this->_foreach['orderdata']['iteration'] <= 1)): ?>
								<div class="col first">		
								<?php endif; ?>
								
								<div class="line clearfix">
									<div class="col1z"><?php echo $this->_tpl_vars['i']['name']; ?>
:</div>
									<div class="col2z">
									<?php if ($this->_tpl_vars['ID'] == 'first_draft_deadline'): ?>
									<?php $this->assign('TIMETOSHOW', ($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']])); ?>
									<?php echo ((is_array($_tmp=$this->_tpl_vars['TIMETOSHOW'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>

									<?php elseif ($this->_tpl_vars['ID'] == 'pages_type'): ?>
									<?php echo $this->_tpl_vars['profile']['data']['number_of_pages']; ?>
, <?php echo $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]; ?>

									<?php elseif ($this->_tpl_vars['ID'] == 'additional_extras'): ?>
										<?php if (count($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]) > 0): ?>
											<?php echo implode(",", $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]); ?>

										<?php endif; ?>
									<?php else: ?>
									<?php echo $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]; ?>

									<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
							
							
							<?php if (($this->_foreach['orderdata']['iteration'] == $this->_foreach['orderdata']['total'])): ?>
								</div>	
							<?php elseif ($this->_foreach['orderdata']['iteration'] == ceil($this->_tpl_vars['coldiv2'])): ?>
								</div>
								<div class="col">
							<?php endif; ?>
							
						<?php endforeach; endif; unset($_from); ?>
					<?php endif; ?>
				</div>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_TopicInFull')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Topic in full') : gL($_tmp, 'Topic in full')); ?>
:</h2>
					<p>
						<?php echo $this->_tpl_vars['profile']['data']['topic']; ?>

					</p>
				</div>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_Details')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Details') : gL($_tmp, 'Details')); ?>
:</h2>
					<p>
						<?php echo $this->_tpl_vars['profile']['data']['work_details']; ?>

					</p>
				</div>
				<?php if (count($this->_tpl_vars['profile']['data']['additional_extras']) > 0): ?>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_Extras')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Extras') : gL($_tmp, 'Extras')); ?>
:</h2>
					<p class="bolded">
						<?php echo implode(",", $this->_tpl_vars['profile']['data']['additional_extras']); ?>

					</p>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="order_collapsible open css3">
			<a name="apply"></a>
			<div class="title_line"><div class="inner wrap_onlymob"><?php echo ((is_array($_tmp='order_ReasonForApply')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Reason for applying') : gL($_tmp, 'Reason for applying')); ?>
</div></div>
			<div class="collapsible"><div class="inner wrap_onlymob">
				<div class="reason">
					<div class="cinput"><textarea data-default="<?php echo ((is_array($_tmp='order_ReasonForApplyDefault')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Write your reason here') : gL($_tmp, 'Write your reason here')); ?>
" class="apply" name="reason" id="reason"></textarea></div>
					<div class="promise"><?php echo ((is_array($_tmp='order_WritersPromosedPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Your promosed price for this order:') : gL($_tmp, 'Your promosed price for this order:')); ?>
</div>
					<div class="cols clearfix">
						<div class="col1">
							<div class="cinput"><input type="text" name="amount" id="amount" class="apply"></div>
						</div>
						<div class="col2">
							<a href="javascript:;" onclick="profile.orderApply();" class="btn1" title="<?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
						</div>
						<div class="col3"><?php echo ((is_array($_tmp='order_WritersPromosedPriceText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'If you want you can offer your price, the more competitive your price is the best are chanses to be chonsen') : gL($_tmp, 'If you want you can offer your price, the more competitive your price is the best are chanses to be chonsen')); ?>
</div>
					</div>
				</div>
			</div></div>
		</div>
		<div class="backlink_cont wrap_onlymob">
			<a href="javascript:;" onclick="history.go(-1);" class="backlink"><?php echo ((is_array($_tmp='order_Back')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to order list') : gL($_tmp, 'Back to order list')); ?>
</a>
		</div>
	</div>
</section>