<?php /* Smarty version 2.6.26, created on 2014-07-18 14:57:40
         compiled from /home/papersst/public_html/system/config/../../system/lib/CKEditor/tmpl/ckeditor.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/lib/CKEditor/tmpl/ckeditor.html', 5, false),)), $this); ?>
          
<div class="ckeditor" id="ckeditor">  
	 <textarea name="ckeditor-area" id="ckeditor-area"></textarea>
	 
     <div class="btn"><a href="javascript:;" onclick="saveEditor(); return false;"><span><?php echo ((is_array($_tmp='save_and_close')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and close') : gLA($_tmp, 'Save and close')); ?>
</span></a></div>
     <div class="btn cancel"><a href="javascript:;" onclick="cancelEditor(); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
</div>

<div class="clr"><!-- clear --></div>
<script type="text/javascript">

	var	moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
	var	mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
	var	additionalParms = "";
	var	idField = '<?php echo $this->_tpl_vars['idField']; ?>
';
	var	mode = '<?php echo $this->_tpl_vars['mode']; ?>
';

	$(document).ready(function(){

		if (typeof(CKEDITOR.instances['ckeditor-area']) != 'undefined') {
			delete CKEDITOR.instances['ckeditor-area'];
		}	

		if (mode == 'advanced') {
			var config = {
	                toolbar: 'AdwebAdvanced',
	                height: '400px',
	                format_tags: 'p;h2;h3;h4;h5;h6;pre;address;div'
		            };
		} else {
			var config = {
	                toolbar: 'AdwebSimple',
	                height: '400px',
	                format_tags: 'p;h2;h3;h4;h5;h6;pre;address;div'
		            };
		}		
		
		$('#ckeditor-area').ckeditor(config);	
		$('#ckeditor-area').val($('#' + idField).val());
		
		
	});	

	function getRequestParameters() {
		return additionalParms;
	}

	function getRequestUrl() {
		return mainUrl + moduleName + "/";
	}

	function cancelEditor() {
		$('#ckeditor').dialog('close');
	}

	function saveEditor() {
		$('#' + idField).val($('#ckeditor-area').val());
		$('#ckeditor').dialog('close');
	}
	
</script>