<?php /* Smarty version 2.6.26, created on 2014-07-22 13:25:50
         compiled from /home/papersst/public_html/system/config/../../system/app/in/orders/tmpl/view.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/orders/tmpl/view.html', 10, false),)), $this); ?>
<div class="content">
    <form id="edit_form">
        <input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />

		<div id="orderTabs">
			<ul>
					
				<li>
					<a rel="general_data_form" href="#general_data_form"  >
						<?php echo ((is_array($_tmp='general')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Main info') : gLA($_tmp, 'Main info')); ?>

					</a>
				</li>
				<li>
					<a rel="files_form" href="#files_form"  >
						<?php echo ((is_array($_tmp='order_files_customer')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Files customer') : gLA($_tmp, 'Files customer')); ?>

					</a>
				</li>
				<li>
					<a rel="files_form2" href="#files_form2"  >
						<?php echo ((is_array($_tmp='order_files_writers')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Files writers') : gLA($_tmp, 'Files writers')); ?>

					</a>
				</li>
				<li>
					<a rel="messages_form" href="#messages_form"  >
						<?php echo ((is_array($_tmp='order_messages_customer')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Messages customer') : gLA($_tmp, 'Messages customer')); ?>

					</a>
				</li>
				<li>
					<a rel="messages_form2" href="#messages_form2"  >
						<?php echo ((is_array($_tmp='order_messages_writers')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Messages writers') : gLA($_tmp, 'Messages writers')); ?>

					</a>
				</li>
				<li>
					<a id="profile_form_link" rel="profile_form" href="#profile_form"  >
						<?php echo ((is_array($_tmp='order_profile')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Customer') : gLA($_tmp, 'Customer')); ?>

					</a>
				</li>
				
				<?php if ($this->_tpl_vars['edit']['statusOrig'] == 2): ?>
				<li>
					<a rel="writers_form" href="#writers_form"  >
						Writers
					</a>
				</li>
				<?php endif; ?>	
					
			</ul>

			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "general_data_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "files_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "files_form_writers.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages_form_writers.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "profile_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			
			<?php if ($this->_tpl_vars['edit']['statusOrig'] == 2): ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "writers_form.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php endif; ?>
		</div>


		        <table class="bttns-table">
            <tr>
                <td>
                	<?php if ($this->_tpl_vars['edit']['statusOrig'] == 1): ?>
                	
                	<div class="btn" style="margin-right: 10px;">
                        <a href="javascript:;" onclick="moduleAccept(<?php echo $this->_tpl_vars['edit']['id']; ?>
, true); return false;">
                            <span>Accept</span>
                        </a>
                    </div>
                	
                	<?php endif; ?>
                	
                	<?php if ($this->_tpl_vars['edit']['statusOrig'] != 3 && $this->_tpl_vars['edit']['statusOrig'] != 4): ?>
                	
                	<div class="btn" style="margin-right: 10px;">
                        <a href="javascript:;" onclick="moduleOrderCancel(<?php echo $this->_tpl_vars['edit']['id']; ?>
, true); return false;">
                            <span>Cancel order</span>
                        </a>
                    </div>
                    
                    <div class="btn" style="margin-right: 10px;">
                        <a href="javascript:;" onclick="$('#profile_form_link').click();">
                            <span>Call customer</span>
                        </a>
                    </div>
                	
                	<?php endif; ?>
                
                    <div class="btn" style="margin-right: 10px;">
                        <a href="javascript:;" onclick="saveData('save');">
                            <span>Save</span>
                        </a>
                    </div>
                    <div class="btn orange" style="margin-right: 10px;">
                        <a href="javascript:;" onclick="saveData('apply');">
                            <span>Apply</span>
                        </a>
                    </div>

                    <div class="btn cancel">
                        <a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl();">
                            <span>Back to list</span>
                        </a>
                    </div>
                </td>
            </tr>
        </table>
		
    </form>
</div>
<script type="text/javascript">
    
    moduleTable.mainUrl		= '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
    moduleTable.moduleName	= '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
    
    $(document).ready(function() {

    	$('ul.lang-tabs a').click(function() {
			var curChildIndex = $(this).parent().prevAll().length + 1;
			$(this).parent().parent().children('.active').removeClass('active');
			$(this).parent().addClass('active');
			$(this).parent().parent().next('.areaBlock').children('.open').fadeOut('fast',function() {
				$(this).removeClass('open');
				$(this).parent().children('div:nth-child('+curChildIndex+')').fadeIn('normal',function() {
					$(this).addClass('open');
				});
			});
			return false;
		});

    	$('#orderTabs').tabs();	   
    	
    });
    
    

</script>