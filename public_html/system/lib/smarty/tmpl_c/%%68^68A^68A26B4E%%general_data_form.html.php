<?php /* Smarty version 2.6.26, created on 2015-01-04 22:37:12
         compiled from general_data_form.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'general_data_form.html', 33, false),array('modifier', 'implode', 'general_data_form.html', 120, false),)), $this); ?>
<div id="general_data_form" class="main-tab">
	<table class="inner-table">

        <tr>
            <td>Order ID:</td>
            <td><?php echo $this->_tpl_vars['edit']['id']; ?>
</td>
        </tr>
        <tr>
            <td>Customer ID:</td>
            <td><?php echo $this->_tpl_vars['edit']['user_id']; ?>
</td>
        </tr>
        <tr>
            <td>Time left:</td>
            <td><?php echo $this->_tpl_vars['edit']['timeleft']; ?>
</td>
        </tr>
        <tr>
            <td>Payment status:</td>
            <td><?php echo $this->_tpl_vars['edit']['paid']; ?>
</td>
        </tr>
        <tr>
            <td>Order status:</td>
            <td>
            	<select class="simple" name="status" id="status">
            		<option value="1" <?php if ($this->_tpl_vars['edit']['status'] == 1): ?>selected="selected"<?php endif; ?>>Pending</option>
            		<option value="2" <?php if ($this->_tpl_vars['edit']['status'] == 2): ?>selected="selected"<?php endif; ?>>In Progress</option>
            		<option value="3" <?php if ($this->_tpl_vars['edit']['status'] == 3): ?>selected="selected"<?php endif; ?>>Finished</option>
            		<option value="4" <?php if ($this->_tpl_vars['edit']['status'] == 4): ?>selected="selected"<?php endif; ?>>Canceled</option>
            	</select>
            </td>
        </tr>
        <tr>
            <td>Deadline:</td>
            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['first_draft_deadline'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p")); ?>
</td>
        </tr>
        <tr>
            <td>Writers Deadline:</td>
            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['writers_deadline'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p")); ?>
</td>
        </tr>
        <tr>
            <td>Price:</td>
            <td><?php echo $this->_tpl_vars['edit']['price']; ?>
$</td>
        </tr>
        
        <tr>
            <td colspan="2"><b>Order information</b></td>
        </tr>
        <tr>
            <td>Type of services:</td>
            <td><?php echo $this->_tpl_vars['edit']['type_of_services']; ?>
</td>
        </tr>
        <tr>
            <td>Academic levels:</td>
            <td><?php echo $this->_tpl_vars['edit']['academic_levels']; ?>
</td>
        </tr>
        <tr>
            <td>Type of work:</td>
            <td><?php echo $this->_tpl_vars['edit']['type_of_work']; ?>
</td>
        </tr>
        <tr>
            <td>Type of institution:</td>
            <td><?php echo $this->_tpl_vars['edit']['type_of_institution']; ?>
</td>
        </tr>
        <tr>
            <td>Institution program name:</td>
            <td><?php echo $this->_tpl_vars['edit']['institution_program_name']; ?>
</td>
        </tr>
        <tr>
            <td>Job title or industry segment:</td>
            <td><?php echo $this->_tpl_vars['edit']['job_title_or_industry_segment']; ?>
</td>
        </tr>
        <tr>
            <td>Reasons for applying:</td>
            <td><?php echo $this->_tpl_vars['edit']['reasons_for_applying']; ?>
</td>
        </tr>
        <tr>
            <td>Subject:</td>
            <td><?php echo $this->_tpl_vars['edit']['subject']; ?>
</td>
        </tr>
        <tr>
            <td>Paper format:</td>
            <td><?php echo $this->_tpl_vars['edit']['paper_format']; ?>
</td>
        </tr>
        <tr>
            <td>Sources:</td>
            <td><?php echo $this->_tpl_vars['edit']['how_many_sources_do_you_need']; ?>
</td>
        </tr>
        <tr>
            <td>Topic:</td>
            <td><?php echo $this->_tpl_vars['edit']['topic']; ?>
</td>
        </tr>
        
        <tr>
            <td>Work details:</td>
            <td><?php echo $this->_tpl_vars['edit']['work_details']; ?>
</td>
        </tr>
        <tr>
        	<?php if ($this->_tpl_vars['edit']['type_of_services'] == 'Multiple Choices'): ?>
            <td>Number of questions:</td>
            <?php elseif ($this->_tpl_vars['edit']['type_of_services'] == 'Problem solving'): ?>
            <td>Number of problems:</td>
            <?php else: ?>
            <td>Number of pages:</td>
            <?php endif; ?>
            <td><?php echo $this->_tpl_vars['edit']['number_of_pages']; ?>
</td>
        </tr>
        <tr>
            <td>Pages type:</td>
            <td><?php echo $this->_tpl_vars['edit']['pages_type']; ?>
</td>
        </tr>
        <tr>
            <td>Preferred writer:</td>
            <td><?php echo $this->_tpl_vars['edit']['preferred_writer']; ?>
</td>
        </tr>
        <tr>
            <td>Writers id:</td>
            <td><?php echo $this->_tpl_vars['edit']['writer_id']; ?>
</td>
        </tr>
        <tr>
            <td>Additional extras:</td>
            <td><?php echo implode('<br />', $this->_tpl_vars['edit']['additional_extras']); ?>
</td>
        </tr>
        <tr>
            <td colspan="2"><b>Admin comment:</b></td>
        </tr>
        <tr>
            <td colspan="2">
            	<textarea class="simple" id="admin_comment" name="admin_comment" rows="5" style="width:500px"><?php echo $this->_tpl_vars['edit']['admin_comment']; ?>
</textarea>
			</td>
        </tr>
	</table>
</div>