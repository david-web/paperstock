<?php /* Smarty version 2.6.26, created on 2014-11-05 21:32:14
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/edit.html', 8, false),array('modifier', 'number_format', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/edit.html', 15, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/edit.html', 106, false),array('modifier', 'implode', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/edit.html', 109, false),)), $this); ?>
<section class="green_title order">
	<section class="wrap">
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
	</section>
</section>
<div class="sp27-37-28"></div>
<section class="wrap clearfix">
	<p><?php echo ((is_array($_tmp='profile_EditText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Text text text. Profile edit text.') : gL($_tmp, 'Text text text. Profile edit text.')); ?>
</p>
</section>
<div class="sp23-26-23"></div>

<section class="wrap clearfix profile_page">
	<div class="block block1 clearfix">
		<div class="tri_top"><div class="outer"><div class="inner">
			<div class="stat1"><?php echo ((is_array($_tmp=$this->_tpl_vars['userData']['total_amount'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, '.', '') : number_format($_tmp, 2, '.', '')); ?>
$</div>
			<div class="stat1t"><?php echo ((is_array($_tmp='writers_TotalAmount')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total earnings') : gL($_tmp, 'Total earnings')); ?>
</div>
		</div></div></div>
		<div class="tri_btm">
			<div class="duo1_left"><div class="outer"><div class="inner">
				<div class="stat2t"><?php echo ((is_array($_tmp='writers_ProfileRating')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Rating') : gL($_tmp, 'Rating')); ?>
</div>
				<div class="stat2"><?php echo ((is_array($_tmp=$this->_tpl_vars['userData']['rating'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, '.', '') : number_format($_tmp, 2, '.', '')); ?>
</div>
			</div></div></div>
			<div class="duo1_right"><div class="outer"><div class="inner">
				<div class="stat2t"><?php echo ((is_array($_tmp='writers_ProfileStatus')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Status') : gL($_tmp, 'Status')); ?>
</div>
				<div class="stat2">
					<?php if ($this->_tpl_vars['userData']['rating'] >= 4): ?>
					<?php echo ((is_array($_tmp='writers_ProfileVipStatus')) ? $this->_run_mod_handler('gL', true, $_tmp, 'VIP') : gL($_tmp, 'VIP')); ?>

					<?php else: ?>
					<?php echo ((is_array($_tmp='writers_ProfileSimpleStatus')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Regular writer') : gL($_tmp, 'Regular writer')); ?>

					<?php endif; ?>
				</div>
			</div></div></div>
		</div>
	</div>
	<div class="block block2 clearfix">
		<div class="tri_top"><div class="outer"><div class="inner">
			<div class="stat1"><?php echo $this->_tpl_vars['userData']['completed_orders']; ?>
</div>
			<div class="stat1t"><?php echo ((is_array($_tmp='writers_CompletedOrders')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Completed orders') : gL($_tmp, 'Completed orders')); ?>
</div>
		</div></div></div>
		<div class="tri_btm">
			<div class="duo2_right"><div class="outer"><div class="inner">
				<div class="stat2t"><?php echo ((is_array($_tmp='writers_SuccessfulApplications')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Successful applications') : gL($_tmp, 'Successful applications')); ?>
</div>
				<div class="stat2"><?php echo $this->_tpl_vars['userData']['applications']; ?>
</div>
			</div></div></div>
			<div class="duo2_left"><div class="outer"><div class="inner">
				<div class="stat2t"><?php echo ((is_array($_tmp='writers_TotalApplications')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total applications') : gL($_tmp, 'Total applications')); ?>
</div>
				<div class="stat2"><?php echo $this->_tpl_vars['userData']['total_applications']; ?>
</div>
			</div></div></div>
		</div>
	</div>
	<div class="block block3 clearfix">
		<div class="duo_top">
			<?php if ($this->_tpl_vars['userData']['id']): ?>
			<div class="field clearfix">
				<div class="col1b"><?php echo ((is_array($_tmp='profile_EditField_ID')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cliend ID') : gL($_tmp, 'Cliend ID')); ?>
</div>
				<div class="col2b"><?php echo $this->_tpl_vars['userData']['id']; ?>
</div>
			</div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['userData']['first_name']): ?>
			<div class="field clearfix">
				<div class="col1b"><?php echo ((is_array($_tmp='profile_EditField_Name')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Name') : gL($_tmp, 'Name')); ?>
</div>
				<div class="col2b"><?php echo $this->_tpl_vars['userData']['first_name']; ?>
</div>
			</div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['userData']['last_name']): ?>
			<div class="field clearfix">
				<div class="col1b"><?php echo ((is_array($_tmp='profile_EditField_LastName')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Last name') : gL($_tmp, 'Last name')); ?>
</div>
				<div class="col2b"><?php echo $this->_tpl_vars['userData']['last_name']; ?>
</div>
			</div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['userData']['email']): ?>
			<div class="field clearfix">
				<div class="col1b"><?php echo ((is_array($_tmp='profile_EditField_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>
</div>
				<div class="col2b"><?php echo $this->_tpl_vars['userData']['email']; ?>
</div>
			</div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['userData']['country']): ?>
			<div class="field clearfix">
				<div class="col1b"><?php echo ((is_array($_tmp='profile_EditField_Country')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Country') : gL($_tmp, 'Country')); ?>
</div>
				<div class="col2b"><?php echo $this->_tpl_vars['userData']['country']; ?>
</div>
			</div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['userData']['phone']): ?>
			<div class="field clearfix">
				<div class="col1b"><?php echo ((is_array($_tmp='profile_EditField_Phone')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Phone') : gL($_tmp, 'Phone')); ?>
</div>
				<div class="col2b"><?php echo $this->_tpl_vars['userData']['phone']; ?>
</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="duo_btm">
			<a href="javascript:;" onclick="profile.changePassword();" class="link trigger_changepw"><?php echo ((is_array($_tmp='profile_EditChangePassword')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Change passoword') : gL($_tmp, 'Change passoword')); ?>
</a>
		</div>
	</div>
	<div class="c"></div>
	<div class="block block4 clearfix">
		<h2><?php echo ((is_array($_tmp='writers_YourSettings')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Your settings') : gL($_tmp, 'Your settings')); ?>
</h2>
		<div class="col1c">
			<div class="field2">
				<div class="valuet"><?php echo ((is_array($_tmp='writers_EditField_24/7')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Are you will available 24/7?') : gL($_tmp, 'Are you will available 24/7?')); ?>
</div>
				<div class="value"><?php echo $this->_tpl_vars['userData']['available_24_7']; ?>
</div>
			</div>
			<div class="field2">
				<div class="valuet"><?php echo ((is_array($_tmp='writers_EditField_have_worked')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Have you ever worked for any other online academic assistance companies?') : gL($_tmp, 'Have you ever worked for any other online academic assistance companies?')); ?>
</div>
				<div class="value"><?php echo $this->_tpl_vars['userData']['have_worked']; ?>
</div>
			</div>
			<?php if (count($this->_tpl_vars['userData']['familiar_with']) > 0): ?>
			<div class="field2">
				<div class="valuet"><?php echo ((is_array($_tmp='writers_EditField_familiar_with')) ? $this->_run_mod_handler('gL', true, $_tmp, 'What citation styles are you familiar with?') : gL($_tmp, 'What citation styles are you familiar with?')); ?>
</div>
				<div class="value"><?php echo implode(', ', $this->_tpl_vars['userData']['familiar_with']); ?>
</div>
			</div>
			<?php endif; ?>
			<div class="field2">
				<div class="valuet"><?php echo ((is_array($_tmp='writers_EditField_native_language')) ? $this->_run_mod_handler('gL', true, $_tmp, 'What is your native language?') : gL($_tmp, 'What is your native language?')); ?>
</div>
				<div class="value"><?php echo $this->_tpl_vars['userData']['native_language']; ?>
</div>
			</div>
			<div class="field2">
				<div class="valuet"><?php echo ((is_array($_tmp='writers_EditField_academic_degree')) ? $this->_run_mod_handler('gL', true, $_tmp, 'What is your highest verifable academic degree?') : gL($_tmp, 'What is your highest verifable academic degree?')); ?>
</div>
				<div class="value"><?php echo $this->_tpl_vars['userData']['academic_degree']; ?>
</div>
			</div>
			<?php if (count($this->_tpl_vars['userData']['disciplines']) > 0): ?>
			<div class="field2">
				<div class="valuet"><?php echo ((is_array($_tmp='writers_EditField_disciplines')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Please select 3-7 disciplines you are proficient in and indicate sotware you have access to:') : gL($_tmp, 'Please select 3-7 disciplines you are proficient in and indicate sotware you have access to:')); ?>
</div>
				<div class="value"><?php echo implode(', ', $this->_tpl_vars['userData']['disciplines']); ?>
</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="col2c">
			<div class="field3">
				<div class="valuet"><?php echo ((is_array($_tmp='writers_EditField_about_me')) ? $this->_run_mod_handler('gL', true, $_tmp, 'About me:') : gL($_tmp, 'About me:')); ?>
</div>
				<div class="value"><?php echo $this->_tpl_vars['userData']['about_me']; ?>
</div>
			</div>
		</div>
	</div>
</section>