<?php /* Smarty version 2.6.26, created on 2014-12-28 21:06:42
         compiled from /home/papersst/public_html/system/config/../../admin/tmpl/main.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../admin/tmpl/main.html', 4, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../admin/tmpl/main.html', 68, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->_tpl_vars['AD_CMS_LANGUAGE']; ?>
" lang="<?php echo $this->_tpl_vars['AD_CMS_LANGUAGE']; ?>
">
<head>
	<title><?php echo ((is_array($_tmp='title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS 2.5.5', '', true) : gLA($_tmp, 'Beattum CMS 2.5.5', '', true)); ?>
</title>
	<meta name="title" content="<?php echo ((is_array($_tmp='title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS 2.5.5', '', true) : gLA($_tmp, 'Beattum CMS 2.5.5', '', true)); ?>
" />
	<meta name="description" content="<?php echo ((is_array($_tmp='description')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS - Content managment system', '', true) : gLA($_tmp, 'Beattum CMS - Content managment system', '', true)); ?>
" />
	<meta name="keywords" content="<?php echo ((is_array($_tmp='keywords')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS', '', true) : gLA($_tmp, 'Beattum CMS', '', true)); ?>
" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta content="www.Beattum.lv" name="author" />
	<meta name="robots" content="noindex, nofollow" />
	<link type="image/ico" href="<?php echo $this->_tpl_vars['AD_CMS_IMAGE_FOLDER']; ?>
favicon.ico" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['AD_CMS_CSS_FOLDER']; ?>
main.css" media="screen, projection" />   
    <!--[if lte IE 7]>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['AD_CMS_CSS_FOLDER']; ?>
ie.css"/>
    <![endif]-->
    <!--[if IE 6]>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['AD_CMS_CSS_FOLDER']; ?>
ie6.css"/>
    <![endif]-->
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
jquery.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
functions.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_JS_FOLDER']; ?>
functions.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
moduleTable.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
json.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
jquery.cookie.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
jquery.tree.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
ajax-uploader.js"></script>
	
	<!-- jQueryUI -->
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
jquery-ui/jquery-ui-1.8.2.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['AD_CMS_CSS_FOLDER']; ?>
jquery-ui/jquery-ui.css" media="screen, projection" />
	
	<!-- CKEditor -->
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CKEDITOR_FOLDER']; ?>
ckeditor.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CKEDITOR_FOLDER']; ?>
adapters/jquery.js"></script> 
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['AD_CMS_JS_FOLDER']; ?>
jquery_ckeditor_fixer.js"></script>
	 
	<script type="text/javascript">
		 $(document).ready(function() {
			 $.datepicker.setDefaults({
				dateFormat: 'dd-mm-yy'
			});
		  });
	</script>
	<?php echo $this->_tpl_vars['MODULE_HEAD']; ?>

	
</head>
<body>
<div id="ajax-loader"></div>
<div id="loader"><img src="<?php echo $this->_tpl_vars['AD_CMS_IMAGE_FOLDER']; ?>
preloader.gif" alt="<?php echo ((is_array($_tmp='ajax_loader')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Loading...', '', true) : gLA($_tmp, 'Loading...', '', true)); ?>
" /></div>

<div id="wrapper">
	<div id="header">
		<h1 id="logo"><img src="<?php echo $this->_tpl_vars['AD_CMS_IMAGE_FOLDER']; ?>
design/cms.png" alt="<?php echo ((is_array($_tmp='title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Beattum CMS 2.5.5', '', true) : gLA($_tmp, 'Beattum CMS 2.5.5', '', true)); ?>
" /></h1>
		<div class="inner">
			<p class="log-out">
				<a href="?logout" title="<?php echo ((is_array($_tmp='logout')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Log out', '', true) : gLA($_tmp, 'Log out', '', true)); ?>
"><?php echo ((is_array($_tmp='logout')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Log out', '', true) : gLA($_tmp, 'Log out', '', true)); ?>
</a>
			</p>
			<form id="language-form" method="post" action="">
				<fieldset>
					<legend><?php echo ((is_array($_tmp='language_changer')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Interface language', '', true) : gLA($_tmp, 'Interface language', '', true)); ?>
 form</legend>
					<label for="cmsLang"><?php echo ((is_array($_tmp='language_changer')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Interface language', '', true) : gLA($_tmp, 'Interface language', '', true)); ?>
:</label>
					<select onchange="$('#language-form').submit();" id="cmsLang" name="cmsLang">
						<?php echo $this->_tpl_vars['language_menu']; ?>

					</select>
				</fieldset>
			</form>
			<p style="float:right;margin-top:5px;">
				Current Time: <?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%D %H:%M:%S") : smarty_modifier_date_format($_tmp, "%D %H:%M:%S")); ?>

			</p>
		</div>
		<div class="clr"><!-- clear --></div>
	</div>
	<ul id="menu">
		<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['mainMenu']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
        <li <?php if ($this->_tpl_vars['mainMenu'][$this->_sections['item']['index']]['active']): ?>  class="active"<?php endif; ?>>
        	<a href="<?php echo $this->_tpl_vars['mainMenu'][$this->_sections['item']['index']]['link']; ?>
">
        		<span><?php echo $this->_tpl_vars['mainMenu'][$this->_sections['item']['index']]['name']; ?>
</span>
        	</a>
        </li>
        <?php endfor; endif; ?>
    </ul>
    <div class="clr"><!-- clear --></div>
	<div id="holder">
		<?php if ($this->_tpl_vars['modules_Menu']): ?>
			<?php echo $this->_tpl_vars['modules_Menu']; ?>

		<?php endif; ?>
		
		
		<?php echo $this->_tpl_vars['ADMIN_MODULE_BLOCK']; ?>

		
		<div class="clr"><!-- clear --></div>
	</div>
</div>
</body>
</html>