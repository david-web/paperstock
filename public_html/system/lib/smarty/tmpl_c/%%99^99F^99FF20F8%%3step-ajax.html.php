<?php /* Smarty version 2.6.26, created on 2014-08-12 10:30:58
         compiled from /home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/3step-ajax.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/3step-ajax.html', 4, false),array('modifier', 'cat', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/3step-ajax.html', 8, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/3step-ajax.html', 30, false),array('modifier', 'ceil', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/3step-ajax.html', 43, false),array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/3step-ajax.html', 54, false),array('function', 'math', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/3step-ajax.html', 5, false),)), $this); ?>
<section class="col1a">
		<section class="form_summary">
			<section class="wrap_onlymob clearfix">
				<?php if (count($this->_tpl_vars['order']['order']['3step']) > 0): ?>
					<?php echo smarty_function_math(array('assign' => 'coldiv2','equation' => "x / y",'x' => count($this->_tpl_vars['order']['order']['3step']),'y' => 2), $this);?>

					<?php $_from = $this->_tpl_vars['order']['order']['3step']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
						
						<?php $this->assign('ID', ((is_array($_tmp='mob_')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['k']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['k']))); ?>
						<?php if ($this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]): ?>
							
							<?php $this->assign('IDVAL', $this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]); ?>
							
							<?php if (($this->_foreach['orderdata']['iteration'] <= 1)): ?>
							<div class="col first">		
							<?php endif; ?>
							
							<div class="field clearfix">
								<div class="name"><?php echo $this->_tpl_vars['i']['name']; ?>
:</div>
								<div class="value">
								<?php if ($this->_tpl_vars['i']['values']): ?>
									<?php if ($this->_tpl_vars['k'] == 17): ?>
										<?php echo $_SESSION['order']['fields']['number_of_pages']; ?>
, <?php echo $this->_tpl_vars['i']['values'][$this->_tpl_vars['IDVAL']]['name']; ?>
 
									<?php else: ?>
										<?php echo $this->_tpl_vars['i']['values'][$this->_tpl_vars['IDVAL']]['name']; ?>

									<?php endif; ?>	
								<?php else: ?>
								
								<?php if ($this->_tpl_vars['k'] == 18): ?>
								<?php $this->assign('TIMETOSHOW', ($this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]+time()+$this->_tpl_vars['order']['order']['deadlinedays'])); ?>
								<?php echo ((is_array($_tmp=$this->_tpl_vars['TIMETOSHOW'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %I %p") : smarty_modifier_date_format($_tmp, "%e %B %I %p")); ?>

								<?php else: ?>
								<?php echo $this->_tpl_vars['order']['order']['fields'][$this->_tpl_vars['ID']]; ?>

								<?php endif; ?>
			
								<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						
						
						<?php if (($this->_foreach['orderdata']['iteration'] == $this->_foreach['orderdata']['total'])): ?>
							</div>	
						<?php elseif ($this->_foreach['orderdata']['iteration'] == ceil($this->_tpl_vars['coldiv2'])): ?>
							</div>
							<div class="col">
						<?php endif; ?>
						
					<?php endforeach; endif; unset($_from); ?>
				<?php endif; ?>
			</section>
		</section>
		<div class="ctabs">
			<div class="tab_top first css3" data-tab="1">
				<div class="text"><?php echo ((is_array($_tmp='order_LoginTabTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Login') : gL($_tmp, 'Login')); ?>
</div>
			</div>
			<div class="tab_top last css3" data-tab="2">
				<div class="text"><?php echo ((is_array($_tmp='order_RegisterTabTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Register') : gL($_tmp, 'Register')); ?>
</div>
			</div>
			<div class="c"></div>
			<div class="tab_bottom" data-tab="1">
				<section class="wrap_onlymob">
					<section class="order_form">
						<div class="explanation">
							<?php echo ((is_array($_tmp='order_LoginTabText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h2>Please authorize to complete your order!</h2>
							<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>') : gL($_tmp, '<h2>Please authorize to complete your order!</h2>
							<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>')); ?>

						</div>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip"><?php echo ((is_array($_tmp='order_LoginTab_Fields_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>

										<?php if (((is_array($_tmp='order_LoginTab_Fields_EmailHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint'))): ?>
										<div class="tip css3"><?php echo ((is_array($_tmp='order_LoginTab_Fields_EmailHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint')); ?>
</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b clearfix">
								<div class="col1e">
									<div class="cinput css3">
										<input type="text" class="login" name="email" id="email" value="" />
									</div>
								</div>
							</div>
						</div>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip"><?php echo ((is_array($_tmp='order_LoginTab_Fields_Password')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Password') : gL($_tmp, 'Password')); ?>

										<?php if (((is_array($_tmp='order_LoginTab_Fields_PasswordHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint'))): ?>
										<div class="tip css3"><?php echo ((is_array($_tmp='order_LoginTab_Fields_PasswordHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint')); ?>
</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b clearfix">
								<div class="col1e">
									<div class="cinput css3">
										<input type="password" class="login" name="password" id="password" value="" />
									</div>
								</div>
								<div class="col2e clearfix">
									<a href="#" class="forgot"><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Forgot your password?') : gL($_tmp, 'Forgot your password?')); ?>
</a>
								</div>
							</div>
						</div>
						<div class="forgot_pw_block" style="display: none;">
							<hr>
							<div class="explanation">
								<h2><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Forgot your password?') : gL($_tmp, 'Forgot your password?')); ?>
</h2>
							<p><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.') : gL($_tmp, 'Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.')); ?>
</p>
							</div>
							<div class="info_submitted_msg css3" style="display:none;"><?php echo ((is_array($_tmp='profile_ForgotPasswordOkMsg')) ? $this->_run_mod_handler('gL', true, $_tmp, 'New password has been sent to your email! <a href="#">Sign in with a new password</a>') : gL($_tmp, 'New password has been sent to your email! <a href="#">Sign in with a new password</a>')); ?>
</div>
							<div class="field clearfix">
								<div class="col1b">
									<div class="label_cont"><div class="label_center">
										<label><?php echo ((is_array($_tmp='order_LoginTab_Fields_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>
</label>
									</div></div>
								</div>
								<div class="col2b clearfix">
									<div class="col1e">
										<div class="cinput css3">
											<input type="text" id="password_reminder">
										</div>
									</div>
									<div class="col2e clearfix">
										<a href="javascript:;" onclick="profile.passwordReminder();" class="btn4 css3"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<?php if ($this->_tpl_vars['order']['order']['prevStep']): ?>
						<div class="summary has_back clearfix">
							<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back dt"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a>
							<div class="price set"><?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price $') : gL($_tmp, 'Total price $')); ?>
<span id="priceTotal"></span></div>
							<a href="javascript:;" onclick="order.profileLogin(<?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
);" class="btn3 css3"><?php echo ((is_array($_tmp='order_GoToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Go to step') : gL($_tmp, 'Go to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
</a>
							<div class="back_cont mob"><a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a></div>
						</div>
						<?php else: ?>
						<div class="summary clearfix">
							<div class="price"><?php echo ((is_array($_tmp='order_ApproxPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Approximate price $') : gL($_tmp, 'Approximate price $')); ?>
<span id="priceTotal"></span></div>
							<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
);" class="btn3 css3"><?php echo ((is_array($_tmp='order_GoToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Go to step') : gL($_tmp, 'Go to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
</a>
						</div>
						<?php endif; ?>
					</section>
				</section>
			</div>
			<div class="tab_bottom" data-tab="2">
				<section class="wrap_onlymob">
					<section class="order_form">
						<div class="explanation">
							<?php echo ((is_array($_tmp='order_RegisterTabText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h2>Please authorize to complete your order!</h2>
							<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>') : gL($_tmp, '<h2>Please authorize to complete your order!</h2>
							<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>')); ?>

						</div>
						<?php $_from = $this->_tpl_vars['order']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
						<?php if ($this->_tpl_vars['i']['type'] == 'tabselect'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b">
								<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
										<?php if ($this->_tpl_vars['v']['hint']): ?>
										<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
										<?php endif; ?>
										<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
									</div>
									<?php endforeach; endif; unset($_from); ?>
								</div>
								<div class="horizontal_dropdown">
									<select class="selectpicker register" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
										<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
										<?php endforeach; endif; unset($_from); ?>
									</select>
								</div>
							</div>
						</div>
						<?php elseif ($this->_tpl_vars['i']['type'] == 'select'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b">
								<div class="select_cont">
									<select class="selectpicker register" data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='order_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
										<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
										<option rel="<?php echo $this->_tpl_vars['v']['hint']; ?>
" value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
										<?php endforeach; endif; unset($_from); ?>
									</select>
								</div>
								<?php if ($this->_tpl_vars['i']['hint_below']): ?>
								<div class="note">
									<p>
										<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
									</p>
								</div>
								<?php endif; ?>
							</div>
						</div>
						<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b">
								<div class="cinput css3">
									<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
								</div>
							</div>
						</div>
						<?php elseif ($this->_tpl_vars['i']['type'] == 'pages'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b clearfix">
								<div class="col1c">
									<div class="cinput css3">
										<input type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="page_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
									</div>
								</div>
								<div class="col2c clearfix">
									<div class="col1d">
										<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
											<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
											<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
												<?php if ($this->_tpl_vars['v']['hint']): ?>
												<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
												<?php endif; ?>
												<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
											</div>
											<?php endforeach; endif; unset($_from); ?>
										</div>
										<div class="horizontal_dropdown">
											<select class="selectpicker" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
												<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
												<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-show=".show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
												<?php endforeach; endif; unset($_from); ?>
											</select>
										</div>
									</div>
									<div class="col2d">
										<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
										<?php if ($this->_tpl_vars['v']['hint']): ?>
										<div class="note2 hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_foreach['orderdatavalues']['iteration'] != 1): ?>style="display:none;"<?php endif; ?>>
											<?php echo $this->_tpl_vars['v']['hint']; ?>

										</div>
										<?php endif; ?>
										<?php endforeach; endif; unset($_from); ?>
									</div>
								</div>
							</div>
						</div>
						<?php elseif ($this->_tpl_vars['i']['type'] == 'hourdate'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b clearfix select_group" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<div class="col1c">
									<div class="special1 css3 hours"><?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
									<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<div class="option has_tip" data-value="8h">
											<div class="tip css3">8 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
											<div class="text">8</div>
										</div>
										<div class="option has_tip" data-value="24h">
											<div class="tip css3">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
											<div class="text">24</div>
										</div>
									</div>
								</div>
								<div class="col2c">
									<div class="special1 css3 days"><?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<div class="option has_tip active" data-value="2d">
											<div class="tip css3">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
											<div class="text">2</div>
										</div>
										<div class="option has_tip" data-value="3d">
											<div class="tip css3">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
											<div class="text">3</div>
										</div>
										<div class="option has_tip" data-value="6d">
											<div class="tip css3">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
											<div class="text">6</div>
										</div>
										<div class="option has_tip" data-value="10d">
											<div class="tip css3">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
											<div class="text">10</div>
										</div>
										<div class="option has_tip" data-value="14d">
											<div class="tip css3">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
											<div class="text">14</div>
										</div>
									</div>
								</div>
								<div class="horizontal_dropdown">
									<select class="selectpicker" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<option value="8h">8 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
										<option value="24h">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
										<option value="2d" selected="selected">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
										<option value="3d">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
										<option value="6d">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
										<option value="10d">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
										<option value="14d">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
									</select>
								</div>
								<div class="c"></div>
								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="note3">
									<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
								</div>
								<?php endif; ?>
							</div>
						</div>
						<?php elseif ($this->_tpl_vars['i']['type'] == 'extra'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b">
								<div class="checkbox_block css3 clearfix">
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<div class="option">
										<div class="checkbox"></div>
										<div class="text">
											<span class="has_tip"><?php echo $this->_tpl_vars['v']['name']; ?>

												<?php if ($this->_tpl_vars['v']['hint']): ?>
												<span class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</span>
												<?php endif; ?>
											</span>
										</div>
										<div class="price">$<?php echo $this->_tpl_vars['v']['price']; ?>
</div>
									</div>
									<?php endforeach; endif; unset($_from); ?>
								</div>
							</div>
						</div>
						<?php elseif ($this->_tpl_vars['i']['type'] == 'password'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b clearfix">
								<div class="col1e">
									<div class="cinput css3">
										<input class="register" type="password" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
									</div>
								</div>	
							</div>
						</div>
						<?php elseif ($this->_tpl_vars['i']['type'] == 'phone'): ?>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label class="has_tip">
										<?php echo $this->_tpl_vars['i']['name']; ?>

										<?php if ($this->_tpl_vars['i']['hint_over']): ?>
										<div class="tip css3">
											<?php echo $this->_tpl_vars['i']['hint_over']; ?>

										</div>
										<?php endif; ?>
									</label>
								</div></div>
							</div>
							<div class="col2b clearfix">
								<div class="col1e clearfix">
									<div class="col1f">
										<div class="cinput_nochange css3"><span id="countryCode"></span></div>
									</div>
									<div class="col2f">
										<div class="cinput css3">
											<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<?php endforeach; endif; unset($_from); ?>
						<div class="field clearfix">
							<div class="col1g">
								<input type="checkbox" class="styled register" name="agreement" id="agreement" value="1" />
							</div>
							<div class="col2g clearfix">
								<label>
									<?php echo ((is_array($_tmp='order_RegisterAgreementText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>') : gL($_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>')); ?>

								</label>
							</div>
							<div class="invalid_msg"></div>
						</div>
						<hr>
						<?php if ($this->_tpl_vars['order']['order']['prevStep']): ?>
						<div class="summary has_back clearfix">
							<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back dt"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a>
							<div class="price set"><?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price $') : gL($_tmp, 'Total price $')); ?>
<span id="priceTotal2"></span></div>
							<a href="javascript:;" onclick="order.profileRegister(<?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
);" class="btn3 css3"><?php echo ((is_array($_tmp='order_GoToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Go to step') : gL($_tmp, 'Go to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
</a>
							<div class="back_cont mob"><a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
);" class="back"><?php echo ((is_array($_tmp='order_BackToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to step') : gL($_tmp, 'Back to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['prevStep']; ?>
</a></div>
						</div>
						<?php else: ?>
						<div class="summary clearfix">
							<div class="price"><?php echo ((is_array($_tmp='order_ApproxPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Approximate price $') : gL($_tmp, 'Approximate price $')); ?>
<span id="priceTotal2"></span></div>
							<a href="javascript:;" onclick="order.changeStep(<?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
);" class="btn3 css3"><?php echo ((is_array($_tmp='order_GoToStep')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Go to step') : gL($_tmp, 'Go to step')); ?>
 <?php echo $this->_tpl_vars['order']['order']['nextStep']; ?>
</a>
						</div>
						<?php endif; ?>
					</section>
				</section>
			</div>
		</div>
	</section>
<script>
$(document).ready(function(){
	order.calculatePrice();
	
	$('.order_form .forgot').click(function(e){
		e.preventDefault();
		var c=$('.order_form .forgot_pw_block').stop(true);
		if(c.hasClass('open'))
		{
			c.show().removeClass('open').slideUp(function(){
				c.css('height','');
			});
		}
		else
		{
			c.hide().addClass('open').slideDown(function(){
				c.css('height','');
			});
		}
	});
	
	$("#mob_29, #mob_41").change(function() {
		$('#countryCode').html("+" + $('option:selected', this).attr('rel'));
	});
});	
</script>