<?php /* Smarty version 2.6.26, created on 2014-08-06 17:28:20
         compiled from /home/papersst/public_html/system/config/../../system/app/in/roles/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/roles/tmpl/edit.html', 9, false),)), $this); ?>
<td id="editForma" colspan="9" class="open">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td>
				<table class="inner-table">
					<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='general_data')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'General data') : gLA($_tmp, 'General data')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Name') : gLA($_tmp, 'Name')); ?>
:</td>
			            <td>
                        	<div>
								<div class="block-holder open">
	                                <div class="inner-block">
	                                    <ul class="lang-tabs">
	                                    	<?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
	                                    	<li<?php if (($this->_foreach['langauges']['iteration'] <= 1)): ?> class="active" <?php endif; ?>><a href="#"><?php echo $this->_tpl_vars['lng']['label']; ?>
</a></li>
								            <?php endforeach; endif; unset($_from); ?>
	                                    </ul>
	                                   
	                                    <div class="areaBlock">
		                                    <?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages2'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages2']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages2']['iteration']++;
?>
											<div class="text-block<?php if (($this->_foreach['languages2']['iteration'] <= 1)): ?> open<?php endif; ?>">
		                                        <input class="long required name" name="name_<?php echo $this->_tpl_vars['lk']; ?>
" id="name_<?php echo $this->_tpl_vars['lk']; ?>
" cols="20" rows="5" value="<?php echo $this->_tpl_vars['edit']['name'][$this->_tpl_vars['lk']]; ?>
"/>
		                                        <div class="clr"><!-- clear --></div>
		                                    </div>
								            <?php endforeach; endif; unset($_from); ?>
							            </div>             
	                                </div>
	                            </div>
                            </div>
                        </td>
			        </tr>
					
    			</table>
    		</td>
			<td>
    			<table id="userModules" class="inner-table">
			        <tr>
			            <td colspan="4"><b><?php echo ((is_array($_tmp='modules_list')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Modules list') : gLA($_tmp, 'Modules list')); ?>
</b></td>
			        </tr>
			        <tr>
                        <td>
                        	<div>
	                        	<?php $_from = $this->_tpl_vars['modules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['m']):
?>
								<div class="block-holder<?php if (( $this->_tpl_vars['k'] == 0 )): ?> open<?php endif; ?>">
	                                <h4><a href="#"><?php echo $this->_tpl_vars['m']['name']; ?>
</a></h4>
	                                <div class="inner-block">
                                    	<?php $_from = $this->_tpl_vars['roles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rk'] => $this->_tpl_vars['r']):
?>
                                    	<label for="<?php echo $this->_tpl_vars['r']; ?>
_<?php echo $this->_tpl_vars['m']['id']; ?>
"><?php echo ((is_array($_tmp="role_".($this->_tpl_vars['r']))) ? $this->_run_mod_handler('gLA', true, $_tmp, ($this->_tpl_vars['r'])) : gLA($_tmp, ($this->_tpl_vars['r']))); ?>
:</label> <input class="roles" type="checkbox" value="1" id="<?php echo $this->_tpl_vars['r']; ?>
_<?php echo $this->_tpl_vars['m']['id']; ?>
" <?php if ($this->_tpl_vars['edit']['roles'][$this->_tpl_vars['m']['id']][$this->_tpl_vars['r']]): ?> checked="checked"<?php endif; ?> />
							            <?php endforeach; endif; unset($_from); ?>
	                                </div>
	                            </div>
								<?php endforeach; endif; unset($_from); ?>
							</div>
                        </td>
                    </tr>	        
				</table>
			</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save_and_close')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and close') : gLA($_tmp, 'Save and close')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="$('#edit_<?php echo $this->_tpl_vars['edit']['id']; ?>
').html('');"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table> 
    <script type="text/javascript">
    $(function () {
		$('ul.lang-tabs a').click(function() {
			var curChildIndex = $(this).parent().prevAll().length + 1;
			$(this).parent().parent().children('.active').removeClass('active');
			$(this).parent().addClass('active');
			$(this).parent().parent().next('.areaBlock').children('.open').fadeOut('fast',function() {
				$(this).removeClass('open');
				$(this).parent().children('div:nth-child('+curChildIndex+')').fadeIn('normal',function() {
					$(this).addClass('open');
				});
			});
			return false;
		});
		$('div.block-holder h4').click(function() {
			$(this).parent().parent().children('.open').removeClass('open');
			$(this).parent().addClass('open');
			
			return false;
		});
		
    	checkForAdmin();
        
		var tabindex = 1;
		$('#editForma :input,select').each(function() {
			if (this.type != "hidden" && this.id != "cmsLang") {
				var $input = $(this);
				$input.attr("tabindex", tabindex);
				tabindex++;
			}
		});
    });	
	</script>
</td>