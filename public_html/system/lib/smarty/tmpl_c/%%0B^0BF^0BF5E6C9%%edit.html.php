<?php /* Smarty version 2.6.26, created on 2014-08-28 17:16:33
         compiled from /home/papersst/public_html/system/config/../../system/app/in/faq/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/faq/tmpl/edit.html', 10, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/faq/tmpl/edit.html', 14, false),)), $this); ?>
<div class="content">
	<div id="modulePath">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='general_data')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'General data') : gLA($_tmp, 'General data')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
*:</td>
			            <td><input type="text" class="long simple" name="title" id="title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
			        	<td><?php echo ((is_array($_tmp='parent_page')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Category') : gLA($_tmp, 'Category')); ?>
:</td>
			        	<td>
			        	<input type="hidden" class="simple" name="content_id" id="content_id" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['content_id'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" />
						<input type="text" class="notInsert" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['content_idTitle'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" id="content_idTitle" name="content_idTitle" readonly="readonly" />
						<a href="#" onclick="openSiteMapDialog('content_id', 'content_idTitle', ''); return false;" class="select-btn"><?php echo ((is_array($_tmp='select')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Select') : gLA($_tmp, 'Select')); ?>
</a>
						<a href="#" onclick="$('#content_id').val(''); $('#content_idTitle').val(''); return false;"><?php echo ((is_array($_tmp='clear')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Clear') : gLA($_tmp, 'Clear')); ?>
</a>
			        	</td>
						
			        </tr>
			        <tr>
						<td><?php echo ((is_array($_tmp='enabled')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enabled') : gLA($_tmp, 'Enabled')); ?>
</td>
						<td><input type="checkbox" value="1" id="enable" class="active simple" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					
					<?php if ($this->_tpl_vars['edit']['send_email'] && ! $this->_tpl_vars['edit']['send_email_done']): ?>
					<tr>
						<td><?php echo ((is_array($_tmp='send_answer_to_user')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'User have checked that wanna get answer, send him now without publication? ') : gLA($_tmp, 'User have checked that wanna get answer, send him now without publication? ')); ?>
</td>
						<td><input type="checkbox" value="1" id="send_done" class="active simple" /></td>
					</tr>
					<?php endif; ?>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_question')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Question') : gLA($_tmp, 'Question')); ?>
*:</td>
			            <td>
			            	<textarea name="question" id="question" class="simple" cols="100" rows="4" class="t-desc"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['question'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_answer')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Answer') : gLA($_tmp, 'Answer')); ?>
*:</td>
			            <td>
			            	<textarea name="answer" id="answer" class="simple" cols="100" rows="6"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['answer'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>
			            	<p class="wys"><a href="#" onclick="openCkEditor('answer', 'advanced'); return false;"><?php echo ((is_array($_tmp='wysiwyg')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'WYSIWYG') : gLA($_tmp, 'WYSIWYG')); ?>
</a></p>
			            </td>
			        </tr>
			        
    			</table>
    		</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl() + '#content_id:<?php echo $this->_tpl_vars['edit']['content_id']; ?>
/';"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
	    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
            moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
            
            $(document).ready(function() {

                    var tabindex = 1;
                    $('#editForma :input,select').each(function() {
                            if (this.type != "hidden") {
                                    var $input = $(this);
                                    $input.attr("tabindex", tabindex);
                                    tabindex++;
                            }
                    });
                    

            });
			
	</script>
	</div>
</div>