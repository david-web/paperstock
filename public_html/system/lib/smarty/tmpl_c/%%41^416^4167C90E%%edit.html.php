<?php /* Smarty version 2.6.26, created on 2014-09-08 11:04:02
         compiled from /home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/edit.html', 10, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/edit.html', 29, false),array('modifier', 'is_array', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/edit.html', 136, false),array('modifier', 'in_array', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/edit.html', 136, false),)), $this); ?>
<div class="content">
	<div id="modulePath">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='general_data')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'General data') : gLA($_tmp, 'General data')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='m_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Name') : gLA($_tmp, 'Name')); ?>
*:</td>
			            <td>
			            	<div class="block-holder open">
			                	<div class="inner-block">
								
			                        <ul class="lang-tabs">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                            <li <?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> class="active"<?php endif; ?> >
			                                <a href="javascript: void(0);"><?php echo $this->_tpl_vars['lng']['title']; ?>
</a>
			                            </li>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </ul>
			
			                        <div class="areaBlock">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                                <div class="text-block<?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> open<?php endif; ?>">
			                                    <input name="name" rel="<?php echo $this->_tpl_vars['lng']['lang']; ?>
" id="name_<?php echo $this->_tpl_vars['lng']['lang']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['data'][$this->_tpl_vars['lng']['lang']]['name'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" class="simple long required" type="text"  style="width:350px;" />
			                                    
			                                    <div class="clr"><!-- clear --></div>
			                                </div>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </div>
			
			                    </div>
			                </div>
			            
			            
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='unique_key')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Key') : gLA($_tmp, 'Key')); ?>
*:</td>
			            <td><input type="text" class="long simple required" name="db_field" id="db_field" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['db_field'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" readonly="readonly" /></td>
			        </tr>
			        <tr>
						<td><?php echo ((is_array($_tmp='save_in_db')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save IN DB') : gLA($_tmp, 'Save IN DB')); ?>
</td>
						<td><input type="checkbox" value="1" id="save_in_db" class="active simple" <?php if ($this->_tpl_vars['edit']['save_in_db']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
			        <tr>
						<td><?php echo ((is_array($_tmp='enabled')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enabled') : gLA($_tmp, 'Enabled')); ?>
</td>
						<td><input type="checkbox" value="1" id="enable" class="active simple" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
						<td><?php echo ((is_array($_tmp='hidden')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Hidden') : gLA($_tmp, 'Hidden')); ?>
</td>
						<td><input type="checkbox" value="1" id="hidden" class="active simple" <?php if ($this->_tpl_vars['edit']['hidden']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
						<td><?php echo ((is_array($_tmp='show_in_3step_')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Show in header') : gLA($_tmp, 'Show in header')); ?>
</td>
						<td><input type="checkbox" value="1" id="show_in_3step" class="active simple" <?php if ($this->_tpl_vars['edit']['show_in_3step']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
						<td><?php echo ((is_array($_tmp='show_in_inquiry')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Show in Inquiry') : gLA($_tmp, 'Show in Inquiry')); ?>
</td>
						<td><input type="checkbox" value="1" id="inquiry" class="active simple" <?php if ($this->_tpl_vars['edit']['inquiry']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
		                <td><?php echo ((is_array($_tmp='m_type')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Type') : gLA($_tmp, 'Type')); ?>
*:</td>
		                <td>
		                    <select name="type" id="type" class="long simple required">
		                        <option value=""><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option><?php echo $this->_tpl_vars['edit']['cfg']['types']; ?>

		                        <?php $_from = $this->_tpl_vars['config']['types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['select'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['select']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['select']['iteration']++;
?>
			                		<option <?php if ($this->_tpl_vars['k'] == $this->_tpl_vars['edit']['type']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
			                	<?php endforeach; endif; unset($_from); ?> 
		                    </select>
		                </td>
		            </tr>
		            <tr>
		                <td><?php echo ((is_array($_tmp='module')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Module') : gLA($_tmp, 'Module')); ?>
*:</td>
		                <td>
		                    <select name="module" id="module" class="long simple required">
		                        <option value=""><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
		                        <?php $_from = $this->_tpl_vars['config']['modules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['select'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['select']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['select']['iteration']++;
?>
			                		<option <?php if ($this->_tpl_vars['k'] == $this->_tpl_vars['edit']['module']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
			                	<?php endforeach; endif; unset($_from); ?> 
		                    </select>
		                </td>
		            </tr>
		            <tr>
		                <td><?php echo ((is_array($_tmp='form_step')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Form step') : gLA($_tmp, 'Form step')); ?>
:</td>
		                <td>
		                    <select name="form_step" id="form_step" class="long simple" data="<?php echo $this->_tpl_vars['edit']['form_step']; ?>
">
		                        <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
		                    </select>
		                </td>
		            </tr>
		            <!-- 
		            <tr>
			            <td><?php echo ((is_array($_tmp='css_class')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'CSS Class') : gLA($_tmp, 'CSS Class')); ?>
:</td>
			            <td><input type="text" class="long simple" name="css_class" id="css_class" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['css_class'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='css_style')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'CSS Style') : gLA($_tmp, 'CSS Style')); ?>
:</td>
			            <td><input type="text" class="long simple" name="css_style" id="css_style" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['css_style'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			         -->
			        <tr>
		                <td><?php echo ((is_array($_tmp='hint')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Hint') : gLA($_tmp, 'Hint')); ?>
:</td>
		                <td>
		                    <textarea rows="4" maxlength="150" cols="100" class="simple lead" id="hint" name="hint" style="width: 99%;"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['hint'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>                    
		                    <div class="clr"><!-- clear --></div>
		                </td>
		            </tr>
		            <tr>
		                <td><?php echo ((is_array($_tmp='hint_below')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Hint below') : gLA($_tmp, 'Hint below')); ?>
:</td>
		                <td>
		                    <textarea rows="4" maxlength="150" cols="100" class="simple lead" id="hint_below" name="hint_below" style="width: 99%;"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['hint_below'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>                    
		                    <div class="clr"><!-- clear --></div>
		                </td>
		            </tr>
		            <tr>
		                <td><?php echo ((is_array($_tmp='hint_over')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Hint over') : gLA($_tmp, 'Hint over')); ?>
:</td>
		                <td>
		                    <textarea rows="4" maxlength="150" cols="100" class="simple lead" id="hint_over" name="hint_over" style="width: 99%;"><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['hint_over'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</textarea>                    
		                    <div class="clr"><!-- clear --></div>
		                </td>
		            </tr>
		            <tr>
						<td><?php echo ((is_array($_tmp='required')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Required') : gLA($_tmp, 'Required')); ?>
</td>
						<td><input type="checkbox" value="1" id="required" name="required" class="active simple" <?php if ($this->_tpl_vars['edit']['required']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
		                <td><?php echo ((is_array($_tmp='form_filters')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Form filters') : gLA($_tmp, 'Form filters')); ?>
:</td>
		                <td>
		                    <select name="form_filters" id="form_filters" multiple="multiple" class="long simple" size="10">
		                    <?php $_from = $this->_tpl_vars['config']['form_filters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['select'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['select']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['select']['iteration']++;
?>
			                	<option <?php if (((is_array($_tmp=$this->_tpl_vars['edit']['form_filters'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp)) && ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['edit']['form_filters']) : in_array($_tmp, $this->_tpl_vars['edit']['form_filters']))): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
			                <?php endforeach; endif; unset($_from); ?>    
		                    </select>
		                </td>
		            </tr>
		            <tr>
		                <td><?php echo ((is_array($_tmp='form_errors')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Form errors') : gLA($_tmp, 'Form errors')); ?>
:</td>
		                <td>
		                    <select name="form_errors" id="form_errors" multiple="multiple" class="long simple" size="10">
		                    <?php $_from = $this->_tpl_vars['config']['form_errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['select'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['select']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['select']['iteration']++;
?>
			                	<option <?php if (((is_array($_tmp=$this->_tpl_vars['edit']['form_errors'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp)) && ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['edit']['form_errors']) : in_array($_tmp, $this->_tpl_vars['edit']['form_errors']))): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
			                <?php endforeach; endif; unset($_from); ?>    
		                    </select>
		                </td>
		            </tr>
		            <tr>
		                <td><?php echo ((is_array($_tmp='field_selected_action')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Field selected action') : gLA($_tmp, 'Field selected action')); ?>
:</td>
		                <td>
		                    <select name="field_selected_action" id="field_selected_action" class="long simple">
		                        <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
		                        <?php $_from = $this->_tpl_vars['config']['form_selactions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['select'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['select']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['select']['iteration']++;
?>
			                		<option <?php if ($this->_tpl_vars['i'] == $this->_tpl_vars['edit']['field_selected_action']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option>
			                	<?php endforeach; endif; unset($_from); ?> 
		                    </select>
		                </td>
		            </tr>
			        <tr>
						<td><?php echo ((is_array($_tmp='price')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Price') : gLA($_tmp, 'Price')); ?>
:</td>
						<td><input type="checkbox" value="1" id="price" class="active simple" <?php if ($this->_tpl_vars['edit']['price']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
						<td><?php echo ((is_array($_tmp='startpage')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Startpage') : gLA($_tmp, 'Startpage')); ?>
:</td>
						<td><input type="checkbox" value="1" id="startpage" class="active simple" <?php if ($this->_tpl_vars['edit']['startpage']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<!-- 
			        <tr>
			            <td><?php echo ((is_array($_tmp='default_val')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Default val') : gLA($_tmp, 'Default val')); ?>
:</td>
			            <td><input type="text" class="long simple" name="default_val" id="default_val" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['default_val'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" /></td>
			        </tr>
			         -->
    			</table>
    		</td>
		</tr>
	</table>
	<table class="inner-table" id="add-value-table">

			<tr>
	            <td><a href="javascript:;" onclick="addValue();"><?php echo ((is_array($_tmp='add_value')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Add value') : gLA($_tmp, 'Add value')); ?>
</a></td>
	        </tr>
	        
	        <?php $_from = $this->_tpl_vars['edit']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['values'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['values']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['values']['iteration']++;
?>
							
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "add-value.html", 'smarty_include_vars' => array('value' => $this->_tpl_vars['i'],'id' => ($this->_foreach['values']['iteration']-1),'config' => $this->_tpl_vars['config'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			 
			<?php endforeach; endif; unset($_from); ?> 
	        
		</table>
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl() + '#content_id:<?php echo $this->_tpl_vars['edit']['content_id']; ?>
/';"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
	    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
        moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';

		var data = {};
		data = <?php echo $this->_tpl_vars['configJson']; ?>
;
        
        $(document).ready(function() {

                var tabindex = 1;
                $('#editForma :input,select').each(function() {
                        if (this.type != "hidden") {
                                var $input = $(this);
                                $input.attr("tabindex", tabindex);
                                tabindex++;
                        }
                });     
                
                $('#module').change(function() {
                	checkModuleStepTab();
                	
                	if ($('#module').val() == 'order') {
                		$('#startpage').parent().parent().show();
                		$('#price').parent().parent().show();
                		$('#inquiry').parent().parent().show();
                		$('#show_in_3step').parent().parent().show();
                	} else {
                		$('#startpage').parent().parent().show();
                		$('#price').parent().parent().hide();
                		$('#inquiry').parent().parent().hide();
                		$('#show_in_3step').parent().parent().hide();
                	}
                });
                
                $('#module').trigger('change');
        });
			
	</script>
	</div>
</div>