<?php /* Smarty version 2.6.26, created on 2014-07-31 21:24:08
         compiled from /home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/contacts.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/content/tmpl/paper/contacts.html', 13, false),)), $this); ?>
<section id="content">
	<section class="green_title">
		<section class="wrap">
			<div class="location">
				<?php echo $this->_tpl_vars['web']['pagePath']; ?>

			</div>
			<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
		</section>
	</section>
	<section class="contact_page wrap clearfix">
		<div class="top">
			<h2>
				<?php echo ((is_array($_tmp='contacts_H2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Customer Support Representatives 24/7<span class="no_320"><br>Luctus, est in condimentum tristique, est orci blandit tortor</span>') : gL($_tmp, 'Customer Support Representatives 24/7<span class="no_320"><br>Luctus, est in condimentum tristique, est orci blandit tortor</span>')); ?>

			</h2>
		</div>
		<div class="middle clearfix">
			<div class="col col1">
				<div class="title css3"><?php echo ((is_array($_tmp='contacts_CallUs')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Call us') : gL($_tmp, 'Call us')); ?>
</div>
				<div class="inner css3 clearfix">
					<div class="info">
						<?php echo ((is_array($_tmp='contacts_CallUs_Text')) ? $this->_run_mod_handler('gL', true, $_tmp, '<div class="line clearfix">
							<div class="name">United Kingdom:</div>
							<div class="value">44-203-670-0050</div>
						</div>
						<div class="line clearfix">
							<div class="name">Australia:</div>
							<div class="value">61-283-550-175</div>
						</div>') : gL($_tmp, '<div class="line clearfix">
							<div class="name">United Kingdom:</div>
							<div class="value">44-203-670-0050</div>
						</div>
						<div class="line clearfix">
							<div class="name">Australia:</div>
							<div class="value">61-283-550-175</div>
						</div>')); ?>

					</div>
					<div class="or">
						<div class="text"><?php echo ((is_array($_tmp='contacts_CallUs_Or')) ? $this->_run_mod_handler('gL', true, $_tmp, 'or') : gL($_tmp, 'or')); ?>
</div>
					</div>
					<a href="skype:PapersStock.com?chat" class="btn5"><?php echo ((is_array($_tmp='contacts_ChatNow')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Chat now') : gL($_tmp, 'Chat now')); ?>
</a>
				</div>
			</div>
			<div class="col col2">
				<div class="title css3"><?php echo ((is_array($_tmp='contacts_Support')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Support') : gL($_tmp, 'Support')); ?>
</div>
				<div class="inner css3 clearfix">
					<?php echo ((is_array($_tmp='contacts_SupportText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<p>For questions about orders, performance control, suggestions, complaints, etc.</p>
					<a href="mailto:support@papersstock.com" class="btn1">support@papersstock.com</a>') : gL($_tmp, '<p>For questions about orders, performance control, suggestions, complaints, etc.</p>
					<a href="mailto:support@papersstock.com" class="btn1">support@papersstock.com</a>')); ?>

					
				</div>
			</div>
			<div class="col col3">
				<div class="title css3"><?php echo ((is_array($_tmp='contacts_Business')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Business') : gL($_tmp, 'Business')); ?>
</div>
				<div class="inner css3 clearfix">
					<?php echo ((is_array($_tmp='contacts_BusinessText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<p>For business Ut nec rutrum magna. Vivamus auctor imperdiet lacus</p>
					<a href="mailto:business@papersstock.com" class="btn1">business@papersstock.com</a>') : gL($_tmp, '<p>For business Ut nec rutrum magna. Vivamus auctor imperdiet lacus</p>
					<a href="mailto:business@papersstock.com" class="btn1">business@papersstock.com</a>')); ?>

					
				</div>
			</div>
		</div>
		<div class="bottom">
			
			<div class="title"><?php echo ((is_array($_tmp='contacts_BottomTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'FH Business Ltd') : gL($_tmp, 'FH Business Ltd')); ?>
</div>
			<div class="line"><?php echo ((is_array($_tmp='contacts_BottomLine1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Registered in England (Registration number: 08694707)') : gL($_tmp, 'Registered in England (Registration number: 08694707)')); ?>
</div>
			<div class="line"><?php echo ((is_array($_tmp='contacts_BottomLine2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Address: Suite 13087, 43 Bedford str., London, England, WC2E 9HA') : gL($_tmp, 'Address: Suite 13087, 43 Bedford str., London, England, WC2E 9HA')); ?>
</div>
		</div>
	</section>
	<div class="sp45-45-45"></div>
</section>