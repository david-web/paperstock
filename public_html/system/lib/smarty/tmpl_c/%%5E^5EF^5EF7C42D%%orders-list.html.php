<?php /* Smarty version 2.6.26, created on 2014-12-28 21:09:12
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'getLM', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-list.html', 6, false),array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-list.html', 8, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-list.html', 51, false),)), $this); ?>
<section class="green_title order sp26-26-0">
	<section class="wrap">
		<div class="btn_cont">

			<div class="orders ver_320">
				<select class="selectpicker" data-width="100%" onchange="orderFilter(this, '<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
');">
					<?php $_from = $this->_tpl_vars['profile']['config']['status']['apply']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderstatus'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderstatus']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderstatus']['iteration']++;
?>
					<option value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if (! $_GET['status'] || $_GET['status'] == $this->_tpl_vars['k']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>

		</div>
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
	</section>
</section>
<section class="wrap mob_nowrap clearfix">
	<nav class="list_menu ver_1024 ver_768 clearfix">
		<ul class="show ver_1024">
			<?php $_from = $this->_tpl_vars['profile']['config']['status']['apply']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderstatus'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderstatus']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderstatus']['iteration']++;
?>
			
			<?php if (( ! $_GET['status'] && $this->_tpl_vars['k'] == 0 ) || $_GET['status'] == $this->_tpl_vars['k']): ?>
			<li  class="active">
				<div class="item"><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</div>
			</li>
			<?php else: ?>
			<li>
				<a href="<?php if ($this->_tpl_vars['k'] == 0): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
<?php else: ?><?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
status:<?php echo $this->_tpl_vars['k']; ?>
/<?php endif; ?>" class="item">
					<?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>

				</a>
			</li>
			<?php endif; ?>
			
			<?php endforeach; endif; unset($_from); ?>
		</ul>
		<div class="show2 ver_768">
			<select class="selectpicker" data-width="100%" onchange="orderFilter(this, '<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
');">
				<?php $_from = $this->_tpl_vars['profile']['config']['status']['apply']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderstatus'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderstatus']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderstatus']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if (! $_GET['status'] || $_GET['status'] == $this->_tpl_vars['k']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['i'])) ? $this->_run_mod_handler('gL', true, $_tmp) : gL($_tmp)); ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</div>
		<div class="sort">
			<select class="selectpicker" id="ordersSort" data-width="100%" onchange="ordersSort('<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
');">
				<option <?php if ($_GET['sort'] == 'deadline' || ! $_GET['sort']): ?>selected="selected"<?php endif; ?> value="deadline"><?php echo ((is_array($_tmp='profile_SortByDeadline')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sort by deadline') : gL($_tmp, 'Sort by deadline')); ?>
</option>
				<option <?php if ($_GET['sort'] == 'created'): ?>selected="selected"<?php endif; ?> value="created"><?php echo ((is_array($_tmp='profile_SortByCreated')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sort by created') : gL($_tmp, 'Sort by created')); ?>
</option>
			</select>
		</div>
	</nav>
	
	<?php if (count($this->_tpl_vars['profile']['data']) > 0): ?>
	
	<div class="order_list ver_1024 ver_768">
		<div class="header css3 clearfix">
			<div class="col col1"><?php echo ((is_array($_tmp='profile_OrdersTable_ID')) ? $this->_run_mod_handler('gL', true, $_tmp, 'ID') : gL($_tmp, 'ID')); ?>
</div>
			<div class="col col2"><?php echo ((is_array($_tmp='profile_OrdersTable_Price')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Price') : gL($_tmp, 'Price')); ?>
</div>
			<div class="col col3"><?php echo ((is_array($_tmp='profile_OrdersTable_ItemsCount')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Items count') : gL($_tmp, 'Items count')); ?>
</div>
			<div class="col col4"><?php echo ((is_array($_tmp='profile_OrdersTable_TypeOfPaper')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Type of paper') : gL($_tmp, 'Type of paper')); ?>
</div>
			<div class="col col5"><?php echo ((is_array($_tmp='profile_OrdersTable_Subject')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Subject') : gL($_tmp, 'Subject')); ?>
</div>
			<div class="col col6"><?php echo ((is_array($_tmp='profile_OrdersTable_Topic')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Topic') : gL($_tmp, 'Topic')); ?>
</div>
			<div class="col col7"><?php echo ((is_array($_tmp='profile_OrdersTable_Deadline')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Deadline') : gL($_tmp, 'Deadline')); ?>
</div>
		</div>
		<?php $_from = $this->_tpl_vars['profile']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
		<div class="line">
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
id:<?php echo $this->_tpl_vars['i']['id']; ?>
/" class="line_link <?php if ($this->_tpl_vars['i']['status'] == 1): ?>bolded<?php endif; ?> clearfix">
				<div class="col col1"><?php echo $this->_tpl_vars['i']['id']; ?>
</div>
				<div class="col col2"><?php echo $this->_tpl_vars['i']['price']; ?>
</div>
				<div class="col col3"><?php echo $this->_tpl_vars['i']['number_of_pages']; ?>
</div>
				<div class="col col4"><?php echo $this->_tpl_vars['i']['type_of_work']; ?>
</div>
				<div class="col col5"><?php echo $this->_tpl_vars['i']['subject']; ?>
</div>
				<div class="col col6"><?php echo $this->_tpl_vars['i']['topic']; ?>
</div>
				<div class="col col7"><?php echo $this->_tpl_vars['i']['diffDays']; ?>
d <?php echo $this->_tpl_vars['i']['diffHours']; ?>
h</div>
				<div class="col col8"></div>
			</a>
			
			<?php if ($this->_tpl_vars['i']['status'] == 3 && $this->_tpl_vars['i']['writer_id'] != ''): ?>
				<?php if ($this->_tpl_vars['i']['rated']): ?>
					<a href="javascript:;" class="btn6 css3"><?php echo ((is_array($_tmp='profile_OrdersRated')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Rated') : gL($_tmp, 'Rated')); ?>
</a>
				<?php else: ?>
					<a href="javascript:;" onclick="profile.getOrderRatesForm(<?php echo $this->_tpl_vars['i']['id']; ?>
);" class="btn6 css3"><?php echo ((is_array($_tmp='profile_OrdersRate')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Rate') : gL($_tmp, 'Rate')); ?>
</a>
				<?php endif; ?>
			<?php elseif (! $this->_tpl_vars['i']['apply']): ?>
			<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
id:<?php echo $this->_tpl_vars['i']['id']; ?>
/" class="btn6 css3 purple"><?php echo ((is_array($_tmp='profile_OrdersApply')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Apply') : gL($_tmp, 'Apply')); ?>
</a>
			<?php endif; ?>
			
			<?php if (count($this->_tpl_vars['i']['messages']) > 0 || count($this->_tpl_vars['i']['files']) > 0): ?>
			<div class="icons">
				
				<?php if (count($this->_tpl_vars['i']['messages']) > 0): ?>
				<a href="#" class="icon purple"></a>
				<?php endif; ?>
				
				<?php if (count($this->_tpl_vars['i']['files']) > 0): ?>
				<a href="#" class="icon yellow"><?php echo count($this->_tpl_vars['i']['files']); ?>
</a>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endforeach; endif; unset($_from); ?>
		
		
		
	</div>
	<div class="order_list2 ver_320">
			<?php $_from = $this->_tpl_vars['profile']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
			<div class="item">
				<div class="wrap_onlymob">
					<div class="info1"><a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['siteData']['mirros_profile_orders_page'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
id:<?php echo $this->_tpl_vars['i']['id']; ?>
/"><?php echo ((is_array($_tmp='profile_OrdersTable_ID')) ? $this->_run_mod_handler('gL', true, $_tmp, 'ID') : gL($_tmp, 'ID')); ?>
: <?php echo $this->_tpl_vars['i']['id']; ?>
</a></div>
					<div class="info2 clearfix">
						<div class="col1">
							<div class="title"><?php echo ((is_array($_tmp='profile_OrdersTable_TypeOfPaper')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Type of paper') : gL($_tmp, 'Type of paper')); ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['i']['type_of_work']; ?>
</div>
						</div>
						<div class="col2">
							<div class="title"><?php echo ((is_array($_tmp='profile_OrdersTable_Subject')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Subject') : gL($_tmp, 'Subject')); ?>
:</div>
							<div class="value"><?php echo $this->_tpl_vars['i']['subject']; ?>
</div>
						</div>
					</div>
					<div class="info3">
						<div class="title"><?php echo ((is_array($_tmp='profile_OrdersTable_Topic')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Topic') : gL($_tmp, 'Topic')); ?>
:</div>
						<div class="value"><?php echo $this->_tpl_vars['i']['topic']; ?>
</div>
					</div>
				</div>
				<div class="line">
					<div class="wrap_onlymob clearfix">
						<div class="col col1 bolded"><?php echo $this->_tpl_vars['i']['price']; ?>
</div>
						<div class="col col3"><?php echo $this->_tpl_vars['i']['diffDays']; ?>
d <?php echo $this->_tpl_vars['i']['diffHours']; ?>
h</div>
					</div>
				</div>
			</div>
			<?php endforeach; endif; unset($_from); ?>
			
		</div>
	
	<?php else: ?>
	
	<div class="order_list_empty wrap mob_nowrap css3">
		<div class="wrap_onlymob">
			<h2><?php echo ((is_array($_tmp='profile_OrdersEmptyTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'No orders yet') : gL($_tmp, 'No orders yet')); ?>
</h2>
			<?php echo ((is_array($_tmp='profile_OrdersEmptyText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
			<br>
			<p>Praesent in porta magna. Nunc hendrerit libero sed justo pharetra, eu tincidunt tellus tempus.</p>') : gL($_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
			<br>
			<p>Praesent in porta magna. Nunc hendrerit libero sed justo pharetra, eu tincidunt tellus tempus.</p>')); ?>

			
		</div>
	</div>
	
	<?php endif; ?>
	
	
	
</section>