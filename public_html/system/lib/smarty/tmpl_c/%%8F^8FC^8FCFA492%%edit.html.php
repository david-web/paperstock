<?php /* Smarty version 2.6.26, created on 2014-08-05 17:07:56
         compiled from /home/papersst/public_html/system/config/../../system/app/in/tests/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/tests/tmpl/edit.html', 10, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/tests/tmpl/edit.html', 26, false),)), $this); ?>
<div class="content">
	<div id="modulePath">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td class="left-td">
				<table class="inner-table">
					<tr>
			            <td><?php echo ((is_array($_tmp='title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
*:</td>
			            <td>
			            	<div class="block-holder open">
			                	<div class="inner-block">
								
			                        <ul class="lang-tabs">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                            <li <?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> class="active"<?php endif; ?> >
			                                <a href="javascript: void(0);"><?php echo $this->_tpl_vars['lng']['title']; ?>
</a>
			                            </li>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </ul>
			
			                        <div class="areaBlock">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                                <div class="text-block<?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> open<?php endif; ?>">
			                                    <input name="title" rel="<?php echo $this->_tpl_vars['lng']['lang']; ?>
" id="title_<?php echo $this->_tpl_vars['lng']['lang']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['data'][$this->_tpl_vars['lng']['lang']]['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" class="simple long required" type="text"  style="width:350px;" />
			                                    
			                                    <div class="clr"><!-- clear --></div>
			                                </div>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </div>
			
			                    </div>
			                </div>
			            
			            
			            </td>
			        </tr>
			        <tr>
		                <td><?php echo ((is_array($_tmp='type')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Type') : gLA($_tmp, 'Type')); ?>
*:</td>
		                <td>
		                    <select name="type" id="type" class="long simple required">
		                        <option value=""><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
		                        <option <?php if (1 == $this->_tpl_vars['edit']['type']): ?> selected="selected" <?php endif; ?> value="1">1</option>
		                        <option <?php if (2 == $this->_tpl_vars['edit']['type']): ?> selected="selected" <?php endif; ?> value="2">2</option> 
		                    </select>
		                </td>
		            </tr>
			        <tr>
						<td><?php echo ((is_array($_tmp='enabled')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enabled') : gLA($_tmp, 'Enabled')); ?>
</td>
						<td><input type="checkbox" value="1" id="enable" class="active simple" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> /></td>
					</tr>
					<tr>
			            <td>Upload file:</td>
			            <td>
			            	<div id="filenameDiv">
			            		<input type="text" value="<?php echo $this->_tpl_vars['edit']['filename']; ?>
" id="filename" class="simple">
			            	</div>
			            	<a href="#" id="filenameButton" class="select-btn"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='description')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Description') : gLA($_tmp, 'Description')); ?>
:</td>
			            <td>
			            	<div class="block-holder open">
			                	<div class="inner-block">
								
			                        <ul class="lang-tabs">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                            <li <?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> class="active"<?php endif; ?> >
			                                <a href="javascript: void(0);"><?php echo $this->_tpl_vars['lng']['title']; ?>
</a>
			                            </li>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </ul>
			
			                        <div class="areaBlock">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                                <div class="text-block<?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> open<?php endif; ?>">
			                                    <input name="description" rel="<?php echo $this->_tpl_vars['lng']['lang']; ?>
" id="description_<?php echo $this->_tpl_vars['lng']['lang']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['data'][$this->_tpl_vars['lng']['lang']]['description'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" class="simple long" type="text"  style="width:350px;" />
			                                    
			                                    <div class="clr"><!-- clear --></div>
			                                </div>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </div>
			
			                    </div>
			                </div>
			            
			            
			            </td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='footerTitle')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Footer title') : gLA($_tmp, 'Footer title')); ?>
:</td>
			            <td>
			            	<div class="block-holder open">
			                	<div class="inner-block">
								
			                        <ul class="lang-tabs">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                            <li <?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> class="active"<?php endif; ?> >
			                                <a href="javascript: void(0);"><?php echo $this->_tpl_vars['lng']['title']; ?>
</a>
			                            </li>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </ul>
			
			                        <div class="areaBlock">
			                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
			                                <div class="text-block<?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> open<?php endif; ?>">
			                                    <input name="header" rel="<?php echo $this->_tpl_vars['lng']['lang']; ?>
" id="header_<?php echo $this->_tpl_vars['lng']['lang']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['data'][$this->_tpl_vars['lng']['lang']]['header'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" class="simple long" type="text"  style="width:350px;" />
			                                    
			                                    <div class="clr"><!-- clear --></div>
			                                </div>
			                            <?php endforeach; endif; unset($_from); ?>
			                        </div>
			
			                    </div>
			                </div>
			            
			            
			            </td>
			        </tr>
    			</table>
    		</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
            	<?php if ($this->_tpl_vars['edit']['id']): ?>
                <div class="btn orange"><a href="javascript:;" onclick="checkFields('apply');"><span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span></a></div>
                <?php endif; ?>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="window.location.href = moduleTable.getRequestUrl();"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
	    moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
        moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
        
        $(document).ready(function() {

                var tabindex = 1;
                $('#editForma :input,select').each(function() {
                        if (this.type != "hidden") {
                                var $input = $(this);
                                $input.attr("tabindex", tabindex);
                                tabindex++;
                        }
                });
                
                loadUploadSimple('tests/', 'filename');
        });
			
	</script>
	</div>
</div>