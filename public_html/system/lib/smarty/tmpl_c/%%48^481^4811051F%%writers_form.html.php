<?php /* Smarty version 2.6.26, created on 2014-07-22 13:27:20
         compiled from writers_form.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'implode', 'writers_form.html', 26, false),)), $this); ?>
<div id="writers_form" class="main-tab">
	<table class="cms-table">
	
		<tr>
			<th><b>Writer ID</b></th>
			<th><b>Raiting</b></th>
			<th><b>Completed orders</b></th>
			<th><b>Disciplines</b></th>
			<th><b>Amount</b></th>
			<th><b>Reason</b></th>
			<th></th>
		</tr>
		<?php $_from = $this->_tpl_vars['edit']['writers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
				
		<tr>
			<td>
				<?php echo $this->_tpl_vars['i']['writer_id']; ?>

			</td>
			<td>
				<?php echo $this->_tpl_vars['i']['rating']; ?>

			</td>
			<td>
				<?php echo $this->_tpl_vars['i']['completed_orders']; ?>

			</td>
			<td>
				<?php echo implode(', ', $this->_tpl_vars['i']['disciplines']); ?>

			</td>
			<td>
				<?php echo $this->_tpl_vars['i']['amount']; ?>

			</td>
			<td>
				<?php echo $this->_tpl_vars['i']['reason']; ?>

			</td>
			<td>
				<?php if ($this->_tpl_vars['edit']['writer_id']): ?>
					<?php if ($this->_tpl_vars['edit']['writer_id'] == $this->_tpl_vars['i']['writer_id']): ?>
						Applied
					<?php endif; ?>
				<?php else: ?>
				<a href="javascript:;" onclick="moduleAssignWriter(<?php echo $this->_tpl_vars['i']['writer_id']; ?>
);">Assign</a>
				<?php endif; ?>
			</td>	
		</tr>

		<?php endforeach; endif; unset($_from); ?>
		
		
	</table>
</div>