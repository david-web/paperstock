<?php /* Smarty version 2.6.26, created on 2014-07-22 13:25:50
         compiled from files_form_writers.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'md5', 'files_form_writers.html', 12, false),array('modifier', 'date_format', 'files_form_writers.html', 13, false),array('modifier', 'gLA', 'files_form_writers.html', 61, false),)), $this); ?>
<div id="files_form2" class="main-tab">
	<table class="inner-table">
		<tr>
			<td colspan="2">
				<b>Writer files</b>
			</td>
		</tr>
		<?php $_from = $this->_tpl_vars['edit']['files']['2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
		<?php if ($this->_tpl_vars['i']['user_id'] && $this->_tpl_vars['i']['type'] == 2): ?>
		<tr>
			<td colspan="2">
				<a href="/download.php?file=orders/<?php echo md5($this->_tpl_vars['edit']['id']); ?>
/<?php echo $this->_tpl_vars['i']['filename']; ?>
"><?php echo $this->_tpl_vars['i']['filename']; ?>
</a>
				<p><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p")); ?>
</p>
				<p>
				<?php if ($this->_tpl_vars['i']['enabled']): ?>
				<a href="javascript:;" onclick="moduleEnableFile(<?php echo $this->_tpl_vars['i']['id']; ?>
, '0');">Disable</a>
				<?php else: ?>
				<a href="javascript:;" onclick="moduleEnableFile(<?php echo $this->_tpl_vars['i']['id']; ?>
, '1');">Enable</a>
				<?php endif; ?>
				/ <a href="javascript:;" onclick="moduleDeleteFile(<?php echo $this->_tpl_vars['i']['id']; ?>
);">Delete</a>
				</p>
			</td>	
		</tr>
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td colspan="2">
				<b>Administrator files</b>
			</td>
		</tr>
		<?php $_from = $this->_tpl_vars['edit']['files']['2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
		<?php if (! $this->_tpl_vars['i']['user_id']): ?>
		<tr>
			<td colspan="2">
				<a href="/download.php?file=orders/<?php echo md5($this->_tpl_vars['edit']['id']); ?>
/<?php echo $this->_tpl_vars['i']['filename']; ?>
"><?php echo $this->_tpl_vars['i']['filename']; ?>
</a>
				<p><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p")); ?>
</p>
				<p>
				<?php if ($this->_tpl_vars['i']['enabled']): ?>
				<a href="javascript:;" onclick="moduleEnableFile(<?php echo $this->_tpl_vars['i']['id']; ?>
, '0');">Disable</a>
				<?php else: ?>
				<a href="javascript:;" onclick="moduleEnableFile(<?php echo $this->_tpl_vars['i']['id']; ?>
, '1');">Enable</a>
				<?php endif; ?>
				/ <a href="javascript:;" onclick="moduleDeleteFile(<?php echo $this->_tpl_vars['i']['id']; ?>
);">Delete</a>
				
				<?php if ($this->_tpl_vars['i']['ext'] == 'jpg' || $this->_tpl_vars['i']['ext'] == 'jpeg'): ?>
				/ <a href="javascript:;" onclick="modulePreviewFile(<?php echo $this->_tpl_vars['i']['id']; ?>
);">Make preview</a>
				<?php endif; ?>
				</p>
			</td>	
		</tr>
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		
         <tr>
            <td>Upload file:</td>
            <td>
            	<div id="filename_writersDiv">
            		<input type="text" value="" id="filename_writers" class="files">
            	</div>
            	<a href="#" id="filename_writersButton" class="select-btn"><?php echo ((is_array($_tmp='upload')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Upload') : gLA($_tmp, 'Upload')); ?>
</a>
            </td>
        </tr>
	</table>
</div>
<script type="text/javascript">
   
           
$(document).ready(function() {

	loadUploadSimple('orders/<?php echo md5($this->_tpl_vars['edit']['id']); ?>
/', 'filename_writers');

});
		
</script>