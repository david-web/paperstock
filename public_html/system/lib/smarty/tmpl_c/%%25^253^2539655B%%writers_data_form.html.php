<?php /* Smarty version 2.6.26, created on 2014-09-16 23:23:00
         compiled from writers_data_form.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', 'writers_data_form.html', 7, false),array('modifier', 'date_format', 'writers_data_form.html', 29, false),array('modifier', 'number_format', 'writers_data_form.html', 55, false),array('modifier', 'is_array', 'writers_data_form.html', 84, false),array('modifier', 'implode', 'writers_data_form.html', 85, false),)), $this); ?>
<div id="general_data_form" class="main-tab">
	<table class="inner-table">
		<tr>
			<td colspan="2"><b>Basic information</b></td>
		</tr>
        <tr>
            <td><?php echo ((is_array($_tmp='email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Email') : gLA($_tmp, 'Email')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['email']; ?>
</td>
        </tr>
		<tr>
            <td><?php echo ((is_array($_tmp='first_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'First name') : gLA($_tmp, 'First name')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['first_name']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='last_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Last name') : gLA($_tmp, 'Last name')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['last_name']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='profile_country')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Country') : gLA($_tmp, 'Country')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['country']; ?>
</td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Phone') : gLA($_tmp, 'Phone')); ?>
:</td>
            <td><?php echo $this->_tpl_vars['edit']['phone']; ?>
</td>
        </tr>
        
        <tr>
            <td>Registration date:</td>
            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp)); ?>
</td>
        </tr>
        
        <tr>
            <td>Night calls allowed value:</td>
            <td><?php echo $this->_tpl_vars['edit']['available_24_7']; ?>
</td>
        </tr>
        
        <tr>
            <td>Worked for  online academic assistance companies value:</td>
            <td><?php echo $this->_tpl_vars['edit']['have_worked']; ?>
</td>
        </tr>
        
        <tr>
			<td colspan="2"><b>Additional information</b></td>
		</tr>
		<tr>
            <td>Total applications:</td>
            <td><?php echo $this->_tpl_vars['edit']['total_applications']; ?>
</td>
        </tr>
        <tr>
            <td>Completed orders:</td>
            <td><?php echo $this->_tpl_vars['edit']['completed_orders']; ?>
</td>
        </tr>
        <tr>
            <td>Total earnings:</td>
            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['total_amount'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, '.', '') : number_format($_tmp, 2, '.', '')); ?>
$</td>
        </tr>
        <tr>
            <td>Total paid:</td>
            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['total_paid_amount'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, '.', '') : number_format($_tmp, 2, '.', '')); ?>
$</td>
        </tr>
        <tr>
            <td>Ratio:</td>
            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['edit']['rating'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, '.', '') : number_format($_tmp, 2, '.', '')); ?>
</td>
        </tr>
        <tr>
            <td>Status:</td>
            <td>
            	<?php if ($this->_tpl_vars['edit']['rating'] >= 4): ?>
				VIP
				<?php else: ?>
				Regular
				<?php endif; ?>
				 / 
				<?php echo $this->_tpl_vars['edit']['status']; ?>

            </td>
        </tr>
        <tr>
            <td>Academic degree:</td>
            <td><?php echo $this->_tpl_vars['edit']['academic_degree']; ?>
</td>
        </tr>
        <tr>
            <td>Citation styles:</td>
            <td>
            	<?php if (is_array($this->_tpl_vars['edit']['familiar_with'])): ?>
            	<?php echo implode(', ', $this->_tpl_vars['edit']['familiar_with']); ?>

            	<?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Native language:</td>
            <td><?php echo $this->_tpl_vars['edit']['native_language']; ?>
</td>
        </tr>
        <tr>
            <td>Disciplines of proficienty:</td>
            <td>
            	<?php if (is_array($this->_tpl_vars['edit']['disciplines'])): ?>
            	<?php echo implode(', ', $this->_tpl_vars['edit']['disciplines']); ?>

            	<?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>About writer:</td>
            <td><?php echo $this->_tpl_vars['edit']['about_me']; ?>
</td>
        </tr>
        <tr>
			<td colspan="2"><b>Test block</b></td>
		</tr>
		<tr>
            <td>Number of revisions:</td>
            <td>
            	<input type="text" class="simple" id="number_revisions" value="<?php echo $this->_tpl_vars['edit']['number_revisions']; ?>
" />
            </td>
        </tr>
        <tr>
            <td>Number of times plagiarism was detected:</td>
            <td>
            	<input type="text" class="simple" id="number_plagiarism" value="<?php echo $this->_tpl_vars['edit']['number_plagiarism']; ?>
" />
            </td>
        </tr>
        <tr>
            <td>Grammar test results:</td>
            <td>
            	<input type="text" class="simple" id="grammar_results" value="<?php echo $this->_tpl_vars['edit']['grammar_results']; ?>
" />
            </td>
        </tr>
        <tr>
            <td>APA test results:</td>
            <td>
            	<input type="text" class="simple" id="apa_results" value="<?php echo $this->_tpl_vars['edit']['apa_results']; ?>
" />
            </td>
        </tr>
        <tr>
            <td>MLA test results:</td>
            <td>
            	<input type="text" class="simple" id="mla_results" value="<?php echo $this->_tpl_vars['edit']['mla_results']; ?>
" />
            </td>
        </tr>
        <tr>
            <td>Assay Score:</td>
            <td>
            	<input type="text" class="simple" id="assay_score" value="<?php echo $this->_tpl_vars['edit']['assay_score']; ?>
" />
            </td>
        </tr>
        <tr>
            <td colspan="2"><b>Admin comment:</b></td>
        </tr>
        <tr>
            <td colspan="2">
            	<textarea class="simple" id="admin_comment" name="admin_comment" rows="5" style="width:500px"><?php echo $this->_tpl_vars['edit']['admin_comment']; ?>
</textarea>
			</td>
        </tr>
 	</table>
</div>