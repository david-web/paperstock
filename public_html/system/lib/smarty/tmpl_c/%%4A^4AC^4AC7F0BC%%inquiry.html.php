<?php /* Smarty version 2.6.26, created on 2014-07-27 12:25:51
         compiled from /home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/inquiry.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/inquiry.html', 59, false),array('modifier', 'getLM', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/inquiry.html', 381, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/orders/tmpl/inquiry.html', 711, false),)), $this); ?>
<section class="col1a">
	<div class="form_bg">
		<section class="order_form wrap_onlymob">
			
			<?php $_from = $this->_tpl_vars['order']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
			<?php if ($this->_tpl_vars['i']['type'] == 'tabselect'): ?>
			<div class="field clearfix">
				<div class="col1b">
					<div class="label_cont"><div class="label_center">
						<label class="has_tip">
							<?php echo $this->_tpl_vars['i']['name']; ?>

							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="tip css3">
								<?php echo $this->_tpl_vars['i']['hint_over']; ?>

							</div>
							<?php endif; ?>
						</label>
					</div></div>
				</div>
				<div class="col2b">
					<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
						<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
						<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
							<?php if ($this->_tpl_vars['v']['hint']): ?>
							<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
							<?php endif; ?>
							<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
 </div>
						</div>
						<?php endforeach; endif; unset($_from); ?>
					</div>
					<div class="horizontal_dropdown">
						<select class="selectpicker inquiry" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" >
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<option  value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>>
								<?php echo $this->_tpl_vars['v']['name']; ?>
 
							</option>
							<?php endforeach; endif; unset($_from); ?>
						</select>
					</div>
				</div>
			</div>
			<?php elseif ($this->_tpl_vars['i']['type'] == 'select'): ?>
			<div class="field clearfix">
				<div class="col1b">
					<div class="label_cont"><div class="label_center">
						<label class="has_tip">
							<?php echo $this->_tpl_vars['i']['name']; ?>

							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="tip css3">
								<?php echo $this->_tpl_vars['i']['hint_over']; ?>

							</div>
							<?php endif; ?>
						</label>
					</div></div>
				</div>
				<div class="col2b">
					<div class="select_cont">
						<select class="selectpicker inquiry" data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='order_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							
								<?php if ($this->_tpl_vars['v']['parent']): ?>
									
									<?php if ($this->_tpl_vars['optgroupOpened']): ?>
										</optgroup>	
									<?php endif; ?>
									
									<?php $this->assign('optgroupOpened', $this->_tpl_vars['v']['id']); ?>
									
									<optgroup label="<?php echo $this->_tpl_vars['v']['name']; ?>
">
								<?php else: ?>	
									<option <?php if ($this->_tpl_vars['optgroupOpened']): ?>data-groupid="<?php echo $this->_tpl_vars['optgroupOpened']; ?>
"<?php endif; ?> value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>	
								<?php endif; ?>
								
								<?php if (($this->_foreach['orderdatavalues']['iteration'] == $this->_foreach['orderdatavalues']['total']) && $this->_tpl_vars['optgroupOpened']): ?>
									</optgroup>	
								<?php endif; ?>	
							<?php endforeach; endif; unset($_from); ?>
						</select>
					</div>
					<?php if ($this->_tpl_vars['i']['hint_below']): ?>
					<div class="note">
						<p>
							<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
						</p>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
			<div class="field clearfix">
				<div class="col1b">
					<div class="label_cont"><div class="label_center">
						<label class="has_tip">
							<?php echo $this->_tpl_vars['i']['name']; ?>

							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="tip css3">
								<?php echo $this->_tpl_vars['i']['hint_over']; ?>

							</div>
							<?php endif; ?>
						</label>
					</div></div>
				</div>
				<div class="col2b">
					<div class="cinput css3">
						<input type="text" class="inquiry" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
					</div>
				</div>
			</div>
			<?php elseif ($this->_tpl_vars['i']['type'] == 'short_text'): ?>
			<div class="field clearfix">
				<div class="col1b">
					<div class="label_cont"><div class="label_center">
						<label class="has_tip">
							<?php echo $this->_tpl_vars['i']['name']; ?>

							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="tip css3">
								<?php echo $this->_tpl_vars['i']['hint_over']; ?>

							</div>
							<?php endif; ?>
						</label>
					</div></div>
				</div>
				<div class="col2b">
					<div class="cinput css3 input1">
						<input type="text" class="inquiry" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
					</div>
				</div>
			</div>
			<?php elseif ($this->_tpl_vars['i']['type'] == 'textarea'): ?>
			<div class="field clearfix">
				<div class="col1b">
					<div class="label_cont"><div class="label_center">
						<label class="has_tip">
							<?php echo $this->_tpl_vars['i']['name']; ?>

							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="tip css3">
								<?php echo $this->_tpl_vars['i']['hint_over']; ?>

							</div>
							<?php endif; ?>
						</label>
					</div></div>
				</div>
				<div class="col2b">
					<div class="cinput css3">
						<textarea name="<?php echo $this->_tpl_vars['i']['id']; ?>
" class="inquiry" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['selected']; ?>
</textarea>
					</div>
				</div>
			</div>
			<?php elseif ($this->_tpl_vars['i']['type'] == 'pages'): ?>
			<div class="field clearfix">
				<div class="col1b">
					<div class="label_cont"><div class="label_center">
						<label class="has_tip">
							<?php echo $this->_tpl_vars['i']['name']; ?>

							
							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="tip css3">
								<?php echo $this->_tpl_vars['i']['hint_over']; ?>

							</div>
							<?php endif; ?>
						</label>
					</div></div>
				</div>
				<div class="col2b clearfix">
					<div class="col1c">
						<div class="cinput css3">
							<input type="text" class="inquiry" name="number_of_pages" id="number_of_pages" value="<?php if ($_SESSION['order']['fields']['number_of_pages']): ?><?php echo $_SESSION['order']['fields']['number_of_pages']; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']['default_val']; ?>
<?php endif; ?>">
						</div>
					</div>
					
					<div class="col2c clearfix">
						<div class="col1d">
							<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
									<?php if ($this->_tpl_vars['v']['hint']): ?>
									<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
									<?php endif; ?>
									<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
								</div>
								<?php endforeach; endif; unset($_from); ?>
							</div>
							<div class="horizontal_dropdown">
								<select class="selectpicker inquiry" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-show=".show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>
							</div>
						</div>
						<div class="col2d">
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<?php if ($this->_tpl_vars['v']['hint']): ?>
							<div class="note2 hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_foreach['orderdatavalues']['iteration'] != 1): ?>style="display:none;"<?php endif; ?>>
								<?php echo $this->_tpl_vars['v']['hint']; ?>

							</div>
							<?php endif; ?>
							<?php endforeach; endif; unset($_from); ?>
						</div>
					</div>
					
				</div>
			</div>
			<?php elseif ($this->_tpl_vars['i']['type'] == 'hourdate'): ?>
			
			<div class="field clearfix">
				<div class="col1b">
					<div class="label_cont"><div class="label_center">
						<label class="has_tip">
							<?php echo $this->_tpl_vars['i']['name']; ?>

							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="tip css3">
								<?php echo $this->_tpl_vars['i']['hint_over']; ?>

							</div>
							<?php endif; ?>
						</label>
					</div></div>
				</div>
				<div class="col2b clearfix select_group" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
					<div class="col1c">
						<div class="special1 css3 hours"><?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
						<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<div class="option has_tip <?php if (32400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="32400">
								<div class="tip css3">9 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
								<div class="text">9</div>
							</div>
							<div class="option has_tip <?php if (86400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="86400">
								<div class="tip css3">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
								<div class="text">24</div>
							</div>
						</div>
					</div>
					<div class="col2c">
						<div class="special1 css3 days"><?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
						<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<div class="option has_tip  <?php if (172800 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="172800">
								<div class="tip css3">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
								<div class="text">2</div>
							</div>
							<div class="option has_tip <?php if (259200 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="259200">
								<div class="tip css3">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
								<div class="text">3</div>
							</div>
							<div class="option has_tip <?php if (518400 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="518400">
								<div class="tip css3">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
								<div class="text">6</div>
							</div>
							<div class="option has_tip <?php if (864000 == $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="864000">
								<div class="tip css3">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
								<div class="text">10</div>
							</div>
							<div class="option has_tip <?php if (1209600 == $this->_tpl_vars['i']['selected'] || ! $this->_tpl_vars['i']['selected']): ?>active<?php endif; ?>" data-value="1209600">
								<div class="tip css3">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
								<div class="text">14</div>
							</div>
						</div>
					</div>
					<div class="horizontal_dropdown">
						<select class="selectpicker inquiry" onchange="order.changeDeadlineNotice();" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<option value="32400" <?php if (32400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>9 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
							<option value="86400" <?php if (86400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
							<option value="172800" <?php if (172800 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
							<option value="259200" <?php if (259200 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
							<option value="518400" <?php if (518400 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
							<option value="864000" <?php if (864000 == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
							<option value="1209600" <?php if (1209600 == $this->_tpl_vars['i']['selected'] || ! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>>14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
						</select>
					</div>
					<div class="c"></div>
					<div class="note3">
		
						<div><?php echo ((is_array($_tmp='order_DeadLineNotice_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'The deadline for the first draft is') : gL($_tmp, 'The deadline for the first draft is')); ?>

							<div class="has_tip">
								<span id="deadline1"></span>
								<div class="tip css3">
									<?php echo ((is_array($_tmp='order_DeadLineNoticeHint_1')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint 1') : gL($_tmp, 'Hint 1')); ?>

								</div>
							</div>
						</div>
						<div>
							<?php echo ((is_array($_tmp='order_DeadLineNotice_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'We estimate that your final submission deadline is') : gL($_tmp, 'We estimate that your final submission deadline is')); ?>

							<div class="has_tip">
								<span id="deadline2"></span>
								<div class="tip css3">
									<?php echo ((is_array($_tmp='order_DeadLineNoticeHint_2')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint 2') : gL($_tmp, 'Hint 2')); ?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</section>
	</div>
	<div class="sp20-20-10"></div>

	<div class="ctabs">
		<div class="tab_top first css3" data-tab="1">
			<div class="text"><?php echo ((is_array($_tmp='order_LoginTabTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Login') : gL($_tmp, 'Login')); ?>
</div>
		</div>
		<div class="tab_top last css3" data-tab="2">
			<div class="text"><?php echo ((is_array($_tmp='order_RegisterTabTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Register') : gL($_tmp, 'Register')); ?>
</div>
		</div>
		<div class="c"></div>
		<div class="tab_bottom" data-tab="1">
			<section class="wrap_onlymob">
				<section class="order_form">
					<div class="explanation">
						<?php echo ((is_array($_tmp='order_LoginTabText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h2>Please authorize to complete your order!</h2>
						<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>') : gL($_tmp, '<h2>Please authorize to complete your order!</h2>
						<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>')); ?>

					</div>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip"><?php echo ((is_array($_tmp='order_LoginTab_Fields_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>

									<?php if (((is_array($_tmp='order_LoginTab_Fields_EmailHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint'))): ?>
									<div class="tip css3"><?php echo ((is_array($_tmp='order_LoginTab_Fields_EmailHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint')); ?>
</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix">
							<div class="col1e">
								<div class="cinput css3">
									<input type="text" class="login" name="email" id="email" value="" />
								</div>
							</div>
						</div>
					</div>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip"><?php echo ((is_array($_tmp='order_LoginTab_Fields_Password')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Password') : gL($_tmp, 'Password')); ?>

									<?php if (((is_array($_tmp='order_LoginTab_Fields_PasswordHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint'))): ?>
									<div class="tip css3"><?php echo ((is_array($_tmp='order_LoginTab_Fields_PasswordHint')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hint') : gL($_tmp, 'Hint')); ?>
</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix">
							<div class="col1e">
								<div class="cinput css3">
									<input type="password" class="login" name="password" id="password" value="" />
								</div>
							</div>
							<div class="col2e clearfix">
								<a href="#" class="forgot"><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Forgot your password?') : gL($_tmp, 'Forgot your password?')); ?>
</a>
							</div>
						</div>
					</div>
					<div class="forgot_pw_block" style="display: none;">
						<hr>
						<div class="explanation">
							<h2><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Forgot your password?') : gL($_tmp, 'Forgot your password?')); ?>
</h2>
						<p><?php echo ((is_array($_tmp='profile_ForgotPasswordTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.') : gL($_tmp, 'Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.')); ?>
</p>
						</div>
						<div class="info_submitted_msg css3" style="display:none;"><?php echo ((is_array($_tmp='profile_ForgotPasswordOkMsg')) ? $this->_run_mod_handler('gL', true, $_tmp, 'New password has been sent to your email! <a href="#">Sign in with a new password</a>') : gL($_tmp, 'New password has been sent to your email! <a href="#">Sign in with a new password</a>')); ?>
</div>
						<div class="field clearfix">
							<div class="col1b">
								<div class="label_cont"><div class="label_center">
									<label><?php echo ((is_array($_tmp='order_LoginTab_Fields_Email')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Email') : gL($_tmp, 'Email')); ?>
</label>
								</div></div>
							</div>
							<div class="col2b clearfix">
								<div class="col1e">
									<div class="cinput css3">
										<input type="text" id="password_reminder">
									</div>
								</div>
								<div class="col2e clearfix">
									<a href="javascript:;" onclick="profile.passwordReminder();" class="btn4 css3"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="summary free clearfix">
						<a href="javascript:;" onclick="order.loginInquiry();" class="btn3 css3" title="<?php echo ((is_array($_tmp='inquiry_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit inquiry') : gL($_tmp, 'Submit inquiry')); ?>
"><?php echo ((is_array($_tmp='inquiry_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit inquiry') : gL($_tmp, 'Submit inquiry')); ?>
</a>
						<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['mainpageId'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="cancel ver_320" title="<?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
"><?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
					</div>
				</section>
			</section>
		</div>
		<div class="tab_bottom" data-tab="2">
			<section class="wrap_onlymob">
				<section class="order_form">
					<div class="explanation">
						<?php echo ((is_array($_tmp='order_RegisterTabText')) ? $this->_run_mod_handler('gL', true, $_tmp, '<h2>Please authorize to complete your order!</h2>
						<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>') : gL($_tmp, '<h2>Please authorize to complete your order!</h2>
						<p>Sed semper lorem vel massa porttitor tincidunt. Maecenas pretium pulvinar tellus nec tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>')); ?>

					</div>
					<?php $_from = $this->_tpl_vars['order']['order']['profile']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
					<?php if ($this->_tpl_vars['i']['type'] == 'tabselect'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b">
							<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
									<?php if ($this->_tpl_vars['v']['hint']): ?>
									<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
									<?php endif; ?>
									<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
								</div>
								<?php endforeach; endif; unset($_from); ?>
							</div>
							<div class="horizontal_dropdown">
								<select class="selectpicker register" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>
							</div>
						</div>
					</div>
					<?php elseif ($this->_tpl_vars['i']['type'] == 'select'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b">
							<div class="select_cont">
								<select class="selectpicker register" data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='order_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>
							</div>
							<?php if ($this->_tpl_vars['i']['hint_below']): ?>
							<div class="note">
								<p>
									<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
								</p>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b">
							<div class="cinput css3">
								<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
							</div>
						</div>
					</div>
					<?php elseif ($this->_tpl_vars['i']['type'] == 'pages'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix">
							<div class="col1c">
								<div class="cinput css3">
									<input type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="page_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
								</div>
							</div>
							<div class="col2c clearfix">
								<div class="col1d">
									<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
										<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
											<?php if ($this->_tpl_vars['v']['hint']): ?>
											<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
											<?php endif; ?>
											<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
										</div>
										<?php endforeach; endif; unset($_from); ?>
									</div>
									<div class="horizontal_dropdown">
										<select class="selectpicker" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
											<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
											<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-show=".show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
											<?php endforeach; endif; unset($_from); ?>
										</select>
									</div>
								</div>
								<div class="col2d">
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<?php if ($this->_tpl_vars['v']['hint']): ?>
									<div class="note2 hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_foreach['orderdatavalues']['iteration'] != 1): ?>style="display:none;"<?php endif; ?>>
										<?php echo $this->_tpl_vars['v']['hint']; ?>

									</div>
									<?php endif; ?>
									<?php endforeach; endif; unset($_from); ?>
								</div>
							</div>
						</div>
					</div>
					<?php elseif ($this->_tpl_vars['i']['type'] == 'hourdate'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix select_group" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<div class="col1c">
								<div class="special1 css3 hours"><?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
								<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<div class="option has_tip" data-value="8h">
										<div class="tip css3">8 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
										<div class="text">8</div>
									</div>
									<div class="option has_tip" data-value="24h">
										<div class="tip css3">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
										<div class="text">24</div>
									</div>
								</div>
							</div>
							<div class="col2c">
								<div class="special1 css3 days"><?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
								<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<div class="option has_tip active" data-value="2d">
										<div class="tip css3">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
										<div class="text">2</div>
									</div>
									<div class="option has_tip" data-value="3d">
										<div class="tip css3">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
										<div class="text">3</div>
									</div>
									<div class="option has_tip" data-value="6d">
										<div class="tip css3">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
										<div class="text">6</div>
									</div>
									<div class="option has_tip" data-value="10d">
										<div class="tip css3">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
										<div class="text">10</div>
									</div>
									<div class="option has_tip" data-value="14d">
										<div class="tip css3">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
										<div class="text">14</div>
									</div>
								</div>
							</div>
							<div class="horizontal_dropdown">
								<select class="selectpicker" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<option value="8h">8 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
									<option value="24h">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
									<option value="2d" selected="selected">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
									<option value="3d">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
									<option value="6d">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
									<option value="10d">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
									<option value="14d">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								</select>
							</div>
							<div class="c"></div>
							<?php if ($this->_tpl_vars['i']['hint_over']): ?>
							<div class="note3">
								<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
							</div>
							<?php endif; ?>
						</div>
					</div>
					<?php elseif ($this->_tpl_vars['i']['type'] == 'extra'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b">
							<div class="checkbox_block css3 clearfix">
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<div class="option">
									<div class="checkbox"></div>
									<div class="text">
										<span class="has_tip"><?php echo $this->_tpl_vars['v']['name']; ?>

											<?php if ($this->_tpl_vars['v']['hint']): ?>
											<span class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</span>
											<?php endif; ?>
										</span>
									</div>
									<div class="price">$<?php echo $this->_tpl_vars['v']['price']; ?>
</div>
								</div>
								<?php endforeach; endif; unset($_from); ?>
							</div>
						</div>
					</div>
					<?php elseif ($this->_tpl_vars['i']['type'] == 'password'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix">
							<div class="col1e">
								<div class="cinput css3">
									<input class="register" type="password" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
								</div>
							</div>	
						</div>
					</div>
					<?php elseif ($this->_tpl_vars['i']['type'] == 'phone'): ?>
					<div class="field clearfix">
						<div class="col1b">
							<div class="label_cont"><div class="label_center">
								<label class="has_tip">
									<?php echo $this->_tpl_vars['i']['name']; ?>

									<?php if ($this->_tpl_vars['i']['hint_over']): ?>
									<div class="tip css3">
										<?php echo $this->_tpl_vars['i']['hint_over']; ?>

									</div>
									<?php endif; ?>
								</label>
							</div></div>
						</div>
						<div class="col2b clearfix">
							<div class="col1e clearfix">
								<div class="col1f">
									<div class="cinput_nochange css3"><?php echo ((is_array($_tmp='order_RegisterPhoneCode')) ? $this->_run_mod_handler('gL', true, $_tmp, '+371') : gL($_tmp, '+371')); ?>
</div>
								</div>
								<div class="col2f">
									<div class="cinput css3">
										<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?>
					<div class="field clearfix">
						<div class="col1g">
							<input type="checkbox" class="styled register" name="agreement" id="agreement" value="1" />
						</div>
						<div class="col2g clearfix">
							<label>
								<?php echo ((is_array($_tmp='order_RegisterAgreementText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>') : gL($_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>')); ?>

							</label>
						</div>
						<div class="invalid_msg"></div>
					</div>
					<hr>
					<div class="summary free clearfix">
						<a href="javascript:;" onclick="order.registerInquiry();" class="btn3 css3" title="<?php echo ((is_array($_tmp='inquiry_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit inquiry') : gL($_tmp, 'Submit inquiry')); ?>
"><?php echo ((is_array($_tmp='inquiry_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit inquiry') : gL($_tmp, 'Submit inquiry')); ?>
</a>
						<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['mainpageId'])) ? $this->_run_mod_handler('getLM', true, $_tmp) : getLinkByMirror($_tmp)); ?>
" class="cancel ver_320" title="<?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
"><?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
					</div>
				</section>
			</section>
		</div>
	</div>
</section>	
	
<section class="col2a">
	<section class="side_safety css3">
		<?php if (count($this->_tpl_vars['menu']['ORDERS_RIGHT']) > 0): ?>	
				<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['menu']['ORDERS_RIGHT']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
				<div><a href="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
"><?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
</a></div>	
				<?php endfor; endif; ?>
			<?php endif; ?>
	</section>
	<section class="small_stats clearfix">
		<img alt="<?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats.png">
		<div class="nr"><?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
</div>
		<div class="text"><?php echo ((is_array($_tmp='news_OurLiveStats_OrdersDelivered_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Orders<br>delivered') : gL($_tmp, 'Orders<br>delivered')); ?>
</div>
	</section>
</section>	
<script>
$(document).ready(function(){
	
	order.changeDeadlineNotice();
});
</script>	