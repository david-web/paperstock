<?php /* Smarty version 2.6.26, created on 2015-02-02 06:51:34
         compiled from /home/papersst/public_html/system/config/../../system/app/in/templates/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/templates/tmpl/edit.html', 9, false),)), $this); ?>
<td id="editForma" colspan="9" class="open">
	<div class="error-msg" id="errorBlock" style="display:none;"></div>
	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
	<table class="holder-table">
		<tr>
			<td>
				<table class="inner-table">
					<tr>
						<td colspan="2"><b><?php echo ((is_array($_tmp='general_data')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'General data') : gLA($_tmp, 'General data')); ?>
</b></td>
			        </tr>
			        <tr>
			            <td><?php echo ((is_array($_tmp='name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Name') : gLA($_tmp, 'Name')); ?>
:</td>
			            <td><input type="text" class="long" name="filename" id="filename" value="<?php echo $this->_tpl_vars['edit']['filename']; ?>
" /></td>
			        </tr>
    			</table>
    		</td>
    		<td>
    			<table class="inner-table">
			        <tr>
			            <td colspan="4"><b><?php echo ((is_array($_tmp='translations')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Translations') : gLA($_tmp, 'Translations')); ?>
</b></td>
			        </tr>
			        <tr>
                        <td>
                        	<div>
								<div class="block-holder open">
	                                <div class="inner-block">
	                                    <ul class="lang-tabs">
	                                    	<?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
	                                    	<li<?php if (($this->_foreach['langauges']['iteration'] <= 1)): ?> class="active" <?php endif; ?>><a href="#"><?php echo $this->_tpl_vars['lng']['label']; ?>
</a></li>
								            <?php endforeach; endif; unset($_from); ?>
	                                    </ul>
	                                   
	                                    <div class="areaBlock">
		                                    <?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages2'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages2']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages2']['iteration']++;
?>
		                                   	<div class="text-block<?php if (($this->_foreach['languages2']['iteration'] <= 1)): ?> open<?php endif; ?>">
		                                        <textarea name="value_<?php echo $this->_tpl_vars['lk']; ?>
" id="value_<?php echo $this->_tpl_vars['lk']; ?>
" cols="20" rows="5"><?php echo $this->_tpl_vars['edit']['translations'][$this->_tpl_vars['lk']]; ?>
</textarea>
		                                        <div class="clr"><!-- clear --></div>
		                                    </div>
								            <?php endforeach; endif; unset($_from); ?>
							            </div>             
	                                </div>
	                            </div>
                            </div>
                        </td>
                    </tr>	        
				</table>
			</td>
		</tr>
	</table>
    <table class="bttns-table">
        <tr>
            <td>
                <div class="btn"><a href="javascript:;" onclick="checkFields('save');"><span><?php echo ((is_array($_tmp='save_and_close')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save and close') : gLA($_tmp, 'Save and close')); ?>
</span></a></div>
                <div class="btn cancel"><a href="javascript:;" onclick="$('#edit_<?php echo $this->_tpl_vars['edit']['id']; ?>
').html('');"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
            </td>
        </tr>
    </table>
    </form>
    
    <script type="text/javascript">
    $(function () {
	    $('ul.lang-tabs a').click(function() {
			var curChildIndex = $(this).parent().prevAll().length + 1;
			$(this).parent().parent().children('.active').removeClass('active');
			$(this).parent().addClass('active');
			$(this).parent().parent().next('.areaBlock').children('.open').fadeOut('fast',function() {
				$(this).removeClass('open');
				$(this).parent().children('div:nth-child('+curChildIndex+')').fadeIn('normal',function() {
					$(this).addClass('open');
				});
			});
			return false;
		});
	    
		var tabindex = 1;
		$('#editForma :input,select').each(function() {
			if (this.type != "hidden" && this.id != "cmsLang") {
				var $input = $(this);
				$input.attr("tabindex", tabindex);
				tabindex++;
			}
		});
	 });		
	</script>
</td>