<?php /* Smarty version 2.6.26, created on 2014-07-22 13:39:09
         compiled from tests_form.html */ ?>
<div id="tests_form" class="main-tab">
	<table class="cms-table">
		<tr>
			<th><b>Test</b></th>
			<th><b>Filename</b></th>
			<th><b>Comment / Score</b></th>
			<th><b>Actions</b></th>
		</tr>
		<?php $_from = $this->_tpl_vars['edit']['tests']['tests']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['tests'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['tests']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['tests']['iteration']++;
?>
		<tr>
		<?php $this->assign('ID', $this->_tpl_vars['i']['id']); ?>
			<td><?php echo $this->_tpl_vars['i']['title']; ?>
</td>
			<td>
				<?php if ($this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['filename']): ?>
				<a href="/download.php?file=tests/results/<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['filename']; ?>
" class="link"><?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['filename']; ?>
</a>
				<?php endif; ?>
			</td>
			<td>
				<?php if ($this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['id']): ?>
					<?php if ($this->_tpl_vars['i']['type'] == 2): ?>
					<input type="text" rel="<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['id']; ?>
" id="test<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['id']; ?>
" name="admin_comment" class="tests" value="<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['admin_comment']; ?>
" />
					<?php else: ?>
					<input type="text" rel="<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['id']; ?>
" id="test<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['id']; ?>
" name="score" class="tests" value="<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['score']; ?>
" />
					<?php endif; ?>
				<?php endif; ?>
			</td>
			<td>
				<?php if ($this->_tpl_vars['i']['type'] == 2): ?>
				<a href="javascript:;" onclick="publishManualy(<?php echo $this->_tpl_vars['edit']['tests']['userTests'][$this->_tpl_vars['ID']]['id']; ?>
);">Publish manualy</a>
				<?php endif; ?>
			</td>
		<tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
	<a href="javascript:;" onclick="setStatus(2);">Accept</a> / <a href="javascript:;" onclick="setStatus(3);">Denied</a>	
</div>