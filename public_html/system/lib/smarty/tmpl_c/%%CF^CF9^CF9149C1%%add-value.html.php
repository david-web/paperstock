<?php /* Smarty version 2.6.26, created on 2014-08-18 14:58:19
         compiled from /home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/add-value.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/add-value.html', 8, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/add-value.html', 23, false),array('modifier', 'is_array', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/add-value.html', 78, false),array('modifier', 'in_array', '/home/papersst/public_html/system/config/../../system/app/in/forms/tmpl/add-value.html', 78, false),)), $this); ?>
<tr class="a block-tr add-value" id="add-value-<?php echo $this->_tpl_vars['id']; ?>
">
    <td>
    	<input type="hidden" name="id" class="values" value="<?php echo $this->_tpl_vars['value']['id']; ?>
" />
        <table class="inner-table">
            <tr>
                
                <td>
					<?php echo ((is_array($_tmp='m_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Name') : gLA($_tmp, 'Name')); ?>
:<br />
					<div class="block-holder open">
	                	<div class="inner-block">
						
	                        <ul class="lang-tabs">
	                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
	                            <li <?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> class="active"<?php endif; ?> >
	                                <a href="javascript: void(0);"><?php echo $this->_tpl_vars['lng']['title']; ?>
</a>
	                            </li>
	                            <?php endforeach; endif; unset($_from); ?>
	                        </ul>
	
	                        <div class="areaBlock">
	                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
	                                <div class="text-block<?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> open<?php endif; ?>">
	                                    <input name="name" rel="<?php echo $this->_tpl_vars['lng']['lang']; ?>
" id="name_<?php echo $this->_tpl_vars['lng']['lang']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['value']['data'][$this->_tpl_vars['lng']['lang']]['name'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" class="values" type="text"  style="width:200px;" />
	                                    
	                                    <div class="clr"><!-- clear --></div>
	                                </div>
	                            <?php endforeach; endif; unset($_from); ?>
	                        </div>
	
	                    </div>
	                </div>
	
				</td>
				
				<td>
					<?php echo ((is_array($_tmp='m_hint_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Hint') : gLA($_tmp, 'Hint')); ?>
:<br />
					<div class="block-holder open">
	                	<div class="inner-block">
						
	                        <ul class="lang-tabs">
	                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
	                            <li <?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> class="active"<?php endif; ?> >
	                                <a href="javascript: void(0);"><?php echo $this->_tpl_vars['lng']['title']; ?>
</a>
	                            </li>
	                            <?php endforeach; endif; unset($_from); ?>
	                        </ul>
	
	                        <div class="areaBlock">
	                            <?php $_from = $this->_tpl_vars['langs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['languages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['languages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['lk'] => $this->_tpl_vars['lng']):
        $this->_foreach['languages']['iteration']++;
?>
	                                <div class="text-block<?php if (($this->_foreach['languages']['iteration'] <= 1)): ?> open<?php endif; ?>">
	                                    <input name="hint" rel="<?php echo $this->_tpl_vars['lng']['lang']; ?>
" id="hint_<?php echo $this->_tpl_vars['lng']['lang']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['value']['data'][$this->_tpl_vars['lng']['lang']]['hint'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" class="values" type="text"  style="width:200px;" />
	                                    
	                                    <div class="clr"><!-- clear --></div>
	                                </div>
	                            <?php endforeach; endif; unset($_from); ?>
	                        </div>
	
	                    </div>
	                </div>
	
				</td>
				
				<td>
					<?php echo ((is_array($_tmp='parent')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Parent') : gLA($_tmp, 'Parent')); ?>
: <br /> 
					<input type="checkbox" class="values" name="parent" id="values_parent" value="1" <?php if ($this->_tpl_vars['value']['parent']): ?> checked="checked" <?php endif; ?> />
				</td>
				
				<td>
					<?php echo ((is_array($_tmp='price')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Price') : gLA($_tmp, 'Price')); ?>
: <br /> 
					<input type="text" class="values" name="price" id="values_price" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['value']['price'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
" />
				</td>
				
				<td>
					<?php echo ((is_array($_tmp='show_fields')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Show fields') : gLA($_tmp, 'Show fields')); ?>
: <br /> 
					<select name="show_fields" id="show_fields" class="long values" multiple="multiple"  size="5">
                        <option value="0"><?php echo ((is_array($_tmp='m_select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
                        <?php $_from = $this->_tpl_vars['config']['parents']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['select'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['select']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
        $this->_foreach['select']['iteration']++;
?>
	                		<option <?php if (((is_array($_tmp=$this->_tpl_vars['value']['show_fields'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp)) && ((is_array($_tmp=$this->_tpl_vars['item']['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['value']['show_fields']) : in_array($_tmp, $this->_tpl_vars['value']['show_fields']))): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
	                	<?php endforeach; endif; unset($_from); ?> 
                    </select>
				</td>

                <td>
                    <a href="javascript: void(0);" onclick="$(this).parents('.add-value').remove();">
                       <?php echo ((is_array($_tmp='m_delete')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Delete') : gLA($_tmp, 'Delete')); ?>

                    </a>
                </td>
            </tr>
 
        </table>

        <script type="text/javascript">
            $(function() {

                loadCmsTabs();
            });
        </script>
    </td>
</tr>