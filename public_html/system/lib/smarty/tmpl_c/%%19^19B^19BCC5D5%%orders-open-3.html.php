<?php /* Smarty version 2.6.26, created on 2015-01-11 18:36:57
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 3, false),array('modifier', 'date_format', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 14, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 32, false),array('modifier', 'implode', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 54, false),array('modifier', 'ceil', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 66, false),array('modifier', 'pathinfo', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 114, false),array('modifier', 'md5', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 121, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 132, false),array('function', 'math', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/8/orders-open-3.html', 33, false),)), $this); ?>
<section class="green_title order sp30-26-20">
	<section class="wrap">
		<h1><?php echo ((is_array($_tmp='order')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Order') : gL($_tmp, 'Order')); ?>
 <?php echo $this->_tpl_vars['profile']['data']['id']; ?>
</h1>
	</section>
</section>
<section class="wrap mob_nowrap clearfix">
	<div class="col1k">
		<div class="order_status wrap_onlymob css3 clearfix">
			<div class="time_header ver_1024 ver_320"><?php echo ((is_array($_tmp='order_TimeLeft')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Time left') : gL($_tmp, 'Time left')); ?>
</div>
			<div class="time_block">
				<div class="days"><div class="nr"><?php echo $this->_tpl_vars['profile']['data']['diffDays']; ?>
</div><div class="text"><?php echo ((is_array($_tmp='order_Days')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div></div>
				<div class="hours"><div class="nr"><?php echo $this->_tpl_vars['profile']['data']['diffHours']; ?>
</div><div class="text"><?php echo ((is_array($_tmp='order_Hours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div></div>
				<div class="clearfix"></div>
				<div class="deadline"><?php echo ((is_array($_tmp='order_Deadline')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Deadline') : gL($_tmp, 'Deadline')); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['data']['first_draft_deadline'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>
</div>
			</div>
			<div class="status notpaid clearfix">
				<div class="status_cont">
					<a href="javascript:;" class="btn_status css3 blue"><?php echo ((is_array($_tmp='order_Status_WriterAppointedYes')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Writer appointed: YES') : gL($_tmp, 'Writer appointed: YES')); ?>
</a>
					<a href="#" class="btn_pay notpaid css3"><?php echo ((is_array($_tmp='order_NotPaid')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Not paid') : gL($_tmp, 'Not paid')); ?>
</a>
				</div>
				<div class="price_cont">
					<div class="price"><?php echo ((is_array($_tmp='order_YouPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'You price') : gL($_tmp, 'You price')); ?>
 <?php echo $this->_tpl_vars['profile']['data']['writers_apply']['amount']; ?>
</div>
					<div class="price2"><?php echo ((is_array($_tmp='order_TotalPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Total price') : gL($_tmp, 'Total price')); ?>
 <?php echo $this->_tpl_vars['profile']['data']['price']; ?>
</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col2k">
		<div class="order_info wrap_onlymob css3">
			<div class="inner">
				<div class="twocols clearfix">
					<?php if (count($this->_tpl_vars['profile']['order']['3step']) > 0): ?>
						<?php echo smarty_function_math(array('assign' => 'coldiv2','equation' => "x / y",'x' => count($this->_tpl_vars['profile']['order']['3step']),'y' => 2), $this);?>

						<?php $_from = $this->_tpl_vars['profile']['order']['3step']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdata'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdata']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orderdata']['iteration']++;
?>
							
							<?php $this->assign('ID', $this->_tpl_vars['i']['db_field']); ?>
							
							<?php if ($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']] && $this->_tpl_vars['ID'] != 'preferred_writer'): ?>
							
								<?php if (($this->_foreach['orderdata']['iteration'] <= 1)): ?>
								<div class="col first">		
								<?php endif; ?>
								
								<div class="line clearfix">
									<div class="col1z"><?php echo $this->_tpl_vars['i']['name']; ?>
:</div>
									<div class="col2z">
									<?php if ($this->_tpl_vars['ID'] == 'first_draft_deadline'): ?>
									<?php $this->assign('TIMETOSHOW', ($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']])); ?>
									<?php echo ((is_array($_tmp=$this->_tpl_vars['TIMETOSHOW'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>

									<?php elseif ($this->_tpl_vars['ID'] == 'pages_type'): ?>
									<?php echo $this->_tpl_vars['profile']['data']['number_of_pages']; ?>
, <?php echo $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]; ?>

									<?php elseif ($this->_tpl_vars['ID'] == 'additional_extras'): ?>
										<?php if (count($this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]) > 0): ?>
											<?php echo implode(",", $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]); ?>

										<?php endif; ?>
									<?php else: ?>
									<?php echo $this->_tpl_vars['profile']['data'][$this->_tpl_vars['ID']]; ?>

									<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
							
							
							<?php if (($this->_foreach['orderdata']['iteration'] == $this->_foreach['orderdata']['total'])): ?>
								</div>	
							<?php elseif ($this->_foreach['orderdata']['iteration'] == ceil($this->_tpl_vars['coldiv2'])): ?>
								</div>
								<div class="col">
							<?php endif; ?>
							
						<?php endforeach; endif; unset($_from); ?>
					<?php endif; ?>
				</div>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_TopicInFull')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Topic in full') : gL($_tmp, 'Topic in full')); ?>
:</h2>
					<p>
						<?php echo $this->_tpl_vars['profile']['data']['topic']; ?>

					</p>
				</div>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_Details')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Details') : gL($_tmp, 'Details')); ?>
:</h2>
					<p>
						<?php echo $this->_tpl_vars['profile']['data']['work_details']; ?>

					</p>
				</div>
				<?php if (count($this->_tpl_vars['profile']['data']['additional_extras']) > 0): ?>
				<hr>
				<div class="block">
					<h2><?php echo ((is_array($_tmp='order_Extras')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Extras') : gL($_tmp, 'Extras')); ?>
:</h2>
					<p class="bolded">
						<?php echo implode(",", $this->_tpl_vars['profile']['data']['additional_extras']); ?>

					</p>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="order_collapsible open css3">
			<div class="trigger_line"><div class="inner wrap_onlymob"><?php echo ((is_array($_tmp='order_Files')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Files') : gL($_tmp, 'Files')); ?>
</div></div>
			<div class="collapsible"><div class="inner wrap_onlymob">
				<div class="files">
					<a href="javascript:;"  onclick="$('#fileForm').show();$('#newFile').hide();" id="newFile" class="btn1 m10bo"><?php echo ((is_array($_tmp='order_AddNewFile')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Add new file') : gL($_tmp, 'Add new file')); ?>
</a>
					<div id="fileForm" style="display:none;">
						
						<a href="javascript:;" id="uploadButton" class="btn1 m10bo"><?php echo ((is_array($_tmp='order_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
</a>
						<input type="text" value="" id="uploadButtonFile">
						<br /><br />
						
						<a href="javascript:;" onclick="profile.uploadFile();" class="btn1 m10bo"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
						<a href="javascript:;" onclick="$('#uploadButtonFile').val('');$('#fileForm').hide();$('#newFile').show();"><?php echo ((is_array($_tmp='order_Cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
					</div>
					<?php $_from = $this->_tpl_vars['profile']['data']['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
					<?php $this->assign('pathinfo', pathinfo($this->_tpl_vars['i']['filename'])); ?>
					<div class="file clearfix css3">
						<div class="col1z">
							<div class="ico <?php if ($this->_tpl_vars['pathinfo']['extension'] == 'jpg' || $this->_tpl_vars['pathinfo']['extension'] == 'gif' || $this->_tpl_vars['pathinfo']['extension'] == 'png' || $this->_tpl_vars['pathinfo']['extension'] == 'jpeg'): ?>jpg<?php else: ?>docx<?php endif; ?>"></div>
							<div class="type"><?php echo $this->_tpl_vars['pathinfo']['extension']; ?>
</div>
						</div>
						<div class="col2z">
							<div class="link"><a href="/download.php?file=orders/<?php echo md5($this->_tpl_vars['profile']['data']['id']); ?>
/<?php echo $this->_tpl_vars['i']['filename']; ?>
"><?php echo $this->_tpl_vars['i']['filename']; ?>
</a></div>
							<div class="author"><?php if ($this->_tpl_vars['i']['user_id']): ?><?php echo ((is_array($_tmp='order_MessagesYou')) ? $this->_run_mod_handler('gL', true, $_tmp, 'You') : gL($_tmp, 'You')); ?>
<?php else: ?><?php echo ((is_array($_tmp='order_MessagesAdministrator')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Administrator') : gL($_tmp, 'Administrator')); ?>
<?php endif; ?>, <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>
</div>
						</div>
					</div>
					<?php endforeach; endif; unset($_from); ?>
				</div>
			</div></div>
		</div>
		<div class="order_collapsible open css3">
			<div class="title_line"><div class="inner wrap_onlymob"><?php echo ((is_array($_tmp='order_ReasonForApply')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Reason for applying') : gL($_tmp, 'Reason for applying')); ?>
</div></div>
			<div class="collapsible"><div class="inner wrap_onlymob">
				<p><?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['data']['writers_apply']['reason'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</p>
				<p class="price"><?php echo ((is_array($_tmp='order_WritersPromosedPrice')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Your promosed price for this order:') : gL($_tmp, 'Your promosed price for this order:')); ?>
 $<?php echo $this->_tpl_vars['profile']['data']['writers_apply']['amount']; ?>
</p>
			</div></div>
		</div>
		<div class="order_collapsible open css3">
			<div class="trigger_line"><div class="inner wrap_onlymob"><?php echo ((is_array($_tmp='order_Messages')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Messages') : gL($_tmp, 'Messages')); ?>
</div></div>
			<div class="collapsible"><div class="inner wrap_onlymob">
				<div class="messages">
					<a href="javascript:;"  onclick="$('#msgBlock').show();$('#newMsg').hide();" id="newMsg" class="btn1 m10bo"><?php echo ((is_array($_tmp='order_NewMessage')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Create new message') : gL($_tmp, 'Create new message')); ?>
</a>
					<div class="new_msg clearfix" id="msgBlock" style="display:none;">
						<div class="cinput default"><textarea id="message" data-default=""></textarea></div>
						<a href="javascript:;" onclick="profile.orderNewMessage();" class="btn1"><?php echo ((is_array($_tmp='order_Submit')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Submit') : gL($_tmp, 'Submit')); ?>
</a>
						<a href="javascript:;" onclick="$('#msgBlock').hide();$('#newMsg').show();" class="btn5"><?php echo ((is_array($_tmp='order_Cancel')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Cancel') : gL($_tmp, 'Cancel')); ?>
</a>
					</div>
					<?php $_from = $this->_tpl_vars['profile']['data']['messages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['orders']['iteration']++;
?>
					<div class="msg css3<?php if ($this->_tpl_vars['i']['user_id']): ?> self<?php endif; ?>"><div class="inner">
						<?php echo $this->_tpl_vars['i']['message']; ?>

						<div class="author"><?php if ($this->_tpl_vars['i']['user_id']): ?><?php echo ((is_array($_tmp='order_MessagesYou')) ? $this->_run_mod_handler('gL', true, $_tmp, 'You') : gL($_tmp, 'You')); ?>
<?php else: ?><?php echo ((is_array($_tmp='order_MessagesAdministrator')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Administrator') : gL($_tmp, 'Administrator')); ?>
<?php endif; ?>, <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d %Y, %l:%M %p %Z") : smarty_modifier_date_format($_tmp, "%b %d %Y, %l:%M %p %Z")); ?>
</div>
					</div></div>
					<?php endforeach; endif; unset($_from); ?>
				</div>
			</div></div>
		</div>
		<div class="backlink_cont wrap_onlymob">
			<a href="javascript:;" onclick="history.go(-1);" class="backlink"><?php echo ((is_array($_tmp='order_Back')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Back to order list') : gL($_tmp, 'Back to order list')); ?>
</a>
		</div>
	</div>
</section>
<script type="text/javascript">
       
$(document).ready(function() {

	loadUploadButton('orders/temp/', 'uploadButton');

});

</script>