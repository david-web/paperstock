<?php /* Smarty version 2.6.26, created on 2014-07-24 15:35:05
         compiled from /home/papersst/public_html/system/config/../../system/app/in/bonuscodes/tmpl/edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/bonuscodes/tmpl/edit.html', 6, false),array('modifier', 'cat', '/home/papersst/public_html/system/config/../../system/app/in/bonuscodes/tmpl/edit.html', 19, false),array('modifier', 'clear', '/home/papersst/public_html/system/config/../../system/app/in/bonuscodes/tmpl/edit.html', 33, false),array('modifier', 'hsc', '/home/papersst/public_html/system/config/../../system/app/in/bonuscodes/tmpl/edit.html', 33, false),)), $this); ?>
<div class="content">
    <input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['edit']['id']; ?>
" />
    <table class="search-table" style="margin-bottom: 4px;">
        <tr>
            <td>
                <strong><?php echo ((is_array($_tmp='m_bonus_codes_edit')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Bonus codes edit') : gLA($_tmp, 'Bonus codes edit')); ?>
</strong>
            </td>
        </tr>
        <tr class="search-table ok-msg hidden" id="okBlock">
            <td><?php echo ((is_array($_tmp='m_data_saved')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Data saved!') : gLA($_tmp, 'Data saved!')); ?>
</td>
        </tr>
    </table>
    <div class="error-msg hidden" id="errorBlock"></div>
    <table class="holder-table">
        <?php if ($this->_tpl_vars['edit']['status']): ?>
        <tr>
            <td style="width: 250px;"><?php echo ((is_array($_tmp='status')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Status') : gLA($_tmp, 'Status')); ?>
:</td>
            <td id="status_title">
                <?php $this->assign('bonus_status', ((is_array($_tmp='bonus_')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['edit']['status']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['edit']['status']))); ?>
                <?php echo ((is_array($_tmp=$this->_tpl_vars['bonus_status'])) ? $this->_run_mod_handler('gLA', true, $_tmp) : gLA($_tmp)); ?>

            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td style="width: 250px;"><?php echo ((is_array($_tmp='enable')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Enable') : gLA($_tmp, 'Enable')); ?>
:</td>
            <td>
                <input type="checkbox" value="1" id="enable" class="active simple" <?php if ($this->_tpl_vars['edit']['enable']): ?> checked="checked" <?php endif; ?> <?php if (! empty ( $this->_tpl_vars['edit']['disable_checkbox'] )): ?>disabled = "disabled"<?php endif; ?> />
            </td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='m_title')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Title') : gLA($_tmp, 'Title')); ?>
*:</td>
            <td>
                <input type="text" class="simple long required" name="title" id="title" value="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['edit']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)))) ? $this->_run_mod_handler('hsc', true, $_tmp) : hsc($_tmp)); ?>
" maxlength="255" />
            </td>
        </tr>
        <tr id="codesButton">
            <td><?php echo ((is_array($_tmp='code')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Code') : gLA($_tmp, 'Code')); ?>
*:</td>
            <td>
                <input type="text" class="simple long required" name="code" id="code" value="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['edit']['code'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)))) ? $this->_run_mod_handler('hsc', true, $_tmp) : hsc($_tmp)); ?>
" />
            </td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='valid_from')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Valid from') : gLA($_tmp, 'Valid from')); ?>
*:</td>
            <td>
                <input type="text" class="long simple required" name="valid_from" id="valid_from" value="<?php echo $this->_tpl_vars['edit']['valid_from']; ?>
" maxlength="10" style="float: left; width: 80px;" />
                <a onclick="$('#valid_from').focus();" href="javascript:void(0)" class="calendar">&nbsp;</a>
            </td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='valid_to')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Valid to') : gLA($_tmp, 'Valid to')); ?>
*:</td>
            <td>
                <input type="text" class="long simple required" name="valid_to" id="valid_to" value="<?php echo $this->_tpl_vars['edit']['valid_to']; ?>
" maxlength="10" style="float: left; width: 80px;" />
                <a onclick="$('#valid_to').focus();" href="javascript:void(0)" class="calendar">&nbsp;</a>
            </td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='value')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Value') : gLA($_tmp, 'Value')); ?>
*:</td>
            <td>
                <input type="text" class="simple long decimal required" name="discount_value" id="discount_value" value="<?php echo $this->_tpl_vars['edit']['discount_value']; ?>
" style="float: left; width: 80px;" /><span class="percent_end"> %</span>
            </td>
        </tr>
        <tr>
            <td><?php echo ((is_array($_tmp='max_uses')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Max uses') : gLA($_tmp, 'Max uses')); ?>
*:</td>
            <td>
                <input type="text" class="simple long numeric required" name="max_uses" id="max_uses" value="<?php if ($this->_tpl_vars['edit']['max_uses']): ?><?php echo $this->_tpl_vars['edit']['max_uses']; ?>
<?php else: ?>1<?php endif; ?>" maxlength="11" />
            </td>
        </tr>
    </table>
    <table class="bttns-table">
        <tr>
            <td>
                <?php if ($this->_tpl_vars['edit']['id']): ?>
                    <div class="btn orange">
                        <a href="javascript: void(0);" onclick="checkFields('apply'); return false;">
                            <span><?php echo ((is_array($_tmp='apply')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Apply') : gLA($_tmp, 'Apply')); ?>
</span>
                        </a>
                    </div>
                <?php endif; ?>
                <div class="btn">
                    <a href="javascript: void(0);" onclick="checkFields('save'); return false;">
                        <span><?php echo ((is_array($_tmp='save')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Save') : gLA($_tmp, 'Save')); ?>
</span>
                    </a>
                </div>
                <div class="btn cancel">
                    <a href="javascript: void(0);" onclick="window.location.href = moduleTable.getRequestUrl();">
                        <span><?php echo ((is_array($_tmp='cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span>
                    </a>
                </div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(function() {
        moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
        moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
        
        $id = parseInt('<?php echo $this->_tpl_vars['edit']['id']; ?>
');
        
    });
    
    $(document).ready(function(){
        
        var min = $('#valid_from').datepicker('getDate') || new Date();
        $('#valid_from').datepicker({
            dateFormat  : 'dd.mm.yy',
            minDate     : 0,
        });
        
        $('#valid_to').datepicker({
            dateFormat  : 'dd.mm.yy',
            minDate     : $('#valid_from').datepicker('getDate') || new Date(),
        });
    });
    
</script>