<?php /* Smarty version 2.6.26, created on 2014-07-16 02:06:03
         compiled from /home/papersst/public_html/system/config/../../system/app/in/payments/tmpl/payments.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gLA', '/home/papersst/public_html/system/config/../../system/app/in/payments/tmpl/payments.html', 7, false),)), $this); ?>
<div class="content">
	<form class="forma" method="post" action="" onsubmit="return false;">
		<fieldset>
			<table class="search-table" cellpadding="0" cellspacing="0">
			    <tr>
			        <td>
			            <label for="paid"><?php echo ((is_array($_tmp='select_paid')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Paid') : gLA($_tmp, 'Paid')); ?>
:</label>
			            <select class="filter" id="paid" name="paid">
							<option value=""><?php echo ((is_array($_tmp='select_option')) ? $this->_run_mod_handler('gLA', true, $_tmp, '-- Select --') : gLA($_tmp, '-- Select --')); ?>
</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
			        </td>
			        <td>
			            <label for="id"><?php echo ((is_array($_tmp='id')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Order ID') : gLA($_tmp, 'Order ID')); ?>
:</label>
			            <input class="filter" type="text" id="id" name="id" />
			        </td>
			        <td>
			            <label for="filterName"><?php echo ((is_array($_tmp='name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client Name') : gLA($_tmp, 'Client Name')); ?>
:</label>
			            <input class="filter" type="text" id="filterName" name="filterName" />
			        </td>
			        <td>
			            <label for="filterLastName"><?php echo ((is_array($_tmp='last_name')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client last Name') : gLA($_tmp, 'Client last Name')); ?>
:</label>
			            <input class="filter" type="text" id="filterLastName" name="filterLastName" />
			        </td>
			        <td>
			            <label for="filterEmail"><?php echo ((is_array($_tmp='email')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client email') : gLA($_tmp, 'Client email')); ?>
:</label>
			            <input class="filter" type="text" id="filterEmail" name="filterEmail" />
			        </td>
			        <td>
			            <label for="filterPhone"><?php echo ((is_array($_tmp='phone')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Client phone') : gLA($_tmp, 'Client phone')); ?>
:</label>
			            <input class="filter" type="text" id="filterPhone" name="filterPhone" />
			        </td>
				</tr>
				<tr>
					<td>
			            <label for="created_from"><?php echo ((is_array($_tmp='Payment_date_from')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Payment date from') : gLA($_tmp, 'Payment date from')); ?>
:</label>
			            <input class="filter" type="text" id="created_from" name="created_from" />
			        </td> 
			        <td>
			            <label for="created_to"><?php echo ((is_array($_tmp='Payment_date_to')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Payment date from') : gLA($_tmp, 'Payment date from')); ?>
:</label>
			            <input class="filter" type="text" id="created_to" name="created_to" />
			        </td>
			        <td>
			            <label for="price_from"><?php echo ((is_array($_tmp='Payment_amount_from')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Payment amount') : gLA($_tmp, 'Payment amount')); ?>
:</label>
			            <input class="filter" type="text" id="price_from" name="price_from" />
			        </td>  
			        <td>
			            <label for="price_to"><?php echo ((is_array($_tmp='Payment_amount_to')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Payment amount') : gLA($_tmp, 'Payment amount')); ?>
:</label>
			            <input class="filter" type="text" id="price_to" name="price_to" />
			        </td> 
			        <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="changeFilter(); return false;"><span><?php echo ((is_array($_tmp='m_search')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Search') : gLA($_tmp, 'Search')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
	               <td class="bttns">
	                   <div class="btn"><a href="javascript:;" onclick="clearFilter(); return false;"><span><?php echo ((is_array($_tmp='m_cancel')) ? $this->_run_mod_handler('gLA', true, $_tmp, 'Cancel') : gLA($_tmp, 'Cancel')); ?>
</span></a></div>
	                   <div class="clr"><!-- clear --></div>
	               </td>
			    </tr>
			</table>
			
			<?php echo $this->_tpl_vars['tTable']; ?>

			
			<div id="modulePath">
			</div>
			
			<?php echo $this->_tpl_vars['bTable']; ?>

	
		</fieldset>
	</form>
	
	<script type="text/javascript">
		moduleTable.mainUrl = '<?php echo $this->_tpl_vars['MAIN_URL']; ?>
';
		moduleTable.moduleName = '<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
';
		moduleTable.usePaging = true;
		<?php if ($this->_tpl_vars['MODULE_FROM']): ?>
	    moduleTable.from = <?php echo $this->_tpl_vars['MODULE_FROM']; ?>
;
	    <?php endif; ?>

		$(document).ready(function() {
			
			updateModule();	
			
			$("#created_from").datepicker();
			$("#created_to").datepicker();
			
		});
	</script>
</div>	