<?php /* Smarty version 2.6.26, created on 2014-08-28 17:18:11
         compiled from /home/papersst/public_html/system/config/../../system/app/out/faq/tmpl/list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/faq/tmpl/list.html', 2, false),)), $this); ?>
 <div class="col1j">
 	<?php if (count($this->_tpl_vars['list']) > 0): ?>
	<ul class="faq">
		<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['item'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['item']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['data']):
        $this->_foreach['item']['iteration']++;
?>
		<li <?php if (($this->_foreach['item']['iteration'] <= 1)): ?>class="open"<?php endif; ?>>
			<div class="trigger">
				<?php echo $this->_tpl_vars['data']['question']; ?>

			</div>
			<div class="expand">
				<p>
					<?php echo $this->_tpl_vars['data']['answer']; ?>
	
				</p>
			</div>
		</li>
		<?php endforeach; endif; unset($_from); ?>
	</ul>	
	<?php endif; ?>	
	<div class="aftercontent no_links">
		<div class="backsocial clearfix">
			<a href="#" class="back css3"></a>
			<div class="social css3">
				<div class="g-plus" data-action="share" data-annotation="bubble"></div>
			</div>
		</div>
	</div>
</div>