<?php /* Smarty version 2.6.26, created on 2015-07-14 18:05:03
         compiled from X:%5Chome%5Cpaperstock.catv%5Cpublic_html%5Csystem%5Cconfig%5C..%5C../system/app/out/news/tmpl/item.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/news/tmpl/item.html', 3, false),array('modifier', 'clear', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/news/tmpl/item.html', 7, false),array('modifier', 'count', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/news/tmpl/item.html', 13, false),array('modifier', 'gL', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/news/tmpl/item.html', 15, false),array('modifier', 'getLink', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/news/tmpl/item.html', 19, false),array('modifier', 'getDocUrl', 'X:\\home\\paperstock.catv\\public_html\\system\\config\\..\\../system/app/out/news/tmpl/item.html', 19, false),)), $this); ?>
<div class="col1j opendoc">
	<?php if ($this->_tpl_vars['news']['date']): ?>
	<div class="date"><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['datem'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d %b %Y") : smarty_modifier_date_format($_tmp, "%d %b %Y")); ?>
</div>
	<?php endif; ?>
	<div class="userinput">
		<?php if ($this->_tpl_vars['news']['text_image']): ?>
		<img alt="<?php if ($this->_tpl_vars['news']['text_image_alt']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['text_image_alt'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
<?php else: ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['title'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
<?php endif; ?>" src="<?php echo $this->_tpl_vars['AD_UPLOAD_FOLDER']; ?>
<?php echo $this->_tpl_vars['newsConfig']['big']['upload_path']; ?>
<?php echo $this->_tpl_vars['news']['text_image']; ?>
">
		<?php endif; ?>
		<p><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['lead'])) ? $this->_run_mod_handler('clear', true, $_tmp) : clear($_tmp)); ?>
</p>
    	<?php echo $this->_tpl_vars['news']['text']; ?>
	
	</div>
	<div class="aftercontent">
		<?php if (count($this->_tpl_vars['news']['links']) > 0): ?>
		<div class="link_list clearfix">
			<h2><?php echo ((is_array($_tmp='news_LinksBlockTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Related links') : gL($_tmp, 'Related links')); ?>
</h2>
			<ul>
				<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['news']['links']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
					<li>
						<a href="<?php if ($this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkDocId']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrlId'])) ? $this->_run_mod_handler('getLink', true, $_tmp) : getLink($_tmp)); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkDocId'])) ? $this->_run_mod_handler('getDocUrl', true, $_tmp) : getDocUrl($_tmp)); ?>
.html<?php elseif ($this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrlId']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrlId'])) ? $this->_run_mod_handler('getLink', true, $_tmp) : getLink($_tmp)); ?>
<?php else: ?><?php echo $this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrl']; ?>
<?php endif; ?>" target="<?php echo $this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkTarget']; ?>
" title="<?php if ($this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkTitle']): ?><?php echo $this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkTitle']; ?>
<?php else: ?><?php if ($this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrlId']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrlId'])) ? $this->_run_mod_handler('getLink', true, $_tmp) : getLink($_tmp)); ?>
<?php else: ?><?php echo $this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrl']; ?>
<?php endif; ?><?php endif; ?>">
							<?php if ($this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkTitle']): ?><?php echo $this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkTitle']; ?>
<?php else: ?><?php if ($this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrlId']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrlId'])) ? $this->_run_mod_handler('getLink', true, $_tmp) : getLink($_tmp)); ?>
<?php else: ?><?php echo $this->_tpl_vars['news']['links'][$this->_sections['item']['index']]['linkUrl']; ?>
<?php endif; ?><?php endif; ?>
						</a>
					</li>
				<?php endfor; endif; ?>
			</ul>
		</div>
		<?php endif; ?>
		<?php if (count($this->_tpl_vars['news']['files']) > 0): ?>
		<div class="link_list clearfix">
			<h2><?php echo ((is_array($_tmp='news_FilesBlockTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Attached files') : gL($_tmp, 'Attached files')); ?>
</h2>
			<ul>
				<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['news']['files']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
				<li class="file">
					<a href="/download.php?file=<?php echo $this->_tpl_vars['news']['files'][$this->_sections['item']['index']]['path']; ?>
<?php echo $this->_tpl_vars['news']['files'][$this->_sections['item']['index']]['fileName']; ?>
" target="_blank" title="<?php echo $this->_tpl_vars['news']['files'][$this->_sections['item']['index']]['fileTitle']; ?>
">
						<span><?php echo $this->_tpl_vars['news']['files'][$this->_sections['item']['index']]['fileTitle']; ?>
</span> *.<?php echo $this->_tpl_vars['news']['files'][$this->_sections['item']['index']]['ext']; ?>
, <?php echo $this->_tpl_vars['news']['files'][$this->_sections['item']['index']]['size']; ?>

					</a> 
				</li>
				<?php endfor; endif; ?>
			</ul>
		</div>
		<?php endif; ?>
		<div class="backsocial clearfix">
			<a href="javascript:;" onclick="history.go(-1);" class="back css3"></a>
			<div class="social css3">
				
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style ">
				<!--<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>-->
				<a class="addthis_counter addthis_pill_style"></a>
				</div>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-533581334c9f2941"></script>
				<!-- AddThis Button END -->
				
				<!--<div class="g-plus" data-action="share" data-annotation="bubble"></div>-->
			</div>
		</div>
	</div>
</div>