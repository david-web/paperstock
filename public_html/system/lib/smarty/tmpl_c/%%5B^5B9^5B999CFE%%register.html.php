<?php /* Smarty version 2.6.26, created on 2014-07-16 23:10:42
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/register.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/register.html', 4, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/register.html', 325, false),)), $this); ?>
<section class="wrap clearfix">
	<section class="col1a">
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
		<h2 class="notlogged"><?php echo ((is_array($_tmp='profile_SignUp_LeadText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Condimentum tristique  get sollicitudin. Already a member? <a href="#">Sign in</a>') : gL($_tmp, 'Condimentum tristique  get sollicitudin. Already a member? <a href="#">Sign in</a>')); ?>
</h2>
	</section>
</section>

<section class="form_bg">
	<section class="wrap clearfix">
		<section class="col1a">
			<section class="order_form">
				<?php $_from = $this->_tpl_vars['profile']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['register'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['register']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['register']['iteration']++;
?>
				<?php if ($this->_tpl_vars['i']['type'] == 'tabselect'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
								<?php if ($this->_tpl_vars['v']['hint']): ?>
								<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
								<?php endif; ?>
								<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
							</div>
							<?php endforeach; endif; unset($_from); ?>
						</div>
						<div class="horizontal_dropdown">
							<select class="selectpicker register" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'select'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="select_cont">
							<select class="selectpicker register" data-width="100%" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<option value="" <?php if (! $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp='order_FormSelectDefaultValue')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Select value') : gL($_tmp, 'Select value')); ?>
</option>
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<option rel="<?php echo $this->_tpl_vars['v']['hint']; ?>
" value="<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>
						</div>
						<?php if ($this->_tpl_vars['i']['hint_below']): ?>
						<div class="note">
							<p>
								<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
							</p>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'text'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="cinput css3">
							<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'pages'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix">
						<div class="col1c">
							<div class="cinput css3">
								<input type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="page_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
							</div>
						</div>
						<div class="col2c clearfix">
							<div class="col1d">
								<div class="horizontal_select clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
									<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
									<div class="option has_tip <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>active<?php endif; ?>" data-value="<?php echo $this->_tpl_vars['v']['id']; ?>
">
										<?php if ($this->_tpl_vars['v']['hint']): ?>
										<div class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</div>
										<?php endif; ?>
										<div class="text"><?php echo $this->_tpl_vars['v']['name']; ?>
</div>
									</div>
									<?php endforeach; endif; unset($_from); ?>
								</div>
								<div class="horizontal_dropdown">
									<select class="selectpicker" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" data-hide=".hide_<?php echo $this->_tpl_vars['i']['id']; ?>
">
										<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
										<option value="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-show=".show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_tpl_vars['v']['id'] == $this->_tpl_vars['i']['selected'] || ( ! $this->_tpl_vars['i']['selected'] && $this->_foreach['orderdatavalues']['iteration'] == 1 )): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['v']['name']; ?>
</option>
										<?php endforeach; endif; unset($_from); ?>
									</select>
								</div>
							</div>
							<div class="col2d">
								<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
								<?php if ($this->_tpl_vars['v']['hint']): ?>
								<div class="note2 hide_<?php echo $this->_tpl_vars['i']['id']; ?>
 show_<?php echo $this->_tpl_vars['i']['id']; ?>
_<?php echo $this->_tpl_vars['v']['id']; ?>
" <?php if ($this->_foreach['orderdatavalues']['iteration'] != 1): ?>style="display:none;"<?php endif; ?>>
									<?php echo $this->_tpl_vars['v']['hint']; ?>

								</div>
								<?php endif; ?>
								<?php endforeach; endif; unset($_from); ?>
							</div>
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'hourdate'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix select_group" id="dt_<?php echo $this->_tpl_vars['i']['id']; ?>
">
						<div class="col1c">
							<div class="special1 css3 hours"><?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
							<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<div class="option has_tip" data-value="8h">
									<div class="tip css3">8 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
									<div class="text">8</div>
								</div>
								<div class="option has_tip" data-value="24h">
									<div class="tip css3">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</div>
									<div class="text">24</div>
								</div>
							</div>
						</div>
						<div class="col2c">
							<div class="special1 css3 days"><?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
							<div class="horizontal_select flat_top clearfix" data-linkto="#mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<div class="option has_tip active" data-value="2d">
									<div class="tip css3">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">2</div>
								</div>
								<div class="option has_tip" data-value="3d">
									<div class="tip css3">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">3</div>
								</div>
								<div class="option has_tip" data-value="6d">
									<div class="tip css3">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">6</div>
								</div>
								<div class="option has_tip" data-value="10d">
									<div class="tip css3">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">10</div>
								</div>
								<div class="option has_tip" data-value="14d">
									<div class="tip css3">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</div>
									<div class="text">14</div>
								</div>
							</div>
						</div>
						<div class="horizontal_dropdown">
							<select class="selectpicker" data-width="100%" data-linkto="#dt_<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
">
								<option value="8h">8 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
								<option value="24h">24 <?php echo ((is_array($_tmp='orders_FormsFieldHours')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Hours') : gL($_tmp, 'Hours')); ?>
</option>
								<option value="2d" selected="selected">2 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="3d">3 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="6d">6 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="10d">10 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
								<option value="14d">14 <?php echo ((is_array($_tmp='orders_FormsFieldDays')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Days') : gL($_tmp, 'Days')); ?>
</option>
							</select>
						</div>
						<div class="c"></div>
						<?php if ($this->_tpl_vars['i']['hint_over']): ?>
						<div class="note3">
							<?php echo $this->_tpl_vars['i']['hint_below']; ?>
	
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'extra'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b">
						<div class="checkbox_block css3 clearfix">
							<?php $_from = $this->_tpl_vars['i']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['orderdatavalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['orderdatavalues']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['vk'] => $this->_tpl_vars['v']):
        $this->_foreach['orderdatavalues']['iteration']++;
?>
							<div class="option">
								<div class="checkbox"></div>
								<div class="text">
									<span class="has_tip"><?php echo $this->_tpl_vars['v']['name']; ?>

										<?php if ($this->_tpl_vars['v']['hint']): ?>
										<span class="tip css3"><?php echo $this->_tpl_vars['v']['hint']; ?>
</span>
										<?php endif; ?>
									</span>
								</div>
								<div class="price">$<?php echo $this->_tpl_vars['v']['price']; ?>
</div>
							</div>
							<?php endforeach; endif; unset($_from); ?>
						</div>
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'password'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix">
						<div class="col1e">
							<div class="cinput css3">
								<input class="register" type="password" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
							</div>
						</div>	
					</div>
				</div>
				<?php elseif ($this->_tpl_vars['i']['type'] == 'phone'): ?>
				<div class="field clearfix">
					<div class="col1b">
						<div class="label_cont"><div class="label_center">
							<label class="has_tip">
								<?php echo $this->_tpl_vars['i']['name']; ?>

								<?php if ($this->_tpl_vars['i']['hint_over']): ?>
								<div class="tip css3">
									<?php echo $this->_tpl_vars['i']['hint_over']; ?>

								</div>
								<?php endif; ?>
							</label>
						</div></div>
					</div>
					<div class="col2b clearfix">
						<div class="col1e clearfix">
							<div class="col1f">
								<div class="cinput_nochange css3"><span id="countryCode"></span></div>
							</div>
							<div class="col2f">
								<div class="cinput css3">
									<input class="register" type="text" name="<?php echo $this->_tpl_vars['i']['id']; ?>
" id="mob_<?php echo $this->_tpl_vars['i']['id']; ?>
" value="<?php echo $this->_tpl_vars['i']['selected']; ?>
">
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				<div class="field clearfix">
					<div class="col1g">
						<input type="checkbox" class="styled register" name="agreement" id="agreement" value="1" />
					</div>
					<div class="col2g clearfix">
						<label>
							<?php echo ((is_array($_tmp='profile_RegisterAgreementText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>') : gL($_tmp, 'I agree with: <a href="#">Money back guarantee</a>, <a href="#">Privacy policy</a>, <a href="#">Terms of Use</a>')); ?>

						</label>
					</div>
					<div class="invalid_msg"></div>
				</div>
				<hr>
				<div class="authorize_cont">
					<a href="javascript:;" onclick="profile.profileRegister();" class="btn3 css3"><?php echo ((is_array($_tmp='profile_RegisterButton')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Register') : gL($_tmp, 'Register')); ?>
</a>
				</div>
			</section>
		</section>
		<section class="col2a">
			<section class="side_safety css3">
				<?php if (count($this->_tpl_vars['menu']['ORDERS_RIGHT']) > 0): ?>	
						<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['menu']['ORDERS_RIGHT']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
						<div><a href="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
"><?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
</a></div>	
						<?php endfor; endif; ?>
					<?php endif; ?>
			</section>
			<section class="small_stats clearfix">
				<img alt="<?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats.png">
				<div class="nr"><?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
</div>
				<div class="text"><?php echo ((is_array($_tmp='news_OurLiveStats_OrdersDelivered_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Orders<br>delivered') : gL($_tmp, 'Orders<br>delivered')); ?>
</div>
			</section>
		</section>
	</section>
</section>

<script type="text/javascript">

	$(function () { 

		$('.order_form').keyup(function(e){
			if (!e) var e = window.event;
			var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : null;
			if (keyCode == 13) {
				profile.profileRegister();
			}
			return false;	
		});

		$("#mob_29, #mob_41").change(function() {
			$('#countryCode').html("+" + $('option:selected', this).attr('rel'));
		});
	});	
</script>