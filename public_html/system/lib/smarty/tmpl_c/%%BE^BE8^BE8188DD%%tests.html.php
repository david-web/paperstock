<?php /* Smarty version 2.6.26, created on 2014-07-16 02:06:55
         compiled from /home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/tests.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'gL', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/tests.html', 13, false),array('modifier', 'is_array', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/tests.html', 68, false),array('modifier', 'array_key_exists', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/tests.html', 68, false),array('modifier', 'count', '/home/papersst/public_html/system/config/../../system/app/out/profile/tmpl/tests.html', 116, false),)), $this); ?>
<section class="green_title order">
	<section class="wrap">
		<h1><?php echo $this->_tpl_vars['web']['title']; ?>
</h1>
	</section>
</section>

<?php if ($this->_tpl_vars['userData']['status'] == 2): ?>
<div class="sp40-40-0"></div>
<section class="wrap mob_nowrap clearfix">
	<section class="col1a">
		<div class="message_green">
			<div class="wrap_onlymob">
				<h2><?php echo ((is_array($_tmp='writers_TestsApprovedTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Congratulations, you are accepted!') : gL($_tmp, 'Congratulations, you are accepted!')); ?>
</h2>
				<?php echo ((is_array($_tmp='writers_TestsApprovedTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>') : gL($_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>')); ?>

			</div>
		</div>
	</section>
</section>
<?php endif; ?>

<div class="sp32-32-16"></div>
<section class="wrap clearfix">
	<section class="col1a">
		<p><?php echo ((is_array($_tmp='profile_TestsText')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Text text text. Tests page text.') : gL($_tmp, 'Text text text. Tests page text.')); ?>
</p>
	</section>
</section>
<div class="sp24-24-16"></div>

<section class="wrap clearfix">
	<section class="col1a">
		
		<?php if ($this->_tpl_vars['userData']['status'] == 3): ?>
		<div class="message_red">
			<div class="wrap_onlymob">
				<h2><?php echo ((is_array($_tmp='writers_TestsDeniedTitle')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Sorry, but your application are denied!') : gL($_tmp, 'Sorry, but your application are denied!')); ?>
</h2>
				<?php echo ((is_array($_tmp='writers_TestsDeniedLead')) ? $this->_run_mod_handler('gL', true, $_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
				<p>Pellentesque sit amet varius ante. Integer aliquam felis ut commodo sagittis. Suspendisse tincidunt dictum tellus, id facilisis mauris tempus sed.</p>
				<p>In eu rutrum ante, sit amet dapibus mi. Suspendisse dapibus blandit vehicula.</p>
				<p>Quisque aliquam augue consectetur ante tincidunt sollicitudin. Curabitur dapibus felis consectetur tellus scelerisque vulputate.</p>') : gL($_tmp, '<p>Mauris lacus enim, imperdiet at nisi non, porttitor hendrerit massa. Donec condimentum ligula eros, ut aliquet risus fringilla afusce dapibus quam eu adipiscing tempus.</p>
				<p>Pellentesque sit amet varius ante. Integer aliquam felis ut commodo sagittis. Suspendisse tincidunt dictum tellus, id facilisis mauris tempus sed.</p>
				<p>In eu rutrum ante, sit amet dapibus mi. Suspendisse dapibus blandit vehicula.</p>
				<p>Quisque aliquam augue consectetur ante tincidunt sollicitudin. Curabitur dapibus felis consectetur tellus scelerisque vulputate.</p>')); ?>

			</div>
		</div>
		<?php else: ?>
	
			<?php $_from = $this->_tpl_vars['tests']['tests']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['tests'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['tests']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['tests']['iteration']++;
?>
			<?php $this->assign('ID', $this->_tpl_vars['i']['id']); ?>
			<div class="test_block">
				<div class="test_cont">
					<div class="title"><?php echo $this->_tpl_vars['i']['title']; ?>
</div>
					<div class="inner clearfix">
						<div class="col_l">
							<?php echo $this->_tpl_vars['i']['description']; ?>

						</div>
						<div class="col_r clearfix" id="test<?php echo $this->_tpl_vars['i']['id']; ?>
">
						
						<?php if ($this->_tpl_vars['tests']['userTests'][$this->_tpl_vars['ID']]['filename']): ?>
							<div class="file doc">
								<a href="javascript:;" onclick="profile.removeTest(<?php echo $this->_tpl_vars['i']['id']; ?>
);" class="remove"></a>
								<a href="/download.php?file=tests/results/<?php echo $this->_tpl_vars['tests']['userTests'][$this->_tpl_vars['ID']]['filename']; ?>
" class="link"><?php echo $this->_tpl_vars['tests']['userTests'][$this->_tpl_vars['ID']]['filename']; ?>
</a>
							</div>
						<?php else: ?>
						
								<?php if ($this->_tpl_vars['i']['type'] == 2): ?>
						
								<a href="javascript:;" id="uploadButton_<?php echo $this->_tpl_vars['i']['id']; ?>
" class="btn1 upload" title="<?php echo ((is_array($_tmp='profile_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
"><?php echo ((is_array($_tmp='profile_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
</a>
								
								<?php else: ?>
								
									<?php if (is_array($this->_tpl_vars['tests']['userTests']) && array_key_exists($this->_tpl_vars['i']['id'], $this->_tpl_vars['tests']['userTests'])): ?>
									<a  href="javascript:;" id="uploadButton_<?php echo $this->_tpl_vars['i']['id']; ?>
" class="btn1 upload" title="<?php echo ((is_array($_tmp='profile_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
"><?php echo ((is_array($_tmp='profile_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
</a>
									<a href="javascript:;" onclick="return profile.startTest(<?php echo $this->_tpl_vars['i']['id']; ?>
, '/download.php?file=tests/<?php echo $this->_tpl_vars['i']['filename']; ?>
');" class="btn1" title="<?php echo ((is_array($_tmp='profile_StartNow')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Start now') : gL($_tmp, 'Start now')); ?>
"><?php echo ((is_array($_tmp='profile_StartNow')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Start now') : gL($_tmp, 'Start now')); ?>
</a>
									<?php else: ?>
									<a  href="javascript:;" id="uploadButton_<?php echo $this->_tpl_vars['i']['id']; ?>
" class="btn5 upload" title="<?php echo ((is_array($_tmp='profile_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
"><?php echo ((is_array($_tmp='profile_Upload')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Upload') : gL($_tmp, 'Upload')); ?>
</a>
									<a href="javascript:;" onclick="return profile.startTest(<?php echo $this->_tpl_vars['i']['id']; ?>
, '/download.php?file=tests/<?php echo $this->_tpl_vars['i']['filename']; ?>
');" class="btn1" title="<?php echo ((is_array($_tmp='profile_StartNow')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Start now') : gL($_tmp, 'Start now')); ?>
"><?php echo ((is_array($_tmp='profile_StartNow')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Start now') : gL($_tmp, 'Start now')); ?>
</a>
									<?php endif; ?>
								
								<?php endif; ?>
								
								<input type="text" value="" id="uploadButton_<?php echo $this->_tpl_vars['i']['id']; ?>
File" style="display:none;" onchange="profile.uploadTest(<?php echo $this->_tpl_vars['i']['id']; ?>
);">
								<?php if ($this->_tpl_vars['i']['type'] == 2 || is_array($this->_tpl_vars['tests']['userTests']) && array_key_exists($this->_tpl_vars['i']['id'], $this->_tpl_vars['tests']['userTests'])): ?>
								<script type="text/javascript">
			       
								$(document).ready(function() {
								
									loadUploadButton('tests/results/', 'uploadButton_<?php echo $this->_tpl_vars['i']['id']; ?>
');
								
								});
								
								</script>
								<?php endif; ?>
							<?php endif; ?>
						
						
	
						</div>
					</div>
				</div>
			
			<?php if ($this->_tpl_vars['i']['comment']): ?>
			<div class="revision">
				<div class="title"><?php echo ((is_array($_tmp='writers_RevisionComment')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Revision comment:') : gL($_tmp, 'Revision comment:')); ?>
</div>
				<div class="text"><?php echo $this->_tpl_vars['i']['comment']; ?>
</div>
			</div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['i']['header']): ?>
			<div class="comment">
				<p><?php echo $this->_tpl_vars['i']['header']; ?>
</p>
			</div>
			<?php endif; ?>
			</div>
			<?php endforeach; endif; unset($_from); ?>
		
		<?php endif; ?>
	</section>
	<section class="col2a">
		<section class="side_safety css3">
			<?php if (count($this->_tpl_vars['menu']['ORDERS_RIGHT']) > 0): ?>	
					<?php unset($this->_sections['item']);
$this->_sections['item']['name'] = 'item';
$this->_sections['item']['loop'] = is_array($_loop=$this->_tpl_vars['menu']['ORDERS_RIGHT']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['item']['show'] = true;
$this->_sections['item']['max'] = $this->_sections['item']['loop'];
$this->_sections['item']['step'] = 1;
$this->_sections['item']['start'] = $this->_sections['item']['step'] > 0 ? 0 : $this->_sections['item']['loop']-1;
if ($this->_sections['item']['show']) {
    $this->_sections['item']['total'] = $this->_sections['item']['loop'];
    if ($this->_sections['item']['total'] == 0)
        $this->_sections['item']['show'] = false;
} else
    $this->_sections['item']['total'] = 0;
if ($this->_sections['item']['show']):

            for ($this->_sections['item']['index'] = $this->_sections['item']['start'], $this->_sections['item']['iteration'] = 1;
                 $this->_sections['item']['iteration'] <= $this->_sections['item']['total'];
                 $this->_sections['item']['index'] += $this->_sections['item']['step'], $this->_sections['item']['iteration']++):
$this->_sections['item']['rownum'] = $this->_sections['item']['iteration'];
$this->_sections['item']['index_prev'] = $this->_sections['item']['index'] - $this->_sections['item']['step'];
$this->_sections['item']['index_next'] = $this->_sections['item']['index'] + $this->_sections['item']['step'];
$this->_sections['item']['first']      = ($this->_sections['item']['iteration'] == 1);
$this->_sections['item']['last']       = ($this->_sections['item']['iteration'] == $this->_sections['item']['total']);
?>
					<div><a href="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
"><?php echo $this->_tpl_vars['menu']['ORDERS_RIGHT'][$this->_sections['item']['index']]['title']; ?>
</a></div>	
					<?php endfor; endif; ?>
				<?php endif; ?>
		</section>
		<section class="small_stats clearfix">
			<img alt="<?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
" src="<?php echo $this->_tpl_vars['AD_IMAGE_FOLDER']; ?>
_stats.png">
			<div class="nr"><?php echo ((is_array($_tmp='startpage_OurLiveStats_OrdersDelivered_Number')) ? $this->_run_mod_handler('gL', true, $_tmp, '150') : gL($_tmp, '150')); ?>
</div>
			<div class="text"><?php echo ((is_array($_tmp='news_OurLiveStats_OrdersDelivered_Title')) ? $this->_run_mod_handler('gL', true, $_tmp, 'Orders<br>delivered') : gL($_tmp, 'Orders<br>delivered')); ?>
</div>
		</section>
	</section>
</section>