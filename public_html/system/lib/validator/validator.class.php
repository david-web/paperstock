<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/** 
 * Main validator class
 * This class contain only general validation functions
 * If need more advanced functions please make new class, that extend that one 
 * and include it in constructor.
 * 30.06.2010
 */
class Validator {
	
	public $rData;
	
	/**
	 * Class constructor
	 */
	public function __construct() {

	}
	
	/**
	 * Check field for correct value
	 * With this function we checking all fields values
	 * 
	 * @param mix		field name
	 * @param mix		field value
	 * @param array 	fields array from config
	 * @return array	data with fields errors or true if no errors
	 */
	public function checkValue($fieldName, $fieldValue, $array) {
		
		$this->webArray = $array;
		
		$fieldValue = clearText($fieldValue);
		
		$this->rData["fieldName"] = $fieldName;
		if (isset($this->webArray[$fieldName])) {
			$this->rData["fields"][$fieldName] = $fieldValue;
			
			if (isset($this->webArray[$fieldName]['form']['divClass'])) {
				$this->rData["divClass"][$fieldName] = $this->webArray[$fieldName]['form']['divClass'];
			}
			
			if (isset($this->webArray[$fieldName]["required"]) && $this->webArray[$fieldName]["required"] && empty($fieldValue)) {
				
				$this->rData["error"] = true;
				$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_Empty');
				
				return $this->rData;
			}
			if (isset($this->webArray[$fieldName]["errors"]) && count($this->webArray[$fieldName]["errors"])) {
				
				foreach ($this->webArray[$fieldName]["errors"] AS $func) {
						
					$params = array($fieldValue, $fieldName);
					if (isset($this->webArray[$fieldName]["params"][$func])) {
						$params = array_merge($params, $this->webArray[$fieldName]["params"][$func]);
					}
					
					if (!call_user_func_array(array(&$this, $func), $params)) {
						
						return $this->rData;
					} 
				}	
			}
			
		} else {
			$this->rData["divClass"][$fieldName] = '';
		}
				 
		return $this->rData;
	}
	
	/**
	 * Check string for personal code
	 * Chec pc checksum
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkPersCode($fieldValue, $fieldName) {
		
		$fieldValue = trim(str_replace("-", "", $fieldValue));

  
		if (!$fieldValue || strlen($fieldValue) != 11) {
        	$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkPersCode');
			
			return false;
        }

 
		$checksum = 1;
		for ($i = 0; $i < 10; $i++) {
			$checksum -= (int)substr($fieldValue, $i, 1) * (int)substr("01060307091005080402", $i * 2, 2);
		}

        if (($checksum - floor($checksum / 11) * 11) != (int)substr($fieldValue, 10, 1)) {
        	$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkPersCode');
			
			return false;
        }

		return true;
	}
	
	/**
	 * Check string for correct IBAN code
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkIban($fieldValue, $fieldName) {
		$iban = false;
		$fieldValue = strtoupper(trim($fieldValue));

		if(preg_match('/^LV\d{2}[A-Z]{4}\d{13}$/', $fieldValue)) {
			$number = substr($fieldValue, 4) . substr($fieldValue, 0, 4);
			$number = str_replace(
						array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),
						array(10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35),
						$number
			);

			$iban = (1 == my_bcmod($number, 97)) ? true : false;
		}
		
		if ($iban) {
			return true;
		} else {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkIban');
			
			return false;
		}
	}
	
	/**
	 * Check string for Alpha 
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkAlpha($fieldValue, $fieldName) {
		
		if (!ctype_alpha($fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkAlpha');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check string for Not Alpha 
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkNotAlpha($fieldValue, $fieldName) {

		if (preg_match('/([[:alpha:]])/iu', $fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkNotAlpha');

			return false;
		}
		
		return true;
	}
	
	/**
	 * Check number for decimal value
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkDecimal($fieldValue, $fieldName) {

		if (!preg_match('/^\+?+\d+((\.|\,)\d{0,2})?$/', $fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkNotAlpha');

			return false;
		}
		
		return true;
	}
	
	/**
	 * Check string for Numbers 
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkNum($fieldValue, $fieldName) {
		
		if (!ctype_digit ($fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkNum');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check string for AlphaNumeric
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkAlphaNum($fieldValue, $fieldName) {
		
		if (!ctype_alnum($fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkAlphaNum');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check string for password type
	 * Must be 6 or more symbols and must contain one digit and one alpha
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkPass($fieldValue, $fieldName) {
		
		if (!preg_match("/^.*(?=.{6,32})(?=.*\d)(?=.*[a-zA-Z]).*$/", $fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkPass');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check string lenght 
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @param int		start lenght
	 * @param int		end lenght
	 * @return bool		true or false
	 */
	public function checkLen($fieldValue, $fieldName, $s, $e) {
		if (strlen($fieldValue) < $s || strlen($fieldValue) > $e) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkLen');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check string for email valid address
	 * 
	 * @param string	field value
	 * @param string	field name
	 * @return bool		true or false
	 */
	public function checkEmail($fieldValue, $fieldName) {
		if (!isValidEmail($fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkEmail');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check correct hour
	 * from 00 - 23
	 * 
	 * @param string	field value
     * @param string	field name
	 * @return bool		true or false
	 */
	public function checkHour($fieldValue, $fieldName) {
		
		if ($fieldValue > 23 || $fieldValue < 0) {
			
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkHour');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check correct hour & minutes
	 * 
	 * @param string	field value
     * @param string	field name
	 * @return bool		true or false
	 */
	public function checkNumHour($fieldValue, $fieldName) {
		$time = explode(':', $fieldValue);
		if (isset($time[0], $time[1]) && $this->checkNum($time[0], $fieldName) && $this->checkNum($time[1], $fieldName)) {
			
			if (!$this->checkHour($time[0], $fieldName)) {
				return false;
			}

			if (!$this->checkMinutes($time[1], $fieldName)) {			
				return false;
			} 
			
		} else {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check correct minutes
	 * from 0 - 59
	 * 
	 * @param string	field value
     * @param string	field name
	 * @return bool		true or false
	 */
	public function checkMinutes($fieldValue, $fieldName) {
		
		if ($fieldValue > 59 || $fieldValue < 0) {
			
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkMinutes');
			
			return false;
		}
		
		return true;
	}
	
	/**
     * Check regex from config array value
     *
     * @param string	field value
     * @param string	field name
     * @param regex		regex value
     * @return bool		true or false
     */
    public function checkRegex($fieldValue, $fieldName, $regex) {

		if (!preg_match($regex, $fieldValue)) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkRegex');

			return false;
		}
		
		return true;
    }
    
    /**
     * Check phone number
     *
     * @param string	field value
     * @param string	field name
     * @return bool		true or false
     */
	public function checkPhoneNumber($fieldValue, $fieldName) {

        if (!preg_match("/^\+?\s?[0-9\s]+$/", $fieldValue)) {
            $this->rData["error"] = true;
            $this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkPhoneNumber');

            return false;
        }

        return true;
    }
    
	/**
     * Function to check & not allow past dates
     *
     * @param string	field value
     * @param string	field name
     * @return bool		true or false
     */
    public function checkNotPastDate($fieldValue, $fieldName) {

		$date = strtotime($fieldValue);
		$today = strtotime(date('d.m.Y', time()));
		if ($date < $today) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkNotPastDate');

			return false;
        }
        
        return true;
    }
	
	/**
	 * Check if one of group elements selected
	 * 
	 * @param string	field value
     * @param string	field name
     * @param array		all group fields values
	 * @return bool		true or false
	 */
	public function checkGroupCkboxRequired($fieldValue, $fieldName, $values){
		
		$this->rData["fields"][$fieldName] = array();
		if (!empty($values)){
			$selected = false;
			foreach ($values as $name => $value) {
				if ($value == 'on') {
					$this->rData["fields"][$name] = true;
					$this->rData["fields"][$fieldName][] = $name;
					$selected = true;
				}
			}
			if (!$selected) {
				$this->rData["error"] = true;
				$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_group_empty');
				return false;
			}
		} else {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_group_empty');
			return false;
		}
		return true;
		
	}
	
	/**
	 * Check positive numeric value
	 * 
	 * @param string	field value
     * @param string	field name
	 * @return bool		true or false
	 */
	public function checkPosNum($fieldValue, $fieldName) {
		if (!$this->checkNum($fieldValue, $fieldName)) {
			return false;
		}
		if ($fieldValue <= 0) {
			$this->rData["error"] = true;
			$this->rData["errorFields"][$fieldName] = gL('Error_' . $fieldName . '_checkPosNum');
			return false;
		}
		
		return true;
	}

}

?>