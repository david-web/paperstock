<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */
// ------------------------------------------------------------------------

/**
 * Function that generates correct site backlink (even if opening a direct url)
 * @author Ņikita Maļgins
 */
function getBackLink() {
    $useExternal = false;
    $internal = "javascript:history.back();";

    $parent = getClosestParent();
    if ($parent) {
        $external = $parent;
    } else {
        $external = "";
    }

    if (isset($_SERVER['HTTP_REFERER'])) {
        $referer = $_SERVER['HTTP_REFERER'];
        $referer = parse_url($referer);
        if ($referer['host'] != $_SERVER['HTTP_HOST']) {
            // external link
            $useExternal = true;
        }
    } else {
        $useExternal = true;
    }
    if ($useExternal) {
        return $external;
    }
    return $internal;
}

/**
 * Function returns full link to previous page
 * 
 * @return type 
 */
function getClosestParent() {
    $cfg = &loadLibClass('config');
    $current = $_SERVER['REQUEST_URI'];
    $current = explode("/", $current);

    if ($current[count($current) - 1] == "") {
        // if wasn't docUrl
        unset($current[count($current) - 1]);
    }

    if ($cfg->get("langInTheEnd") && checkLangEnabled($current[count($current) - 1])) {
        // removing last element
        unset($current[count($current) - 2]);
    } else {
        // removing last element
        unset($current[count($current) - 1]);
    }

    if (!empty($current)) {
        return implode("/", $current) . "/";
    }

    return false;
}

/**
 * 
 */
function redirectToClosestParent() {
    $parent = getClosestParent();
    if ($parent) {
        redirect($parent);
    }
}

/**
 *
 * @param string $string
 * @param type $chars
 * @param type $words
 * @param type $end
 * @return string 
 */
function cutString($string, $chars = false, $words = false, $end = "...") {
    if ($chars || $words) {
        if ($chars) {
            if (mb_strlen($string) > $chars) {
                $i = $chars;
                while (substr_unicode($string, $i, 1) != " ") {
                    $i--;
                }
                $string = substr_unicode($string, 0, $i) . ($end ? " " . $end : "");
            }
        }

        if ($words) {
            $explode = explode(" ", $string);
            if (count($explode) > $words) {
                $string = implode(" ", array_slice($explode, 0, $words));
            }
        }
    }
    return $string;
}

function clearTabs($string) {
    $string = preg_replace('/[\s\t\r\n]+/', ' ', $string);
    return $string;
}

/**
 *
 * @param type $str
 * @param type $s
 * @param type $l
 * @return type 
 */
function substr_unicode($str, $s, $l = null) {
    return join("", array_slice(preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY), $s, $l));
}

/**
 * Return curent page url 
 * @author Rolands EĆ…ā€ Ć„Ā£elis <rolands@efumo.lv>
 * @return string
 */
function curPageURL() {
    $pageURL = 'http';
    if (isset($_SERVER['HTTPS']) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function curPostUrl(){
    $pageURL = $_SERVER["REQUEST_URI"];
    return $pageURL;
}

/**
 * Returns all enable site langs
 * @param   $lang AS string, selected value
 * @author  JÄ�nis Å akars <janis.sakars@efumo.lv>
 */
function getAllSiteLangs($lang = '') {
    
    $values = array();
    
    $mdb = &loadLibClass('db');

	$query = new query($mdb, "SELECT `lang`, `title` FROM `ad_languages` WHERE `enable`='1' ORDER BY `sort` ASC");
    while($query->getrow()) {
        $values[$query->field('lang')] = $query->field('title');
    }
    
    return dropDownFieldOptions($values, $lang, true);
}

function reSort($table, $id, $params = array(), $field = 'sort') {
    $mdb = &loadLibClass("db");
    $where = array();
    if(!empty($params)){
        foreach($params as $key => $val){
            $where[] = "`".$key."`='".  mres($val)."'";
        }
    }
    $where[] = "`id`!=".$id;
    $whereSql = implode(" AND ", $where);
    $dbQuery = "UPDATE `".$table."` SET `" . $field . "` =`" . $field . "`+1".(strlen($whereSql) ? " WHERE ".$whereSql : "");
    $query = new query($mdb, $dbQuery);
}

function showFileSize($filePath, $limit) {
	
	if(file_exists($filePath)) {
		
		$size = filesize($filePath);
		
		if($size <= $limit * 1048576) {
			return round($size / 1024, 2) . gL("fileSizeKb", "Kb");
		}
		else {
			return round($size / 1048576, 2) . gL("fileSizeMb", "Mb");
		}
	}
	else {
		return false;
	}
}

/**
 *  GET first words from string
 * @param string $string      given string      
 * @param type $wordCount   word count
 * return string              first words
 * 
 */
function getFirstWords($string, $wordCount = 25, $delimiter = '' ) {
    $string = strip_tags($string);
    $symbols = array(
        "\t" => ' ',
        "\n" => ' ',
        "\r" => ' ',
        ' '  => ' ',
        '_'  => ' ',
        '!'  => ' ',
        '@'  => ' ',
        '#'  => ' ',
        '$'  => ' ',
        '%'  => ' ',
        '^'  => ' ',
        '&'  => '&',
        '*'  => ' ',
        '('  => ' ',
        ')'  => ' ',
        '+'  => ' ',
        '{'  => ' ',
        '}'  => ' ',
        '['  => ' ',
        ']'  => ' ',
        '"'  => ' ',
        '"'  => ' ',
        '"'  => ' ',
        ';'  => ';',
        ','  => ' ',
        '.'  => ' ',
        '>'  => ' ',
        '<'  => ' ',
        '/'  => ' ',
        '\\' => ' ',
        '"'  => ' ',
        'ā€˛'  => ' ',
        'ā€¯'  => ' ',
        '&nbsp;'  => ' ',
        '&scaron;'  => 'Å�',
        '~'  => ' '
    );
    
    foreach($symbols as $replace => $cail){
        $string = str_replace($replace, $cail, $string);
    } 
    $description = '';
    $words = explode(' ', trim($string));

    if(count($words)) {
        $i = 1;

        foreach($words as $key => $word) {
            if(trim($word) != "" ) {
                    if($i > 1) {
                        $description .= $delimiter . ' ';
                    }
                    $description .= $word;
                if($i == $wordCount ) {
                    break;
                }

                $i++;
            }
        }
    }
    
    return $description;
}

/*
 * @param string $text      given string      
 * @param type $words       word count
 * return string            limited words
 */
function generateLimitedWordsText($text, $words = 25,  $separator = '') {
	$newText = '';
	$text_array  = preg_split("/[\s]+/", $text);
		$word_count = count($text_array);
		if($word_count > $words){
			$word_count = $words;
		} 
		for($i = 0; $i < $word_count; $i++){
			if($i == 0){
				$newText .= $text_array[$i];
			} else {
				$newText .= $separator.' '.$text_array[$i];
			}
		}
	return $newText;
}


/**
 * Send e-mail template
 * @param   $tmpl_key AS string
 * @param   $values AS array
 * @param   $lang AS string
 */
function sendEmailTemplate($tmpl_key, $values, $lang) {
    
    global $mdb, $config;
    
    $query = new query($mdb, "SELECT `id` FROM `mod_email_templates` WHERE `template_key` = '" . $tmpl_key .'" LIMIT 1');
    $temp_id = $query->getOne();
    
    $query = new query($mdb, "SELECT * FROM `mod_email_templates_data` WHERE `email_templates_id` = " . intval($tmpl_id) ." AND `lang`='". $lang ."' LIMIT 1");
    if($query->num_rows()) {
        $tmpl_values = $query->getrow();
        
        $to = $tmpl_values['to_email'];
        
        $from = $tmpl_values['from_email'];
        
        $subject = $tmpl_values['email_subject'];
        
        $content = stripslashes($tmpl_values['email_body']);
        
        $query = new query($mdb, "SELECT `variable_key` FROM `mod_email_templates_variable` WHERE `email_templates_id` = '" . intval($tmpl_id) .'"');
        $variables = $query->getArray();   
        foreach($variables as $key => $val) {
            if(isset($values[$key])) {
                $to = str_replace('{var:' . $key . '}', stripslashes($values[$key]), $to);
                $from = str_replace('{var:' . $key . '}', stripslashes($values[$key]), $from);
                $subject = str_replace('{var:' . $key . '}', stripslashes($values[$key]), $subject);
                $content = str_replace('{var:' . $key . '}', stripslashes($values[$key]), $content);
            }
        }
        
        return sendMail($to, $subject, $content, array(), $from);
    }
    
    return false;
}

/**
 * Check if folder exist and is writalble
 * @param string upload path
 * @return bool
 */
function check_upload_path($upload_path){

	$root = dirname(dirname(dirname(__FILE__)));
	$full_upload_path = $root . AD_UPLOAD_FOLDER.$upload_path;

	if( !is_dir($full_upload_path) ){
		$path = explode('/', $upload_path);

		$folder_name = '';

		foreach($path as $folder){

			$folder_name .= $folder.'/';
			$upload_folder_path = $root . AD_UPLOAD_FOLDER.$folder_name;
				
			if(!is_dir($upload_folder_path) && !empty($folder_name)){

				mkdir($upload_folder_path);
				return chmod($upload_folder_path, 0777);
			}
			else if( is_dir($upload_folder_path) && !empty($folder_name) && !is_writable($upload_folder_path) ){

				return chmod($upload_folder_path, 0777);
			}


		}
	}
	else if( is_dir($full_upload_path) && !is_writable($full_upload_path) ){
		return @chmod($full_upload_path, 0777);
	}

	return true;
}

function get_site_langs_with_code() {
	$mdb = &loadLibClass('db');

	$dbQuery = "SELECT `lang`, `title`, UPPER(`lang`) AS code FROM `ad_languages` ORDER BY `sort` ASC";
	$query = new query($mdb, $dbQuery);

	return $query->getArray();
}

function paymentUpdateBonusCodeCount($code) {
	global $mdb;

	if ($code) {

		$dbQuery = "SELECT `used`, `max_uses` FROM `mod_bonus_codes` WHERE `code` = '" . mres($code) ."'";
		$query = new query($mdb, $dbQuery);
		$query->getrow();
		if (($query->field('max_uses') -  $query->field('used')) == 1) {
			$set = ", `enable` ='0', `status` = 'ended' ";
		} else {
			$set = "";
		}

		$dbQuery = "UPDATE `mod_bonus_codes` SET `used` = `used` + 1 " . $set . " WHERE `code` = '" . mres($code) ."'";
		new query($mdb, $dbQuery);

	}

}

function sendOrderEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();
	
	$dbQuery = "SELECT o.*, p.email FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();
		
		$body = '';
		$body .= $row['type_of_services'] . '<br />';
		$body .= $row['academic_levels'] . '<br />';
		$body .= $row['type_of_work'] . '<br />';
		$body .= $row['subject'] . '<br />';
		$body .= $row['topic'] . '<br />';
		$body .= $row['number_of_pages'] . ' ' . $row['pages_type'] . '<br />';
		$body .= date("d.m.Y H:i:s", $row['first_draft_deadline']) . '<br />';
		
		$body = str_replace(array('{{orderID}}', '{{orderPrice}}', '{{orderPaid}}', '{{orderDetails}}'), 
				array($row['id'], $row['price'], ($row['paid'] ? "Paid" : "Not paid"), $body), $cfg->getData('mailBodyOrder/' . $row['lang']));
		
		
		
		sendMail($row['email'], $cfg->getData('mailSubjOrder/' . $row['lang']), $body, array(), $cfg->getData('mailFromOrder/' . $row['lang']), true);
	} else {
		return false;
	}	
}

function sendAppointedEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT o.*, p.email, w.email AS writer FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
							LEFT JOIN `mod_writers` w ON (o.writer_id = w.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();

		$body = str_replace(array('{{orderID}}'),
				array('<a href="' . getDCountryLink($row['site_id'], 1) . getLink(getMirror($cfg->getData('mirros_profile_orders_page'), $row['site_id'], $row['lang'])) . 'id:' . $row['id'] . '/">' . $row['id'] . '</a>'), $cfg->getData('mailBodyAppointed/' . $row['site_id'] . '/' . $row['lang']));



		sendMail($row['email'], $cfg->getData('mailSubjAppointed/' . $row['site_id'] . '/' . $row['lang']), $body, array(), $cfg->getData('mailFromAppointed/' . $row['site_id'] . '/' . $row['lang']), true);
		
		if (!empty($row['writer'])) {
			$body = str_replace(array('{{orderID}}'),
					array('<a href="' . getDCountryLink(8, 1) . getLink(getMirror($cfg->getData('mirros_profile_orders_page'), $row['site_id'], $row['lang'])) . 'id:' . $row['id'] . '/">' . $row['id'] . '</a>'), $cfg->getData('mailBodyAppointed/8/' . $row['lang']));
			
			
			
			sendMail($row['writer'], $cfg->getData('mailSubjAppointed/8/' . $row['lang']), $body, array(), $cfg->getData('mailFromAppointed/8/' . $row['lang']), true);
		}
		
	} else {
		return false;
	}
}

function sendUploadedFileEmail($id, $country) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT o.*, p.email FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();

		$body = str_replace(array('{{orderId}}', '{{userId}}'),
				array($row['id'], $row['user_id']), $cfg->getData('mailBodyFile/' . $country . '/' . $row['lang']));



		sendMail($cfg->getData('mailToFile/' . $country . '/' . $row['lang']), $cfg->getData('mailSubjFile/' . $country . '/' . $row['lang']), $body, array(), $cfg->getData('mailFromFile/' . $country . '/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendOrderMessageEmail($id, $message, $country) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT o.*, p.email FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();

		$body = str_replace(array('{{orderId}}', '{{userId}}', '{{message}}'),
				array($row['id'], $row['user_id'], $message), $cfg->getData('mailBodyMessage/' . $country . '/' . $row['lang']));



		sendMail($cfg->getData('mailToMessage/' . $country . '/' . $row['lang']), $cfg->getData('mailSubjMessage/' . $country . '/' . $row['lang']), $body, array(), $cfg->getData('mailFromMessage/' . $country . '/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendOrderApplyEmail($id, $reason, $price) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT o.*, p.email FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();

		$body = str_replace(array('{{orderId}}', '{{userId}}', '{{reason}}', '{{price}}'),
				array($row['id'], $row['user_id'], $reason, $price), $cfg->getData('mailBodyOrderApply/' . $row['lang']));



		sendMail($cfg->getData('mailToOrderApply/' . $row['lang']), $cfg->getData('mailSubjOrderApply/' . $row['lang']), $body, array(), $cfg->getData('mailFromOrderApply/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendUserAprooveEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT o.*, p.email FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();

		$body = str_replace(array('{{orderID}}', '{{userId}}'),
				array($row['id'], $row['user_id']), $cfg->getData('mailBodyUserAproove/' . $row['lang']));



		sendMail($cfg->getData('mailToUserAproove/' . $row['lang']), $cfg->getData('mailSubjUserAproove/' . $row['lang']), $body, array(), $cfg->getData('mailFromUserAproove/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendWriterTestsEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT * FROM `mod_writers`
						WHERE id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();

		$body = str_replace(array('{{userId}}'),
				array($row['id']), $cfg->getData('mailBodyTest/' . $row['lang']));



		sendMail($cfg->getData('mailToTest/' . $row['lang']), $cfg->getData('mailSubjTest/' . $row['lang']), $body, array(), $cfg->getData('mailFromTest/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendWriterAproovedEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT * FROM `mod_writers`
						WHERE id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();


		sendMail($row['email'], $cfg->getData('mailSubjTestAprooved/' . $row['lang']), $cfg->getData('mailBodyTestAprooved/' . $row['lang']), array(), $cfg->getData('mailFromTestAprooved/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendWriterBlockEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT * FROM `mod_writers`
						WHERE id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();


		sendMail($row['email'], $cfg->getData('mailSubjTestBlock/' . $row['lang']), $cfg->getData('mailBodyTestBlock/' . $row['lang']), array(), $cfg->getData('mailFromTestBlock/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendOrderAcceptedEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT o.*, p.email FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();


		sendMail($row['email'], $cfg->getData('mailSubjOrderAccepted/' . $row['lang']), $cfg->getData('mailBodyOrderAccepted/' . $row['lang']), array(), $cfg->getData('mailFromOrderAccepted/' . $row['lang']), true);
	} else {
		return false;
	}
}

function sendOrderCanceledEmail($id) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	$cfg->getSiteData();

	$dbQuery = "SELECT o.*, p.email, w.email AS writer FROM `mod_orders` o
							LEFT JOIN `mod_profiles` p ON (o.user_id = p.id)
							LEFT JOIN `mod_writers` w ON (o.writer_id = w.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$row = $query->getrow();


		sendMail($row['email'], $cfg->getData('mailSubjOrderCanceled/' . $row['lang']), $cfg->getData('mailBodyOrderCanceled/' . $row['lang']), array(), $cfg->getData('mailFromOrderCanceled/' . $row['lang']), true);
		
		if (!empty($row['writer'])) {
			$body = str_replace(array('{{orderID}}'),
					array('<a href="' . getDCountryLink(8, 1) . getLink(getMirror($cfg->getData('mirros_profile_orders_page'), $row['site_id'], $row['lang'])) . 'id:' . $row['id'] . '/">' . $row['id'] . '</a>'), $cfg->getData('mailBodyAppointed/8/' . $row['lang']));
			
			
			
			sendMail($row['writer'], $cfg->getData('mailSubjOrderCanceled/' . $row['lang']), $cfg->getData('mailBodyOrderCanceled/' . $row['lang']), array(), $cfg->getData('mailFromOrderCanceled/' . $row['lang']), true);
		} else {
			$dbQuery = "SELECT w.email AS writer FROM `mod_orders` o
							LEFT JOIN `mod_writers_to_orders` wto ON (wto.order_id = o.id)
							LEFT JOIN `mod_writers` w ON (wto.writer_id = w.id)
						WHERE o.id = '" . mres($id) . "'
						LIMIT 1";
			$query = new query($mdb, $dbQuery);
			if ($query->num_rows() > 0) {
				while ($query->getrow()) {
					sendMail($query->field('writer'), $cfg->getData('mailSubjOrderCanceled/' . $row['lang']), $cfg->getData('mailBodyOrderCanceled/' . $row['lang']), array(), $cfg->getData('mailFromOrderCanceled/' . $row['lang']), true);
				}
			}
		}
		
	} else {
		return false;
	}
}
