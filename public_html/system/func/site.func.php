<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/** 
 * Adweb site functions
 * 21.04.2010
 */

/**
 * Get simple label value from msg table 
 * 
 * @param string	laybel name
 * @param string	add value
 * @param string	language
 * @param string	description
 */
function gL($name, $value = '', $lang = '') {

	$mdb = &loadLibClass('db');
	$labels = &loadLibClass('labels');	
	
	if(class_exists('Module')) {
		
		if($lang == "") {
			$lang = Module :: $lang;			
		}
		
		$country = Module :: getCountry();
		$cLang = Module :: getLang();
		$moduleId = Module :: $moduleId;
		
	} else {
		
		$moduleId = 1;
		$country = getCountry();
		$lang = $cLang = getDefLangInCountry($country);
		
	}
	
	if ($labels->getLabel($name) && $cLang == $lang) {
		
		return removeLineBreaks($labels->getLabel($name));
		
	} else {
		
		return $labels->getOneLabel($name, $value, $lang, $country, $moduleId);

	}
		
}

/**
 * Getting default site language
 */
function getDefLangInCountry($c) {
	$mdb = &loadLibClass('db');
	
	$dbQuery = "SELECT `lang` FROM `ad_languages` l, `ad_languages_to_ct` lc WHERE lc.lang_id = l.id  AND lc.country_id = '" . $c . "' AND lc.default = '1'";
	$query = new query($mdb, $dbQuery);
	$lang = $query->getOne();
	if($lang) {
		return $lang;
	} 
}

/**
 * Get simple label value from msg table 
 * 
 * @param string	label name
 * @param int		country id
 */
function gL2($name, $c, $lang = '') {
	
	$mdb = &loadLibClass('db');
	
	$lang = $lang ? $lang : getDefLangInCountry($c);

	$dbQuery = "SELECT `id`, `type` FROM `ad_messages` m
				WHERE m.name = '" . $name . "' 
				LIMIT 0,1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$query->getrow();
		$id = $query->field('id');
		$type = $query->field('type');
		
		if ($type == 'l') {
			$country = '0';
		} else {
			$country = $c;
		}
		
		$dbQuery = "SELECT `value` FROM `ad_messages_info` 
								WHERE 
									`id` = '" . $id . "'
									AND `country` = '" . $country . "'
									AND `lang` = '" . $lang . "'";
		$query->query($mdb, $dbQuery);
		if ($query->num_rows() > 0) {
			return $query->getOne();
		} else {
			return $name;
		}
	} else {
		return $name;
	}
		
}

function gLA($name, $value  = '', $lang = '', $global = false, $js = false) {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config.cms');

	$lang = $cfg->getCmsLang();	

	$moduleId = false ;
	if(class_exists('Module') && !$global) {
		$moduleId = Module::$moduleId;
		
	}
		
	$dbQuery = "
		SELECT `id` FROM `ad_messages_backend` m
		WHERE m.`name` = '" . $name . "' 
	";

	$dbQuery .= " LIMIT 0 , 1 ";
	$query = new query($mdb, $dbQuery);

	if ($query->num_rows() > 0) {
		$query->getrow();
		$id = $query->field('id');
		
		$dbQuery = "
			SELECT `value` FROM `ad_messages_backend_info` 
			WHERE 
				`id` = '" . $id . "'
				AND `lang` = '" . $lang . "'
		";
		$query->query($mdb, $dbQuery);
		
		if ($query->num_rows() > 0) {
			return $query->getOne();
		} 
		else {
			return $value;
		}
	} else {
		
		$message_data = array(
			'name' => $name, 
			'enable' => 1,
			'date' => time()
		);

		if($moduleId){
			$message_data['module_id'] = $moduleId;
		}

		if($js){
			$message_data['js'] = 1;
		}

		$mId_new = saveValuesInDb('ad_messages_backend', $message_data);	
		
		if($mId_new){
			$message_values = array(
				'id' => $mId_new, 
				'lang' => 'en', 
				'value' => $value
			);
			saveValuesInDb('ad_messages_backend_info', $message_values);
		}

		return $value;
	}
		
}
	
/**
 * Getting default site language
 */
function getDefaultLang($c = '') {
	$mdb = &loadLibClass('db');
	$cfg = &loadLibClass('config');
	
	if ($cfg->get('defaultLang')) {
		return $cfg->get('defaultLang');
	}
	
	$dbQuery = "SELECT `lang` FROM `ad_languages` l, `ad_languages_to_ct` lc 
						WHERE lc.lang_id = l.id  
								AND lc.country_id = '" . ($c ? $c : getCountry()) . "' 
								AND lc.default = '1'";
	$query = new query($mdb, $dbQuery);
	if($lang = $query->getOne()) {
		$cfg->set('defaultLang', $lang);
		return $lang;
	} else {
		logWarn("Error: no set default language!", __FILE__, __LINE__);
		showError("Error: no set default language!");
	}	
}

/**
 * Checking for enabled current site language
 * 
 * @param string	current site language
 */
function checkLangEnabled($lang) {
	$mdb = &loadLibClass('db');
	
	$dbQuery = "SELECT `id` FROM `ad_languages` WHERE `enable` = '1' AND `lang` = '" . mres($lang) . "' LIMIT 0,1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		
		return true;
	}
	
	return false;
}

/**
 * Redirect to default site page
 * 
 */
function openDefaultPage() {
    
	$pageId = getDefaultPageId(getS('ad_language'));

	if ($pageId) {
		redirect(getLink($pageId), true);
	} else {
		showError("ERROR! Have not founded default page!");
	}
	
}


/**
 * Getting default site language page id
 */
function getDefaultPageId($lang = '') {
	$mdb = &loadLibClass('db');
	
	if ($lang && $id = getLangMainPage($lang)) {
		return $id;
	}
	
	$dbQuery = "SELECT `main_id` FROM `ad_languages_to_ct` WHERE `default` = '1' AND `country_id` = '" . getCountry() . "'";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$id = $query->getOne();
		if (!$id) {
			showError("ERROR! Have not founded default page!");
		}
		
		return $id;
	} else {
		showError("ERROR! Have not founded default page!");
	}
}

/**
 * Getting default page url
 */
function getDefaultPageUrl() {
	$mdb = &loadLibClass('db');
	
	if (getS('ad_language') && getS('ad_language') != getDefaultLang() && $id = getLangMainPage(getS('ad_language'))) {
		
		redirect(getLink($id));
	}
	
	$dbQuery = "SELECT `url` FROM `ad_languages_to_ct` ct, `ad_content` c WHERE 
				`ct`.`default` = '1' AND `ct`.`country_id` = '" . getCountry() . "'
				AND `c`.`id` = `ct`.`main_id`";
        $query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		$url = $query->getOne();
		if (!$url) {
			showError("ERROR! Have not founded default page!");
		}
		
		return makeUrlWithLangInTheEnd($url, false);
	} else {
		showError("ERROR! Have not founded default page!");
	}
}

/**
 * Get page url by id
 * 
 * @param int	page id
 */
function getLink($id) {
	$mdb = &loadLibClass('db');
	
	$dbQuery = "SELECT `url` FROM `ad_content` WHERE `id` = '" . $id . "' LIMIT 0,1";
	$query = new query($mdb, $dbQuery);

	if ($query->num_rows() > 0) {
		
		return AD_WEB_FOLDER . makeUrlWithLangInTheEnd($query->getOne());
	}
	else {
		logWarn("ERROR! Have not founded this page! ID: " . $id);
	}
}

/**
 * Get page url by id
 * 
 * @param int	page id
 */
function getDocUrl($id) {
	$mdb = &loadLibClass('db');
	
	$dbQuery = "SELECT `page_url` FROM `mod_news` WHERE `id` = '" . $id . "' LIMIT 0,1";
	$query = new query($mdb, $dbQuery);

	if ($query->num_rows() > 0) {
		
		return $query->getOne();
	}
	
}
     
/**
 * Get page mirror
 * 
 * @param int		page id
 * @param int		country id
 * @param string	language
 */
function getMirror($id, $c = '', $l = '') {
	$mdb = &loadLibClass('db');
	$m = &loadLibClass('mirrors');
	
	$c = $c ? $c : (class_exists('Module') ? Module :: getCountry() : getDefLangInCountry($c));
	$l = $l ? $l : (class_exists('Module') ? Module :: getLang() : getDefLangInCountry($c));

	if (@isset($m->alias[$id]) && @isset($m->mirrors[$m->alias[$id]][$c][$l]["id"])) {
		return $m->mirrors[$m->alias[$id]][$c][$l]["id"];
	}
	else {
		
		$dbQuery = "
			SELECT `mirror_id` 
			FROM `ad_content` 
			WHERE 
				`id` = '" . $id . "' AND `mirror_id` <> ''
		";
		$query = new query($mdb, $dbQuery);

		if ($query->num_rows() > 0) {
			
			$m->alias[$id] = $query->getOne();
			
			$dbQuery = "
				SELECT `id`, `url` 
				FROM `ad_content` 
				WHERE 
					`mirror_id` = '" . $m->alias[$id] . "'
					AND `lang` = '" . $l . "'
					AND `country` = '" . $c . "' LIMIT 1";
			$query = new query($mdb, $dbQuery);

			if ($query->num_rows() > 0) {
				$query->getrow();
				
				$m->mirrors[$m->alias[$id]][$c][$l]["id"] = $query->field("id");
				$m->mirrors[$m->alias[$id]][$c][$l]["url"] = makeUrlWithLangInTheEnd($query->field("url"));
				
				return $query->field("id");
			}
			else {
				return $id;
			}
		}
		else {
			return $id;
		}
	}
}

/**
 * Make url with lang in the end
 * if enabled this
 * 
 * @param string	url
 */
function makeUrlWithLangInTheEnd($url, $remove = true) {
	$cfg = &loadLibClass('config');
	
	if ($cfg->get("langInTheEnd")) {
		
		if ($url[strlen($url) - 1] == '/') {
			$url = substr($url, 0, -1);
		}
		
		$url = explode("/", $url);
		
		if ($url[0] == $cfg->get("defaultLang") && $remove) {
			$lang = '';
			
		} else {
			$lang = $url[0] . '/';
		}
		
		unset($url[0]);
		
		$path = '';
		if (count($url) > 0) {
			$path = implode("/", $url) . '/';
		}
		
		return $path . $lang;
		
	} else {
		return $url;
	}
}

/**
 * Get link by mirror
 * 
 * @param int		page id
 * @param int		country id
 * @param string	language
 */
function getLM($id, $c = '', $l = '') {
	
	return getLink(getMirror($id, $c, $l));
}

/**
 * Get link by mirror
 * Used only in smarty. Collect all ids in array. 
 * In the end assign all URL's
 * 
 * @param int		page id
 */
function getLinkByMirror($id) {
	
	if ($id) {
		if (!in_array($id, Module :: $linkIds)) {
			Module :: $linkIds[] = $id;
		}
		return '{{' . $id . '}}';
	}
	
}

/**
 * Get default country link
 * 
 * @param int	country id
 */
function getDCountryLink($id, $ssl = '') {
	$mdb = &loadLibClass('db');
	
	$dbQuery = "SELECT `domain` FROM `ad_countries_domains` WHERE `country_id` = '" . $id . "' AND `default` = '1' LIMIT 0,1";
	$query = new query($mdb, $dbQuery);
	
	return ($ssl ? 'https://' : 'http://') . $query->getOne();
}

/**
 * Get language main page
 * 
 * @param string 	language value
 */
function getLangMainPage($lang) {
	$mdb = &loadLibClass('db');
	
	$dbQuery = "SELECT `main_id` FROM `ad_languages` l, `ad_languages_to_ct` lc WHERE lc.lang_id = l.id  AND lc.country_id = '" . getCountry() . "' AND l.lang = '" . $lang . "' AND l.enable = '1' LIMIT 0,1";
	$query = new query($mdb, $dbQuery);
	if ($query->num_rows() > 0) {
		return $query->getOne();
	}
	return false;
}

/**
 * Get current country
 * 
 */
function getCountry() {
	$mdb = &loadLibClass('db');
	
	$dbQuery = "SELECT `country_id` FROM `ad_countries_domains` WHERE `domain` = '" . (isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : "") . "' ORDER BY `id` ASC LIMIT 1";
	$query = new query($mdb, $dbQuery);
	
	return $query->getOne();
}


/**
 * Print out value from siteData. If given name exist in db gets it, if name doesn't exist function will create one
 * @param string $name Field naem
 * @param string $tab Tab where to put this field
 * @param string $block Blpck where to put this field
 * @param string $value = '' Field value for all languages
 * @param string $type = 'text' Field type f.e. text textarea checkbox radio etc
 * @param int $mlang = '0' Multilanguage support for current field on = 1, off = 0
 * @return string $value Current field value
 * @author Maris Melnikovs <maris.melnikovs@efumo.lv>
 */
function getSiteData($name, $tab, $block, $value = '', $type = 'text', $mlang = '0', $required = '0', $title = ''){

	if (!empty($name) || !empty($tab) || !empty($block)) {
		$db = &loadLibClass('db');
		$check = new query($db, "SELECT `id` FROM `ad_sitedata` WHERE `name` = '$name'");
		
		if ($check->num_rows() > 0) {
			$id = $check->getOne();
			if ($mlang == true) {
				$select = new query($db, "SELECT `value` FROM `ad_sitedata_values` WHERE `fid` = '". $id ."' AND `lang` = '". Module :: getLang() ."'");
			}
			else {
				$select = new query($db, "SELECT `value` FROM `ad_sitedata_values` WHERE `fid` = '". $id ."'");
			}
			$select->num_rows();
			$value = $select->getOne();
			return $value;
		}
		else {
			$type = (empty($type)) ? 'text' : $type;
			$insert = new query($db, "
				INSERT INTO `ad_sitedata`
				( `name`, `tab`, `block`, `title`, `type`, `mlang`, `required`)
				VALUES( '".$name."', '".$tab."', '".$block."', '".$title."', '".$type."', '".$mlang."', '".$required."')
			");
			$last_id = $db->get_insert_id();

			if ($mlang == true) {
				$values_query = "INSERT INTO `ad_sitedata_values` ( `fid`, `lang`, `value`) VALUES";
				$count = 1;
				foreach (getSiteLangs() as $lng_key => $lng_value) {
					$values_query .= "('". $last_id ."', '". $lng_value['lang'] ."', '".$value."')";
					if($count < count(getSiteLangs())) {
						$values_query .= ", ";
					}
					$count++;
				}
				$values = new query($db, $values_query);
			}
			else {
				$values = new query($db, "
					INSERT INTO `ad_sitedata_values`
					( `fid`,  `value`)
					VALUES('". $last_id ."', '".$value."')
				");
			}

			return $value;
		}
	}
	else {
		return false;
	}
	
}


/**
 *  Get languages from database
 * @author Rolands Eņģelis <rolands@efumo.lv>
 * @return array
 */
function getLanguagesForAdmin($return_keys=false){
    $languages=array();
    global $mdb;
    $dbQuery="SELECT `id`, `lang`, `title` FROM `ad_languages` WHERE `enable`='1' ORDER BY sort ASC";
    $query=new query($mdb, $dbQuery);
    $languages_data=$query->getArray();
    if($languages_data){
        foreach($languages_data as $l){
            $key=$return_keys!==false ? $l["lang"] : $l["id"]; 
            $languages[$key]=$l["title"];
        }
    }
    return $languages;
}
?>