<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/** 
 * Adweb configuration file
 * Main config file 
 * Require all need config files
 * Run and load all needed classes and functions 
 * 15.02.2010
 */

/** 
 * Including user configuration file.
 * This file will be created automatic after installing Adweb
 */ 
if (file_exists(dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.user.php")) {
	require_once("config.user.php");
} else {
	header('Location: /install/');
}

/**
 * Set Default time zone.
 */
date_default_timezone_set("America/Chicago");

/** 
 * Including contstants configuration file.
 */ 
require_once("config.constants.php");

/**
 * Including specific project configuration file.
 */
require_once('config.custom.php');


/** 
 * Including contstants configuration file.
 */ 
require_once(AD_FUNC_FOLDER . "common.func.php");

/** 
 * loading Adweb base class
 */ 
loadLibClass('base');

/** 
 * loading Adweb worting time class
 */
$wk = &loadLibClass('workTime');
$wk->mark("start");

/** 
 * loading Adweb main config class
 */
$cfg = &loadLibClass('config', true, $config);

/** 
 * loading main functions
 */
loadFunc("functions");
loadFunc("site");

/** 
 * loading db class
 * and connecting to database
 */
$mdb = &loadLibClass('db');
$mdb->open($cfg->get("db_db"), $cfg->get("db_host"), $cfg->get("db_user"), $cfg->get("db_password"));

/**
 * loading smarty class but not init
 */
loadLibClass('smarty', false);

if (in_array(getIp(), $cfg->get('debugIp'))) { 
	ini_set("display_errors", 1);
	ini_set("track_errors", 1);
	error_reporting(E_ALL);
}




define("AD_IMAGE_FOLDER",  AD_IMAGE_FOLDER_MAIN . getCountry() . "/");
define("AD_SERVER_IMAGE_FOLDER", AD_SRV_ROOT . AD_IMAGE_FOLDER);

?>