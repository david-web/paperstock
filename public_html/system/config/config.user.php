<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/** 
 * ADWEB
 * User configuration file.
 * 15.02.2010
 */

define("AD_WEB_FOLDER", "/");
define("AD_MAINPAGE_MIRROR_ID", 4);
define("RESULT_PAGE_ID_OK", 48);
define("RESULT_PAGE_ID_ERROR", 48);


/** 
 * Database configuration
 */



if(isDEMO()) {
	
	$config["db_host"] = "localhost";
	$config["db_db"] = "paper";
	$config["db_user"] = "paper";
	$config["db_password"] = "XvcaHpHU2f";

} elseif (isLOCAL()) {
	
  $config["db_host"] = "localhost";
	$config["db_db"] = "papersst_writers";
	$config["db_user"] = "root";
	$config["db_password"] = "";
        
	
} else {
	
	// PROD configurations
	$config["db_host"] = "localhost";
	$config["db_db"] = "papersst_writers";
	$config["db_user"] = "papersst_writers";
	$config["db_password"] = "8fdgh778";
        
        error_reporting(0);
}

//echo "db_pass: ".$config["db_password"];die();


/**
 * 
 * Check if demo server
 */
function isDEMO() {

	if(isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == '95.85.54.160')) {
		return true;
	}

	return false;
}

/**
 * 
 * Check if local server
 */
function isLOCAL() {

	if(isset($_SERVER['SERVER_ADDR']) && in_array($_SERVER['SERVER_ADDR'], array('127.0.0.1', '127.1.0.1', '127.0.1.1'))) {
		return true;
	}

	return false;
}


/** 
 * open sessions
 */
session_start();

/** 
 * some servers not support $_SERVER['REDIRECT_URL']
 */
if (isset($_SERVER['QUERY_STRING']) && isset($_SERVER['REQUEST_URI'])) {
	$_SERVER['REDIRECT_URL'] = str_replace('?' . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
}

?>