<?php

global $config;

$config['support_email'] = 'david@beattum.com';

// module content pages - ID`s
$config['module_pages'] = array(
);

$config['payment_step_pages'] = array(
);

// Module image sizes
$config['image_config'] = array(
    'news' => array(
        'big' => array(
            'width' => '700',
            'height' => '296',
            'upload_path' => 'news/700x296/'
        ),
        'small' => array(
            'width' => '220',
            'height' => '110',
            'upload_path' => 'news/220x110/'
        ),
        'original' => array(
            'upload_path' => 'news/original/'
        )
    ),
    'promoblock' => array(
        'original' => array(
            'upload_path' => 'promoblock/original/'
        ),
        
    ),
    'countries' => array(
        'original' => array(
            'upload_path' => 'countries/original/'
        )
    )

);

$current_dir = dirname(__FILE__) . '/';
require_once($current_dir . 'config.custom_dbtables.php');
require_once($current_dir . 'config.payments.php');
