<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		Dailis Tukans <dailis@efumo.lv>
 * @copyright           Copyright (c) 2011, Efumo.
 * @link		http://adweb.lv
 * @version		1
 */

// ------------------------------------------------------------------------

/** 
 * ADWEB
 * Payments configuration file.
 * 21.10.2011
 */

/**
 * Path to payments lib
 */
define("PAYMENTS_FOLDER", dirname(__FILE__) . "/../lib/payments/");

/**
 * Use SSL
 */
if (isDEMO() || isLOCAL()) {
    define("PAYMENTS_SSL", "0");
    define("PAYPAL_ACCOUNT", "david@beattum.com");
    define("PAYPAL_SANDBOX", 1);
}
else {
    define("PAYMENTS_SSL", "0");
    define("PAYPAL_ACCOUNT", "david@beattum.com");
    define("PAYPAL_SANDBOX", 1);
}



?>