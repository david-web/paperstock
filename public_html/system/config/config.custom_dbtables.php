<?php

// Database tables based on module name
$config['db_tables'] = array(

    // content
    'content'           => 'ad_content',
    'mop'               => 'ad_modules_on_page',
    'modules'           => 'ad_modules',

    // news
    'news'               => 'mod_news',
	'faq'               => 'mod_faq',

    //Blog 
    'blog'               => 'mod_blog',

    //Users
    'users' => array(
        'self' => 'ad_users',
        'roles' => 'ad_user_roles'
    ),
		
	//Banners
	'banners' => 'mod_banners',

    //Clients
    'profiles' => 'mod_profiles',
	'writers' => 'mod_writers',

    //Coments
    'comments' => 'mod_comments',

    // languages
    'languages'         => 'ad_languages',
    
    //Promoblocks
    'promoblock' => array(
        'self'         => 'mod_promo_blocks',
    ),
     'subscribers' => array(
        'self'         => 'mod_subscribers',
        'types'        => 'mod_subscribers_type'
    ),


	
    'forms' => array(
        'self' => 'mod_forms',
        'data' => 'mod_forms_data',
        'values' => 'mod_forms_values',
    	'values_data' => 'mod_forms_values_data'
    ),
    
    'orders' => array(
    	'self' => 'mod_orders',
    	'messages' => 'mod_orders_messages',
    	'files' => 'mod_orders_files',
    	'writers_rates' => 'mod_writers_rates',
    	'support_rates' => 'mod_support_rates',
    	'writers' => 'mod_writers_to_orders',
    ),
    
    'tests' => array(
    	'self' => 'mod_tests',
   		'data' => 'mod_tests_data',
   		'writers' => 'mod_writers_tests',
    ),
    
    'payments' => 'pproc_pp_transaction',

    
);

?>