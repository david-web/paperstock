var profile = {
	
	langs : {},
	
	registerInquiry : function(step) {
		
		sendData = {};
		sendData['fieldsInquiry'] = order.getInquiryForm();
		
		ajaxRequest('/profile/saveInquiry/', sendData, function(data) {
            
			if (data.errors) {
				order.setErrors(data.errors);
			} else {
				
				window.location.href = data.location;	
				
			}
			
		});
	},
	
	previewComment : function() {
		if ($.trim($('#preview_comment').val())) {
			sendData = {};
			sendData['preview_comment'] = $('#preview_comment').val();
			
			ajaxRequest('/profile/previewComment/', sendData, function(data) {
	            
				if (data) {
					$('.popup').remove();
					$('.popup_bg').remove();
					
					window.location.reload();
				}
				
			});
		}
	},
	
	previewOrder : function($file) {
		$('.popup').remove();
		$('.popup_bg').remove();
		
		sendData = {};
		sendData['filename'] = $file;
		
		ajaxRequest('/profile/previewOrder/', sendData, function(data) {
            
			if (data) {
				$('body').append(data);
			}
			
		});
	},
	
	acceptOrder : function () {
		
		ajaxRequest('/profile/acceptOrder/', sendData, function(data) {
            
			if (data) {
				$('.popup').remove();
				$('.popup_bg').remove();
				
				window.location.reload();
			}
			
		});
		
	},
	
	rateOrder : function () {
		
		if ($('input:radio[name=support]:checked').val() && $('input:radio[name=writers]:checked').val() &&
				$('input:radio[name=support]:checked').val() != 'undefined' && $('input:radio[name=writers]:checked').val() != 'undefined') {
					
			sendData = {};
			sendData['support'] = $('input:radio[name=support]:checked').val();
			sendData['writers'] = $('input:radio[name=writers]:checked').val();

			ajaxRequest('/profile/rateOrder/', sendData, function(data) {
	            
				$('.popup').remove();
				$('.popup_bg').remove();
				location.reload();
				
			});
		}
	},
	
	uploadFile : function () {
		if ($.trim($('#uploadButtonFile').val())) {
			sendData = {};
			sendData['file'] = $('#uploadButtonFile').val();
			
			ajaxRequest('/profile/uploadFile/', sendData, function(data) {
	            
				location.reload();
				
			});
		}
	},
	
	orderNewMessage : function () {
		if ($.trim($('#message').val())) {
			
			sendData = {};
			sendData['message'] = $('#message').val();
			
			ajaxRequest('/profile/orderNewMessage/', sendData, function(data) {
	            
				location.reload();
				
			});
			
			
		}
	},
	
	getOrderRatesForm : function ($id) {
		
		$('.popup').remove();
		$('.popup_bg').remove();
		
		sendData = {};
		if ($id) {
			sendData['orderID'] = $id;
		}
		
		ajaxRequest('/profile/getOrderRatesForm/', sendData, function(data) {
            
			if (data) {
				$('body').append(data);
			}
			
		});
	},
	
	changePassword : function () {
		
		$('.popup').remove();
		$('.popup_bg').remove();
		
		ajaxRequest('/profile/changePassword/', '', function(data) {
            
			if (data) {
				$('body').append(data);
			}
			
		});
	},
	
	editAdditionalInfo : function () {
		$('.popup').remove();
		$('.popup_bg').remove();
		
		ajaxRequest('/profile/editAdditionalInfo/', '', function(data) {
            
			if (data) {
				$('body').append(data);
				loadHtmlForm();
			}
			
		});
	},
	
	setNewPassword : function () {
		
		profile.removeErrors();
		
		sendData = {};
		$('.password').each(function(n, element) {
			
			sendData[$(this).attr('id')] = $(this).val();
			
		});
		
		ajaxRequest('/profile/setNewPassword/', sendData, function(data) {
            
			if (data.errors) {
				profile.setErrors(data.errors);
			} else {
				$('.popup').remove();
				$('.popup_bg').remove();
			}
			
		});
	},
	
	saveExtraInfo : function () {
		
		profile.removeErrors();
		
		sendData = {};
		$('.extrainfo').each(function(n, element) {
			
			sendData[$(this).attr('id')] = $(this).val();
			
		});
		
		ajaxRequest('/profile/saveExtraInfo/', sendData, function(data) {
            
			if (data.errors) {
				profile.setErrors(data.errors);
			} else {
				window.location.reload();
			}
			
		});
	},
	
	profileRegister : function () {
		
		order.removeErrors();
		
		sendData = {};
		sendData['action'] = 'register';
		sendData['fields'] = {};
		$('.order_form input.register,select.register,checkbox.register').each(function(n, element) {
			
			if ($(this).attr('type') == 'checkbox') {
				
				if ($(this).is(':checked')) {
					sendData['fields'][$(this).attr('id')] = $(this).val();
				} else {
					sendData['fields'][$(this).attr('id')] = '0';
				}
				
			} else {
				sendData['fields'][$(this).attr('id')] = $(this).val();
			}
			
		});
		
		ajaxRequest('/profile/register/', sendData, function(data) {
            
			if (data.errors) {
				order.setErrors(data.errors);
			} else {
				window.location.href = data.location;
			}
			
		});
		
	},
	
	profileLogin : function () {
		
		order.removeErrors();
		
		sendData = {};
		sendData['action'] = 'login';
		sendData['fields'] = {};
		$('.order_form input.login,select.login,checkbox.login').each(function(n, element) {
			
			sendData['fields'][$(this).attr('id')] = $(this).val();
			
		});
		
		ajaxRequest('/profile/login/', sendData, function(data) {
            
			if (data.errors) {
				order.setErrors(data.errors);
			} else {
				window.location.href = data.location;
			}
			
		});
		
	},
	
	passwordReminder : function () {
		
		profile.removeErrors();
		
		if ($('#password_reminder').val()) {
			sendData = {};
			sendData['email'] = $('#password_reminder').val();
			
			ajaxRequest('/profile/passwordReminder/', sendData, function(data) {
	            
				if (data.errors) {
					order.setErrors(data.errors);
				} else {
					$('.forgot_pw_block').find('.field').hide();
					$('.forgot_pw_block').find('.info_submitted_msg').show();
				}
				
			});
		}
		
		
		
	},
	
	removeErrors : function () {
		$('.wrong.default.invalid').each(function(n, element) {
			$(this).removeClass('wrong').removeClass('wrong').removeClass('invalid');
		});
		$('div.invalid_msg').remove();
	}, 
	
	setErrors : function (errors) {
		if (typeof(errors.fields) != 'undefined') {
			$.each(errors.fields, function( index, value ) {
				if ($('#' + index).attr('type') == 'text' || $('#' + index).attr('type') == 'password' || $('#' + index).is("textarea")) {
					  $('#' + index).parent().addClass('wrong').addClass('default').addClass('invalid');
					  $('#' + index).parent().parent().append('<div class="invalid_msg">' + value + '</div>');
				  } else if ($('#' + index).attr('type') == 'checkbox') {
					  $('#' + index).parent().parent().append('<div class="invalid_msg">' + value + '</div>');
				  } else {  
					  $('#' + index).addClass('wrong').next().addClass('wrong');
					  $('#' + index).parent().append('<div class="invalid_msg">' + value + '</div>');
				  }
				});
		}
		
		
		if (typeof(errors.global) != 'undefined') {
			alert(errors.global);
		}
	},
    
};
