var order = {
	
	langs : {},
	calcPrice : true,
	
	changeDeadlineNotice : function () {
		if ($('#mob_18').val()) {
			sendData = {};
			sendData['deadline'] = $('#mob_18').val();
			
			ajaxRequest('/orders/changeDeadlineNotice/', sendData, function(info) {
	            //console.log(info);
	            
	            if (info) {
	            	$('#deadline1').html(info.deadline1);
	            	$('#deadline2').html(info.deadline2);
	            }
			});
		}
	},
	
	calculatePrice : function () {
		sendData = {};
		sendData['fields'] = {};

		$('.calcprice').each(function(n, element) {
			
			if (typeof($(this).attr('id')) != 'undefined') {
				
				if ($(this).is("select")) {
					
					$selEl = $(this).find('option:selected');
					
					if ($selEl.data('groupid')) {
						sendData['fields'][$(this).attr('id') + '-groupid'] = $selEl.data('groupid');
						sendData['fields'][$(this).attr('id')] = $(this).val();
					} else {
						sendData['fields'][$(this).attr('id')] = $(this).val();
					}
					
				} else {
					sendData['fields'][$(this).attr('id')] = $(this).val();
				}
				
			}
		
		});
		
		if ($('.extra').length > 0) {
			sendData['fields']['additional_extras'] = {};
			
			$('.extra').each(function(n, element) {
				
				if (typeof($(this).attr('id')) != 'undefined') {
					
					if ($(this).is(":checked")) {
						
						sendData['fields']['additional_extras'][$(this).attr('id')] = $(this).attr('id');
						
					}
					
				}
			
			});
			if (Object.keys(sendData['fields']['additional_extras']).length < 1) {
				sendData['fields']['additional_extras'] = '';
			}
		}

		if (order.calcPrice) {
			
			order.calcPrice = true;
			
			ajaxRequest('/orders/calculatePrice/', sendData, function(info) {
	            //console.log(info);
	            
	            if (info) {
	            	$('#priceTotal').html(info);
	            	$('#priceTotal2').html(info);
	            }
			});
		}
		
	},
	
	changeStep : function(step) {
		
		order.removeErrors();
		
		sendData = {};
		sendData['step'] = step;
		sendData['fields'] = {};
		$('.order_form input,select,checkbox,textarea').each(function(n, element) {
			
			if ($(this).hasClass('extra')) {
				
				if ($(this).is(':checked')) {
					if (typeof(sendData['fields'][$(this).attr('name')]) == 'undefined') {
						sendData['fields'][$(this).attr('name')] = {};
					}
					
					sendData['fields'][$(this).attr('name')][$(this).attr('id')] = $(this).attr('id');
				}
				
			} else {
				sendData['fields'][$(this).attr('id')] = $(this).val();
			}
			
		});
		
		ajaxRequest('/orders/checkData/', sendData, function(data) {
            
			if (data.errors) {
				
				order.setErrors(data.errors);
				
			} else {
				sendData = {};

				ajaxRequest('/orders/changeStep/', sendData, function(info) {
		            if (typeof(info.header) != 'undefined') {
		            	$('#orderHeader').html(info.header);
		            }
		            if (typeof(info.body) != 'undefined') {
		            	$('#orderBody').html(info.body);
		            	
		            	$('html, body').animate({
		                    scrollTop: $(".form_steps").offset().top
		                }, 500);
		            }
		            
		            // HTML
		            loadHtmlForm();
				});
			}
			
		});
		
		
	},
	
	getInquiryForm : function() {
		order.removeErrors();
		
		fields = {};
		fields = {};
		$('.inquiry').each(function(n, element) {
			
			
			fields[$(this).attr('id')] = $(this).val();

			
		});
		
		return fields;
	},
	
	loginInquiry : function(step) {
		
		
		sendData = {};
		sendData['action'] = 'login';
		sendData['fields'] = {};
		$('.order_form input.login,select.login,checkbox.login').each(function(n, element) {
			
			sendData['fields'][$(this).attr('id')] = $(this).val();
			
		});
		
		sendData['fieldsInquiry'] = order.getInquiryForm();
		console.log(sendData);
		
		ajaxRequest('/orders/saveInquiry/', sendData, function(data) {
            
			if (data.errors) {
				order.setErrors(data.errors);
			} else {
				
				window.location.href = data.location;	
				
			}
			
		});
	},
	
	registerInquiry : function(step) {
		
		
		sendData = {};
		sendData['action'] = 'register';
		sendData['fields'] = {};
		$('.order_form input.register,select.register,checkbox.register').each(function(n, element) {
			
			if ($(this).attr('type') == 'checkbox') {
				
				if ($(this).is(':checked')) {
					sendData['fields'][$(this).attr('id')] = $(this).val();
				} else {
					sendData['fields'][$(this).attr('id')] = '0';
				}
				
			} else {
				sendData['fields'][$(this).attr('id')] = $(this).val();
			}
			
		});
		
		sendData['fieldsInquiry'] = order.getInquiryForm();
		
		ajaxRequest('/orders/saveInquiry/', sendData, function(data) {
            
			if (data.errors) {
				order.setErrors(data.errors);
			} else {
				
				window.location.href = data.location;	
				
			}
			
		});
	},
	
	profileRegister : function (step) {
		
		order.removeErrors();
		
		sendData = {};
		sendData['step'] = step;
		sendData['action'] = 'register';
		sendData['fields'] = {};
		$('.order_form input.register,select.register,checkbox.register').each(function(n, element) {
			
			if ($(this).attr('type') == 'checkbox') {
				
				if ($(this).is(':checked')) {
					sendData['fields'][$(this).attr('id')] = $(this).val();
				} else {
					sendData['fields'][$(this).attr('id')] = '0';
				}
				
			} else {
				sendData['fields'][$(this).attr('id')] = $(this).val();
			}
			
		});
		
		ajaxRequest('/orders/checkData/', sendData, function(data) {
            
			if (data.errors) {
				order.setErrors(data.errors);
			} else {
				sendData = {};

				ajaxRequest('/orders/changeStep/', sendData, function(info) {
		            if (typeof(info.header) != 'undefined') {
		            	$('#orderHeader').html(info.header);
		            }
		            if (typeof(info.body) != 'undefined') {
		            	$('#orderBody').html(info.body);
		            }
		            
		            // HTML
		            loadHtmlForm();
				});
			}
			
		});
		
	},
	
	profileLogin : function (step) {
		
		order.removeErrors();
		
		sendData = {};
		sendData['step'] = step;
		sendData['action'] = 'login';
		sendData['fields'] = {};
		$('.order_form input.login,select.login,checkbox.login').each(function(n, element) {
			
			sendData['fields'][$(this).attr('id')] = $(this).val();
			
		});
		
		ajaxRequest('/orders/checkData/', sendData, function(data) {
            
			if (data.errors) {
				order.setErrors(data.errors);
			} else {
				sendData = {};

				ajaxRequest('/orders/changeStep/', sendData, function(info) {
		            if (typeof(info.header) != 'undefined') {
		            	$('#orderHeader').html(info.header);
		            }
		            if (typeof(info.body) != 'undefined') {
		            	$('#orderBody').html(info.body);
		            }
		            
		            // HTML
		            loadHtmlForm();
				});
			}
			
		});
		
	},
	
	removeErrors : function () {
		$('.wrong.default.invalid').each(function(n, element) {
			$(this).removeClass('wrong').removeClass('wrong').removeClass('invalid');
		});
		$('div.invalid_msg').remove();
	}, 
	
	setErrors : function (errors) {
		if (typeof(errors.fields) != 'undefined') {
			$.each(errors.fields, function( index, value ) {
				  if ($('#' + index).attr('type') == 'text' || $('#' + index).attr('type') == 'password' || $('#' + index).is("textarea")) {
					  $('#' + index).parent().addClass('wrong').addClass('default').addClass('invalid');
					  $('#' + index).parent().parent().append('<div class="invalid_msg">' + value + '</div>');
				  } else if ($('#' + index).attr('type') == 'checkbox') {
					  $('#' + index).parent().parent().append('<div class="invalid_msg">' + value + '</div>');
				  } else {  
					  $('#' + index).addClass('wrong').next().addClass('wrong');
					  $('#' + index).parent().append('<div class="invalid_msg">' + value + '</div>');
				  }
				});
		}
		
		
		if (typeof(errors.global) != 'undefined') {
			alert(errors.global);
		}
	},
    
};
