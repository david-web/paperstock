function ajaxRequest(url, data, processor) {	
	
	if (typeof(webLang) != 'undefined' && webLang != '') {
		if (typeof(data) == 'object') {
			data['webLang'] = webLang;
		} 
		
		if (typeof(data) == 'string') {
			data += '&webLang=' + webLang;
		}
	}
	
	return $.post(url, data, processor, "json");
}

function evalJson(json){
	return eval('(' + json + ')');
}

function isFieldEmpty(field) {
	if ($(field).value == "") {
		$(field).focus();
		return true;
	}
	return false;
}

function isFieldEmail(field) {
	if (!/[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]{2,4}$/.test($(field).val())) {
		return true;
	}
	return false;
}

function editPencil(name, url) {
	ajaxRequest(url, "name=" + name, function (data) {
		
	});
}

function pausecomp(millis)
{
 var date = new Date();
 var curDate = null;
 do { curDate = new Date(); }
 while(curDate-date < millis);
}

function loadHtmlForm() {
	$('.selectpicker').selectpicker();
    $('select.selectpicker.default').change(function(){
		$(this).next().removeClass('default');
	});
    horizontal_select();
	checkbox_block();
	$('.horizontal_select .option').click(function(){
		if($(this).closest('.select_group').length)
		{
			$(this).closest('.select_group').find('.horizontal_select>.option').not($(this)).removeClass('active');
		}
		else
		{
			$(this).closest('.horizontal_select').find('>.option').not($(this)).removeClass('active');
		}
		$(this).addClass('active');
		if(typeof($(this).closest('.horizontal_select').data('linkto'))!='undefined')
		{
			var c=$($(this).closest('.horizontal_select').data('linkto')).find('option');
			while(c.length)
			{
				if(c.first().val()==$(this).data('value'))
				{
					c.first().parent().val(c.first().val()).trigger('change');
					$('.selectpicker').selectpicker('refresh');
					break;
				}
				else
				{
					c=c.not(c.first());
				}
			}
		}
	});
	
	$('.horizontal_dropdown select').change(function(){
		if(typeof($(this).data('linkto'))!='undefined')
		{
			var c=$($(this).data('linkto')).find('.option');
			while(c.length)
			{
				if(c.first().data('value')==$(this).val())
				{
					$($(this).data('linkto')).find('.option').removeClass('active');
					c.first().addClass('active');
					break;
				}
				else
				{
					c=c.not(c.first());
				}
			}
		}
	});
	
	$('select[data-hide]').change(function(){
		$($(this).data('hide')).hide();
		$($(this).find('option[value='+$(this).val()+']').data('show')).show();
	});
	
	cinput($('.cinput'));
	
	$('.checkbox_block .option').click(function(){
		var th=$(this),cb=$('input[type=checkbox]',th);
		th.toggleClass('active');
		if(th.hasClass('active'))cb.prop('checked',true);
		else cb.prop('checked',false);
		cb.trigger('change');
	});
	
	$('.ctabs').each(function(){
		var c=this;
		if($('.tab_top.active',c).length!=1)
		{
			$('.tab_top',c).removeClass('active').first().addClass('active');
		}
		$('.tab_bottom.active',c).removeClass('active');
		$('.tab_bottom[data-tab='+$('.tab_top.active',c).data('tab')+']',c).addClass('active');
		$('.tab_top',c).click(function(){
			if(!$(this).hasClass('active'))
			{
				$('.tab_top.active',c).removeClass('active');
				$(this).addClass('active');
				$('.tab_bottom.active',c).removeClass('active');
				$('.tab_bottom[data-tab='+$(this).data('tab')+']',c).addClass('active');
			}
		});
	});
	
	$('.styled[type=checkbox], .styled[type=radio]').stylecb();
}

function loadUploadButton(folder, element) {
	
	if ($('#' + element).length > 0) {
		var button = $('#' + element), interval;

		$.ajax_upload(button, {
					action : '/profile/upload/?folder=' + folder,
					name : 'uploadFile',
					onSubmit : function(file, ext) {				
						//$("<img id='imgLoading' src='/admin/images/design/loading.gif'>").appendTo("#" + element);
						this.disable();

					},
					onComplete : function(file, response) {

						this.enable();

					},
					
					onSuccess : function ( response ) {
						
						$('#' + element + 'File').val(response.info.file_name);
						$('#' + element + 'File').change();
					},
					onError : function ( response ) {
						alert(response.errorMsg);
					}
				});
	}
	
	
}

function ordersSort($url) {
	if ($('#ordersSort').val()) {
		window.location.href = $url + 'sort:' + $('#ordersSort').val() + '/';
	}
}

function orderFilter(e, $url) {
	if ($(e).val()) {
		window.location.href = $url + 'status:' + $(e).val() + '/';
	}
}
