function ver()
{
	if($('.ver_320').css('display')!='none')return 3;
	else if($('.ver_768').css('display')!='none')return 2;
	return 1;
}
var v=ver();

$(document).ready(function(){
/*default script snippets*//*iphone delegation*/$('*').click(function(){});/*email _at_ replace*/$('a[href*="mailto"]').each(function(){var t=$(this),n=t.attr('href').replace('_at_','@'),m=t.text().replace('_at_','@');t.attr('href',n).text(m);});/*empty links disable*/$('[href="#"]').click(function(e){e.preventDefault();});/*css3pie*/if(window.PIE){$('.css3').each(function(){PIE.attach(this);});$(window).scroll(function(){updatePIEButtons()});}updatePIEButtons=function(){if(window.PIE){$('.css3').each(function(){PIE.detach(this);PIE.attach(this);});}};/*data-goto*/$('[data-goto]').click(function(e){e.preventDefault();$('html,body').stop(true).animate({'scrollTop':$('#'+$(this).data('goto')).first().offset().top+'px'},1000);});

	footer();
	whyus();
	var statistics_interval,statistics_container=$('.statistics .list_cont .button_cont');
	$('.button',statistics_container).removeClass('active').first().addClass('active');
	$('.button',statistics_container).click(function(){
		var c=$(this).closest('.list_cont'),i=$(this).index();
		$('.button',c).removeClass('active');
		$(this).addClass('active');
		$('.list',c).stop(true).animate({'left':-i*$('.list .item',c).first().outerWidth(true)});
	});
	statistics_interval=setInterval(function(){
		if(ver()==3)
		{
			if($('.button.active',statistics_container).next().length)
			{
				var c=statistics_container.closest('.list_cont'),i=$('.button.active',statistics_container).next().index(),ne=$('.button.active',statistics_container).next();
				$('.button',c).removeClass('active');
				ne.addClass('active');
				$('.list',c).stop(true).animate({'left':-i*$('.list .item',c).first().outerWidth(true)});
			}
			else
			{
				var c=statistics_container.closest('.list_cont'),i=$('.button',statistics_container).first().index(),ne=$('.button',statistics_container).first();
				$('.button',c).removeClass('active');
				ne.addClass('active');
				$('.list',c).stop(true).animate({'left':-i*$('.list .item',c).first().outerWidth(true)});
			}
		}
	},5000);
	$('.statistics').hover(function(){
		clearInterval(statistics_interval);
	},function(){
		statistics_interval=setInterval(function(){
			if(ver()==3)
			{
				if($('.button.active',statistics_container).next().length)
				{
					var c=statistics_container.closest('.list_cont'),i=$('.button.active',statistics_container).next().index(),ne=$('.button.active',statistics_container).next();
					$('.button',c).removeClass('active');
					ne.addClass('active');
					$('.list',c).stop(true).animate({'left':-i*$('.list .item',c).first().outerWidth(true)});
				}
				else
				{
					var c=statistics_container.closest('.list_cont'),i=$('.button',statistics_container).first().index(),ne=$('.button',statistics_container).first();
					$('.button',c).removeClass('active');
					ne.addClass('active');
					$('.list',c).stop(true).animate({'left':-i*$('.list .item',c).first().outerWidth(true)});
				}
			}
		},5000);
	});
	if($.isFunction($.fn.swipe))
	{
		$('.statistics').swipe({
			swipeLeft:function(){
				if($('.button.active',statistics_container).next().length)
				{
					$('.button.active',statistics_container).next().trigger('click');
				}
				else
				{
					$('.button',statistics_container).first().trigger('click');
				}
			},
			swipeRight:function(){
				if($('.button.active',statistics_container).prev().length)
				{
					$('.button.active',statistics_container).prev().trigger('click');
				}
				else
				{
					$('.button',statistics_container).last().trigger('click');
				}
			},
			threshold:40,
			excludedElements:$.fn.swipe.defaults.excludedElements+", .buttons"
		});
	}
	
	$('.startpage_slider').each(function(){
		var c=$(this),inner=$('.slider_cont .inner',c);
		$('.slider_cont .arrow.left',c).click(function(){
			if(!c.hasClass('animating'))
			{
				var todelete=$('.item',inner).last(),tomove=todelete.clone();
				tomove.css('margin-left',-todelete.outerWidth(true)).prependTo(inner).animate({'margin-left':todelete.css('margin-left')},function(){
					todelete.remove();
					tomove.css('margin-left','');
					c.removeClass('animating');
				});
			}
		});
		$('.slider_cont .arrow.right',c).click(function(){
			if(!c.hasClass('animating'))
			{
				var todelete=$('.item',inner).first(),tomove=todelete.clone();
				tomove.appendTo(inner);
				todelete.animate({'margin-left':-todelete.outerWidth(true)},function(){
					todelete.remove();
					c.removeClass('animating');
				});
			}
		});
	});
	
	$('.side_slider').each(function(){
		var c=$(this),inner=$('.slider_cont .inner',c);
		$('.slider_cont .arrow.left',c).click(function(){
			if(!c.hasClass('animating'))
			{
				var todelete=$('.item',inner).last(),tomove=todelete.clone();
				tomove.css('margin-left',-todelete.outerWidth(true)).prependTo(inner).animate({'margin-left':todelete.css('margin-left')},function(){
					todelete.remove();
					tomove.css('margin-left','');
					c.removeClass('animating');
				});
			}
		});
		$('.slider_cont .arrow.right',c).click(function(){
			if(!c.hasClass('animating'))
			{
				var todelete=$('.item',inner).first(),tomove=todelete.clone();
				tomove.appendTo(inner);
				todelete.animate({'margin-left':-todelete.outerWidth(true)},function(){
					todelete.remove();
					c.removeClass('animating');
				});
			}
		});
	});
	
	$('.selectpicker').selectpicker();
	$('.dropdown-menu>li').each(function(){
		if($('a .text',this).html().length<1)$(this).addClass('empty');
	});
	$('select.selectpicker.default').change(function(){
		$(this).next().removeClass('default');
	});
	
	$('.num_input').each(function(){
		var th=$(this);
		$('input',th).blur(function(){
			if(isNaN(parseInt($(this).val())))
			{
				$(this).val(1);
			}
			else
			{
				var a=parseInt($(this).val());
				if(a<1)a=1;
				$(this).val(a);
			}
		});
		$('.plus',th).click(function(){
			$('input',th).val(parseInt($('input',th).val())+1);
		});
		$('.minus',th).click(function(){
			var a=parseInt($('input',th).val())-1;
			if(a<1)a=1;
			$('input',th).val(a);
		});
	});
	
	$('.contacts_block .icon').click(function(e){
		if(ver()==1||ver()==2)
		{
			e.preventDefault();
			var c=$(this).closest('.button');
			if(c.hasClass('open'))
			{
				c.parent().removeClass('open').find('.button').removeClass('open');
			}
			else
			{
				c.parent().addClass('open').find('.button').removeClass('open');
				c.addClass('open');
			}
		}
	});
	
	$('.btn1,.btn2,.btn3,.btn4,.btn5,.btn6,.btn7,.btn8').each(function(){
		var a=$(this).html();
		$(this).html(a+'<div class="overshad">'+a+'</div>');
	});
	
	$('#menu1 .menu .has_submenu>a').click(function(){
		if($(this).parent().hasClass('open'))
		{
			$('#menu1 .menu .has_submenu.open').removeClass('open');
		}
		else
		{
			$('#menu1 .menu .has_submenu.open').removeClass('open');
			$(this).parent().addClass('open');
		}
	});
	
	$('#menu2 .trigger').click(function(){
		if($(this).parent().hasClass('open'))
		{
			var h=$('#menu2 .menu').stop(true).height();
			$('#menu2').removeClass('open');
			$('#menu2 .menu').css({'display':'block','height':h}).animate({'height':0},function(){
				$('#menu2 .menu').css({'display':'','height':''});
				$('#menu2 .menu .has_submenu').removeClass('open');
				$('#menu2 .menu .submenu').stop(true).css({'display':'','height':''});
				position_header();
			});
		}
		else
		{
			$('#menu2 .menu').stop(true);
			if($('#menu2 .has_submenu').length==1)
			{
				$('#menu2 .has_submenu').addClass('open');
			}
			var h=$('#menu2 .menu').height();
			$('#menu2').addClass('open');
			$('#menu2 .menu').height('');
			if(h==$('#menu2 .menu').height())h=0;
			var hto=$('#menu2 .menu').height();
			$('#menu2 .menu').css({'display':'block','height':h}).animate({'height':hto},function(){
				$('#menu2 .menu').css({'display':'','height':''});
				position_header();
			});
		}
	});
	
	$('#menu2 .has_submenu>a').click(function(){
		var c=$(this).parent();
		if(c.hasClass('open'))
		{
			var h=$('.submenu',c).stop(true).height();
			c.removeClass('open');
			$('.submenu',c).css({'display':'block','height':h}).animate({'height':0},function(){
				$('.submenu',c).css({'display':'','height':''});
				position_header();
			});
		}
		else
		{
			var h=$('.submenu',c).stop(true).height();
			c.addClass('open');
			$('.submenu',c).height('');
			if(h==$('.submenu',c).height())h=0;
			var hto=$('.submenu',c).height();
			$('.submenu',c).css({'display':'block','height':h}).animate({'height':hto},function(){
				$('.submenu',c).css({'display':'','height':''});
				position_header();
			});
		}
	});
	
	$('#menu3 .trigger').click(function(){
		if($(this).parent().hasClass('open'))
		{
			var h=$('#menu3 .menu').stop(true).height();
			$('#menu3').removeClass('open');
			$('#menu3 .menu').css({'display':'block','height':h}).animate({'height':0},function(){
				$('#menu3 .menu').css({'display':'','height':''});
				$('#menu3 .menu .has_submenu').removeClass('open');
				$('#menu3 .menu .submenu').stop(true).css({'display':'','height':''});
				position_header();
			});
		}
		else
		{
			$('#menu3 .menu').stop(true);
			if($('#menu3 .has_submenu').length==1)
			{
				$('#menu3 .has_submenu').addClass('open');
			}
			var h=$('#menu3 .menu').height();
			$('#menu3').addClass('open');
			$('#menu3 .menu').height('');
			if(h==$('#menu3 .menu').height())h=0;
			var hto=$('#menu3 .menu').height();
			$('#menu3 .menu').css({'display':'block','height':h}).animate({'height':hto},function(){
				$('#menu3 .menu').css({'display':'','height':''});
				position_header();
			});
		}
	});
	
	$('#menu3 .has_submenu>a').click(function(){
		var c=$(this).parent();
		if(c.hasClass('open'))
		{
			var h=$('.submenu',c).stop(true).height();
			c.removeClass('open');
			$('.submenu',c).css({'display':'block','height':h}).animate({'height':0},function(){
				$('.submenu',c).css({'display':'','height':''});
				position_header();
			});
		}
		else
		{
			var h=$('.submenu',c).stop(true).height();
			c.addClass('open');
			$('.submenu',c).height('');
			if(h==$('.submenu',c).height())h=0;
			var hto=$('.submenu',c).height();
			$('.submenu',c).css({'display':'block','height':h}).animate({'height':hto},function(){
				$('.submenu',c).css({'display':'','height':''});
				position_header();
			});
		}
	});
	
	$('#menu3 .contact .icon').click(function(e){
		e.preventDefault();
		var c=$(this).closest('.button');
		if(!c.hasClass('open'))
		{
			c.parent().find('.button').removeClass('open');
			c.addClass('open');
		}
	});
	
	if($('#promo').length)
	{
		var p=$('#promo'),promo_interval;
		if($('.buttons .button.active',p).length>1)
		{
			$('.buttons .button.active',p).not($('.buttons .button.active',p).first()).removeClass('active');
		}
		else if($('.buttons .button.active',p).length<1)
		{
			$('.buttons .button',p).first().addClass('active');
		}
		var ba=$('.buttons .button.active',p);
		$('.bg img').removeClass('active');
		$('.bg img[data-img='+ba.data('img')+']').addClass('active');
		$('.items .item').removeClass('active');
		$('.items .item[data-item='+ba.data('item')+']').addClass('active');
		$('.buttons .button',p).click(function(){
			var th=$(this);
			if(!th.hasClass('active'))
			{
				$('.buttons .button.active',p).removeClass('active');
				th.addClass('active');
				$('.items .item.active').removeClass('active');
				$('.items .item[data-item='+th.data('item')+']').addClass('active');
				$('.bg img.active').stop(true).css('display','block').removeClass('active').fadeOut();
				$('.bg img[data-img='+th.data('img')+']').stop(true).css({'display':'','opacity':'','filter':''}).addClass('active');
				
				var a=$('#promo .bg img.active').css({'width':'','height':''});
				if(parseFloat(a.data('proportions'))>a.parent().width()/a.parent().height())
				{
					a.css({'width':'','height':'100%'});
					a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
				}
				else
				{
					a.css({'width':'100%','height':''});
					a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
				}
			}
		});
		promo_interval=setInterval(function(){
			var th;
			if($('.buttons .button.active',p).next().length)
			{
				th=$('.buttons .button.active',p).next();
			}
			else
			{
				th=$('.buttons .button',p).first();
			}
			if(!th.hasClass('active'))
			{
				$('.buttons .button.active',p).removeClass('active');
				th.addClass('active');
				$('.items .item.active').removeClass('active');
				$('.items .item[data-item='+th.data('item')+']').addClass('active');
				$('.bg img.active').stop(true).css('display','block').removeClass('active').fadeOut();
				$('.bg img[data-img='+th.data('img')+']').stop(true).css({'display':'','opacity':'','filter':''}).addClass('active');
				
				var a=$('#promo .bg img.active').css({'width':'','height':''});
				if(parseFloat(a.data('proportions'))>a.parent().width()/a.parent().height())
				{
					a.css({'width':'','height':'100%'});
					a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
				}
				else
				{
					a.css({'width':'100%','height':''});
					a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
				}
			}
		},5000);
		p.hover(function(){
			clearInterval(promo_interval);
		},function(){
			promo_interval=setInterval(function(){
				var th;
				if($('.buttons .button.active',p).next().length)
				{
					th=$('.buttons .button.active',p).next();
				}
				else
				{
					th=$('.buttons .button',p).first();
				}
				if(!th.hasClass('active'))
				{
					$('.buttons .button.active',p).removeClass('active');
					th.addClass('active');
					$('.items .item.active').removeClass('active');
					$('.items .item[data-item='+th.data('item')+']').addClass('active');
					$('.bg img.active').stop(true).css('display','block').removeClass('active').fadeOut();
					$('.bg img[data-img='+th.data('img')+']').stop(true).css({'display':'','opacity':'','filter':''}).addClass('active');
					
					var a=$('#promo .bg img.active').css({'width':'','height':''});
					if(parseFloat(a.data('proportions'))>a.parent().width()/a.parent().height())
					{
						a.css({'width':'','height':'100%'});
						a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
					}
					else
					{
						a.css({'width':'100%','height':''});
						a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
					}
				}
			},5000);
		});
	}
	
	$('#content .order_form label.has_tip').contents().filter(function(){
		return this.nodeType===3;
	}).wrap('<span class="label_inner"></span>');
	
	$('#content .order_form label.has_tip.separate .pre_optional').contents().filter(function(){
		return this.nodeType===3;
	}).wrap('<span class="label_inner"></span>');
	
	$('#content .order_form .checkbox_block .option .text .has_tip').contents().filter(function(){
		return this.nodeType===3;
	}).wrap('<span class="checkbox_inner"></span>');
	
	horizontal_select();
	checkbox_block();
	center_promo_mob_text();
	position_contact();
	position_header();
	position_popup();
	
	$('.horizontal_select .option').click(function(){
		if($(this).closest('.select_group').length)
		{
			$(this).closest('.select_group').find('.horizontal_select>.option').not($(this)).removeClass('active');
		}
		else
		{
			$(this).closest('.horizontal_select').find('>.option').not($(this)).removeClass('active');
		}
		$(this).addClass('active');
		if(typeof($(this).closest('.horizontal_select').data('linkto'))!='undefined')
		{
			var c=$($(this).closest('.horizontal_select').data('linkto')).find('option');
			while(c.length)
			{
				if(c.first().val()==$(this).data('value'))
				{
					c.first().parent().val(c.first().val()).trigger('change');
					$('.selectpicker').selectpicker('refresh');
					break;
				}
				else
				{
					c=c.not(c.first());
				}
			}
		}
	});
	
	$('.horizontal_dropdown select').change(function(){
		if(typeof($(this).data('linkto'))!='undefined')
		{
			var c=$($(this).data('linkto')).find('.option');
			while(c.length)
			{
				if(c.first().data('value')==$(this).val())
				{
					$($(this).data('linkto')).find('.option').removeClass('active');
					c.first().addClass('active');
					break;
				}
				else
				{
					c=c.not(c.first());
				}
			}
		}
	});
	
	$('select[data-hide]').change(function(){
		$($(this).data('hide')).hide();
		$($(this).find('option[value='+$(this).val()+']').data('show')).show();
	});
	
	cinput($('.cinput'));
	
	$('.checkbox_block .option').click(function(){
		var th=$(this),cb=$('input[type=checkbox]',th);
		th.toggleClass('active');
		if(th.hasClass('active'))cb.prop('checked',true);
		else cb.prop('checked',false);
	});
	
	$('.ctabs').each(function(){
		var c=this;
		if($('.tab_top.active',c).length!=1)
		{
			$('.tab_top',c).removeClass('active').first().addClass('active');
		}
		$('.tab_bottom.active',c).removeClass('active');
		$('.tab_bottom[data-tab='+$('.tab_top.active',c).data('tab')+']',c).addClass('active');
		$('.tab_top',c).click(function(){
			if(!$(this).hasClass('active'))
			{
				$('.tab_top.active',c).removeClass('active');
				$(this).addClass('active');
				$('.tab_bottom.active',c).removeClass('active');
				$('.tab_bottom[data-tab='+$(this).data('tab')+']',c).addClass('active');
			}
		});
	});
	
	$('.styled[type=checkbox], .styled[type=radio]').stylecb();
	
	animate_numbers();
	
	$('.userinput').each(function(){
		$('table').wrap('<div class="table_cont"></div>').each(function(){
			$(this).find('tr>td:first-child,tr>th:first-child').addClass('first_col');
			$(this).find('tr>td:last-child,tr>th:last-child').addClass('last_col');
			$(this).find('tr').first().addClass('first_row');
		});
		$('iframe').each(function(){
			$(this).data('width1',$(this).attr('width')).data('height1',$(this).attr('height')).data('width2','280').data('height2',Math.floor($(this).attr('height')/$(this).attr('width')*280)).addClass('youtube_video');
		});
	});
	adjust_youtube();
	
	$('.faq li>.trigger').click(function(){
		if($(this).closest('li').hasClass('open'))
		{
			$(this).closest('li').stop(true);
			var h=$(this).outerHeight();
			$(this).closest('li').animate({'height':h},function(){
				$(this).removeClass('open').css('height','');
				$('body').trigger('resize');
			});
		}
		else
		{
			$(this).closest('li').stop(true);
			var hp=$(this).closest('li').outerHeight();
			$(this).closest('li').addClass('open');
			var h=$(this).outerHeight()+$(this).closest('li').find('.expand').outerHeight();
			$(this).closest('li').css('height',hp+'px').animate({'height':h},function(){
				$(this).css('height','');
				$('body').trigger('resize');
			});
		}
	});
	
	$('.order_collapsible .trigger_line').click(function(){
		if($(this).closest('.order_collapsible').hasClass('open'))
		{
			$(this).closest('.order_collapsible').stop(true);
			var h=$(this).outerHeight();
			$(this).closest('.order_collapsible').animate({'height':h},function(){
				$(this).removeClass('open').css('height','');
				$('body').trigger('resize');
			});
		}
		else
		{
			$(this).closest('.order_collapsible').stop(true);
			var hp=$(this).closest('.order_collapsible').outerHeight();
			$(this).closest('.order_collapsible').addClass('open');
			var h=$(this).outerHeight()+$(this).closest('.order_collapsible').find('.collapsible').outerHeight();
			$(this).closest('.order_collapsible').css('height',hp+'px').animate({'height':h},function(){
				$(this).css('height','');
				$('body').trigger('resize');
			});
		}
	});
	
	$('.popup_bg,.popup .close').click(function(){
		$('.popup_bg,.popup').hide();
	});
	
	adjust_list();
	
	$('.tip').each(function(){
		$(this).show();
		if($(this).height()<=parseInt($(this).css('line-height')))
		{
			$(this).width('auto');
		}
		$(this).css('display','')
	});
	
	$('.has_tip').click(function(){
		if(ver()==3)
		{
			$(this).toggleClass('open_tip');
		}
	});
	
	$('.order_form .forgot').click(function(e){
		e.preventDefault();
		var c=$('.order_form .forgot_pw_block').stop(true);
		if(c.hasClass('open'))
		{
			c.show().removeClass('open').slideUp(function(){
				c.css('height','');
				position_contact();
			});
		}
		else
		{
			c.hide().addClass('open').slideDown(function(){
				c.css('height','');
				position_contact();
			});
		}
	});
	
	adjust_profile_cols();
	adjust_contacts_cols();
	
	$('.trigger_changepw').click(function(){
		$('.popup_bg,.popup_changepw').show();
		position_popup();
	});
	$('.trigger_changeinfo').click(function(){
		$('.popup_bg,.popup_changeinfo').show();
		position_popup();
	});
	
	$('.bootstrap-select.btn-group .btn').click(function(){
		$(this).closest('.bootstrap-select').removeClass('wrong');
	});
	
	$('#content .sidemenu:not(.no_mob) .trigger').click(function(){
		if(ver()==3)
		{
			var c=$(this).closest('.sidemenu'),l=$('.menu',c);
			if(c.hasClass('open'))
			{
				l.stop(true).slideUp(function(){
					c.removeClass('open');
				});
			}
			else
			{
				c.addClass('open');
				l.stop(true).slideDown();
			}
		}
	});
	
	mobile_sidemenu();
	startpage_slider_adjust();
	
	
	$(document).click(function(e){
		if(!$(e.target).closest('.has_tip').length)
		{
			$('.has_tip.open_tip').removeClass('open_tip');
		}
		
		if(!$(e.target).closest('.contacts_block').length)
		{
			$('.contacts_block').removeClass('open').find('.button').removeClass('open');
		}
		
		if(!$(e.target).closest('.has_submenu').length)
		{
			$('#menu1 .menu .has_submenu.open').removeClass('open');
		}
		else
		{
			$('#menu1 .menu .has_submenu.open').not($(e.target).closest('.has_submenu')).removeClass('open');
		}
		
		if(!$(e.target).closest('#menu2').length&&$('#menu2').hasClass('open'))
		{
			var h=$('#menu2 .menu').stop(true).height();
			$('#menu2').removeClass('open');
			$('#menu2 .menu').css({'display':'block','height':h}).animate({'height':0},function(){
				$('#menu2 .menu').css({'display':'','height':''});
				$('#menu2 .menu .has_submenu').removeClass('open');
				$('#menu2 .menu .submenu').stop(true).css({'display':'','height':''});
				position_header();
			});
		}
		
		if(!$(e.target).closest('#menu3').length&&$('#menu3').hasClass('open'))
		{
			var h=$('#menu3 .menu').stop(true).height();
			$('#menu3').removeClass('open');
			$('#menu3 .menu').css({'display':'block','height':h}).animate({'height':0},function(){
				$('#menu3 .menu').css({'display':'','height':''});
				$('#menu3 .menu .has_submenu').removeClass('open');
				$('#menu3 .menu .submenu').stop(true).css({'display':'','height':''});
				position_header();
			});
		}
		
		if($('.faq').length)
		{
			$('.faq li.open').not($(e.target).closest('li')).stop(true).each(function(){
				var h=$('.trigger',this).outerHeight();
				$(this).animate({'height':h},function(){
					$(this).removeClass('open').css('height','');
				});
			});
		}
		
		if(!$(e.target).closest('.sidemenu:not(.no_mob)').length&&ver()==3)
		{
			$('.sidemenu:not(.no_mob) .menu').stop(true).slideUp(function(){
				$(this).closest('.sidemenu').removeClass('open');
			});
		}
	});
	
	$('#promo .bg img').each(function(){
		$(this).data('proportions',$(this).data('width')/$(this).data('height'));
	});
	promo_resize();
	$('#promo .bg img').each(function(){
		$(this).attr('src',$(this).data('src'));
	});
});

$(window).load(function(){
	footer();
	whyus();
	$('.how_it_works .bg img').data('proportions',$('.how_it_works .bg img').width()/$('.how_it_works .bg img').height());
	promo_resize();
	mobile_sidemenu();
	how_it_works();
	position_contact();
	position_header();
	position_popup();
	animate_numbers();
	adjust_youtube();
	adjust_list();
	adjust_contacts_cols();
});

$(window).scroll(function(){
	position_contact();
	position_header();
	position_popup();
	animate_numbers();
});

$(window).resize(function(){
	position_contact();
	promo_resize();
	how_it_works();
	position_header();
	position_popup();
	animate_numbers();
	startpage_slider_adjust();
	$('.has_tip.open_tip').removeClass('open_tip');
	if(v!=ver())
	{
		v=ver();
		footer();
		mobile_sidemenu();
		whyus();
		horizontal_select();
		checkbox_block();
		center_promo_mob_text();
		adjust_youtube();
		adjust_list();
		adjust_profile_cols();
		adjust_contacts_cols();
		$('.contacts_block').removeClass('open').find('.button').removeClass('open');
		$('#menu1 .menu .has_submenu.open').removeClass('open');
		$('#menu2').removeClass('open').find('.menu').stop(true).css({'display':'','height':''}).find('.has_submenu').removeClass('open').find('.submenu').stop(true).css({'display':'','height':''});
		$('#menu3').removeClass('open').find('.menu').stop(true).css({'display':'','height':''}).find('.has_submenu').removeClass('open').find('.submenu').stop(true).css({'display':'','height':''});
	}
});

function startpage_slider_adjust()
{
	$('.startpage_slider').each(function(){
		var c=$(this),inner=$('.slider_cont .inner',c);
		if(ver()==3)
		{
			var ww=$(window).width(),iw=$('.item',inner).first().outerWidth(),im=$('.item',inner).first().outerWidth(true)-iw;
			if(ww<im*2+iw*3)
			{
				inner.css('margin-left',-(Math.floor((3*iw-ww)/2)+2*im)+'px');
			}
			else
			{
				inner.css('margin-left',-(Math.floor((5*iw-ww)/2)+3*im)+'px');
			}
		}
		else
		{
			inner.css('margin-left','');
		}
	});
}

function mobile_sidemenu()
{
	$('#content .sidemenu:not(.no_mob)').each(function(){
		var c=$(this),l=$('.menu',c);
		if(ver()==3)
		{
			c.removeClass('open');
			l.stop(true).css({'height':'','display':'none'});
		}
		else
		{
			c.removeClass('open');
			l.stop(true).css({'height':'','display':''});
		}
	});
}

function position_popup()
{
	var h=$('body').scrollTop();
	if(h<=0)h=$('html').scrollTop();
	
	$('.popup').each(function(){
		var th=$(this),wh=$(window).height(),ph=th.height();
		if(ph<wh)
		{
			th.css('top',h+Math.floor((wh-ph)/2)+'px');
		}
		else
		{
			var pt=parseInt(th.css('top'));
			if(isNaN(pt))pt=0;
			if(pt>=h)
			{
				th.css('top',h+'px');
			}
			else if(h+wh>pt+ph)
			{
				th.css('top',(h+wh-ph)+'px');
			}
		}
	});
}

function adjust_contacts_cols()
{
	$('.contact_page .middle').each(function(){
		var th=$(this),h=111;
		$('.col .btn1,.col .btn4',th).css('margin-top','');
		if(ver()!=2)
		{
			$('.col .btn1,.col .btn4',th).each(function(){
				if($(this).position().top>h)h=$(this).position().top;
			}).each(function(){
				$(this).css('margin-top',h-$(this).position().top+parseInt($(this).css('margin-top')));
			});
		}
		else
		{
			$('.col .btn4',th).each(function(){
				if($(this).position().top>h)h=$(this).position().top;
			}).each(function(){
				$(this).css('margin-top',h-$(this).position().top+parseInt($(this).css('margin-top')));
			});
		}
	});
}

function adjust_profile_cols()
{
	$('.profile_cols').each(function(){
		var th=$(this),h=91;
		$('.col .fields',th).css('min-height','');
		if(ver()!=3)
		{
			$('.col .fields',th).each(function(){
				if($(this).height()>h)h=$(this).height();
			});
			$('.col .fields',th).css('min-height',h+'px');
		}
	});
}

function adjust_list()
{
	$('.order_list .header,.order_list .line').each(function(){
		var th=$(this),h=0;
		$('.col',th).css('min-height','');
		$('.col',th).each(function(){
			if($(this).height()>h)h=$(this).height();
		});
		$('.col',th).css('min-height',h+'px');
	});
}

function adjust_youtube()
{
	$('.youtube_video').each(function(){
		if(ver()==3)
		{
			$(this).css({'width':$(this).data('width2')+'px','height':$(this).data('height2')});
		}
		else
		{
			$(this).css({'width':$(this).data('width1')+'px','height':$(this).data('height1')});
		}
	});
}

function animate_numbers()
{
	if($('.statistics').length)
	{
		var top=$('body').scrollTop();
		if(top<=0)top=$('html').scrollTop();
		var bottom=top+$(window).height();
		var eltop=$('.statistics').position().top;
		var elbot=eltop+$('.statistics').outerHeight();
		if((eltop>top&&eltop<bottom)||(elbot>top&&eltop<bottom)||(eltop<top&&elbot>bottom))
		{
			$('.animate_number.not_done').each(function(){
				$(this).removeClass('not_done').animateNumber({number:parseInt($(this).data('realnumber')),easing:'easeInQuint'},4000);
			});
		}
	}
}

function position_header()
{
	var h=$('body').scrollTop();
	if(h<=0)h=$('html').scrollTop();
	if(ver()==1)
	{
		$('header').css('top',h+'px');
	}
	else
	{
		var wh=$(window).height(),hh=$('header').height()+$('#menu'+ver()+'.open .menu').height();
		if(hh<wh)
		{
			$('header').css('top',h+'px');
		}
		else
		{
			var ht=parseInt($('header').css('top'));
			if(isNaN(ht))ht=0;
			if(ht>=h)
			{
				$('header').css('top',h+'px');
			}
			else if(h+wh>ht+hh)
			{
				$('header').css('top',(h+wh-hh)+'px');
			}
		}
	}
	$('header.startpage_not_top').removeClass('startpage_not_top');
	if(ver()==1&&h>0)
	{
		$('header').addClass('startpage_not_top');
	}
}

function position_contact()
{
	if($('.contacts_block').length)
	{
		if(ver()==3)
		{
			$('.contacts_block').css('top','');
		}
		else if(ver()==2)
		{
			var h=$('body').scrollTop();
			if(h<=0)h=$('html').scrollTop();
			h=$(window).height()+h;
			var h2=$('footer').position().top+parseInt($('footer').css('margin-top'));
			/*if($('footer .whyus').length)
			{
				h2+=$('footer .whyus').height();
			}*/
			if(h>h2)h=h2;
			h-=$('.contacts_block').height();
			$('.contacts_block').css('top',h+'px');
		}
		else
		{
			var h=$('body').scrollTop();
			if(h<=0)h=$('html').scrollTop();
			$('.contacts_block').css('top',(($(window).height()*0.4)+h)+'px');
		}
	}
}

function center_promo_mob_text()
{
	if($('#promo').length)
	{
		$('#promo .items .item').css({'padding-top':'','padding-bottom':''});
		if(ver()==3)
		{
			var h=0;
			$('#promo .items .item').each(function(){
				$(this).show();
				if($(this).height()>h)h=$(this).height();
				$(this).css('display','');
			});
			$('#promo .items .item').each(function(){
				$(this).show();
				var temp=(h-$(this).height())/2;
				$(this).css('display','').css({'padding-top':Math.floor(temp)+'px','padding-bottom':Math.ceil(temp)+'px'});
			});
		}
	}
}

function checkbox_block()
{
	$('.checkbox_block').each(function(){
		if(!$('>.option.first',this).length)$('>.option',this).first().addClass('first').addClass('css3');
		if(!$('>.option.last',this).length)$('>.option',this).last().addClass('last').addClass('css3');
		$('>.option',this).css({'width':'','height':''});
		$('>.option .text',this).css({'min-height':''});
		if(ver()!=3)
		{
			var n=$('>.option',this).length,w=$(this).width()-n-1,we=Math.floor(w/n),over=w-we*n;
			$('>.option',this).each(function(){
				if(over>0)
				{
					$(this).width(we+1);
					over--;
				}
				else
				{
					$(this).width(we);
				}
			});
			var h=0;
			$('>.option .text',this).each(function(){
				if($(this).height()>h)h=$(this).height();
			});
			$('>.option .text',this).css({'min-height':h+'px'});
		}
	});
}

function horizontal_select()
{
	$('.horizontal_select').each(function(){
		if(!$('>.option.first',this).length)$('>.option',this).first().addClass('first').addClass('css3');
		if(!$('>.option.last',this).length)$('>.option',this).last().addClass('last').addClass('css3');
		$('>.option',this).css({'width':'','height':''});
		var n=$('>.option',this).length,w=$(this).width()-n-1,we=Math.floor(w/n),over=w-we*n;
		$('>.option',this).each(function(){
			if(over>0)
			{
				$(this).width(we+1);
				over--;
			}
			else
			{
				$(this).width(we);
			}
		});
		$('>.option .text',this).css({'padding-top':'','padding-bottom':'','margin-top':''});
		if($(this).height()>40)
		{
			$('>.option .text',this).css({'padding-top':'8px','padding-bottom':'4px'});
		}
		$('>.option',this).height($(this).height()-2);
		$('>.option .text',this).each(function(){
			$(this).css('margin-top',Math.floor(($(this).parent().height()-$(this).outerHeight())/2));
		});
	});
}

function whyus()
{
	if($('.whyus').length)
	{
		$('.whyus a,.whyus .extra').css('min-height','');
		if(ver()==3)
		{
			var h=0;
			$('.whyus .extra,.whyus .item1').each(function(){if($(this).height()>h)h=$(this).height();});
			$('.whyus .extra,.whyus .item1').css('min-height',h);
			var h=0;
			$('.whyus .item2,.whyus .item3').each(function(){if($(this).height()>h)h=$(this).height();});
			$('.whyus .item2,.whyus .item3').css('min-height',h);
			var h=0;
			$('.whyus .item4,.whyus .item5').each(function(){if($(this).height()>h)h=$(this).height();});
			$('.whyus .item4,.whyus .item5').css('min-height',h);
		}
		else
		{
			var h=0;
			$('.whyus a').each(function(){if($(this).height()>h)h=$(this).height();});
			$('.whyus a').css('min-height',h);
		}
	}
}

function footer()
{
	$('.footer_push').height('')
	$('footer').css({'height':'','margin-top':''});
	var h=$('footer').outerHeight();
	$('.footer_push').height(h)
	$('footer').css({'height':h,'margin-top':-h});
}

function how_it_works()
{
	if($('.how_it_works').length)
	{
		var a=$('.how_it_works .bg img').css({'width':'','height':''});
		if(parseFloat(a.data('proportions'))>a.parent().width()/a.parent().height())
		{
			a.css({'width':'','height':'100%'});
			a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
		}
		else
		{
			a.css({'width':'100%','height':''});
			a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
		}
	}
}

function promo_resize()
{
	if($('#promo').length)
	{
		var a=$('#promo .bg img.active').css({'width':'','height':''});
		if(parseFloat(a.data('proportions'))>a.parent().width()/a.parent().height())
		{
			a.css({'width':Math.floor(a.data('proportions')*a.parent().height()),'height':a.parent().height()});
			a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
		}
		else
		{
			a.css({'width':a.parent().width(),'height':Math.floor(a.parent().width()/a.data('proportions'))});
			a.css({'margin-top':-Math.floor(a.height()/2),'margin-left':-Math.ceil(a.width()/2)});
		}
	}
}

function cinput(a)
{
	a.each(function(){
		var th=$(this),f=th.find('input, textarea');
		var email = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		if(!f.val())
		{
			th.addClass('default').addClass('invalid');
			f.val(f.data('default'));
		}
		else if(th.data('type')=='email'&&!email.test(f.val()))
		{
			th.addClass('invalid');
		}
		th.click(function(){
			f.trigger('focus');
		});
		f.focus(function(){
			th.removeClass('invalid').removeClass('wrong');
			if(th.hasClass('default'))
			{
				f.val('');
				th.removeClass('default');
			}
		});
		f.blur(function(){
			if(!f.val())
			{
				f.val(f.data('default'));
				th.addClass('default').addClass('invalid');
			}
			else if(th.data('type')=='email'&&!email.test(f.val()))
			{
				th.addClass('invalid');
			}
		});
	});
}

(function($){
	$.fn.stylecb=function(){
		var height = "24";
		
		return this.each(function(){
			var that=this;
			if ((this.type=='checkbox'||this.type=='radio')&&$(this).hasClass('styled')&&!$(this).hasClass('done'))
			{
				$(this).addClass('done');
				
				var span = document.createElement("span");
				$(span).prop('class',$(this).prop('type')+' '+$(this).prop('class'));
				$(span).attr('style',$(this).attr('style'));

				if ($(this).prop("checked"))
				{
					span.style.backgroundPosition = "0 -" + (height*2) + "px";
				}
				this.parentNode.insertBefore(span,this);
				
				$(span).mousedown(function(){
					element = this.nextSibling;
					if($(element).prop("checked"))
					{
						this.style.backgroundPosition = "0 -" + height*3 + "px";
					}
					else
					{
						this.style.backgroundPosition = "0 -" + height + "px";
					}
				});
				
				$(span).mouseup(function(){
					$(this).next().trigger('click');
				});
				
				$(document).mouseup(function(e){
					$('span.styled.done').each(function(){
						var that=$(this);
						if(that.next().prop('checked'))
						{
							that.css('background-position','0 -'+height*2+'px');
						}
						else
						{
							that.css('background-position','0 0');
						}
					});
				});
				
				if($(span).hasClass('radio'))
				{
					$(this).bind('click',function(){
						$('input[type=radio][name='+$(this).attr('name')+']').prev().css('backgroundPosition','0 0');
						$(span).css('backgroundPosition','0 -'+height*2+'px');
					});
				}
				else
				{
					$(this).bind('click',function(){
						if($(this).prop("checked"))
						{
							$(span).css('backgroundPosition','0 0');
						}
						else
						{
							$(span).css('backgroundPosition','0 -'+height*2+'px');
						}
					});
				}
				
				$('label[for='+$(this).prop('id')+']').click(function(e){
					$('#'+$(this).prop('for')).trigger('click');
					$('span.styled.done').each(function(){
						var that=$(this);
						if(that.next().prop('checked'))
						{
							that.css('background-position','0 -'+height*2+'px');
						}
						else
						{
							that.css('background-position','0 0');
						}
					});
					return false;
				});
			}
		});
	};
})(jQuery);

/*
 jQuery animateNumber plugin v0.0.10
 (c) 2013, Alexandr Borisov.
 https://github.com/aishek/jquery-animateNumber
*/
(function(d){var p=function(b){return b.split("").reverse().join("")},l={numberStep:function(b,a){var e=Math.floor(b);d(a.elem).text(e)}},h=function(b){var a=b.elem;a.nodeType&&a.parentNode&&(a=a._animateNumberSetter,a||(a=l.numberStep),a(b.now,b))};d.Tween&&d.Tween.propHooks?d.Tween.propHooks.number={set:h}:d.fx.step.number=h;d.animateNumber={numberStepFactories:{append:function(b){return function(a,e){var k=Math.floor(a);d(e.elem).prop("number",a).text(k+b)}},separator:function(b,a){b=b||" ";a=
a||3;return function(e,k){var c=Math.floor(e).toString(),s=d(k.elem);if(c.length>a){for(var f=c,g=a,l=f.split("").reverse(),c=[],m,q,n,r=0,h=Math.ceil(f.length/g);r<h;r++){m="";for(n=0;n<g;n++){q=r*g+n;if(q===f.length)break;m+=l[q]}c.push(m)}f=c.length-1;g=p(c[f]);c[f]=p(parseInt(g,10).toString());c=c.join(b);c=p(c)}s.prop("number",e).text(c)}}}};d.fn.animateNumber=function(){for(var b=arguments[0],a=d.extend({},l,b),e=d(this),k=[a],c=1,h=arguments.length;c<h;c++)k.push(arguments[c]);if(b.numberStep){var f=
this.each(function(){this._animateNumberSetter=b.numberStep}),g=a.complete;a.complete=function(){f.each(function(){delete this._animateNumberSetter});g&&g.apply(this,arguments)}}return e.animate.apply(e,k)}})(jQuery);