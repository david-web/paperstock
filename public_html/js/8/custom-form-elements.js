(function($){
	$.fn.stylecb=function(){
		var height = "24";
		
		return this.each(function(){
			var that=this;
			if ((this.type=='checkbox'||this.type=='radio')&&$(this).hasClass('styled')&&!$(this).hasClass('done'))
			{
				$(this).addClass('done');
				
				var span = document.createElement("span");
				$(span).prop('class',$(this).prop('type')+' '+$(this).prop('class'));
				$(span).attr('style',$(this).attr('style'));

				if ($(this).prop("checked"))
				{
					span.style.backgroundPosition = "0 -" + (height*2) + "px";
				}
				this.parentNode.insertBefore(span,this);
				
				$(span).mousedown(function(){
					element = this.nextSibling;
					if($(element).prop("checked"))
					{
						this.style.backgroundPosition = "0 -" + height*3 + "px";
					}
					else
					{
						this.style.backgroundPosition = "0 -" + height + "px";
					}
				});
				
				$(span).mouseup(function(){
					$(this).next().trigger('click');
				});
				
				$(document).mouseup(function(e){
					$('span.styled.done').each(function(){
						var that=$(this);
						if(that.next().prop('checked'))
						{
							that.css('background-position','0 -'+height*2+'px');
						}
						else
						{
							that.css('background-position','0 0');
						}
					});
				});
				
				if($(span).hasClass('radio'))
				{
					$(this).bind('click',function(){
						$('input[type=radio][name='+$(this).attr('name')+']').prev().css('backgroundPosition','0 0');
						$(span).css('backgroundPosition','0 -'+height*2+'px');
					});
				}
				else
				{
					$(this).bind('click',function(){
						if($(this).prop("checked"))
						{
							$(span).css('backgroundPosition','0 0');
						}
						else
						{
							$(span).css('backgroundPosition','0 -'+height*2+'px');
						}
					});
				}
				
				$('label[for='+$(this).prop('id')+']').click(function(e){
					$('#'+$(this).prop('for')).trigger('click');
					$('span.styled.done').each(function(){
						var that=$(this);
						if(that.next().prop('checked'))
						{
							that.css('background-position','0 -'+height*2+'px');
						}
						else
						{
							that.css('background-position','0 0');
						}
					});
					return false;
				});
			}
		});
	};
})(jQuery);

(function(a){a.fn.extend({customSelect:function(c){if(typeof document.body.style.maxHeight==="undefined"){return this}var e={customClass:"customSelect",mapClass:true,mapStyle:true},c=a.extend(e,c),d=c.customClass,f=function(h,k){var g=h.find(":selected"),j=k.children(":first"),i=g.html()||"&nbsp;";j.html(i);if(g.attr("disabled")){k.addClass(b("DisabledOption"))}else{k.removeClass(b("DisabledOption"))}setTimeout(function(){k.removeClass(b("Open"));a(document).off("mouseup."+b("Open"))},60)},b=function(g){return d+g};return this.each(function(){var h=a(this),j=a("<span />").addClass(b("Inner")),i=a("<span />"),g=h.position();h.after(i.append(j));i.addClass(d);if(c.mapClass){i.addClass(h.attr("class"))}if(c.mapStyle){i.attr("style",h.attr("style"))}h.addClass("hasCustomSelect").on("update",function(){f(h,i);var l=parseInt(h.outerWidth(),10)-(parseInt(i.outerWidth(),10)-parseInt(i.width(),10));i.css({display:"inline-block"});var k=i.outerHeight();if(h.attr("disabled")){i.addClass(b("Disabled"))}else{i.removeClass(b("Disabled"))}j.css({width:l,display:"inline-block"});h.css({"-webkit-appearance":"menulist-button",width:i.outerWidth(),position:"absolute",opacity:0,height:k,fontSize:i.css("font-size"),left:g.left,top:g.top})}).on("change",function(){i.addClass(b("Changed"));f(h,i)}).on("keyup",function(k){if(!i.hasClass(b("Open"))){h.blur();h.focus()}else{if(k.which==13||k.which==27||k.which==9){f(h,i)}}}).on("mousedown",function(k){i.removeClass(b("Changed"))}).on("mouseup",function(k){if(!i.hasClass(b("Open"))){if(a("."+b("Open")).not(i).length>0&&typeof InstallTrigger!=="undefined"){h.focus()}else{i.addClass(b("Open"));k.stopPropagation();a(document).one("mouseup."+b("Open"),function(l){if(l.target!=h.get(0)&&a.inArray(l.target,h.find("*").get())<0){h.blur()}else{f(h,i)}})}}}).focus(function(){i.removeClass(b("Changed")).addClass(b("Focus"))}).blur(function(){i.removeClass(b("Focus")+" "+b("Open"))}).hover(function(){i.addClass(b("Hover"))},function(){i.removeClass(b("Hover"))}).trigger("update")})}})})(jQuery);


$(document).ready(function(){
	$('.styled[type=checkbox], .styled[type=radio]').stylecb();
});