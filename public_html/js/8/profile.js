var profile = {
	
	langs : {},
	
	rateOrder : function () {
		
		if ($('input:radio[name=support]:checked').val() && $('input:radio[name=support]:checked').val() != 'undefined') {
					
			sendData = {};
			sendData['support'] = $('input:radio[name=support]:checked').val();

			ajaxRequest('/profile/rateOrder/', sendData, function(data) {
	            
				$('.popup').remove();
				$('.popup_bg').remove();
				location.reload();
				
			});
		}
	},
	
	uploadFile : function () {
		if ($.trim($('#uploadButtonFile').val())) {
			sendData = {};
			sendData['file'] = $('#uploadButtonFile').val();
			
			ajaxRequest('/profile/uploadFile/', sendData, function(data) {
	            
				location.reload();
				
			});
		}
	},
	
	orderNewMessage : function () {
		if ($.trim($('#message').val())) {
			
			sendData = {};
			sendData['message'] = $('#message').val();
			
			ajaxRequest('/profile/orderNewMessage/', sendData, function(data) {
	            
				location.reload();
				
			});
			
			
		}
	},
	
	passwordReminder : function () {
		
		profile.removeErrors();
		
		if ($('#password_reminder').val()) {
			sendData = {};
			sendData['email'] = $('#password_reminder').val();
			
			ajaxRequest('/profile/passwordReminder/', sendData, function(data) {
	            
				if (data.errors) {
					profile.setErrors(data.errors);
				} else {
					$('.forgot_pw_block').find('.field').hide();
					$('.forgot_pw_block').find('.info_submitted_msg').show();
				}
				
			});
		}
		
		
		
	},
	
	getOrderRatesForm : function ($id) {
		
		$('.popup').remove();
		$('.popup_bg').remove();
		
		sendData = {};
		if ($id) {
			sendData['orderID'] = $id;
		}
		
		ajaxRequest('/profile/getOrderRatesForm/', sendData, function(data) {
            
			if (data) {
				$('body').append(data);
			}
			
		});
	},
	
	changePassword : function () {
		
		$('.popup').remove();
		$('.popup_bg').remove();
		
		ajaxRequest('/profile/changePassword/', '', function(data) {
            
			if (data) {
				$('body').append(data);
			}
			
		});
	},
	
	startTest : function ($test, $file) {
		
		sendData = {};
		sendData['test'] = $test;
		
		$result = ajaxRequest('/profile/startTest/', sendData, function(data) {
            
			if (data == 'error') {
				return false;
			} else {
				
				$('#test' + $test).find('.upload').removeClass('btn5').addClass('btn1');
				loadUploadButton('tests/results/', 'uploadButton_' + $test);
				window.location = $file;
				return true;
			}
			
		});

	},
	
	uploadTest : function ($id) {
		if ($.trim($('#uploadButton_' + $id + 'File').val())) {
			sendData = {};
			sendData['file'] = $('#uploadButton_' + $id + 'File').val();
			sendData['id'] = $id;
			
			ajaxRequest('/profile/uploadTest/', sendData, function(data) {
	            
				location.reload();
				
			});
		}
	},
	
	orderApply : function() {
		
		if ($.trim($('#amount').val()) && $.trim($('#reason').val())) {
			sendData = {};
			sendData['amount'] = $('#amount').val();
			sendData['reason'] = $('#reason').val();
			
			ajaxRequest('/profile/orderApply/', sendData, function(data) {
	            
				location.reload();
				
			});
		}
		
	},
	
	removeTest : function ($id) {
		sendData = {};
		sendData['id'] = $id;
		
		ajaxRequest('/profile/removeTest/', sendData, function(data) {
            
			location.reload();
			
		});
	},
	
	verify : function () {
		if ($('#verified').val()) {
			 var value = $('#verified').val();
			 if (value.length == 9) {
				 sendData = {};
				 sendData['verified'] = value;
				 
				 ajaxRequest('/profile/verify/', sendData, function(data) {
			            
					if (data.error) {
						alert(data.error);
					} else {
						$('.verification').hide();
						$('.message_green').show();
					}
					
				});
			 }
		}
	},
	
	resendVerify : function () {
		ajaxRequest('/profile/resendVerify/', '', function(data) {
            
			
			
		});
	},
	
	setNewPassword : function () {
		
		profile.removeErrors();
		
		sendData = {};
		$('.password').each(function(n, element) {
			
			sendData[$(this).attr('id')] = $(this).val();
			
		});
		
		ajaxRequest('/profile/setNewPassword/', sendData, function(data) {
            
			if (data.errors) {
				profile.setErrors(data.errors);
			} else {
				$('.popup').remove();
				$('.popup_bg').remove();
			}
			
		});
	},
	
	profileRegister : function () {
		
		profile.removeErrors();
		
		sendData = {};
		sendData['action'] = 'register';
		sendData['fields'] = {};
		$('.order_form input.register,select.register,checkbox.register, textarea.register').each(function(n, element) {
			
			if ($(this).attr('type') == 'checkbox') {
				
				if ($(this).attr('rel')) {
					
					if ($(this).is(':checked')) {
					
						if (typeof(sendData['fields'][$(this).attr('rel')]) == 'undefined') {
							sendData['fields'][$(this).attr('rel')] = new Array();
			            }
	
						sendData['fields'][$(this).attr('rel')].push($(this).val());
					
					}
					
				} else {
					if ($(this).is(':checked')) {
						sendData['fields'][$(this).attr('id')] = $(this).val();
					} else {
						sendData['fields'][$(this).attr('id')] = '0';
					}
				}
					
			} else if ($(this).attr('type') == 'radio')  {
				if ($(this).is(':checked')) {
					sendData['fields'][$(this).attr('id')] = $(this).val();
				}
			} else {
				sendData['fields'][$(this).attr('id')] = $(this).val();
			}
			
		});
		
		ajaxRequest('/profile/register/', sendData, function(data) {
            
			if (data.errors) {
				profile.setErrors(data.errors);
			} else {
				window.location.href = data.location;
			}
			
		});
		
	},
	
	profileLogin : function () {
		
		profile.removeErrors();
		
		sendData = {};
		sendData['action'] = 'login';
		sendData['fields'] = {};
		$('.order_form input.login,select.login,checkbox.login').each(function(n, element) {
			
			sendData['fields'][$(this).attr('id')] = $(this).val();
			
		});
		
		ajaxRequest('/profile/login/', sendData, function(data) {
            
			if (data.errors) {
				profile.setErrors(data.errors);
			} else {
				window.location.href = data.location;
			}
			
		});
		
	},
	
	removeErrors : function () {
		$('.wrong.default.invalid').each(function(n, element) {
			$(this).removeClass('wrong').removeClass('wrong').removeClass('invalid');
		});
		$('div.invalid_msg').remove();
	}, 
	
	setErrors : function (errors) {
		if (typeof(errors.fields) != 'undefined') {
			$.each(errors.fields, function( index, value ) {
				if ($('#' + index).attr('type') == 'text' || $('#' + index).attr('type') == 'password' || $('#' + index).is("textarea")) {
					  $('#' + index).parent().addClass('wrong').addClass('default').addClass('invalid');
					  $('#' + index).parent().parent().append('<div class="invalid_msg">' + value + '</div>');
				  } else if ($('#' + index).attr('type') == 'checkbox') {
					  $('#' + index).parent().parent().append('<div class="invalid_msg">' + value + '</div>');
				  } else {  
					  $('#' + index).addClass('wrong').next().addClass('wrong');
					  $('#' + index).parent().append('<div class="invalid_msg">' + value + '</div>');
				  }
				});
		}
		
		
		if (typeof(errors.global) != 'undefined') {
			alert(errors.global);
		}
	},
    
};
