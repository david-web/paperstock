<?php

/**
 * ADWeb - Content managment system.
 * 
 * Adweb paypal class file.
 * Paypal class using to process payments from Paypal
 *
 * @package			Adweb
 * @author			Dailis Tukans <dailis@efumo.en> on 03.02.2011
 * @copyright		Copyright (c) 2011, Efumo.
 * @link			http://www.efumosoftware.en
 * @version			1.2
 * @modiefied		11.07.2012
 */

// -----------------------------------------------------------------------------

require_once("../../system/config/config.php");
loadFunc("functions");
loadFunc("site");
loadFunc("other");

/**
 * Get Paypal Class
 */
require_once(AD_LIB_FOLDER . 'payments/paypal/paypal.class.php');

/**
 * Get Paypal action type
 */
$i = (isset($_GET['action'])) ? $_GET['action'] : '';

/**
 * Switch by action
 */
switch ($i) {
    
    /**
     * Return OK
     * When payment completed
     */
    case 'retok' :
	
		$backLink = '../../';

		$temp_order_id = (isset($_SESSION['order']['ID']) ? $_SESSION['order']['ID'] : (isset($_GET['p']) ? $_GET['p'] : 0));

		/**
		 * Get main order data
		 */
		$sql = mysql_query("SELECT `site_id`, `lang`, `paid`, `have_a_discount?` FROM `mod_orders` WHERE `id` = " . mysql_real_escape_string($temp_order_id));
		$row = mysql_fetch_row($sql);
		$db_country = $row[0];
		$db_lang = $row[1];
		$db_paid = (int)$row[2];
		$db_bonus_code = $row[3];
		
		$paypal = new Paypal($db_country);

		/**
		 * !!!
		 */
		$sql = mysql_query("SELECT `test_mode`, `account` FROM `mod_payment_methods_paypal` WHERE `id` = " . mysql_real_escape_string($db_country));
		$row = mysql_fetch_row($sql);
		$business_account = $row[1];
		$testmode = (int)$row[0] == 1 ? true : false;
		
		if ($db_paid == 1) {
			$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_OK, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:1/';
		} else {
			
			/**
			 * If we have post data and order, must validate for correct payment and order data
			 */
			if (isset($_POST) && count($_POST) > 1 && isset($_GET) && (int)$temp_order_id > 0 && (int)$_GET['p'] == (int)$temp_order_id) {

				// Set values to variables
				$order_id = $temp_order_id;
				$paypal_data = $_POST;
				$tnx_id = isset($paypal_data['txn_id']) ? $paypal_data['txn_id'] : '';
				$mc_gross = str_replace(",", ".", str_replace(" ", "", (isset($paypal_data['mc_gross']) ? $paypal_data['mc_gross'] : 0)));

				// Set default session data
				$_SESSION['payment']['transaction_id'] = $tnx_id;
				$_SESSION['payment']['result'] = 2;

				/**
				 * Get main order data
				 */
				$sql = mysql_query("SELECT `pp_sum`, `pp_currency` FROM `pproc_pp_transaction` WHERE `order_id` = " . mysql_real_escape_string($order_id));
				$row = mysql_fetch_row($sql);
				$pp_sum = str_replace(",", ".", str_replace(" ", "", $row[0]));
				$pp_currency = trim($row[1]);
				
				/*echo $db_country . '<br>';
				echo $paypal->country . '<br>';
				echo $order_id . '<br>';
				var_dump($paypal_data);die();*/
				/**
				 * If posted PayPal data, order data and PayPal class data are valid, update order status and redirect to ok page
				 */
				if (
					isset($paypal_data['business']) && 
					//(string)trim($paypal->business_account) == (string)trim($paypal_data['business']) &&
					isset($paypal_data['invoice']) && (string)$order_id == (string)$paypal_data['invoice'] &&
					(int)$db_country > 0 && (int)$db_country == (int)$paypal->country 
				) {
					$_SESSION['payment']['result'] = 1;
					
					if (
						strcmp($paypal_data['payment_status'], "Completed") == 0 || 
						strtolower($paypal_data['payment_status']) == "completed"
					) {
						// Update order paypal info data
						$sql = mysql_query("UPDATE `pproc_pp_transaction`
									  SET 
									`status` = '3',
									`time` = '" . time() . "',
									`post_return` = '" . mysql_real_escape_string(serialize($paypal_data)) . "'
									  WHERE `order_id` = '" . mysql_real_escape_string($order_id) . "'");

						// Update order status
						$sql = mysql_query("UPDATE `mod_orders`
									  SET 
									`transaction_id` = '" . mysql_real_escape_string($tnx_id) . "',
									`paid` = '1',
									`paid_time` = '" . mysql_real_escape_string(time()) . "'
									  WHERE `id` = '" . mysql_real_escape_string($order_id) . "'");

						paymentUpdateBonusCodeCount($db_bonus_code);
						// Send email to client and admin
						//$email_content = paymentOrderEmailContent($order_id, $db_country, $db_lang);
						//$attachments = array();
						//$attachments[0] = paymentOrderPdfInvoice($order_id, $db_country, $db_lang);
						//sendMail($db_order_email, gL2('order-email-client-subject', $db_country, $db_lang), $email_content, array(), gL2('order-email-sender-address', $db_country, $db_lang), true, (isset($attachments) ? $attachments : null));
						//sendMail(gL2('order-email-receiver-address', $db_country, $db_lang), gL2('order-email-admin-subject', $db_country, $db_lang) . ' - ' . $db_order_number, $email_content, array(), gL2('order-email-sender-address', $db_country, $db_lang), true, (isset($attachments) ? $attachments : null));
						
						$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_OK, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:1/';
						
					} else {
						
						// Update order paypal info data
						$sql = mysql_query("UPDATE `pproc_pp_transaction`
											SET 
											  `status` = '4',
											  `time` = '" . time() . "',
											  `post_return` = '" . mysql_real_escape_string(serialize($paypal_data)) . "'
											WHERE `order_id` = '" . mysql_real_escape_string($order_id) . "'");

						// Update order status
						$sql = mysql_query("UPDATE `mod_orders`
											SET 
											  `transaction_id` = '" . mysql_real_escape_string($tnx_id) . "',
											  `paid` = '0',
											WHERE `id` = '" . mysql_real_escape_string($order_id) . "'");
						
						$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_ERROR, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:2/?order-submited-error';
					}


				}
				/**
				 * else data not valid.
				 * Show error page
				 */
				else {
					// Update order paypal info data
					/*$sql = mysql_query("UPDATE `pproc_pp_transaction`
								  SET 
								`status` = '2',
								`time` = '" . time() . "',
								`post_return` = '" . mysql_real_escape_string(serialize($paypal_data)) . "'
								  WHERE `order_id` = '" . mysql_real_escape_string($order_id) . "'");*/

					// Set backlink
					$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_ERROR, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:2/?order-submited-error';
				}

			}
			/**
			 * !!! - fix
			 */
			elseif (isset($_POST) && count($_POST) < 1 && isset($_GET) && (int)$temp_order_id > 0 && (int)$_GET['p'] == (int)$temp_order_id) {

				// Set values to variables
				$order_id = $temp_order_id;

				// Set default session data
				$_SESSION['payment']['transaction_id'] = $tnx_id = 'pyp-'.$order_id;
				$_SESSION['payment']['result'] = 2;

				/**
				 * Get main order data
				 */
				$sql = mysql_query("SELECT `pp_sum`, `pp_currency`, `status` FROM `pproc_pp_transaction` WHERE `order_id` = " . mysql_real_escape_string($order_id));
				$row = mysql_fetch_row($sql);
				$pp_sum = str_replace(",", ".", str_replace(" ", "", $row[0]));
				$pp_currency = trim($row[1]);
				$pp_status = trim($row[2]);
				if ((int)$order_id > 0 && (int)$db_country > 0 && (int)$pp_status == 1) {
					
					$_SESSION['payment']['result'] = 1;
					
					if (
						strcmp($paypal_data['payment_status'], "Completed") == 0 || 
						strtolower($paypal_data['payment_status']) == "completed"
					) {
						// Update order paypal info data
						$sql = mysql_query("UPDATE `pproc_pp_transaction`
											SET 
											  `status` = '3',
											  `time` = '" . time() . "',
											  `post_return` = '" . mysql_real_escape_string(serialize(array())) . "'
											WHERE `order_id` = '" . mysql_real_escape_string($order_id) . "'");

						// Update order status
						$sql = mysql_query("UPDATE `mod_orders`
											SET 
											  `transaction_id` = '" . mysql_real_escape_string($tnx_id) . "',
											  `paid` = '1',
											  `paid_time` = '" . mysql_real_escape_string(time()) . "'
											WHERE `id` = '" . mysql_real_escape_string($order_id) . "'");

						paymentUpdateBonusCodeCount($db_bonus_code);
						// Send email to client and admin
						//$email_content = paymentOrderEmailContent($order_id, $db_country, $db_lang);
						//$attachments = array();
						//$attachments[0] = paymentOrderPdfInvoice($order_id, $db_country, $db_lang);
						//sendMail($db_order_email, gL2('order-email-client-subject', $db_country, $db_lang), $email_content, array(), gL2('order-email-sender-address', $db_country, $db_lang), true, (isset($attachments) ? $attachments : null));
						//sendMail(gL2('order-email-receiver-address', $db_country, $db_lang), gL2('order-email-admin-subject', $db_country, $db_lang) . ' - ' . $db_order_number, $email_content, array(), gL2('order-email-sender-address', $db_country, $db_lang), true, (isset($attachments) ? $attachments : null));
						
						$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_OK, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:1/';
						
					} else {
						
						// Update order paypal info data
						$sql = mysql_query("UPDATE `pproc_pp_transaction`
											SET 
											  `status` = '4',
											  `time` = '" . time() . "',
											  `post_return` = '" . mysql_real_escape_string(serialize(array())) . "'
											WHERE `order_id` = '" . mysql_real_escape_string($order_id) . "'");

						// Update order status
						$sql = mysql_query("UPDATE `mod_orders`
											SET 
											  `transaction_id` = '" . mysql_real_escape_string($tnx_id) . "',
											  `paid` = '0',
											WHERE `id` = '" . mysql_real_escape_string($order_id) . "'");
						
						$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_ERROR, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:2/?order-submited-error';
					}
					


				} else {
					// Update order paypal info data
					/*$sql = mysql_query("UPDATE `pproc_pp_transaction`
								  SET 
								`status` = '2',
								`time` = '" . time() . "',
								`post_return` = '" . mysql_real_escape_string(serialize(array())) . "'
								  WHERE `order_id` = '" . mysql_real_escape_string($order_id) . "'");*/

					// Set backlink
					$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_ERROR, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:2/?order-submited-error';
				}
			}
			/**
			 * Show error page
			 */
			else {
				// Set backlink
				$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_ERROR, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:2/?order-submited-error';
			}
			
		}
		
		sendOrderEmail($temp_order_id);
		
		/**
		 * Redirect to order ok/error page
		 */
		header("Location: " . $backLink);
		exit();
        break;


    /**
     * Return FAIL
     * When something goes wrong with payment
     */
    case 'retfail' :
	
		$backLink = '../../';

		$temp_order_id = (isset($_SESSION['temp_order']['id']) ? $_SESSION['temp_order']['id'] : (isset($_GET['p']) ? $_GET['p'] : 0));

		/**
		 * Get main order data
		 */
		$sql = mysql_query("SELECT `site_id`, `lang`, `paid`, `have_a_discount?` FROM `mod_orders` WHERE `id` = " . mysql_real_escape_string($temp_order_id));
		$row = mysql_fetch_row($sql);
		$db_country = $row[0];
		$db_lang = $row[1];
		$db_paid = (int)$row[2];
		$db_bonus_code = $row[3];

		$_SESSION['payment']['transaction_id']  = '';
		$_SESSION['payment']['result']          = 2;

		if (isset($_GET) && (int)$temp_order_id > 0 && (int)$_GET['p'] == (int)$temp_order_id) {

			// 
			$paypal_data = isset($_POST) ? $_POST : array();

			// Update order paypal info data
			$sql = mysql_query("UPDATE `pproc_pp_transaction`
					  SET 
						`status` = '2',
						`time` = '" . time() . "',
						`post_return` = '" . mysql_real_escape_string(serialize($paypal_data)) . "'
					  WHERE `order_id` = '" . mysql_real_escape_string($temp_order_id) . "'");
			
			sendOrderEmail($temp_order_id);

		}

		// Set backlink for error page
		$backLink = getDCountryLink(((int)$db_country > 0 ? $db_country : 1)) . getLM(RESULT_PAGE_ID_ERROR, ((int)$db_country > 0 ? $db_country : 1), ($db_lang != '' ? $db_lang : 'en')) . 'm:2/?order-submited-error';
		
		/**
		 * Redirect to order ok/error page
		 */
		header("Location: " . $backLink);
		exit();
        break;
        
    
    /**
     * IPN checking
     */
    case 'ipn' :
	
		$paypal = new Paypal();
		$dataIpn = $paypal->checkIPN();
		if ( $dataIpn != false && is_array($dataIpn) ) {
			
			$order_id = $temp_order_id = $dataIpn['order_db_id'];

			$sql = mysql_query("SELECT `site_id`, `lang`, `paid`, `have_a_discount?` FROM `mod_orders` WHERE `id` = " . mysql_real_escape_string($temp_order_id));
			$row = mysql_fetch_row($sql);
			$db_country = $row[0];
			$db_lang = $row[1];
			$db_paid = (int)$row[2];
			$db_bonus_code = $row[3];

			$paypal = new Paypal($db_country);

			$sql = mysql_query("SELECT `test_mode`, `account` FROM `mod_payment_methods_paypal` WHERE `id` = " . mysql_real_escape_string($db_country));
			$row = mysql_fetch_row($sql);
			$business_account = $row[1];
			$testmode = (int)$row[0] == 1 ? true : false;

			// Previously not checked/updated etc
			if ( $db_paid == 0 ) {
				
				// Set values to variables
				$paypal_data = $_POST;
				$tnx_id = isset($dataIpn['txn_id']) ? $dataIpn['txn_id'] : '';
				$mc_gross = str_replace(",", ".", str_replace(" ", "", (isset($dataIpn['mc_gross']) ? $dataIpn['mc_gross'] : 0)));

				// Get main payment data
				$sql = mysql_query("SELECT `pp_sum`, `pp_currency` FROM `pproc_pp_transaction` WHERE `order_id` = " . mysql_real_escape_string($order_id));
				$row = mysql_fetch_row($sql);
				$pp_sum = str_replace(",", ".", str_replace(" ", "", $row[0]));
				$pp_currency = trim($row[1]);
				
				// Do some validations, update order status, send email
				if (
					isset($dataIpn['business']) && 
					(
						(isset($dataIpn['invoice']) && (string)$order_id == (string)$dataIpn['invoice'])
							||
						(isset($dataIpn['order_db_id']) && (string)$order_id == (string)$dataIpn['order_db_id'])
					) &&
					(int)$db_country > 0 && (int)$db_country == (int)$paypal->country
				) {
					$_SESSION['payment']['result'] = 1;

					// Update order paypal info data
					$sql = mysql_query("UPDATE `pproc_pp_transaction`
								  SET 
								`status` = '3',
								`time` = '" . time() . "',
								`post_return` = '" . mysql_real_escape_string(serialize($paypal_data)) . "'
								  WHERE `order_id` = '" . mysql_real_escape_string($order_id) . "'");

					// Update order status
					$sql = mysql_query("UPDATE `mod_orders`
								  SET 
								`transaction_id` = '" . mysql_real_escape_string($tnx_id) . "',
								`paid` = '1',
								`paid_time` = '" . mysql_real_escape_string(time()) . "'
								  WHERE `id` = '" . mysql_real_escape_string($order_id) . "'");

					paymentUpdateBonusCodeCount($db_bonus_code);
					// Send email to client and admin
					//$email_content = paymentOrderEmailContent($order_id, $db_country, $db_lang);
					//$attachments = array();
					//$attachments[0] = paymentOrderPdfInvoice($order_id, $db_country, $db_lang);
					//sendMail($db_order_email, gL2('order-email-client-subject', $db_country, $db_lang), $email_content, array(), gL2('order-email-sender-address', $db_country, $db_lang), true, (isset($attachments) ? $attachments : null));
					//sendMail(gL2('order-email-receiver-address', $db_country, $db_lang), gL2('order-email-admin-subject', $db_country, $db_lang) . ' - ' . $db_order_number, $email_content, array(), gL2('order-email-sender-address', $db_country, $db_lang), true, (isset($attachments) ? $attachments : null));
				}
			}
			
			//sendOrderEmail($order_id);
		}

		break;
	
		
    /**
     * If no action found, go to main page
     */
    default:
        if ($backLink == '') $backLink = '../../';
        header('Location: ' . $backLink);
        exit();
        break;
    
}

?>