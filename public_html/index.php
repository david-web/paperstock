<?php

/**
 * ADWeb - Content managment system
 *
 * @package		Adweb
 * @author		David Akopyan <davids@efumo.lv>
 * @copyright	Copyright (c) 2010, Efumo.
 * @link		http://adweb.lv
 * @version		2
 */

// ------------------------------------------------------------------------

/**
 * ADWEB
 * General main file.
 * Includs all needs file and run all needs modules.
 * 09.05.2008
 */

require_once(dirname(__FILE__) . "/system/config/config.php");

/*if ($_SERVER['HTTP_HOST'] == 'www.writersshore.com' &&  !in_array($_SERVER['REMOTE_ADDR'], array('2.30.92.89', '80.232.252.13'))) {
	die("");
}*/

$l = &loadLibClass('loader');

/*loadData();

function loadData($n = 0) {
	global $mdb;
	
	require_once(AD_LIB_FOLDER . 'PHPExcel/PHPExcel.php');
	require_once(AD_LIB_FOLDER . 'PHPExcel/PHPExcel/IOFactory.php');
	
	$reader = PHPExcel_IOFactory::load(AD_CACHE_FOLDER . 'test.xls');

	$reader->setActiveSheetIndex($n);

	$j = 0;
	$sheet = $reader->getActiveSheet();
	$highestRow = $sheet->getHighestRow();
	$highestColumn = $sheet->getHighestColumn();

	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
	for ($row = 1; $row <= $highestRow; ++$row) {
		for ($col = 0; $col <= $highestColumnIndex; ++$col) {
			if ($row == 1) {
				if ($value = trim($sheet->getCellByColumnAndRow($col, $row)->getValue())) {
					$header[$col] = $value;
				}
			} else {
				$import[$row][$col] = trim($sheet->getCellByColumnAndRow($col, $row)->getValue());
			}

		}

	}
	
	foreach ($import AS $data) {
		$dbData = array();
		
		$dbData[] = " `form_id` = '41' ";
		
		$dbQuery = "INSERT INTO `mod_forms_values` SET " . implode(',', $dbData);
		$query = new query($mdb, $dbQuery);
		$id = $mdb->get_insert_id();	
		
		$dbData = array();
		
		$dbData[] = " `value_id` = '" . $id . "' ";
		$dbData[] = " `lang` = 'en' ";
		$dbData[] = " `name` = '" . $data[0] . "' ";
		$dbData[] = " `hint` = '" . $data[1] . "' ";
		
		$dbQuery = "INSERT INTO `mod_forms_values_data` SET " . implode(',', $dbData);
		$query = new query($mdb, $dbQuery);
		
	}

	
}*/

// Free MySQL or other resources on the end of the script
freeResources();

?>
